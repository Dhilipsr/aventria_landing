<?php

namespace App\Http\Resources\Assessment;

use App\PatientJourney;
use App\SharedPriority;
use App\UserCustomerAssessment;
use Illuminate\Http\Resources\Json\JsonResource;

class UserAssessmentCollection extends JsonResource
{
    public function toArray($request)
    {   
        $paitientJourneys = PatientJourney::get();
        $sharedPriorities = SharedPriority::get();
        $questions = [];
        $questions['first_name'] = ($this->getUserInfo) ? $this->getUserInfo->name : null;
        $questions['last_name'] = ($this->getUserInfo) ? $this->getUserInfo->lname : null;
        $questions['date'] = $this->date;
        $questions['customer_name'] = $this->customer_name;
        $questions['customer_title'] = $this->title;
        $questions['organization'] = $this->organization;
        $questions['organization_city'] = $this->city;
        $questions['organization_state'] = ($this->getState) ? $this->getState->state : null;
        // return $sharedPriorities;
        foreach ($paitientJourneys as $paitientJourney){
            foreach ($sharedPriorities as $sharedPriority){
                // get user info
                $userInfo = $this->getUserInfo;
                // get the answer saved by users
                if($userInfo){
                    $answer = UserCustomerAssessment::where(['user_id' => $userInfo->id, 'patient_journey' => $paitientJourney->id,'ref_id' => $this->id])->first();
                    if($answer){
                        $sharedPriorityAnsArray = json_decode($answer->shared_priorities);
                        
                        if(($sharedPriorityAnsArray)){
                            foreach ($sharedPriorityAnsArray as $sharedPriorityAns){
                                if($sharedPriorityAns == $sharedPriority->id){
                                    $status = '1';
                                }else{
                                    $status = '0';
                                }
                            }
                        }else{
                            $status = 0;
                        }
                        
                    }else{
                        $status = '0';
                        
                    }
                    
                }else{
                    $status = '1';
                    
                }
                
                $questions[$paitientJourney->title.' '.$sharedPriority->title] = $status;
                
            }
            $other_answer_query = UserCustomerAssessment::where(['user_id' => $userInfo->id, 'patient_journey' => $paitientJourney->id,'ref_id' => $this->id])->first();
            if($other_answer_query){
                $other_answer = $other_answer_query->other;
            }else{
                $other_answer = null;
            }
            $questions[$paitientJourney->title.' other'] = $other_answer;
        }
        return $questions;
        // return $paitientJournies;
        // return [
        //     'first_name' => ($this->getUserInfo) ? $this->getUserInfo->name : null,
        //     'last_name' => ($this->getUserInfo) ? $this->getUserInfo->lname : null,
        //     'date' => $this->date,
        //     'customer_name' => $this->customer_name,
        //     'customer_title' => $this->title,
        //     'organization' => $this->organization,
        //     'organization_city' => $this->city,
        //     'organization_state' => $this->state,
        // ];
    }
}