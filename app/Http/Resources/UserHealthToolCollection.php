<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Role;
use App\Organizationtype;
use DB;
class UserHealthToolCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {   
        
       
       // return $request->All();

        $organization = Organizationtype::where('id',$this->organization_id)->first();
        // return $organization;
        return 
        [
            'user_first_name' => $this->name,
            'user_last_name' => $this->lname,
            // 'date' => $this->lname,
            // 'customer_name' => $this->lname,
            // 'customer_title' =>$this->lname,
            'organization' => $this->organization,
            // 'organization_city' => $this->organization,
            // 'organization_state' => $this->organization,
            'email' => $this->email,
            'avatar' => asset('storage/.'.$this->avatar),
            'organization_type' => $organization != '' ? $organization->type : '' ,
            'job_title' => $this->job_title,
            'contact' => $this->contact,
            'address' => $this->address,
          
                 //$salix_manager = App\SalixManager::get();
            'account_manager_name' => $this->Name($this->salix_manager_name),
           
            'status' => $this->status == 1 ? 'Active' : 'Pending',
            'last_login' => $this->last_login,
            'last_login_timezone' => $this->last_login_timezone,
            'timezone' => $this->timezone,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_at_timezone' => $this->created_at_time_zone,
            'updated_at_timezone' => $this->updated_at_timezone,
        ];
    }

public function Name($id){
    $null='';
     $data= DB::table('salix_managers')->where('id',$id)->get();
   
    // print_r($data[0]->salix_manager_name);exit;
     if(count($data) !=0){
      return $data[0]->salix_manager_name;
     }else{
         return $null;
     }
    }
}