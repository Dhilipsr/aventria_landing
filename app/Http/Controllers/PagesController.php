<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Mail;
use App\User;
use App\About;
use App\Reason;
use App\EhrHome;
use App\Service;
use App\Support;
use App\Workflow;
use Notification;
use App\Ehrplugin;
use App\Guideline;
use App\Healthtool;
use App\InterceptOnly;
use App\Homeslider;
use App\Patienttype;
use App\QualityCare;
use App\OrderRequest;
use App\SalixManager;
use App\Contactenquiry;
use App\HealthToolsFav;
use App\PatientJourney;
use App\SharedPriority;
use App\SupportRequest;
use App\Targetaudience;
use App\UserAssessment;
use App\Homepagecontact;
use App\HomePageContent;
use TCG\Voyager\Voyager;
use App\Ehrstaticcontent;
use App\Organizationtype;

use App\Homebannercontent;
use App\MobileBannerContent;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\UserCustomerAssessment;
use Illuminate\Validation\Rule;
use App\Healthtoolstaticcontent;
use App\Guidelineandlinkscontent;
use App\Http\Controllers\Session;
use Illuminate\Support\Facades\URL;
use App\Notifications\pluginEnquiry;
use Illuminate\Support\Facades\Hash;
use App\Evidencebasedworkflowscontent;
use App\Http\Resources\UserHealthToolCollection;
use App\Http\Resources\Assessment\UserAssessmentCollection;

class PagesController extends Controller
{
    private $prettyLinkUrl;

    protected function setPrettyLinkUrl(string $link)
    {
        $prettyLinkUrl = $link;
    }

    protected function getPrettyLinkUrl(string $file,int $id,$organization)
    {




        $curl = curl_init();
        $healthtool = Healthtool::find($id);
        $post = array("PageUrl" => $healthtool->api_id,
                        "OrganizationName" => $organization,
                        "Image" => array('$content-type'=> "image/jpeg",
                                                '$content'=> $file
                        ));
                        // return $file;
                        // CURLOPT_URL => 'https://cmsautomate.azure-api.net/cmsautomate/getprettylink',
                        // 'Ocp-Apim-Subscription-Key: 8a28ab2bd1ec41839505d390fa4d4e22',
        curl_setopt_array($curl, array(
                                        CURLOPT_URL => 'https://cmsautomate.azure-api.net/cmsautomate/getprettylink',
                                        CURLOPT_RETURNTRANSFER => true,
                                        CURLOPT_ENCODING => '',
                                        CURLOPT_MAXREDIRS => 10,
                                        CURLOPT_TIMEOUT => 0,
                                        CURLOPT_FOLLOWLOCATION => true,
                                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                        CURLOPT_CUSTOMREQUEST => 'POST',
                                        CURLOPT_POSTFIELDS =>json_encode($post),
                                        CURLOPT_HTTPHEADER => array(
                                            'Ocp-Apim-Subscription-Key: d186020dc07642acb4fc5493d05e4acb',
                                            'Content-Type: application/json'
                                        ),
                                        ));

        $response = curl_exec($curl);
        curl_close($curl);
        // return $response;
        $url = json_decode($response)->PrettyLinkUrl ;
        $this->setPrettyLinkUrl($url);

        return $url ;
    }

    // redirections
    public function index()
    {
        $banner_contents = Homebannercontent::orderBy('sq_no')->get();
        $mobile_banner_contents = MobileBannerContent::orderby('sq_no')->get();
        $page_contents = HomePageContent::first();
        $homepagecontents = HomePageContent::get();
        $slider_images = Homeslider::get();
        // print_r(json_encode($page_contents));
        // die;
        //return $sliderimage;
        return view('welcome')->with(compact([
            'banner_contents',
            'mobile_banner_contents',
            'page_contents',
            'slider_images',
            'homepagecontents',
        ]));
    }

    public function getService($slug)
    {
        $content = $this->serviceData($slug);
        $view = $this->servicePage($slug);
        //return json_encode($content);
        return view($view)->with(compact('content'));
    }


    public function healthtools()
    {
        $healthtools = Healthtool::where(['status' => 1])->orderBy('title','ASC')->paginate(6);
        $healthtool_page_contents = Healthtoolstaticcontent::get();
        $targetaudiences = Targetaudience::where(['status' => 1])->get();
        $patienttypes = Patienttype::where(['status' => 1])->get();
        return view('healthtools')->with(compact([
            'healthtools',
            'targetaudiences',
            'patienttypes',
            'healthtool_page_contents'
        ]));
    }


    public function myaccount()
    {
        $favTools = HealthToolsFav::where(['user_id' => Auth::user()->id])->get();
        return view('myaccount')->with(compact(['favTools']));
    }

    public function shareMyfavorites($user_id)
    {
        // return $user_id;
        $username = user::where(['id' => $user_id])->pluck('name')->first();
        //return $username;
        $favTools = HealthToolsFav::where(['user_id' => $user_id])->get();
        // return  $favTools;
        return view('myfavtool')->with(compact(['favTools','username']));
    }

    public function myaccount_update(Request $request)
    {
        // return $request->all();
         if (!empty($request->update)) {
            if ($request->update == "on") {
            $update = 1;
            }
        }else{
            $update = 0;
        }
        
        $validatedData = $request->validate([
               'name' => ['required', 'string', 'max:255'],
               'lname' => ['required', 'string', 'max:255'],
               'organization' => ['required', 'string', 'max:255'],
               'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore(Auth::id())],
           ],
        [
            'name.required' => 'This field is required.',
            'lname.required' => 'This field is required.',
            'organization.required' => 'This field is required.',
            'job_title.required' => 'This field is required.',
            //'address.required' => 'This field is required.',
            'email.required' => 'This field is required.'
        ]
        );

        $update_profile = User::find(Auth::id());
        $update_profile->name = $request->name;
        $update_profile->lname = $request->lname;
        $update_profile->organization = $request->organization;
        $update_profile->email = $request->email;
        $update_profile->contact = $request->contact;
        $update_profile->update = $update;

        if (!empty($request->avatar)) {
            // change avatar
            if ($request->hasFile('avatar')) {
                $avatar = $request->file('avatar');

                $test_name = Rand(1,1000);

                $name = $test_name.'.'.$avatar->getClientOriginalExtension();
                $destinationPath = storage_path('/app/public/users');
                $avatarPath = $destinationPath. "/".  $name;
                $test =  $avatar->move($destinationPath, $name);
                //$grades->avatar = $name;
                $final_name = $name;
            }
            $update_profile->avatar = 'users/'.$final_name;
        }
        $check = $update_profile->save();

        if (!($check)) {

            return redirect()->back()->with(['message' => "Website Under maintenance, please try again later.!", 'alert-type' => 'error']);
        }else{

            return redirect()->back()->with(['message' => "Profile updated successfully!", 'alert-type' => 'success']);
        }

    }
    public function checkpassword(Request $request)
    {
        $old_password = Hash::check($request->old_password, Auth::user()->password);

        if ($old_password == 1) {
            return response()->json(['status' => '1', 'message' => 'Correct Password!']);
        }else{
            return response()->json(['status' => '0', 'message' => 'Wrong Password!' ]);
        }

    }
    public function changepassword(Request $request)
    {
        // validation for passwords
        $validatedData = $request->validate([
               'new_password' => ['required', 'min:8'],
               'old_password' => ['required'],
               'password_confirmation' => ['required']
           ],
        [
            'new_password.required' => 'This field is required.',
            'old_password.required' => 'This field is required.',
            'password_confirmation.required' => 'This field is required.'
        ]);

        $user = User::find(Auth::user()->id);
        if ($request->new_password == $request->password_confirmation) {
            $user->password = Hash::make($request->new_password);
            $update = $user->save();

              if (!$update) {
                return redirect()->back()->with(['message' =>'Your password has not been updated successfully!', 'alert-type' => 'error']);
              }else{
                $name = Auth::user()->name;
                $email = Auth::user()->email;

            
                Mail::send('password_update_email',['email' => $email],function($message) use ($email){
                $message->subject('Password Changed for LiverLife');
                $message->from(env('MAIL_FROM_ADDRESS'),'LiverLife');
                $message->sender(env('MAIL_FROM_ADDRESS'), 'LiverLife');
                $message->to($email);
                });

                return redirect()->back()->with(['message' => "Your password has been updated successfully!", 'alert-type' => 'success']);
              }
        }else{
            return redirect()->back()->with(['message' => "Passwords do not match!", 'alert-type' => 'error']);
        }

    }

    public function profilepicture(Request $request)
    {
      if($_FILES["file"]["name"] != '')
      {
        $ext = $request->file->extension();
        $temp_name =  $request->file->getClientOriginalName();
        // $hashed = Hash::make($temp_name);
        $hashed = base64_encode($temp_name);
        $uid =  date('YmdHis');
        $new_name = $hashed.$uid;
        $final_name = $new_name.'.'.$ext;
        $save_image = $request->file->storeAs('public/users/',$final_name);
        if (!$save_image) {
          return response()->json(['message' => 2]);
        }else{
          // image saved

          // find his previous image and check if it is default.png
          $check_previous_image = $avatar = User::find(Auth::user()->id);
          if ($check_previous_image->avatar != "users/default.png") {
            $delete_old_profile_picture = unlink('storage/'.$check_previous_image->avatar);

          }
          // find his previous image and check if it is default.png
          $avatar = User::find(Auth::user()->id);
          $avatar->avatar = 'users/'.$final_name;
          $update = $avatar->save();
          if (!$update) {
            return response()->json(['message' => 0]);
          }else{
            return response()->json(['message' => 1]);
          }
        }
      }else{
        return response()->json(['message' => 3]);
      }
    }

    // functionality


    public function results()
    {
        return view('search');
    }

    private function queryBasedHealthTool(string $query)
    {
        $q_healthtool_ids = Healthtool::where('title','like','%'.$query.'%')
                        ->where(['status' => 1])
                        ->pluck('id');

        return $q_healthtool_ids->toArray() ;
    }

    private function targetaudienceHealthTool($ids)
    {
        $ta_healthtool_ids = DB::table('healthtool_targetaudiences')->whereIn('targetaudience_id',$ids)
                                ->distinct('healthtool_id')
                                ->pluck('healthtool_id');

        return $ta_healthtool_ids->toArray() ;
    }

    private function patienttypeHealthtool($ids)
    {
        $pt_healthtool_ids = DB::table('healthtool_paitienttype')->whereIn('patienttype_id',$ids)
                                ->distinct('healthtool_id')
                                ->pluck('healthtool_id');

        return $pt_healthtool_ids->toArray();
    }

    public function healthtoolfilter(Request $request)
    {

        // return $request->all();
            $healthtool_page_contents = Healthtoolstaticcontent::get();
            $targetaudiences = Targetaudience::where(['status' => 1])->get();
            $patienttypes = Patienttype::where(['status' => 1])->get();
            // print_r($request->get('sort'));
            // die;
        if(empty($request->sort)){
            $request->sort = 'ASC';
        }
        $q_healthtool_ids = $ta_healthtool_ids = $pt_healthtool_ids = [];

        if($request->searchbar)
        {
            $q_healthtool_ids = $this->queryBasedHealthTool($request->searchbar) ;
        }

        if($request->targetaudience)
        {
            $ta_healthtool_ids = $this->targetaudienceHealthTool($request->targetaudience);
        }

        if($request->patienttype)
        {
            $pt_healthtool_ids = $this->patienttypeHealthtool($request->patienttype);
        }

        $health_tool_ids = array_unique(array_merge($q_healthtool_ids,$ta_healthtool_ids,$pt_healthtool_ids));

        $totalhealthtools = Healthtool::whereIn('id',$health_tool_ids)
                        ->where(['status' => 1])
                        ->count();

        $healthtools = Healthtool::whereIn('id',$health_tool_ids)
                        ->where(['status' => 1])
                        ->orderBy('title',$request->sort)
                        ->paginate(6);

        return view('result')->with(compact([
                'totalhealthtools',
                'healthtools',
                'targetaudiences',
                'patienttypes',
                'healthtool_page_contents',
            ]));
    }



    public function thankyou()
    {
        if (Auth::check()) {
            if (Auth::user()->status == 0 || Auth::user()->status == NULL) {
                return view('post_register_thankyou');
            }else{
                return redirect(route('healthtools'));
            }

        }else{
            return redirect(route('login'));
        }
    }


    public function team_training()
    {
        return view('team_training');
    }

    public function form()
    {
        return view('form');
    }

    public function customizeTool($healthtool_id)
    {
        $previousUrl = URL::previous();
        $healthTool = Healthtool::where(['id' => $healthtool_id])->first();
        $link = Str::slug($healthTool->title, '-');
        $client_name = Str::slug(Auth::user()->organization, '-');
        return view('customizeTool')->with(compact([
            'healthTool',
            'previousUrl',
             'link',
            'client_name'
        ]));
    }

    public function noLogoUpload(Request $request)
    {
        $tool_id = $request->page_id;
        $organization = $request->organization;
        $file = '';
        $url = $this->getPrettyLinkUrl($file,$tool_id,$organization) ;
        echo $url;
    }

    // public function logoUpload(Request $request)
    // {

    //   $fileUpload_arr = explode('base64,', $request->file);
    //   return $fileUpload_arr;
    //   $hashed = $fileUpload_arr[1];
    //   if($_FILES["file"]["name"] != '')
    //   {
    //    return 101;
    //     $ext = $request->file->extension();
    //     // $temp_name =  $request->file->getClientOriginalName();
    //     // $hashed = Hash::make($temp_name);
    //     // $hashed = base64_encode($temp_name);
    //     $uid =  date('YmdHis');
    //     $new_name = $uid;
    //     $final_name = $new_name.'.'.$ext;
    //     $save_image = $request->file->storeAs('public/userstemplogos/',$final_name);
    //     return $save_image;
    //     if (!$save_image) {
    //       return response()->json(['message' => 2]);
    //     }else{

    //       $getHealthTool = Healthtool::where(['id' => $request->healthtool_id])->first();

    //       $link = Str::slug($getHealthTool->title, '-');
    //       $client_name = Str::slug(Auth::user()->organization, '-');

    //       $avatar = new HealthToolsFav;
    //       $avatar->user_id = Auth::user()->id;
    //       $avatar->healthtool_slug = $request->healthtool_id;
    //       $avatar->link = $link;
    //       $avatar->avatar = 'userstemplogos/'.$final_name;
    //       $save = $avatar->save();
    //       if ($save) {
    //         return response()->json(['status' => 200,'lastId' => $avatar->id,'image' => $avatar->healthtool_slug,'url' => route('healthToolsDetail',['link' => $link,'client_name' => $client_name,'slug' => $request->healthtool_id, 'image' => $avatar->id])]);
    //       }else{
    //         return response()->json(['status' => 1, 'image' => null ]);
    //       }
    //     }
    //   }else{
    //     return response()->json(['message' => 3]);
    //   }
    // }



    public function logoUpload(Request $request)
    {
        $fileUpload_arr = explode('base64,', $request->file);
        $hashed = $fileUpload_arr[1];
        $organization = $request->organization;
        $healthtool_id = $request->healthtool_id;
        $link = $this->getPrettyLinkUrl($hashed,$healthtool_id,$organization);

        return response()->json(['status' => 200,'url' => $link]);
    }


    // public function logoUploadTwo(Request $request)
    // {
    //     if ($request->orgImage == 1) {
    //         // return 1;
    //         $getHealthTool = Healthtool::where(['id' => $request->healthtool_id])->first();

    //         $link = Str::slug($getHealthTool->title, '-');
    //         // $link = json_decode($result)->PrettyLinkUrl;

    //         $client_name = Str::slug(Auth::user()->name, '-');

    //         $avatar = new HealthToolsFav;
    //         $avatar->user_id = Auth::user()->id;
    //         $avatar->healthtool_slug = $request->healthtool_id;
    //         $avatar->link = $link;
    //         $avatar->avatar = Auth::user()->avatar;
    //         $save = $avatar->save();
    //         if ($save) {
    //           return response()->json(['status' => 200,'lastId' => $avatar->id,'image' => $avatar->healthtool_slug,'url' => route('healthToolsDetail',['link' => $link,'client_name' => $client_name,'slug' => $request->healthtool_id, 'image' => $avatar->id])]);
    //         }else{
    //           return response()->json(['status' => 1, 'image' => null ]);
    //         }

    //     }
    // }

    public function logoUploadTwo(Request $request)
    {
        if ($request->orgImage == 1) {
        $getHealthTool = Healthtool::where(['id' => $request->healthtool_id])->first();
        $user_avatar = Auth::user()->avatar;
        $organization = $request->organization;
        $hashed = base64_encode(file_get_contents(storage_path('app/public/'.$user_avatar)));

        $link = $this->getPrettyLinkUrl($hashed,$request->healthtool_id,$organization);

        return response()->json(['status' => 200,'url' => $link]);

        }
    }






    public function healthToolsDetail(Request $request, $link, $client_name, $healthtool_id)
    {

        $healthTool = Healthtool::where(['id' => $healthtool_id])->first();
        if ($request->orgImage) {
            $imageInfo = User::where(['id' => Auth::user()->id])->first();
        }else{
            if ($request->image) {
                $imageInfo = HealthToolsFav::where(['id' => $request->image])->first();
            }else{
                $imageInfo = null;
            }
        }

        // return $healthTool;
        return view('customizeHealthTool')->with(compact([
            'healthTool',
            'imageInfo'
        ]));
    }

    public function postToFav(Request $request)
    {
        // return $request->all();
        $data = $request->all();

        $save = HealthToolsFav::create($data);

        if ($save) {
            return response()->json(['status' => 200 ]);
        }else{
            return response()->json(['status' => 500 ]);
        }
    }



    public function deleteFromFav(Request $request)
    {
        // return $request->all();
        $favTool = HealthToolsFav::where(['id' => $request->id])->first();
        $delete = $favTool->delete();

        if ($delete) {
          return response()->json(['status' => 200 ]);
        }else{
          return response()->json(['status' => 500 ]);
        }
    }

    public function qualityCare()
    {
        $qualityCare = QualityCare::first();
        return view('qualityCare')->with(compact(['qualityCare']));
    }

    public function underReview()
    {
        return view('underReview');
    }

    // Piyush Panchal

    public function CostomerAssesmentTool()
    {
        $patient_journeys = PatientJourney::get();
        $user = auth()->user();
        //return  $user ;
        //$patient_journeys = SharedPriority::get();

        //return $patient_journeys;

        return view('customer_needs_assessment_tool')->with(compact(['patient_journeys','user']));
    }

    public function StoreCostomerAssesmentTool(request $request)
    {
        // return $request->all();
        $validatedData = $request->validate([
               'name' => ['required', 'string', 'max:255'],
               'date' => ['required'],
               'organization' => ['required', 'string', 'max:255'],
               'job_title' => ['required'],
               'State' => ['required'],
               'city' => ['required']
               // 'city' => ['required'],
               // 'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore(Auth::id())],
        ],[
            'name.required' => 'This field is required.',
            'date.required' => 'This field is required.',
            'organization.required' => 'This field is required.',
            'job_title.required' => 'This field is required.',
            'State.required' => 'This field is required.',
            'city.required' => 'This field is required.'
        ]);

        $user_asses = new UserAssessment;
        $user_asses->user_id = Auth::user()->id;
        $user_asses->title = $request->job_title;
        $user_asses->state = $request->State;
        $user_asses->city = $request->city;
        $user_asses->customer_name = $request->name;
        $user_asses->date = $request->date;
        $user_asses->organization = $request->organization;
        $user_asses->save();

        $save='';
        foreach($request->data as $patient_journey => $s_priorities ){

            if(isset($s_priorities['shared_priority'])){
                $prioritys = json_encode($s_priorities['shared_priority']);
            }else{
                $prioritys = null;
            }
            if(isset($s_priorities['others'])){
                $others = $s_priorities['others'];
            }else{
                $others = null;
            }
            if( $others || $prioritys != null ){
                $customer_assessments = new UserCustomerAssessment;
                $customer_assessments->user_id = Auth::user()->id;
                $customer_assessments->ref_id = $user_asses->id;
                $customer_assessments->patient_journey = $patient_journey;
                $customer_assessments->shared_priorities = $prioritys;
                $customer_assessments->other = $others;
                $save = $customer_assessments->save();
            }
        }
        if ($save) {
            return redirect()->back()->with('success', 'successfully saved.');
        }else{
            return redirect()->back()->with('error',config('webResponse.error_message'));
        }
        // if ($save) {
        //     return response()->json(['status' => 200,'lastId' => $avatar->id,'image' => $avatar->healthtool_slug,'url' => route('healthToolsDetail',['link' => $link,'client_name' => $client_name,'slug' => $request->healthtool_id, 'image' => $avatar->id])]);
        // }else{
        //     return response()->json(['status' => 1, 'image' => null ]);
        // }


    }




    public function getUser(Request $request)
    {
        // print_r("check");
        // die;
        if($request->token == 'liver@land#123!')
        {
            $users = User::where('id', '!=' , 1)->get();
            return UserHealthToolCollection::collection($users);
        }
        else
        {
            return response()->json(['status' => 401 , 'message' => 'Unauthenticated']);
        }
    }

    public function getUserAssessments(Request $request)
    {
        if($request->token == 'liver@land#123!')
        {
            $assessments = UserAssessment::get();
            //return $assessments;
            return UserAssessmentCollection::collection($assessments);
        }
        else
        {
            return response()->json(['status' => 401 , 'message' => 'Unauthenticated']);
        }
    }

    // public function contactAccountmanager(){

    //     return view('contact_my_account_manager');

    // }


    public function contactAccountmanagerStore(Request $request)
    {


        // $validatedData = $request->validate([
        //        'manager_id' => ['required'],
        //        'support_id' => ['required']
        //    ]);

        // $manager_name = SalixManager::where('id',$request->manager_id)->first();
        // $support_type = Support::where('id',$request->support_id)->first();

        $salixManagerInfo = SalixManager::where('id',Auth::user()->salix_manager_id)->first();
        //return $salixManagerInfo;
        $userdata = User::find(Auth::id());
        $support_request_data = new SupportRequest;
        $support_request_data->first_name = $userdata->name;
        $support_request_data->last_name = $userdata->lname;
        $support_request_data->organization = $userdata->organization;
        $support_request_data->email = $userdata->email;
        $support_request_data->manager_name = $salixManagerInfo->salix_manager_name;
        // $support_request_data->support_type = $support_type->title;
        // return $support_request_data;
        $request_data = $support_request_data->save();
        // return $request_data;


        if($salixManagerInfo){
            $maildata = array(

            'email' => $userdata->email
            );
            // $maildata = array('name' => $userdata->name.''.$userdata->lname,
            // 'organization' => $userdata->organization,
            // 'account_manager' => $manager_name->salix_manager_name,
            // 'support_type' => $support_type->title,
            // 'email' => $userdata->email
            // );
            // Mail::send('new_registeration_mail',$support_request_data],function($message) use ($user_mail){
            Mail::send('email.support_type', $maildata,function($message) use ($salixManagerInfo){
                $message->subject('ActonMS Customer Request to Contact Account Manager');
                $message->from('noprely@actonms.com','ACTonMS');
                $message->sender('noprely@actonms.com', 'ACTonMS');
                $message->to($salixManagerInfo->email);
            });
        }


        if ($request_data) {

            return redirect()->back()->with('success_contact_manager', 'success contact manager');

        }else{

            return redirect()->back()->with('error',config('webResponse.error_message'));

        }

    }

    public function ehrHome(){

        $ehr_data = EhrHome::first();

        return view('ehr-home')->with(compact(['ehr_data']));
    }

   public function interceptOnly()
    {
        $intercepts = InterceptOnly::where(['status' => 1])->orderBy('id','ASC')->paginate(6);
        return view('interceptonly')->with(compact([
            'intercepts'
        ]));
    }

    public function viewIntercept($intercept_id)
    {
         if (Auth::user()) {  
        $intercept = InterceptOnly::where(['id' => $intercept_id])->first();
        $previous = InterceptOnly::where('id', '<', $intercept_id)->orderBy('id','desc')->first();
        $next = InterceptOnly::where('id', '>', $intercept_id)->orderBy('id')->first();

        return view('viewintercept')->with(compact([
            'intercept',
            'previous',
            'next'
        ]));
         }
        else{
              return redirect(route('login'));
         }
    }


    public function samplePluginstatic(){

        return view('sample.sample');

    }

    public function orderNow(){

        $userdata = User::find(Auth::id());

        $manager_name = SalixManager::where('id',$userdata->salix_manager_id)->first();
        $organization_type_info = Organizationtype::where('id',$userdata->organization_id)->first();
        $order_data = new OrderRequest;
        $order_data->customer_name = $userdata->name;
        $order_data->customer_lname = $userdata->lname;
        $order_data->email = $userdata->email;
        $order_data->organization = $userdata->organization;
        if($organization_type_info){
            $order_data->organization_type = $organization_type_info->type;
            $organization_type_info_email = $organization_type_info->type;
        }else{
            $order_data->organization_type = null;
            $organization_type_info_email = null;
        }

        if($manager_name){
            $order_data->manager_name = $manager_name->salix_manager_name;
        }else{
            $order_data->manager_name = null;
        }


        $order_now = $order_data->save();

        // $maildata = array('name' => $userdata->name.''.$userdata->lname,
        //     'organization' => $userdata->organization,
        //     'account_manager' => $manager_name->salix_manager_name,
        //     'email' => $userdata->email
        // );


        // Notification::route('mail', $order_data->email)->notify(new pluginEnquiry($order_data));
    		$formData = [
    		// 'quick_support' => $quick_support,
        //    'order_set_kit' => $order_set_kit,
           'name' => $userdata->name .' '.$userdata->lname,
           'email' => $userdata->email,
        //    'professional_title' => $request->professional_title,
           'name_of_organization' => $userdata->organization,
        //    'organization_city' => $request->organization_city,
        //    'organization_state' => $request->organization_state,
           'type_of_organization' => $organization_type_info_email,
    		    ];
    		$email = env('MAIL_FROM_ADDRESS');
    // 		$email = 'sanat.leo9@gmail.com';
    		Mail::send('admin_notify',["data1"=>$formData],function($message) use ($email){
                $message->subject('Plugin Enquiry');
                $message->from(env('MAIL_FROM_ADDRESS'),'Liverlife');
                $message->sender(env('MAIL_FROM_ADDRESS'), 'Liverlife');
                $message->to($email);
            });
        // Mail::send('new_registeration_mail',$support_request_data],function($message) use ($user_mail){

        // Mail::send('email.order_now', $maildata,function($message){
        //     $message->subject('Tool Order');
        //     $message->from('piyushpanhalleo9@gmail.com','Novartis');
        //     $message->sender('piyushpanhalleo9@gmail.com', 'Novartis');
        //     $message->to('piyushpanhalleo9@gmail.com');
        // });

        if (!($order_now)) {

            return redirect()->back()->with(['message' => "Website Under maintenance, please try again later.!", 'alert-type' => 'error']);
        }else{

            return redirect()->back()->with(['modalPopUp' => "successfully.!", 'alert-type' => 'success']);
        }

    }

    public function getOrder(Request $request)
    {
        if($request->token == 'liver@land#123!')
        {
            $orders = OrderRequest::get();

            return response()->json(['data' => $orders]);
        }
        else
        {
            return response()->json(['status' => 401 , 'message' => 'Unauthenticated']);
        }
    }

    public function needAssesmentResults()
    {
        return view('needAssesmentResults');
    }
     public function verifyEmailReset()
    {
        $users = User::where('email', '=' , $_POST['email'])->first();
        if(!$users)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }



}