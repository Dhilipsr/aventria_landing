<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register/thank-you';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'organization' => ['required', 'string', 'max:255'],
            'organization_id' => ['required'],
            'job_title' => ['required', 'string'],
            // 'contact' => ['required', 'max:12'],
            'city' => ['required', 'string', 'max:255'],
            'state' => ['required', 'string', 'max:255'],
            'salix_manager_name' =>  ['required'],
            // 'update' => ['required'],
            'agreement_check' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'password_confirmation' => ['required'],
        ],
        [
            'name.required' => 'This field is required.',
            'lname.required' => 'This field is required.',
            'organization.required' => 'This field is required.',
            'organization_id.required' => 'This field is required.',
            'job_title.required' => 'This field is required.',
            'city.required' => 'This field is required.',
            'state.required' => 'This field is required.',
            'email.required' => 'This field is required.',
            'source_id.required' => 'This field is required.',
            'salix_manager_name.required' => 'This field is required.',
            'password.required' => 'This field is required.',
            'password_confirmation.required' => 'This field is required.',
            'agreement_check.required' => 'This field is required.'

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

    protected function create(array $data)
    {
        if (!empty($data['update'])) {
            if ($data['update'] == "on") {
            $update = 1;
            }
        }else{
            $update = 0;
        }

        if (!empty($data['code'])) {
            $code = $data['code'];
        }else{
            $code = null;
        }

        if (!empty($data['agreement_check'])) {
            if ($data['agreement_check'] == "on") {
            $agreement_check = 1;
            }
        }else{
            $agreement_check = 0;
        }
        
        if (isset($data['source_id'])) {
            $source_id = $data['source_id'];
        }else{
            $source_id = null;
        }
       
        $user = User::create([
            'name' => $data['name'],
            'lname' => $data['lname'],
            'organization' => $data['organization'],
            'organization_id' => $data['organization_id'],
            'job_title' => $data['job_title'],
            'code' => $code,
            'contact' => $data['contact'],
            'city' => $data['city'],
            'state' => $data['state'],
            'salix_manager_name' =>  $data['salix_manager_name'],
            'source_id' => $source_id,
            'update' => $update,
            'agreement_check' => $agreement_check,
            'status' => 0,
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

         // send admin an email
        //  $mail_message ="New Registration: ". "\r\n" .
        //  "Hi LiverHealth admin."."\r\n"."New User has registered on the website,waiting for your approval. Check the admin panel now.!". "\r\n" .
        //  "First Name: ".$data['name']. "\r\n" .
        //  "Last Name: ".$data['lname']. "\r\n" .
        //  "Email id: ".$data['email']. "\r\n" ;

        //  Mail::raw(''.$mail_message.'', function ($message) {
        //      $message->from('smtp@liverhealthtools.com', 'LiverHealthNow Support');
        //      $message->sender('smtp@liverhealthtools.com', 'LiverHealthNow Support');

        //      $message->to('liverhealthnow@aventriahealth.com');
        //     //   $message->to('sanat.leo9@gmail.com');

        //      $message->replyTo('liverhealthnow@aventriahealth.com', 'LiverHealthNow Support');

        //      $message->subject('New Registration');

        //  });

         // send user an email
         $user_mail = $data['email'];
         $user_name = $data['name'];
        try 
        {
            Mail::send('new_registeration_mail',[],function($message) use ($user_mail){
                $message->subject('Thank you for registering with LiverLife');
                $message->from(env('MAIL_FROM_ADDRESS'),'LiverLife');
                $message->sender(env('MAIL_FROM_ADDRESS'), 'LiverLife');
                $message->to($user_mail);
            });     
        } catch (Exception $e) {
            return $user;
        } 
       
        return $user;
    }
}