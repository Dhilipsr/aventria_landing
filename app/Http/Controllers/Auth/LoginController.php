<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;

use Timezone;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function showLoginForm()
    {   
        return view('auth.login');
    }

    public function login(Request $request)
    {   
        
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        
        $ip =  $_SERVER['REMOTE_ADDR'];

        $ipInfo = file_get_contents('http://ip-api.com/json/' . $ip);
        $ipInfo = json_decode($ipInfo);
        if(isset($ipInfo->timezone)){
            $timezone = $ipInfo->timezone;
        
            date_default_timezone_set($timezone);
            
            $timezonename = date_default_timezone_get();
            
            $loginTime = date('Y-m-d H:i:s');
        }else{
            $timezonename = null;
            $loginTime = null;
        }
        
        if ($this->attemptLogin($request)) {
            if (Auth::user()->status != 1) {
               Auth::logout(); 
               return redirect()->route('login')->with('under_review','Under review');
            }
            $user = User::where(['id' => Auth::user()->id])->first();
            // echo $loginTime;
            // die;
            $user->last_login_timezone = $timezonename;
            $user->last_login = $loginTime;
            $user->save();
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    public function __construct()
    {   
        $this->middleware('guest')->except('logout');
    }


}