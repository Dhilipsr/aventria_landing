<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Mail;
use Auth;
use App\User;
use Password;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

     public function reset(Request $request)
    {
        $request->validate($this->rules(), $this->validationErrorMessages());
        
        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );

        // If the password was successfully reset, we will redirect the user back to
        $users = User::where('email', '=' , $request->email)->first();
        if($users && $request->email == Auth::user()->email)
            $email = Auth::user()->email;
        else
        {
            $error = \Illuminate\Validation\ValidationException::withMessages([
            'email' => ['These credentials do not match our records'],
            ]);
            throw $error;
        }
        Mail::send('email.password_update_notice',['email' => $email],function($message) use ($email){
            $message->subject('Password Change for LiverLife');
            $message->from(env('MAIL_FROM_ADDRESS'),'LiverLife');
            // $message->from('leo9studio@zohomail.in','Leo9');
            $message->sender(env('MAIL_FROM_ADDRESS'), 'LiverLife');
            // $message->sender('leo9studio@zohomail.in', 'Leo9');
            $message->to($email);
        });
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == Password::PASSWORD_RESET
                    ? $this->sendResetResponse($request, $response)
                    : $this->sendResetFailedResponse($request, $response);
    }
}
