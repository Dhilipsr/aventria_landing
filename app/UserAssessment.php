<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class UserAssessment extends Model
{
    public function getUserInfo()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function getState()
    {
        return $this->belongsTo('App\State', 'state');
    }
}