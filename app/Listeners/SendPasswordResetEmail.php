<?php

namespace App\Listeners;

use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendPasswordResetEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(PasswordReset $event)
    {
        $email = $event->user->email;
         Mail::send('password_update_email',['email' => $email],function($message) use ($email){
                $message->subject('Password Change for LiverLife');
                $message->from(env('MAIL_FROM_ADDRESS'),'LiverLife');
                $message->sender(env('MAIL_FROM_ADDRESS'), 'LiverLife');
                $message->to($email);
        });
    }
}
