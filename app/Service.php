<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Service extends Model
{
    public function sections()
    {
    	return $this->hasMany(ServiceSection::class)->orderBy('seq_no');
    }
}
