<?php

Route::get('/','PagesController@index')->name('index');

Route::post('/checkbeforereset','PagesController@verifyEmailReset');
// Customize your tool starts here
Route::get('/customize-your-tool/{healthtool_id}','PagesController@customizeTool')->name('customizeHealthTools');
Route::get('/view-intercept/{intercept_id}','PagesController@viewintercept')->name('viewintercept');
Route::get('/health-tools-detail/{link}/{client_name}/{healthtool_id}','PagesController@healthToolsDetail')->name('healthToolsDetail');
Route::post('/no-logo-upload','PagesController@noLogoUpload')->name('noLogoUpload');
Route::post('logo-upload','PagesController@logoUpload')->name('logo.upload');
Route::post('logo-upload-two','PagesController@logoUploadTwo')->name('logo.upload.two');
Route::post('post-to-fav','PagesController@postToFav')->name('postToFav');
Route::get('delete-from-fav','PagesController@deleteFromFav')->name('deleteFromFav');
Route::get('under-review','PagesController@underReview')->name('underReview');

// Customize your tool ends here

Route::group(['prefix' => 'service'], function () {
    Route::get('/{slug}', 'PagesController@getService')->name('get.service');
    // Route::get('/define-episode', 'PagesController@defineEpisode')->name('define-episode');
});
Route::get('quality-care', 'PagesController@qualityCare')->name('qualityCare');


Route::get('myfavtool/{user_id}', 'PagesController@shareMyfavorites')->name('share-fav-tool');
Route::group(['middleware' => ['checkuserstatus','auth']], function () {
    Route::get('resources','PagesController@healthtools')->name('resources');
    Route::get('health-tools','PagesController@healthtools')->name('healthtools');
    Route::get('interceptonly','PagesController@interceptonly')->name('interceptonly');

    Route::get('search-results','PagesController@results')->name('results');
    Route::get('home', 'PagesController@healthtools')->name('home');
    Route::get('customer-assessment-tool', 'PagesController@CostomerAssesmentTool')->name('customer-assessment-tool');
    Route::post('customer-assessment-store', 'PagesController@StoreCostomerAssesmentTool')->name('customer-assessment-store');
    //piyush panchal
    Route::get('home', 'PagesController@healthtools')->name('home');
    Route::get('health-tools/results', 'PagesController@healthtoolfilter')->name('healthtoolfilter');
    Route::get('health-tools/results/search', 'PagesController@searchhealthtool')->name('searchhealthtool');
    // Route::get('contactaccountmanager', 'PagesController@contactAccountmanager')->name('contact-account-manager');
    Route::get('contactaccountmanager', 'PagesController@contactAccountmanagerStore')->name('contact-account-manager-store');
    Route::get('sample-plugin-static', 'PagesController@samplePluginstatic')->name('sample-plugin');

    Route::get('ehr-plugin-home', 'PagesController@ehrHome')->name('ehr-home');

    Route::get('ordernow', 'PagesController@orderNow')->name('order-now');


    // Route::get('health-tools/results/sort', 'PagesController@healthtoolajaxfilter')->name('healthtoolajaxfilter');

    Route::get('my-account', 'PagesController@myaccount')->name('myaccount');
    Route::post('my-account/update', 'PagesController@myaccount_update')->name('myaccount.update');
    Route::post('check-old-password','PagesController@checkpassword')->name('password.check');
    Route::post('change-password','PagesController@changepassword')->name('password.change');
    Route::post('upload-profile-picture','PagesController@profilepicture')->name('profilepicture.upload');
    Route::get('need-assesment-results','PagesController@needAssesmentResults')->name('needassesmentresult');
});

Route::get('/register/thank-you', 'PagesController@thankyou');
Route::get('/live_search', 'AjaxSearchController@index');
Route::get('/live_search/action', 'AjaxSearchController@action')->name('live_search.action');

Route::get('/field-tower-team-training', 'PagesController@team_training');
Route::get('/form', 'PagesController@form');

Auth::routes();
// Route::get('password/reset', 'Auth\LoginController@logout')->name('logout');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['prefix' => 'api'], function()
{
    Route::get('/get-healthtools','PagesController@getTools');
    Route::get('/get-user-assesments','PagesController@getUserAssessments');
    Route::get('/get-user','PagesController@getUser');
    Route::get('/get-order-details','PagesController@getOrder');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});