@section('title')
Search Results
@endsection
@extends('layouts.layout')
@section('content')
<!-- Start Here -->
<div class="banner-container">
    <div class="banner-container__top search-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>YOUR SEARCH FOR: "Lorem Ipsum"
                        <span>18 Result(s) found</span> </h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="search-container-up">
    <div class="container">
        <ul class="search-container">
            <li>
                <a href="#" class="search-main-link">Meningitis Immunization for Colleges/Universities</a>
                <span>(Immunization Schedules and Public Data)</span>
                Link: <a href="#" class="search-sub-link">http://immunize.org/laws/menin.asp</a>
            </li>
            <li>
                <a href="#" class="search-main-link">Meningitis Immunization for Colleges/Universities</a>
                <span>(Immunization Schedules and Public Data)</span>
                <span>Link: <a href="#"
                        class="search-sub-link">http://immunize.org/laws/menin.asp</a></span>
            </li>
            <li>
                <a href="#" class="search-main-link">Meningitis Immunization for Colleges/Universities</a>
                <span>(Immunization Schedules and Public Data)</span>
                Link: <a href="#" class="search-sub-link">http://immunize.org/laws/menin.asp</a>
            </li>
        </ul>
        <ul class="pagin">
            <li class="paginNP">Prev</li>
            <li>1</li>
            <li class="active">2</li>
            <li>3</li>
            <li class="paginNP">Next</li>
        </ul>
    </div>
</div>

<button class="successBtn">Succes</button>
<button class="errorBtn">Error</button>
<!--Ends Here -->
@endsection

@section('page-level-js')

@endsection
