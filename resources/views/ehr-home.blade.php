@extends('layouts.layout')

@section('title')
EHR Plugin
@endsection

@section('content')

<!-- Main Section -->
<div class="banner-container">
    <div class="banner-container__top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>EHR Plugin</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row  justify-content-center ehr-text">
            <div class="col-lg-10 col-12 htools ">
                {!! $ehr_data->main_content !!}
            </div>
        </div>
    </div>
</div>
<div class="section-gray">
    <div class="section">
        <div class="container">
            <div class="gray-wrap">
                <div class="image-wrap">
                    {{-- <img src="{{ asset('/img/samplepluginstaticpage.png') }}" alt=""> --}}
                    <img src="{{ asset('storage/' . $ehr_data->sample_page) }}" alt="">
                </div>
                <a href="{{ route('sample-plugin') }}" style="width: 24%;margin-right: -3%;margin-top:-12%;" target="_blank" class="button line view_life_sample">View Live Sample</a>
                <div class="section">
                    <div class="left-align-box">
                        <h3 class="partial-bold order_texts"> {!! $ehr_data->contact_acc_manager !!}</h3>
                        <button id="plugin_order" class="line" data-toggle="modal" data-target="#myModal5">Order Now</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section">
    <div class="container">
        <div class="left-align-box">
            <h3>GINA™ (Guided INtegration Assistant) can help you integrate resources into your EHR system.</h3>
            <a href="https://www.research.net/r/LiverLifePro" target="_blank" class="button line Gina_ehr">GINA™</a>
        </div>
    </div>
</div>




<div id="myModal5" class="modal fade" role="dialog">
    <div class="modal-dialog modal-box">
        <div class="model-close" data-dismiss="modal">
            <!-- <img src="{{ asset('img/close-white.png') }}" alt="img" /> --><img src="img/close-white.png"
                alt="img" />
        </div>
        <div class="default-box">
            <div class="order-now-wrap">
                <h3>
                    EHR Plugin Order Form
                </h3>
                <div class="order-inner">
                    <p>Simple steps to order:</p>
                    <ol>
                        <li>Hit submit below.</li>
                        <li>A member of the Aventria team will contact you to confirm your order.</li>
                        <li>Your order will be sent to you within 1 week (5 business days).</li>
                    </ol>
                    <p>If you have any questions, please contact support@liverlifepro.com.</p>
                </div>
                <a href="{{ route('order-now') }}" class="ehr_submit button wht">Submit</a>
            </div>
        </div>
    </div>
</div>

<div id="successModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-box">
        <div class="model-close" data-dismiss="modal">
            <!-- <img src="{{ asset('img/close-white.png') }}" alt="img" /> --><img src="img/close-white.png"
                alt="img" />
        </div>
        <div class="default-box">
            <div class="order-now-wrap">
                <h3>
                    We have received your order.
                </h3>
                <div class="order-inner text-center">
                    <p>You will receive your plugin via email within 1 week (5 business days).
                        If you are interested in learning about EHR best practices on how to
                        integrate the LiverLife plugin into your existing EHR system, go to the
                        GINA<sup>TM</sup> link on this page.</p>
                </div>

            </div>
        </div>
    </div>
</div>


@endsection

@section('extra-js')

<script type="text/javascript">
    @if(Session::has('modalPopUp'))
        $('#successModal').modal('show');
        // function CheckColors(val) {
        //     var element = document.getElementById('color');
        //     if (val == 'pick a color' || val == 'others')
        //         element.style.display = 'block';
        //     else
        //         element.style.display = 'none';
        // }
        @endif




</script>

@endsection