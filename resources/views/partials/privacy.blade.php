<!-- Modal -->
<div id="privacymodal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-box-license">
        <div class="model-close" data-dismiss="modal"><img src="img/close-blk.png" alt="img" /></div>
        <div class="default-box">
            

                <h1 style="text-align: center;">Privacy Policy</h2>
                <div class="field-item even"><p>Effective: April 20, 2020</p>
<p>Novartis International AG (Switzerland), as data controller, is responsible for the processing of your personal information on this website and/or for the purposes described in this Privacy Policy. In this Privacy Policy, “Novartis”, “we” or “us” refers to Novartis International AG.</p>
<p>Please carefully read this Privacy Policy, which describes the ways in which we collect information about individuals (“Personal Data”), how we hold and use Personal Data and how we respect your privacy rights.</p>
<p>We may change or update this Privacy Policy from time to time by posting a new privacy policy on this website.</p>
<div class="embed"><article class="node-88501 node node-tab internal-node view-mode-node_embed clearfix"><div class="field-collection-container clearfix">
<div class="field field-name-field-accordion field-type-field-collection field-label-hidden accordion-collapsible ui-accordion ui-widget ui-helper-reset" id="ui-id-1" role="tablist">
<div class="field-items">
<div class="field-item even">
<div class="nv-collection">
<div class="entity entity-field-collection-item field-collection-item-field-accordion clearfix">
<div class="content">
<div class="field field-name-field-title field-type-text field-label-hidden ui-accordion-header ui-helper-reset ui-state-default ui-accordion-icons ui-corner-all" role="tab" id="ui-accordion-ui-id-1-header-0" aria-controls="field-collection-item-field-accordion-full-group-accordion-body" aria-selected="false" tabindex="0"><span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>
<div class="field-items">
<div class="field-item even">What Personal Data do we process and for which purposes?</div>
</div>
</div>
<div id="field-collection-item-field-accordion-full-group-accordion-body" class="group-accordion-body field-group-div ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" aria-labelledby="ui-accordion-ui-id-1-header-0" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;"><div class="acc-wrapper">
<div class="field field-name-field-inner-body field-type-text-long field-label-hidden">
<div class="field-items">
<div class="field-item even">
<p>Most of our services do not require any form of registration, allowing you to visit our site without telling us who you are. However, some services may require you to voluntarily provide us with Personal Data, which may include information such as your name, birth date, email address or telephone number. We may collect and use this Personal Data to provide you with products, services, and customer support, to bill you for products and services you request, to market products and services which we think may be of interest to you, or to communicate with you for other purposes which are evident from the circumstances or about which we inform you when we collect your Personal Data.</p>
<p>Personal Data collected from public social media:</p>
<p>We may also collect personal data that you made publicly available on public social media platforms (including blogs, forums etc.), related to Novartis, Novartis products, and more in general about drugs and diseases. This activity, called “social media listening”, is based on our legitimate business interests, for example to (i) have a better understanding of how certain key audiences like healthcare professionals or patients experience certain diseases or react to the use of Novartis products, (ii) have a better understanding of Novartis’ reputation as well as other market trends, (iii) identify key-stakeholders, in particular bloggers and social media influencers, and to initiate contact with them. This may include your personal information in form of comments, messages, blogs, photos and videos, although we will take steps to limit this personal information to the minimum necessary and keep it for no longer than necessary for the social media listening activity. If you want to limit further who can see your information, we recommend that you use the privacy settings available to you on such platforms.</p>
<p>When you share your personal information on a public social media platform, we suggest you also familiarize yourself with the privacy policy of that specific platform as these platforms are not owned and managed by Novartis.</p>
<p>Personal Data used for website usage analytics:</p>
<p>We may also collect and process information about your visit to this website, such as the pages you visit, the website you came from and the searches you perform. We may use such information to help improve the contents of the site and to compile aggregate statistics about people using our site for our internal usage statistics and market research purposes. In doing this, we may install "cookies" that collect the domain name of the user, your internet service provider, your operating system, and the date and time of access. A "cookie" is a small piece of information, which is sent to your browser and stored on your computer’s hard drive. Cookies do not damage your computer. You can set your browser to notify you when you receive a "cookie”, this will enable you to decide if you want to accept it or not. You can also refuse cookies altogether. However, if you do not accept our cookies, you may not be able to use all functionalities of our website.</p>
<p>We do not currently respond to web browser “do not track” signals or other mechanisms that provide a method to opt out of the collection of information across websites or other online services.</p>
<p>Occasionally, we and our third-party advertising and service providers may use internet tags (also known as action tags, single-pixel GIFs, clear GIFs, invisible GIFs and 1-by-1 GIFs) and cookies at this site and may deploy these tags/cookies through a third-party advertising partner or a web analytical service partner which may be located and store the respective information (including your IP address) in a foreign country. These tags/cookies are placed on both online advertisements that bring users to this site and on different pages of this site. We use this technology to measure the visitors' responses to our sites and the effectiveness of our advertising campaigns (including how many times a page is opened and which information is consulted) as well as to evaluate your use of this website. The third-party partner or the web analytical service partner may be able to collect data about visitors to our and other sites because of these internet tags/cookies, may compose reports regarding the website’s activity for us and may provide further services which are related to the use of the website and the internet. They may provide such information to other parties if there is a legal requirement that they do so, or if they hire other parties to process information on their behalf. If you would like more information about web tags and cookies, please visit the Network Advertising Initiative website <a href="https://www.networkadvertising.org" target="_blank" class="ext extlink">https://www.networkadvertising.org<span class="ext" aria-label="(link is external)"></span></a>.</p>
<p>We may use a number of technologies offered by different providers to support website analytics and user tracking, including technologies offered by the providers below. If you wish to prevent or control the use of these technologies, please follow the links mentioned with that particular third party:</p>
<p><strong>Crazy Egg</strong> (Crazy Egg, Inc., 16220 E. Ridgeview Lane, La Mirada, CA, 90638, USA) <a href="https://www.crazyegg.com/privacy" class="ext extlink">Privacy Policy<span class="ext" aria-label="(link is external)"></span></a>, <a href="https://www.crazyegg.com/opt-out" target="_blank" class="ext extlink">Opt-out<span class="ext" aria-label="(link is external)"></span></a></p>
<p><strong>DoubleClick</strong> (Google Inc.,1600 Amphitheatre Parkway, Mountain View, CA 94043, USA) <a href="https://policies.google.com/privacy?hl=en" class="ext extlink">Privacy Policy<span class="ext" aria-label="(link is external)"></span></a>, <a href="https://www.google.com/ads/preferences?hl=en" target="_blank" class="ext extlink">Opt-out<span class="ext" aria-label="(link is external)"></span></a></p>
<p><strong>Facebook</strong> (Facebook, Inc., 1601 S. California Avenue, Palo Alto, CA, 94304, USA) <a href="https://www.facebook.com/privacy/explanation" class="ext extlink">Privacy Policy<span class="ext" aria-label="(link is external)"></span></a>, <a href="https://developers.facebook.com/docs/plugins" class="ext extlink">Plug-in<span class="ext" aria-label="(link is external)"></span></a>, <a href="https://www.facebook.com/ads/preferences/edit/" target="_blank" class="ext extlink">Opt-out<span class="ext" aria-label="(link is external)"></span></a></p>
<p><strong>Google Analytics</strong> (Google Inc.,1600 Amphitheatre Parkway, Mountain View, CA 94043, USA) <a href="https://policies.google.com/privacy?hl=en" class="ext extlink">Privacy Policy<span class="ext" aria-label="(link is external)"></span></a>, <a href="https://tools.google.com/dlpage/gaoptout" target="_blank" class="ext extlink">Opt-out<span class="ext" aria-label="(link is external)"></span></a></p>
<p><strong>LinkedIn</strong> (LinkedIn Ireland Unlimited Company, Wilton Place, Dublin 2, Irland) <a href="https://www.linkedin.com/legal/privacy-policy" class="ext extlink">Privacy Policy<span class="ext" aria-label="(link is external)"></span></a>, <a href="https://www.linkedin.com/psettings/guest-controls/retargeting-opt-out?trk=microsites-frontend_legal_cookie-policy" target="_blank" class="ext extlink">Opt-out<span class="ext" aria-label="(link is external)"></span></a></p>
<p><strong>Twitter</strong> (Twitter Inc., 1355 Market Street, Suite 900, San Francisco, CA, 94103, USA) <a href="https://twitter.com/privacy" class="ext extlink">Privacy Policy<span class="ext" aria-label="(link is external)"></span></a>, <a href="https://twitter.com/about/resources/buttons" class="ext extlink">Plug-in<span class="ext" aria-label="(link is external)"></span></a>, <a href="https://twitter.com/personalization" target="_blank" class="ext extlink">Opt-out<span class="ext" aria-label="(link is external)"></span></a></p>
<p><strong>Glassdoor</strong> (Glassdoor Inc., 100 Shoreline Highway, Mill Valley, CA 94941, USA) <a href="https://www.glassdoor.com/about/privacy.htm" target="_blank" class="ext extlink">Privacy Policy<span class="ext" aria-label="(link is external)"></span></a>, <a href="https://www.glassdoor.com/about/privacy.htm#cookiePolicyAndAdChoices" target="_blank" class="ext extlink">Opt-out<span class="ext" aria-label="(link is external)"></span></a></p>
<p>We may combine, aggregate, or anonymize Personal Data with data we may collect from or about you from other sources, such as public databases, providers of demographic information, joint marketing partners, social media platforms, and other third parties.</p>
<p>We may use your data for our business purposes, including audits, monitoring and prevention of fraud, infringement, and other potential misuse of our products and services, and for modifying our services.</p>
<p>Also, we may use your Personal Data:</p>
<ul><li>if we are required to do so because of an applicable law, requests from public and government authorities (including court order, subpoena, or governmental regulation), even outside your country of residence;</li>
<li>if we need to enforce our terms and conditions;</li>
<li>when we believe in good faith that the use of Personal Data is necessary to protect legal rights, the security or integrity of this website;&nbsp;</li>
<li>to protect your safety or the safety of others;&nbsp;</li>
<li>as part of any criminal or other legal investigation or proceeding in your country or in other countries; or,&nbsp;</li>
<li>to the extent reasonably necessary for development of or to proceed with the negotiation or completion of a corporate or commercial transaction.<br>&nbsp;</li>
</ul></div>
</div>
</div>
</div></div>
</div>
</div>
</div>
</div>
<div class="field-item odd">
<div class="nv-collection">
<div class="entity entity-field-collection-item field-collection-item-field-accordion clearfix">
<div class="content">
<div class="field field-name-field-title field-type-text field-label-hidden ui-accordion-header ui-helper-reset ui-state-default ui-corner-all ui-accordion-icons" role="tab" id="ui-accordion-ui-id-1-header-1" aria-controls="field-collection-item-field-accordion-full-group-accordion-body--2" aria-selected="false" tabindex="-1"><span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>
<div class="field-items">
<div class="field-item even">When and to whom do we disclose your information?</div>
</div>
</div>
<div id="field-collection-item-field-accordion-full-group-accordion-body--2" class="group-accordion-body field-group-div ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" aria-labelledby="ui-accordion-ui-id-1-header-1" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;"><div class="acc-wrapper">
<div class="field field-name-field-inner-body field-type-text-long field-label-hidden">
<div class="field-items">
<div class="field-item even">
<p>This Privacy Policy describes the circumstances in which we may share your Personal Data. We may share your Personal Data with other Novartis subsidiaries and affiliates worldwide. We also may transfer Personal Data to third parties who act on our behalf, for further processing in accordance with the purpose(s) for which the data were originally collected or may otherwise be lawfully processed, such as services delivery, evaluating the usefulness of this website, marketing, advertising, data management, or technical support.&nbsp;</p>
<p>These third parties have contracted with us to only use Personal Data for the agreed upon purpose, and not to sell Personal Data to third parties, and not to disclose it to third parties except as may be permitted by us, as required by law, or as stated in this Privacy Policy.</p>
<p>We may disclose your Personal Data to a third party in the event that the business or a part of it and the customer data connected with it is sold, assigned or transferred, in which case we would require the buyer, assignee or transferee to treat Personal Data in accordance with this Privacy Policy.</p>
<p>Also, we may disclose your Personal Data to a third party if we are required to do so because of an applicable law, requests from public and government authorities (including court order, subpoena, or governmental regulation), even outside your country of residence; if we need to enforce our terms and conditions; when we believe in good faith that the disclosure is necessary to protect legal rights, the security or integrity of this website; to protect your safety or the safety of others; as part of any criminal or other legal investigation or proceeding in your country or in other countries; or to third parties, advisors, and other entities to the extent reasonably necessary for development of or to proceed with the negotiation or completion of a corporate or commercial transaction.</p>
<p>Your Personal Data may also be processed, accessed, or stored in countries outside Switzerland. Such countries may offer a different level of protection of personal data. If we transfer your Personal Data to external companies in other jurisdictions, we will make sure to protect your Personal Data by applying the level of protection required under applicable data privacy laws.</p>
<p>For transfers of personal data among Novartis subsidiaries and affiliates, Novartis has adopted Binding Corporate Rules, a system of principles, rules, and tools, authorized by European law, to regulate the transfer of personal data outside the EEA and Switzerland. Read more about the Novartis Binding Corporate Rules by clicking <a href="https://www.novartis.com/privacy-policy/novartis-binding-corporate-rules-bcr">here</a>.</p>
</div>
</div>
</div>
</div></div>
</div>
</div>
</div>
</div>
<div class="field-item even">
<div class="nv-collection">
<div class="entity entity-field-collection-item field-collection-item-field-accordion clearfix">
<div class="content">
<div class="field field-name-field-title field-type-text field-label-hidden ui-accordion-header ui-helper-reset ui-state-default ui-corner-all ui-accordion-icons" role="tab" id="ui-accordion-ui-id-1-header-2" aria-controls="field-collection-item-field-accordion-full-group-accordion-body--3" aria-selected="false" tabindex="-1"><span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>
<div class="field-items">
<div class="field-item even">How do we protect your Personal Data?</div>
</div>
</div>
<div id="field-collection-item-field-accordion-full-group-accordion-body--3" class="group-accordion-body field-group-div ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" aria-labelledby="ui-accordion-ui-id-1-header-2" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;"><div class="acc-wrapper">
<div class="field field-name-field-inner-body field-type-text-long field-label-hidden">
<div class="field-items">
<div class="field-item even">
<p>We use appropriate technical, administrative, and physical safeguards to protect the information collected through this website. Unfortunately, no organization can guarantee the absolute security of information, especially information transmitted over the internet.</p>
</div>
</div>
</div>
</div></div>
</div>
</div>
</div>
</div>
<div class="field-item odd">
<div class="nv-collection">
<div class="entity entity-field-collection-item field-collection-item-field-accordion clearfix">
<div class="content">
<div class="field field-name-field-title field-type-text field-label-hidden ui-accordion-header ui-helper-reset ui-state-default ui-corner-all ui-accordion-icons" role="tab" id="ui-accordion-ui-id-1-header-3" aria-controls="field-collection-item-field-accordion-full-group-accordion-body--4" aria-selected="false" tabindex="-1"><span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>
<div class="field-items">
<div class="field-item even">How do we deal with information from individuals under the age of 13?</div>
</div>
</div>
<div id="field-collection-item-field-accordion-full-group-accordion-body--4" class="group-accordion-body field-group-div ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" aria-labelledby="ui-accordion-ui-id-1-header-3" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;"><div class="acc-wrapper">
<div class="field field-name-field-inner-body field-type-text-long field-label-hidden">
<div class="field-items">
<div class="field-item even">
<p>Our website is not directed at children. We do not knowingly collect Personal Data from children under the age of 13.</p>
</div>
</div>
</div>
</div></div>
</div>
</div>
</div>
</div>
<div class="field-item even">
<div class="nv-collection">
<div class="entity entity-field-collection-item field-collection-item-field-accordion clearfix">
<div class="content">
<div class="field field-name-field-title field-type-text field-label-hidden ui-accordion-header ui-helper-reset ui-state-default ui-corner-all ui-accordion-icons" role="tab" id="ui-accordion-ui-id-1-header-4" aria-controls="field-collection-item-field-accordion-full-group-accordion-body--5" aria-selected="false" tabindex="-1"><span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>
<div class="field-items">
<div class="field-item even">How long do we store Personal Data?</div>
</div>
</div>
<div id="field-collection-item-field-accordion-full-group-accordion-body--5" class="group-accordion-body field-group-div ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" aria-labelledby="ui-accordion-ui-id-1-header-4" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;"><div class="acc-wrapper">
<div class="field field-name-field-inner-body field-type-text-long field-label-hidden">
<div class="field-items">
<div class="field-item even">
<p>We will only retain your Personal Data for as long as necessary to fulfil the purpose for which they were collected or to comply with legal or regulatory requirements.</p>
</div>
</div>
</div>
</div></div>
</div>
</div>
</div>
</div>
<div class="field-item odd">
<div class="nv-collection">
<div class="entity entity-field-collection-item field-collection-item-field-accordion clearfix">
<div class="content">
<div class="field field-name-field-title field-type-text field-label-hidden ui-accordion-header ui-helper-reset ui-state-default ui-corner-all ui-accordion-icons last" role="tab" id="ui-accordion-ui-id-1-header-5" aria-controls="field-collection-item-field-accordion-full-group-accordion-body--6" aria-selected="false" tabindex="-1"><span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>
<div class="field-items">
<div class="field-item even">What are your rights and how can you exercise them?</div>
</div>
</div>
<div id="field-collection-item-field-accordion-full-group-accordion-body--6" class="group-accordion-body field-group-div ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom last" aria-labelledby="ui-accordion-ui-id-1-header-5" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;"><div class="acc-wrapper">
<div class="field field-name-field-inner-body field-type-text-long field-label-hidden">
<div class="field-items">
<div class="field-item even">
<p>Whenever we process Personal Data, we take reasonable steps to keep your Personal Data accurate and up-to-date for the purposes for which they were collected. We will provide you with the ability to exercise the following rights under the conditions and within the limits set forth in the law.</p>
<p>If you wish to contact us regarding the use of your Personal Data or you want to object in whole or in part to the processing of your Personal Data, please <a href="#contactus">contact us</a>. If you have provided consent, you may withdraw consent. You may also request, subject to confidentiality obligations, to:</p>
<ul><li>access your Personal Data as processed by us;</li>
<li>ask for correction or erasure of your Personal Data; and</li>
<li>request portability, where applicable, of your Personal data, i.e. that the Personal Data you have provided to us, are returned to you or transferred to the person of your choice, in a structured, commonly used and machine-readable format.</li>
</ul></div>
</div>
</div>
</div></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<p class="emptyP"></p></article><br><a id="privacyatnovartis" name="privacyatnovartis"></a></div>
<h2>Privacy at Novartis</h2>
<p>The responsible use of personal data is a core value at Novartis.</p>
<p>Novartis Group fully respects privacy laws and is subject to an internal framework of privacy rules and policies.</p>
<p class="mb">The internal transfer of data is governed by Binding Corporate Rules, so called “<a href="https://www.novartis.com/privacy-policy/novartis-binding-corporate-rules-bcr">BCR</a>”. BCR are a system of privacy principles, rules and tools intended to ensure the protection of personal data. This collection of rules represents today’s best practice to meet the European Economic Area’s (“EEA”) data protection requirements for the transfer of personal data within a Group of companies. To be legally effective, the BCR have been approved by EEA Data Protection Agencies. BCR regulate the mechanism of transfer of data inside the Novartis Group of companies.<a id="contactus" name="contactus"></a></p>
<h2>Contact us</h2>
<p>If you wish to contact us regarding how we use your personal data or you wish to exercise your data privacy rights, please email us at <a href="mailto:global.privacy_office@novartis.com">global.privacy_office@novartis.com</a> or write us to the following address:</p>
<p>Novartis International AG<br>Global Privacy Office<br>Fabrikstrasse 18<br>4056 Basel<br>Switzerland<br>&nbsp;</p>
<p>In order to facilitate the most efficient answer, please provide the following information:</p>
<ul><li>Name of the website you are referring to&nbsp;</li>
<li>Your relationship and interaction with us<a id="cookies" name="cookies"></a></li>
<li>The description of the information you would like to receive from us</li>
</ul><h3>Additional State-Specific Information for United States Residents</h3>
<p class="mb">Some states (including but not limited to California, Nevada and Texas), have state specific rights for their residents. <a href="https://www.novartis.us/state-privacy-notices" target="_blank" class="ext extlink">Click here<span class="ext" aria-label="(link is external)"></span></a> to learn more about those state specific rights.</p>
<h2>Cookies</h2>
<div class="embed"><article class="node-88506 node node-tab internal-node view-mode-node_embed clearfix"><div class="field-collection-container clearfix">
<div class="field field-name-field-accordion field-type-field-collection field-label-hidden accordion-collapsible ui-accordion ui-widget ui-helper-reset" id="ui-id-2" role="tablist">
<div class="field-items">
<div class="field-item even">
<div class="nv-collection">
<div class="entity entity-field-collection-item field-collection-item-field-accordion clearfix">
<div class="content">
<div class="field field-name-field-title field-type-text field-label-hidden ui-accordion-header ui-helper-reset ui-state-default ui-accordion-header-active ui-state-active ui-corner-top ui-accordion-icons" role="tab" id="ui-accordion-ui-id-2-header-0" aria-controls="field-collection-item-field-accordion-full-group-accordion-body--7" aria-selected="true" tabindex="0"><span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-s"></span>
<div class="field-items">
<div class="field-item even">What is a cookie?</div>
</div>
</div>
<div id="field-collection-item-field-accordion-full-group-accordion-body--7" class="group-accordion-body field-group-div ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content-active" aria-labelledby="ui-accordion-ui-id-2-header-0" role="tabpanel" aria-expanded="true" aria-hidden="false" style="display: block;"><div class="acc-wrapper">
<div class="field field-name-field-inner-body field-type-text-long field-label-hidden">
<div class="field-items">
<div class="field-item even">
<p>Cookies are small text files that are sent to your computer when you visit a website. Cookies on Novartis Group company (Novartis) web sites do lots of different jobs, like letting you navigate between pages efficiently, storing your preferences and generally improving your experience of a website.</p>
<p>The EU Directive 2009/136/EC states that we can store cookies on your machine if they are essential to the operation of this site, but that for all others we need your permission to do so.</p>
<p>Novartis sites may use some non-essential cookies. We do not do this to track individual users or to identify them, but to gain useful knowledge about how the sites are used so that we can keep improving them for our users. Without the knowledge we gain from the systems that use these cookies we would not be able to provide the service we do.</p>
</div>
</div>
</div>
</div></div>
</div>
</div>
</div>
</div>
<div class="field-item odd">
<div class="nv-collection">
<div class="entity entity-field-collection-item field-collection-item-field-accordion clearfix">
<div class="content">
<div class="field field-name-field-title field-type-text field-label-hidden ui-accordion-header ui-helper-reset ui-state-default ui-corner-all ui-accordion-icons" role="tab" id="ui-accordion-ui-id-2-header-1" aria-controls="field-collection-item-field-accordion-full-group-accordion-body--8" aria-selected="false" tabindex="-1"><span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>
<div class="field-items">
<div class="field-item even">The types of cookies we use</div>
</div>
</div>
<div id="field-collection-item-field-accordion-full-group-accordion-body--8" class="group-accordion-body field-group-div ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" aria-labelledby="ui-accordion-ui-id-2-header-1" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;"><div class="acc-wrapper">
<div class="field field-name-field-inner-body field-type-text-long field-label-hidden">
<div class="field-items">
<div class="field-item even">
<p>If you decide to set the language, font-size or specific version of the site (e.g. high-contrast), we use “user interface customization cookies”. Once set, you do not need to specify your preferences again on another visit to the site.</p>
<p>If you use parts of the site that require registration to access content, we will place an “authentication cookie” on your computer. This allows you to leave and return to these parts of the site without re-authenticating yourself.</p>
<p>If you have Adobe Flash installed on your computer (most computers do) and you use video players, we store a “flash cookie” on your computer. These cookies are used to store data needed to play back video or audio content and store the user’s preferences.</p>
<p>Novartis likes to understand how visitors use our websites by using web analytics services. They count the number of visitors and tell us things about the visitors’ behavior overall – such as identifying the search engine keywords that lead the user to the site, the typical length of stay on the site or the average number of pages a user views. For this purpose we place a “first party analytics cookie” on your computer.</p>
<p>We may also use services such as Google Analytics to track web statistics. In this case, Google will place a “3rd party cookie” on your computer. This is also the case when we use Google Maps.</p>
<p>Any data collected by using these cookies will be stored and managed by Novartis or one of its trusted affiliates in countries Novartis operates in.</p>
<p>For more information or how to contact Novartis, please refer to the Novartis Data Privacy Policy.</p>
</div>
</div>
</div>
</div></div>
</div>
</div>
</div>
</div>
<div class="field-item even">
<div class="nv-collection">
<div class="entity entity-field-collection-item field-collection-item-field-accordion clearfix">
<div class="content">
<div class="field field-name-field-title field-type-text field-label-hidden ui-accordion-header ui-helper-reset ui-state-default ui-corner-all ui-accordion-icons last" role="tab" id="ui-accordion-ui-id-2-header-2" aria-controls="field-collection-item-field-accordion-full-group-accordion-body--9" aria-selected="false" tabindex="-1"><span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>
<div class="field-items">
<div class="field-item even">How to control cookies</div>
</div>
</div>
<div id="field-collection-item-field-accordion-full-group-accordion-body--9" class="group-accordion-body field-group-div ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom last" aria-labelledby="ui-accordion-ui-id-2-header-2" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;"><div class="acc-wrapper">
<div class="field field-name-field-inner-body field-type-text-long field-label-hidden">
<div class="field-items">
<div class="field-item even">
<p>If you don’t want to receive cookies, you can modify your browser so that it notifies you when cookies are sent to it or you can refuse cookies altogether. You can also delete cookies that have already been set.</p>
<p>If you wish to restrict or block web browser cookies which are set on your device then you can do this through your browser settings; the Help function within your browser should tell you how. Alternatively, you may wish to visit <a href="https://www.aboutcookies.org" target="_blank" class="ext extlink">www.aboutcookies.org<span class="ext" aria-label="(link is external)"></span></a>, which contains comprehensive information on how to do this on a wide variety of desktop browsers. However, if you do not accept our cookies, you may not be able to use all functionalities of your browser software or our website.</p>
</div>
</div>
</div>
</div></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<p class="emptyP"></p></article></div>
<p class="emptyP"></p>
<p class="emptyP"></p>
</div>
                 
            
        </div>
    </div>
</div>