{{-- Header Starts  --}}

<header>
    <div class="small-header">
        <div class="container">
            <ul>
                <li class="text-white">This site is intended for US health care professionals only</li>
                @if(Auth::check())
                  <li><a href="{{ route('myaccount') }}">My Account</a></li>
                    <li><a href="{{ route('logout') }}">Log Out</a></li>
                @else
                     <li><a href="{{ route('login') }}">Log In</a></li>
                    <li ><a href="{{ route('register') }}">Register</a></li>
                 @endif
                <li><a class="srchBtn"><img src="{{asset('img/search.png')}}" alt="img"></a></li>
            </ul>
        </div>
    </div>
    <div class="main-header">
        <div class="container">
            <div class="main-header__in">
                <div class="logo"><a href="{{ route('index') }}"><img src="{{asset('img/logo.png')}}" alt="img"></a></div>
                <ul class="navigation">
                    <li class="@if(Request::path() == "service/screen-patients") active @endif"><a href="{{ route('get.service',['slug' => 'screen-patients']) }}">Screen<br> Patients</a></li>
                    <li class="@if(Request::path() == "service/define-an-episode") active @endif"><a href="{{ route('get.service',['slug' => 'define-an-episode']) }}">Define<br>an Episode</a></li>
                    <li class="@if(Request::path() == "service/coordinate-care") active @endif"><a href="{{ route('get.service',['slug' => 'coordinate-care']) }}">Coordinate<br>Care</a></li>
                    <li class="cta @if(Request::path() == "health-tools") active @endif"><a href="{{ route('healthtools') }}">Get<br>Resources</a></li>
                    @if(Auth::check())
                    <li class="fxHdr fxHdr1"><a href="{{ route('logout') }}">Log Out</a></li>
                    @else
                     <li class="fxHdr fxHdr1"><a href="{{ route('login') }}">Log In</a></li>
                    <li class="fxHdr"><a href="{{ route('register') }}">Register</a></li>
                     @endif
                    <li class="fxHdr"><a class="srchBtn"><img src="{{asset('img/search-clr.png')}}" alt="img"></a></li>
                </ul>
                <div class="mob-menu-box">
                    <div class="mob-srchBtn">
                        <a class="srchBtn"><img src="{{asset('img/mobSearchIcon.png')}}" alt="img"></a>
                    </div>
                    <div class="menu-icon"><span></span></div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="mob-menu-up">
    <div class="mob-menu">
        <div class="mob-close-box"><span class="mob-close"><img src="{{asset('img/close-white.png')}}" alt="img" /></span>
        </div>
        <ul class="navigation">
            @if(Auth::check())
            <li><a href="{{ route('myaccount') }}">My Account</a></li>
            <li ><a href="{{ route('logout') }}">Log Out</a></li>
            @else
             <li><a href="{{ route('login') }}">Log In</a></li>
            <li ><a href="{{ route('register') }}">Register</a></li>
             @endif
            <li><a href="{{ route('get.service',['slug' => 'screen-patients']) }}">Screen Patients</a></li>
            <li><a href="{{ route('get.service',['slug' => 'define-an-episode']) }}">Define an Episode</a></li>
            <li><a href="{{ route('get.service',['slug' => 'coordinate-care']) }}">Coordinate Care</a></li>
            <li><a href="{{ route('healthtools') }}">Get Resources</a></li>
            {{-- <li><a href="#">References</a></li> --}}
            <li><a href="https://www.bauschhealth.com/privacy" target="_blank">Privacy Policy</a></li>
            <li><p>This site is intended for US health care
            professionals only.</p></li>
        </ul>
    </div>
</div> 
<div class="searchContainer">
    <div class="srchBox">
        <div class="container">
            <div class="srch">
                <input type="text" placeholder="Search" id="search">
                <span class="srchClose"></span>
            </div>
            <div class="srch__container">
                <div class="srch__cont">
                    <div class="srch__container__in">
                        <h2>Resources</h2>
                        <ul id="healthtools_ajax_results">
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>

                {{-- <div class="srch-all">

                    <a href="#">Total tools : <span id="total_records"></span></a>

                </div> --}}
            </div>
        </div>
    </div>
</div> 

{{-- <div class="searchContainer">

<div class="srchBox">

    <div class="container">

        <div class="srch">

            <input type="text" placeholder="Search">

            <span class="srchClose"></span>

        </div>

    </div>

</div>

</div> --}}

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">

@csrf

</form>

<!-- Header Ends