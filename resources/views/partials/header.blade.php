{{-- Header Starts --}}
<header>
    <div class="small-header">
        <div class="container">
            <ul>
                <li>This site is intended for US health care professionals only.</li>
                @if (Auth::check())
                <li><a href="{{ route('logout') }}">Log Out</a></li>
                 <li><a href="{{ route('myaccount') }}">My Account</a></li>
                <li class="fxHdr">
                    @else
                <li><a href="{{ route('login') }}">Log In</a></li>
                <li><a href="{{ route('register') }}">Register</a></li>

                @endif
                <li class=""><a class="srchBtn"><img src="{{ asset('img/search.png') }}" alt="img"></a></li>
            </ul>
        </div>
    </div>
    <div class="main-header">
        <div class="container">
            <div class="main-header__in">
                <div class="logo">
                    <a href="{{ route('index') }}"><img src="{{ asset('img/logo.jpg') }}" alt="img"></a>
                </div>
                <ul class="navigation">
                    {{-- {{Auth::user()->need_assessment}} --}}
                     @if (Auth::check())
                        @if (Auth::check() && Auth::user()->intercept_only == 1 )
                       
                            <li class="dropdown">
                                <a class="header_intercept" href="{{ route('interceptonly') }}">Intercept Only</a>
                                <div class="intercept_dropdown-content">
                                    <a href="{{route('viewintercept', ['intercept_id' => '2' ] )}}">Internal Intercept Training Only</a>
                                    <a href="{{route('viewintercept', ['intercept_id' => '3' ] )}}">Disease Overview</a>
                                    <a href="{{route('viewintercept', ['intercept_id' => '4' ] )}}">Customer Engagement Presentation</a>
                                    <a href="{{route('viewintercept', ['intercept_id' => '5' ] )}}">Overview Flashcard</a>
                                </div>
                            </li>
                         @endif
                     @endif
                    <li class="">
                        <a href="{{ route('resources') }}">Resources</a>
                    </li>
                    <li class="">
                        <a href="{{ route('ehr-home') }}">EHR Plugin</a>
                    </li>
                    <!--  <li class=""><a href="#">Get Resources</a></li>
                    <li class=""><a href="#">Sample Plugin</a></li> -->
                    @if (Auth::check())
                    <li class="fxHdr fxHdr1"><a href="{{ route('myaccount') }}">My Account</a></li>
                    <li class="fxHdr"><a href="{{ route('logout') }}">Log Out</a></li>
                    <li class="fxHdr"> <a class="srchBtn"><img src="{{ asset('img/search.png') }}" alt="img"></a></li>
                    @else
                    <li class="fxHdr fxHdr1"><a href="{{ route('login') }}">Log In</a></li>
                    <li class="fxHdr fxHdr1"><a href="{{ route('register') }}">Register</a></li>
                    @endif


                </ul>
                <div class="mob-menu-box">
                    <div class="mob-srchBtn">
                        <a class="srchBtn"><img src="{{ asset('img/mobSearchIcon.png') }}" alt="img"></a>
                    </div>
                    <div class="menu-icon"><span></span></div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="mob-menu-up">
    <div class="mob-menu">
        <div class="mob-close-box"><span class="mob-close"><img src="{{ asset('img/close-white.png') }}"
                    alt="img" /></span>
        </div>
        <ul class="navigation">
            @if (Auth::check())
            <li><a href="{{ route('myaccount') }}">My Account</a></li>
            <li><a href="{{ route('logout') }}">Log Out</a></li>
                @if (Auth::check() && Auth::user()->intercept_only == 1 )
                    <li>
                        <a class="header_intercept" href="{{ route('interceptonly') }}">Intercept Only</a>
                        
                    </li>
                @endif
            <li><a href="{{ route('healthtools') }}">Resources</a></li>
            <li><a href="{{ route('ehr-home') }}">EHR Plugin</a></li>
            @else
            <li><a href="{{ route('login') }}">Log In</a></li>
            <li><a href="{{ route('register') }}">Register</a></li>
            @endif

            <!-- <li><a href="{{ route('get.service', ['slug' => 'screen-patients']) }}">Screen Patients</a></li>
                    <li><a href="{{ route('get.service', ['slug' => 'define-an-episode']) }}">Define an Episode</a></li>
                    <li><a href="{{ route('get.service', ['slug' => 'coordinate-care']) }}">Coordinate Care</a></li>
                    <li><a href="{{ route('healthtools') }}">Get Resources</a></li> -->
            <li><a href="https://www.interceptpharma.com/privacy-policy/" target="_blank">Privacy Policy</a></li>
            <li>
                <p>This site is intended for US health care
                    professionals only.</p>
            </li>
        </ul>
    </div>
</div>
<div class="searchContainer">
    <div class="srchBox">
        <div class="container">
            <div class="srch">
                <input type="text" placeholder="Search" id="search">
                <span class="srchClose"></span>
            </div>
            <div class="srch__container">
                <div class="srch__cont">
                    <div class="srch__container__in">
                        <h2>Resources</h2>
                        <ul id="healthtools_ajax_results">
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>

                {{-- <div class="srch-all">

                        <a href="#">Total tools : <span id="total_records"></span></a>

                    </div> --}}
            </div>
        </div>
    </div>
</div>



<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">

    @csrf

</form>

<!-- Header Ends