@php
$footer = App\Footer::first();
@endphp
<div class="footer-note">
    <div class="container">
        <div class="row">
            <div class="contents-space special_edition_content">
                <p>{!! $footer->subtext !!}</p>
                <p>Use of website is governed by the <a class="reflink" style="text-decoration:underline !important" target="_blank" href="https://www.interceptpharma.com/terms-of-use/">Terms of Use</a> and <a target="_blank" style="text-decoration:underline !important" class="reflink" href=" https://www.interceptpharma.com/privacy-policy/">Privacy Policy.</a></p>
                @if(Route::currentRouteName() == 'customer-assessment-tool' || Route::currentRouteName() ==
                'needassesmentresult')
                <p class="footer-extra">This information is for internal use only and is not to be left with, detailed
                    from, or shown to anyone outside of
                    Novartis Pharmaceuticals Corporation.</p>
                @endif
            </div>
        </div>
    </div>
</div>

<footer>
    <div class="container">
        <div class="row align-items">
            <div class="foo-logo">
                <section class="foo-condition">
                    <img src="{{ asset('img/foo-logo.png') }}" alt="img" />
                    <p>The INTERCEPT logo is a  registered trademark of Intercept Pharmaceuticals, Inc. 
                    <p>10 Hudson Yard, New York, NY 10001 T:844-782-4278 F:646-747-1001</p>
                    <p>Copyright © 2021 Intercept Pharmaceuticals, Inc. All rights reserved.</p>
                </section>
            </div>
            <div class="copy">
                @if(Route::currentRouteName() == 'customer-assessment-tool' || Route::currentRouteName() ==
                'needassesmentresult')
                <p style="text-align:center;"> 6/21 US-NP-PB-1281</p>
                @else
                <p style="text-align:center;"> {{ $footer->code_item }}</p>
                @endif
            </div> 
        </div>
    </div>
    <div class="footer-slip"></div>
</footer>