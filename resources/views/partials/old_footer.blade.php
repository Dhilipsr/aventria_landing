<footer>
    @php
        $footer = App\Footer::first();
    @endphp
        <div class="container">
            <div class="salix-logo"><img src="{{asset('img/salix-logo.png')}}" alt="img" /></div>
            <p>{{ $footer->address}}</p>
            <div class="copy">
                <p>{{ $footer->copyright }}</p>
                <p>{{ $footer->code_item }}</p>
            </div>


        </div>
        <div class="policys">
            <ul class="subfoo-link">
                {{-- <li><a href="">References</a></li> --}}
                <li><a href="https://www.bauschhealth.com/privacy" target="_blank">Privacy Policy</a></li>
            </ul>
            <ul class="foo-link-2">

                <li>California residents: <a
                        href="http://go.aventriahealth.com/SalixLHNCAoptout_SalixACECAOptoutLandingPage.html"
                        target="_blank">
                        Do not sell my personal information</a></li>
            </ul>
        </div>
        <div class="footer-slip"></div>
    </footer>


    {{-- <footer>
        <div class="container">
            <div class="row align-items">
                <div class="foo-logo">


                    <section class="foo-condition">
                        <img src="img/foo-logo.png" alt="img" />
                        <p>Use of website is governed by the <a href="">Terms of Use</a> and <a href="">Privacy Policy.</a> </p>
                        <p>Copyright © 2020 <a href="">Novartis Pharmaceuticals Corporation.</a> All rights reserved. </p>
                    </section>

                </div>
                <div class="copy">
                    <p> 10/20 T-NSF-1395194</p>
                </div>

            </div>
        </div>

        <div class="footer-slip"></div>
    </footer> --}}