@extends('layouts.layout')

@section('title')
Need Assesment Result

@endsection

@section('content')

<!-- Start Here -->

<div class="banner-container">

  <div class="banner-container__top">

    <div class="container">

      <div class="row">

        <div class="col-md-12">

          <h2>Needs Assessment Results</h2>
          <h3>For Internal Use Only</h3>
        </div>

      </div>

    </div>

  </div>

</div>

<div class="section">

  <div class="container">

    <div class="row">

      <div class="col-lg-12 col-12">



        <h3 class="align-left"> These are the results that were captured during the Needs Assessment.</h3>
        <div>
          <iframe width="100%" height="780"
            src="https://datastudio.google.com/embed/reporting/05618bbc-94cd-4145-ae60-131f2ebed84f/page/FWpFC"
            frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

      </div>
      <div class="col-md-12 form-box">

      </div>

    </div>

  </div>

</div>


@endsection



@section('extra-js')
@endsection