@extends('layouts.layout')

@section('title')

{{ $healthTool->title }}

@endsection

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="banner-container">
    <div class="banner-container__top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Customize Your Tool</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class=" customize-pg">
    <div class="container">
        <div class="row   ">
            <div class="w50">
                <div class="customize-img-sec gray_box"><img src="{{asset('storage/'.$healthTool->customize_image)}}"></div>
            </div>
            <div class="w50">
                <div class="customize-sec green_box">
                    <form class="form">
                        <div class="main-area">
                            <section class="side-space">
                                <div class="check-box">
                                    <input type="checkbox" id="chk1" name="update">
                                    <label for="chk1">I do not want to customize my tool<br>
                                        (Go directly to <strong>Preview tool</strong> button below)<span id="loading" style="display:none; font-size:14px; color:red; font-weight:bold"> Please Wait... </span></label>
                                </div>
                            </section>
                            <hr>
                            <section class="side-space">
                                <div class="title"><img src="{{asset('img/settings.png')}}"> Customize Your Health Tool</div>

                                <div class="upload-sec">
                                    <label>Max upload size is 2 mb</label>
                                    <div class="file-upload-wrapper" data-text="Upload your organization’s logo">
                                        <input name="file-upload-field" type="file" class="file-upload-field"
                                            value="" id="file">
                                    </div>
                                </div>

                                <div class="check-box mr-top-00">
                                        <input type="checkbox" id="orgImage" name="orgImage">
                                      <label for="orgImage">Go to my profile settings to grab my organization's pre-loaded logo.</label>
                                      <!--<label for="orgImage">Please upload a logo in your profile settings to use this feature.</label>-->
                                    </div>
                                <input type="hidden" id="avatar_organization" value="{{Auth::user()->avatar}}">
                                <input type="hidden" id="default" value="users/default.png">
                                <div class=" form-box mr-top-01">
                                    <button style="width:50%;" id="create_link">Create Link</button>
                                </div>
                                <div class="wait-pop d-none" id="wait-pop">
                                    <h4>PLEASE WAIT...</h4>
                                    <p>Link is generating. This takes about 15 seconds.</p>
                                </div>


                                <div class="riview-sec">
                                    <label>Click on the URL to review your customized health tool.</label>
                                    <div class="file-copy-wrapper">
                                        <input name="" type="text" class="" value="" id="myInput">
                                    </div>
                                    <button class="copy-url">Copy URL</button>
                                </div>

                                <div class="form-box mr-top-01 preview">
                                    <a href="#" style="width:100%;" class="button" id="previewTool" target="_blank">Preview tool</a>
                                    <label>(Your preview will open in another window)</label>

                                    <button class="favrite-btn"><i class="icon-img"><img style="width:73%;" src="{{asset('img/heart.png')}}"></i>Add tool to favorites
                                        on your profile page
                                    </button>
                                </div>
                                <a href="{{$previousUrl}}" style="text-decoration: underline !important;color:#176771;" class="back">Back to previous page</a>
                            </section>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('extra-js')
{{-- <script type="text/javascript">
    let link = '{{route('healthToolsDetail' , ['link' => $link,'client_name' => $client_name, 'healthtool_id' => $healthTool->id])}}';
  var intermediateLink;
  var image = '{{$healthTool->id}}';
  var lastId;
  var default_image = 'users/default.png';
  var avatar = $('#avatar_organization').val()
  // ajax image upload
  $(document).on('change', '#file', function(){
   var name = document.getElementById("file").files[0].name;
   var form_data = new FormData();
   var ext = name.split('.').pop().toLowerCase();
   if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1)
   {
    alert("Invalid Image File");
   }
   var oFReader = new FileReader();
   oFReader.readAsDataURL(document.getElementById("file").files[0]);
   var f = document.getElementById("file").files[0];
   var fsize = f.size||f.fileSize;
   if(fsize > 2000000)
   {
    alert("Image File Size is very big");
   }
   else
   {
    form_data.append("file", document.getElementById('file').files[0]);
    form_data.append("healthtool_id",'{{$healthTool->id}}');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    console.log(form_data);
    $.ajax({
     url:"{{route('logo.upload')}}",
     method:"POST",
     // data: {'form_data' : form_data,'healthtool_slug' : 'healthtool_slug'},
     data: form_data,
     contentType: false,
     cache: false,
     processData: false,
     success:function(data)
     {
      console.log(data);
      if (data.status == 200) {
        // image changed
         $('#myInput').val('');
        intermediateLink = data.url;
        link = data.url;
        image = data.image;
        lastId = data.lastId;
      }
     }
    });
   }
  });
  // ajax image upload ends
    $(document).on('click', '#create_link', function(e)
    {
        e.preventDefault();

        $('#myInput').val(link);
        $("#previewTool").attr("href", link);
    });

    $(document).on('click', '#orgImage', function(e){
        e.preventDefault();
        $('#file').parent(".file-upload-wrapper").attr("data-text", 'Upload your organization’s logo');
        $('#chk1').prop('checked',false);
        $('#myInput').val('');
        $('#create_link').text('Creating link...');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url:"{{route('logo.upload.two')}}",
            method:"POST",
            data: {'orgImage' : 1, 'healthtool_id': "{{$healthTool->id }}" },
            success:function(data)
            {
                // console.log(data);
                if (data.status == 200) {
                    $('#create_link').text('Create link');
                    intermediateLink = data.url;
                    link = data.url;
                    $('#myInput').val(intermediateLink);
                    $("#previewTool").attr("href", link);
                }
            }
        });
        // });

    });



  // copy url starts
    $('.copy-url').click(function (event) {
        copyT();
        $(this).html("<i><img src='{{asset('img/tickb.svg')}}'></i> Copied");
        event.preventDefault();
    });


  function copyT() {
      /* Get the text field */
      var copyText = document.getElementById("myInput");

      /* Select the text field */
      copyText.select();
      copyText.setSelectionRange(0, 99999); /*For mobile devices*/

      /* Copy the text inside the text field */
      document.execCommand("copy");

      /* Alert the copied text */
      alert("Copied the text: " + copyText.value);
  }
  // copy url ends

  // fav button starts
    $('.favrite-btn').click(function (event) {
        event.preventDefault();
        if(link)
        {
            let user_id = '{{ Auth::user()->id }}';
            let healthtool_id = '{{ last(request()->segments()) }}';


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url:"{{route('postToFav')}}",
                method:"POST",
                data: {'user_id' : user_id , 'healthtool_id' : healthtool_id, 'link' : link },
                success:function(data)
                {
                    console.log(data);
                    if (data.status == 200)
                    {
                        // image changed
                        $('.favrite-btn').html("<i class='icon-img'><img src='{{asset('img/tickb.svg')}}'></i> Added to your profile page");
                    }
                }
            });

        }
        else
        {
            alert('Please select an option first to create a link');
        }


    });
  // fav button ends
</script> --}}
<script type="text/javascript">
    let link ;
    console.log(link)
    var intermediateLink;
    var organization = "{{ Auth::user()->organization }}";

    const toBase64 = file =>
                new Promise((resolve, reject) => {
                    const reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = () => resolve(reader.result);
                    reader.onerror = error => reject(error);
                });

    $(document).on('click', '#chk1', function(e)
    {
        $('#create_link').text('Creating link...');
        if ($(this).is(":checked")) {
            $('#file').parent(".file-upload-wrapper").attr("data-text",'Upload your organization’s logo');
            $('#orgImage').prop('checked',false);
            $('#myInput').val('');
            $('#wait-pop').removeClass('d-none');
            let page_id = "{{ $healthTool->id }}" ;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type : 'POST',
                url : '{{route("noLogoUpload")}}',
                data : {
                    page_id: page_id,
                    'organization': organization,
                },
                success: function(result){
                    console.log(result);
                    link = result;
                    $('#myInput').val(link);
                    $("#previewTool").attr("href", link);
                    intermediateLink = link;
                    $("#create_link").text('Create link');
                    $('#wait-pop').addClass('d-none');
                }
            });

        }else{
                $('#myInput').val('');
                $("#previewTool").attr("href", '#');
                $("#loading").hide();

        }
        // console.log(link);
    });

    $(document).on('change', '#file', function()
    {
        if( $('#chk1').is(':checked') == true )
        {
            $('#chk1').prop('checked',false);
        }
        if( $('#orgImage').is(':checked') == true )
        {
            $('#orgImage').prop('checked',false);
        }
        $('#create_link').text('Creating link...');


        var name = document.getElementById("file").files[0].name;
        let file_exists = $('#file').val();
        var form_data = new FormData();
        var ext = name.split('.').pop().toLowerCase();

        if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1)
        {
            alert("Invalid Image File");
            $('#create_link').text('Create link');
            return false;
        }
        else if(file_exists.length == 0)
        {
            $('#create_link').text('Create link');
            return false;
        }

        var f = document.getElementById("file").files[0];
        var fsize = f.size||f.fileSize;
        if(fsize > 2000000)
        {
            alert("Image File Size is very big");
            $('#create_link').text('Create link');
            return false;
        }
        else
        {
            $(this).parent(".file-upload-wrapper").attr("data-text", $(this).val().replace(/.*(\/|\\)/, ''));
            $('#myInput').val('');
            function Main() {
                fileUpload = document.getElementById('file').files[0];
                const file = fileUpload;
                return toBase64(file);
            }
            $('#wait-pop').removeClass('d-none');
            Main().then(function (response) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                console.log(response);

                $.ajax({
                    url:"{{route('logo.upload')}}",
                    method:"POST",
                    data: {'file' : response,'healthtool_id' : '{{$healthTool->id}}','organization': organization},
                    // data: form_data,
                    success:function(data)
                    {
                        $('#create_link').text('Create link');
                        console.log(data);
                        if (data.status == 200) {
                            // image changed
                            $('#myInput').val(data.url);
                            intermediateLink = data.url;
                            link = data.url;
                            $("#previewTool").attr("href", link);
                            $('#wait-pop').addClass('d-none');
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            });
        }
    });
  // ajax image upload ends
    $(document).on('click', '#create_link', function(e)
    {
        e.preventDefault();

        $('#myInput').val(link);
        $("#previewTool").attr("href", link);
    });

    $(document).on('click', '#orgImage', function(e){
        @if(Auth::user()->avatar == 'users/default.png')
            alert("Please upload a logo in your profile settings to use this feature.");
            $('#orgImage').prop('checked',false);
        @else
        e.preventDefault();
        $('#wait-pop').removeClass('d-none');
        $('#file').parent(".file-upload-wrapper").attr("data-text", 'Upload your organization’s logo');
        $('#chk1').prop('checked',false);
        $('#myInput').val('');
        $('#create_link').text('Creating link...');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url:"{{route('logo.upload.two')}}",
            method:"POST",
            data: {'orgImage' : 1, 'healthtool_id': "{{$healthTool->id }}",'organization': organization },
            success:function(data)
            {
                // console.log(data);
                if (data.status == 200) {
                    $('#create_link').text('Create link');
                    intermediateLink = data.url;
                    link = data.url;
                    $('#myInput').val(intermediateLink);
                    $("#previewTool").attr("href", link);
                    $('#wait-pop').addClass('d-none');
                }
            },
            error:function(jqXHR, exception)
            {
                console.log("HIi");
                $('#create_link').text('Create link');
                alert("Go to my profile settings to grab my organization's pre-loaded logo.");
            }
        });
        // });
         @endif
    });



  // copy url starts
    $('.copy-url').click(function (event) {
        copyT();
        $(this).html("<i><img src='{{asset('img/right-blue.png')}}'></i> Copied");
                $('.copy-url').css({"background":"#fff","color":"#000"});

        event.preventDefault();
    });


  function copyT() {
      /* Get the text field */
      var copyText = document.getElementById("myInput");

      /* Select the text field */
      copyText.select();
      copyText.setSelectionRange(0, 99999); /*For mobile devices*/

      /* Copy the text inside the text field */
      document.execCommand("copy");

      /* Alert the copied text */
      alert("Copied the text: " + copyText.value);
  }
  // copy url ends

  // fav button starts
    $('.favrite-btn').click(function (event) {
        event.preventDefault();
        console.log(link)
        if(link)
        {
            let user_id = '{{ Auth::user()->id }}';
            let healthtool_id = '{{ last(request()->segments()) }}';
            console.log(healthtool_id);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url:"{{route('postToFav')}}",
                method:"POST",
                data: {'user_id' : user_id , 'healthtool_id' : healthtool_id, 'link' : link,'new_link' : 1 },
                success:function(data)
                {
                    console.log(data);
                    if (data.status == 200)
                    {
                        // image changed
                        $('.favrite-btn').html("<i class='icon-img'><img src='{{asset('img/right-blue.png')}}'></i> Added to your profile page");
                    }
                }
            });

        }
        else
        {
            alert('Please select an option first to create a link');
        }


    });
  // fav button ends
</script>
@endsection