@extends('layouts.layout')

@section('title')
Log In
@endsection

@section('content')
{{-- Start Here  --}}
{{-- <div class="banner-container">
    <div class="banner-container__top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Log In</h2>
                </div>
            </div>
        </div>
    </div>
</div> --}}
<div class="banner-container">
    <div class="banner-container__top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Log In</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="login-box section">
    <div class="default-box">
        <h2>Log in to LiverLife</h2>
        <p>Welcome back! Enter your email address and password below<br> for access to your account.</p>
    </div>
    <div class="container pad">
        <div class="row">
            <div class="col-md-12">
                <p class="ind-txt float-right"><span>*</span> Required Field</p>
            </div>
        </div>
        <form method="POST" action="{{route('login')}}">
            <div class="row">
                @csrf
                <div class="col-md-6 form-box @error('email') error-box @enderror">
                    <label class="req">Email Address</label>
                    <input type="email" class="form-field" name="email" id="email" value="{{ old('email') }}">
                   <div class="error error_email"><img src="{{asset('img/error-arrow.png')}}" alt="error" />
                        @error('email')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
                <div class="col-md-6 form-box @error('password') error-box @enderror">
                    <label class="req">Password</label>
                    <input type="password" class="form-field" name="password" id="password" required="required">

                    <div class="error error_password"><img src="{{asset('img/error-arrow.png')}}" alt="error" />
                        @error('password')
                        {{ $message }}
                        @enderror
                    </div>

                </div>
                <div class="col-md-12">
                    <div class="check-box">
                        <input type="checkbox" id="chk1" name="remember">
                        <label for="chk1">Keep me signed in</label>
                    </div>
                </div>
                <div class="col-md-12 form-box">
                    <button id="login" class="line">Sign In</button>
                </div>
                <div class="col-md-12 forgot">
                    <a data-toggle="modal" data-target="#myModal" class="">Forgot your password?</a>
                </div>
            </div>
        </form>
    </div>
    <div class="default-box login-register">
        <div class="form-box">
            <h3>Don't have an account?</h3>
            <p>Haven’t joined yet? Sign up now.</p>
        </div>
        <div class="form-box">
            <a href="{{route('register')}}" class="button line">Register</a>
        </div>
    </div>
</div>
<!-- Modal Starts here-->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-box">
        <div class="model-close" data-dismiss="modal"><img src="{{asset('img/close-white.png')}}" alt="img" /></div>
        <div class="default-box new_pass">
            <h2>Password Recovery</h2>
            <p>Please enter your email address. <br> You will receive a link via email to reset your password. </p>
        </div>

        <form method="POST" class='new_pass' action="{{ route('password.email') }}">
            <div class="row justify-content-center mt-3">
                <div class="col-md-6 form-box">
                    <p class="ind-txt float-right"><span>*</span> Required Field</p>
                    <!-- <label class="req">Enter Email Address</label> -->
                    @csrf
                    <input type="email" id="resetemail" class="form-field" name="email" required
                        placeholder="Your Email Address *">
                    <input type="hidden" id="csrf_token1" name="_token" value="{{ csrf_token() }}" />
                    <div class="error forgot_email"><img src="{{asset('img/error-arrow-wht.png')}}" alt="error" /></div>
                    <div class="form-box mt-4">
                        <button id="reset" class="button">Request New Password</button>
                    </div>
                </div>
            </div>
        </form>
        {{-- <div class="default-box" id='flash_mesg'>
            <h2> You will receive a  password reset LINK via email.</h2>
        <div> --}}
    </div>
</div>
<!-- Modal Ends Here -->
@if(Session::has('under_review'))
<div id="underReviewModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-box">
        <div class="model-close" data-dismiss="modal"><img src="{{asset('img/close-white.png')}}" alt="img" /></div>
        <div class="default-box new_pass">
            <h2>Not Approved Yet</h2>
            <p>We're sorry. Your account has not been approved yet.<br>
                Confirmation of your registration may take up to 48 hours.<br>
                You will be notified when your account has been approved via the email address provided
                during registration. <br>
            </p>
            <p><strong>If it has been more than 48 hours and you have not received a confirmation email,
                    please contact us at support@liverlifepro.com.</strong></p>
        </div>


        {{-- <div class="default-box" id='flash_mesg'>
            <h2> You will receive a  password reset LINK via email.</h2>
        <div> --}}
    </div>
</div>
@endif

@endsection

@section('extra-js')
<script type="text/javascript">
    $(document).ready(function() {
        $('#login').click(function(e) {
            var email = $('#email').val();
            var password = $('#password').val();
            // var email = $('#email').val();
            if (email == "") {
                $('#email').parents('.form-box').addClass('error-box');
                $(".error_email").html('<img src="{{asset("img/error-arrow.png")}}" alt="error" />');
                $(".error_email").append(" This field is required");
                e.preventDefault();
            }

            if (password == "") {
                $('#password').parents('.form-box').addClass('error-box');
                $(".error_password").html('<img src="{{asset("img/error-arrow.png")}}" alt="error" />');
                 $(".error_password").append(" This field is required");
                e.preventDefault();
            }
        });
    });


    $(document).ready(function() {
        $('#flash_mesg').hide();
        $('.new_pass').show();
        $('#reset').click(function(e) {

            // event.preventDefault();
            var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            var emailinput = $('#resetemail').val();

            if(email_reg.test(emailinput) == false || emailinput === "") {

                console.log(1);

            }else {
                console.log(2);

                // $('#flash_mesg').show();
                // $('.new_pass').hide();
            }
        });
    });
    @if(Session::has('under_review'))
    $('#underReviewModal').modal('show');
    @endif
</script>
@endsection