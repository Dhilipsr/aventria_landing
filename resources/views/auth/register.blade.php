@extends('layouts.layout')

@section('title')
Register
@endsection

@section('content')
<!-- Start Here -->
<div class="banner-container">
    <div class="banner-container__top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Registration </h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="reg-form section">
    <div class="container pad">
        <div class="row reset_pass_cont">
            <h3 style="margin:0;">Fill in the information below to register on this website.</h3>
                <h3 style="margin:0;">This site is intended for US health care professionals only.</h3>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-12">
                <p class="ind-txt float-right"><span>*</span> Required Field</p>
            </div>
        </div>
        <form method="POST" action="{{ route('register') }}">
            <div class="row">
                @csrf
                <div class="col-md-6 form-box @error('password') error-box @enderror">
                    <label class="req">Password</label>
                    <input type="password" class="form-field" name="password">
                    <div class="error"><img src="{{ asset('img/error-arrow.png') }}" alt="error" />
                        @error('password')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
                <div class="col-md-6 form-box @error('password_confirmation') error-box @enderror">
                    <label class="req">Confirm Password</label>
                    <input type="password" class="form-field" name="password_confirmation">
                    {{-- <div class="error"><img src="{{ asset('img/error-arrow.png') }}" alt="error" /> This field is
                    required
                </div> --}}
                <div class="error"><img src="{{ asset('img/error-arrow.png') }}" alt="error" />
                    @error('password_confirmation')
                    {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="col-md-6 form-box @error('email') error-box @enderror">
                <label class="req">Email Address</label>
                <input type="email" class="form-field" name="email" value="{{ old('email') }}">
                <div class="error"><img src="{{ asset('img/error-arrow.png') }}" alt="error" />
                    @error('email')
                    {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="clear"></div>
            <div class="col-md-6 form-box @error('name') error-box @enderror">
                <label class="req">First Name</label>
                <input type="text" class="form-field txtOnly_name" name="name" value="{{ old('name') }}">
                <div class="error error_name"><img src="{{ asset('img/error-arrow.png') }}" alt="error" />
                    @error('name')
                    {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="col-md-6 form-box @error('lname') error-box @enderror">
                <label class="req">Last Name</label>
                <input type="text" class="form-field txtOnly_lname" name="lname" value="{{ old('lname') }}">
                <div class="error error_lname"><img src="{{ asset('img/error-arrow.png') }}" alt="error" />
                    @error('lname')
                    {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="col-md-6 form-box @error('organization') error-box @enderror">
                <label class="req">Organization</label>
                <input type="text" class="form-field" name="organization" placeholder=""
                    value="{{ old('organization') }}">
                <div class="error"><img src="{{ asset('img/error-arrow.png') }}" alt="error" />
                    @error('organization')
                    {{ $message }}
                    @enderror
                </div>
            </div>
            @php
            $organizationtypes = App\Organizationtype::where(['status' => 1])->get();
            $sources = App\Source::where(['status' => 1])->get();
            @endphp
            <div class="col-md-6 form-box @error('organization_id') error-box @enderror">
                <label class="req">Organization Type</label>
                <select class="select2 organization_type form-field select2-clr" name="organization_id">
                    <option disabled selected>Select an Option</option>
                    @foreach ($organizationtypes as $organizationtype)
                    <option value="{{ $organizationtype->id }}">{{ $organizationtype->type }}</option>
                    @endforeach
                </select>
                <div class="error"><img src="{{ asset('img/error-arrow.png') }}" alt="error" />
                    @error('organization_id')
                    {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="col-md-6 form-box @error('job_title') error-box @enderror">
                <label class="req">Job Title</label>
                <input type="text" class="form-field" name="job_title">
                <div class="error"><img src="{{ asset('img/error-arrow.png') }}" alt="error"
                        value="{{ old('job_title') }}" />
                    @error('job_title')
                    {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="col-md-6 form-box @error('contact') error-box @enderror">
                <label>Phone Number</label>
                <input class="form-field" id="contact" name="contact">
                <div class="error error_contact"><img src="{{ asset('img/error-arrow.png') }}" alt="error"
                        value="{{ old('contact') }}" />
                    @error('contact')
                    {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="col-md-6 form-box @error('city') error-box @enderror">
                <label class="req">City</label>
                <input type="text" class="form-field" name="city">
                <div class="error"><img src="{{ asset('img/error-arrow.png') }}" alt="error"
                        value="{{ old('city') }}" />
                    @error('city')
                    {{ $message }}
                    @enderror
                </div>
            </div>
             @php
            $states = App\State::where(['status' => 1])->get();
            @endphp
            <div class="col-md-6 form-box @error('organization_id') error-box @enderror">
                <label class="req">State</label>
                <select class="select2 state form-field select2-clr" name="state">
                    <option disabled selected>Select an Option</option>
                    @foreach ($states as $state)
                    <option value="{{ $state->state_code }}">{{ $state->state_name }}</option>
                    @endforeach
                </select>
                <div class="error"><img src="{{ asset('img/error-arrow.png') }}" alt="error" />
                    @error('state')
                    {{ $message }}
                    @enderror
                </div>
            </div>
            <!--<div class="col-md-6 form-box @error('state') error-box @enderror">
                <label class="req">State</label>
                <input type="text" class="form-field" name="state">
                <div class="error"><img src="{{ asset('img/error-arrow.png') }}" alt="error"
                        value="{{ old('state') }}" />
                    @error('state')
                    {{ $message }}
                    @enderror
                </div>
            </div>-->
            
            <div class="col-md-6 form-box @error('source_id') error-box @enderror">
                <label>How did you hear about us?</label>
                <select class="select2 hear_about_us form-field select2-clr" name="source_id" onchange='checkAbout(this.value);'>
                    {{-- <option value="code">I recieved a code</option> --}}
                    <option disabled selected>Select an Option</option>
                    @foreach ($sources as $source)
                    <option value="{{ $source->id }}">{{ $source->sources }}</option>
                    @endforeach

                </select>
                <div class="error"><img src="{{ asset('img/error-arrow.png') }}" alt="error" />
                    @error('source_id')
                    {{ $message }}
                    @enderror
                </div>
            </div>

            @php
            $salix_manager = App\SalixManager::get();
            //$sources = App\Source::where(['status' => 1])->get();
            
         
            @endphp
            <div class="col-md-6 form-box @error('salix_manager_name') error-box @enderror">
                <label class="req">Name of Your Intercept Contact</label>
                <select class="select2 salix_manager form-field select2-clr" name="salix_manager_name">
                    <option></option>
                    @foreach ($salix_manager as $salix_managers)
                  <?php
                    
                  ?>
                    <option value="{{$salix_managers->id}}">{{ $salix_managers->salix_manager_name }}
                    </option>
                    @endforeach
                </select>
                <div class="error"><img src="{{ asset('img/error-arrow.png') }}" alt="error" />
                    @error('salix_manager_name')
                    {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="col-md-6 form-box @error('code') error-box @enderror" id="refcode" @if ($errors->
                first('code')) @endif style="display:none;">
                <label>Add Code Here</label>
                <input type="text" class="form-field" name="code">
                <div class="error"><img src="{{ asset('img/error-arrow.png') }}" alt="error" />
                    @error('code')
                    {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="col-md-12">
                <div class="check-box @error('update') error-box @enderror">
                    <input type="checkbox" id="chk1" name="update">
                    <label for="chk1">I agree to receive periodic updates and communications via
                        email from this
                        website.
                    </label>
                    <div class="error"><img src="{{ asset('img/error-arrow.png') }}" alt="error" />
                        @error('update')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
                <div class="check-box  @error('agreement_check') error-box @enderror">
                    <input type="checkbox" id="chk2" name="agreement_check">
                    <label for="chk2">I accept the
                        <a href="#" data-toggle="modal" data-target="#regModal" class="req">License Agreement.</a>
                    </label>
                    <div class="error"><img src="{{ asset('img/error-arrow.png') }}" alt="error" />
                        @error('agreement_check')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-12 form-box">
                <button class="line register_submit">Submit</button>
            </div>
    </div>
    </form>
</div>
</div>
{{-- <div class="contact-msg">
    <div class="default-box ">
        <h2>Thank you for applying to become a member of ACE!</h2>
        <p>You should expect an email within the next 48 hours with information about how to access the
            materials on this site.</p>
    </div>
</div> --}}
@include('partials.agreement')
{{-- Ends Here --}}
@endsection

@section('extra-js')
<script type="text/javascript">
    
    $(document).ready(function()
    {
        
        $('.organization_type').css('color','gray');
        $('.hear_about_us').css('color','gray');
        $('.select2 option').css('color','black');
        $('.organization_type').change(function()
        {
             $('.organization_type').css('color','black');
        });
        $('.hear_about_us').change(function()
        {
             $('.hear_about_us').css('color','black');
        });
    });
    
    function checkAbout(val) {
            var element = document.getElementById('refcode');
            if (val == 'code')
                element.style.display = 'block';
            else
                element.style.display = 'none';
        }
    
</script>
@endsection