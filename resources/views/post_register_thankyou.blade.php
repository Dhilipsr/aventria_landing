@section('title')
Register
@endsection
@extends('layouts.layout')
@section('content')
<!-- Start Here -->
<div class="banner-container">
    <div class="banner-container__top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Registration</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="contact-msg">
    <div class="default-box ">
        <h2>Thank you for registering with LiverLife.</h2>
        <p>You will soon receive an account activation email that allows
            you to log in to our website.</p>
        </p>
    </div>
</div>
{{-- <table>
    <tr>
        <td style="font:  12px/1.5 arial;">
            <p>Content contained in <a href="https://actonms.com/" target="_blank" style="color: #2B79AC;">ACTonMS.com</a>
                 is being provided Novartis Pharmaceuticals corporation for informational purposes
                only.
                Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.
            </p>
        </td>
    </tr>
    <tr>
        <td height="20px" width=" 200px">
            <img src="https://actonms.org/storage/NovartisLogo.jpg">
        </td>
        <td height="20px">
        </td>
    </tr>
    <tr>
        <td height="20px" style="font-size:12px; color:#000;font-family:arial;">
           <div style="disply:inline-block; margin-right:40px"> Copyright © 2021 Novartis Pharmaceuticals Corporation. All rights reserved. </div>
        </td>
        <td height="20px"  style="font-size:12px; color:#000;font-family:arial;">
            <span style="disply:inline-block; margin-right:5px">3/21</span>  <span style="disply:inline-block; margin-left:5px"> T-NSF-1400562</span>
        </td>
    </tr>
 </table> --}}
@include('partials.agreement')
{{-- Ends Here --}}
@endsection