<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="theme-color" content="#ff5069" />
    <title> </title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('sample_plugin/css/plugin.css') }}">
    <!-- google tag manager sample -->
    
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KMDGNBN');</script>
    
    <!-- google tag manager sample -->
</head>

<body>

    <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KMDGNBN" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Main Container Starts -->
    <div class="mainContainer sample-main">
        <h2 class="water-mark">Sample</h2>

        <!-- <input type="checkbox" />
        <div class="menu">
            <span></span>
            <span></span>
            <span></span>
        </div> -->

        <!-- hamburger  Starts -->
        <!-- <div class="f-row hamburger-menu">
            <nav role="navigation" class="navi">
                <div id="menuToggle">


                    <div id="menu">
                        <div class="strip"></div>
                        <ul>
                            <li class="head"><a href="#">Home</a></li>
                            <li><a href="#">Tool 1</a></li>
                            <li><a href="#">Tool 2</a></li>
                            <li><a href="#">Tool 3</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div> -->
        <!-- hamburger  Ends -->
        <div class="logo" style="padding-left: 54px; padding-top: 10px; width: 400px;"><!-- <img src="{{asset('img/liver.png')}}"> --> 
                        </div> 
        <section class="space-to sample-space-top">
            <!-- Header Starts -->
          <!--   <div class="f-row">
                <header>
                    <div class="plugin-head" style="padding-left: 50px;">Advanced Care Tools to Help Improve
                        Multiple Sclerosis (MS) Patient Engagement</div>
                </header>
            </div>

 -->





                   <div class="">
        <section class="containr">
            <div class="row">
                <div class="col-12">
                    <div class="" style="background: rgb(73,136,147) !important;background: linear-gradient(90deg, rgba(73,136,147,0) 70%, rgba(73,136,147,1) 97%) !important;">
                   <div class="bgC" style="width: 90%;border-top-right-radius: 128px;background: rgb(93,170,182);background: linear-gradient(90deg, rgba(93,170,182,1) 0%, rgba(23,103,113,1) 53%);">
                      <!--   <div class="logo"><img src="{{asset('img/logo.png')}}"> -->
                       <!--  </div> -->
                        <div class="nex">
                            <h2 class="ne header_plugin"><strong class="mobile_font" style="font-weight:800;">Help Improve Outcomes for Patients With Primary Biliary Cholangitis (PBC)</strong>
                            </h4> 
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>









            <!-- Header Ends -->
            <div class="innrContainer d-flex  justify-content-end">
                <nav class="notification" style="padding-right: 40px;">
                    <ul>
                        <li><img src="{{ asset('sample_plugin/img/select.png') }}"> Select tool(s)</li>
                        <li><img src="{{ asset('sample_plugin/img/search-1.png') }}"> Preview tool</li>
                        <li><img src="{{ asset('sample_plugin/img/arrow-1.png') }} "> Description</li>
                    </ul>
                </nav>

            </div>

            <div class="innrContainer ">
                <div class="content-box">








                         <div class="provider_parent_box">
<div class="provider_child_box">
    <div class="row provider_padding" style="padding-left: 70px;"><h2 class="plugin_provider"><strong>Provider</strong></h2>
    </div>
</div>
</div>
<style type="text/css">
    #box
    {

        width: 1000px;
        height: 55px;
       /* background-color: red;*/
    }
</style>














                 <!--    <div class="partition"></div> -->
                <!--  <div class="pro" style="background-color: #e74a21"; >
                    <style type="text/css">
                        .pro
                        {
                            height: 55px;
                        }
                    </style>
                    <h5 class="plugin-head-inner" style="padding-top: 8px; padding-left: 40px; color: white;"  >Provider</h5> -->
               <!--  </div> -->
                    <div class="acc pd-left" style="padding-bottom: 24px;" >
                        <div class="acc__card" style="padding: 30px; padding-bottom: 0px; color: #3D858D; ">
                            <div class="acc__title" >
                                <div class="form-group">
                                    <input type="checkbox" name="selector[]" class="checkbox" id="01"
                                        data-tool-url="https://cc-cms.online/cmsbuilder/page_preview/dist/9cf46abc06ff96bd11dc1e9011d69ba3"
                                        data-print-url="https://cc-cms.online/cmsbuilder/page_preview/dist/9cf46abc06ff96bd11dc1e9011d69ba3"
                                        data-title="PLENVU Dosing Reference Guide">
                                    <label for="01">
                                        Primary Biliary Cholangitis AASLD Guidance Snapshot</label>

                                </div>
                                <div class="action-btn">
                                    <a class="preview"
                                        {{-- href="{{ asset('sample_plugin/sample_plugin_pdf/ACTonMS_1A_Early_treatment_in_MS_033121.pdf') }}"
                                        --}}
                                        href="https://cc-cms.online/cmsbuilder/page_preview/dist/9cf46abc06ff96bd11dc1e9011d69ba3"
                                        target="_blank"><img src="{{ asset('sample_plugin/img/search-1.png') }}"></a>
                                    <a class="drop-down" href=""><img
                                            src="{{ asset('sample_plugin/img/arrow-1.png') }}"></a>
                                </div>
                            </div>
                            <div class="acc__panel  mr-left" style="font-weight: 600;">
                               Guidance statements from the AASLD 2018 PBC Guidance to help you optimize the care of your patients.
                            </div>
                        </div>
                        <div class="acc__card" style="padding: 30px; padding-top:  0px; padding-bottom: 0px; color: #3D858D;">
                            <div class="acc__title">
                                <div class="form-group">
                                    <input type="checkbox" name="selector[]" class="checkbox" id="02"
                                        data-tool-url="https://cc-cms.online/cmsbuilder/page_preview/dist/76a58f7043709c0b96f3712d93fc03c1"
                                        data-print-url="https://cc-cms.online/cmsbuilder/page_preview/dist/76a58f7043709c0b96f3712d93fc03c1"
                                        data-title="PLENVU Co-Pay Cards: Codes and Processing Instructions ">
                                    <label for="02">
                                        Level Check--PBC Predictive Biomarkers
                                    </label>
                                </div>
                                <div class="action-btn">
                                    <a class="preview"
                                        {{-- href="{{ asset('sample_plugin/sample_plugin_pdf/ACTonMS_1B_Overview_of_Clinical_Study_Measures_041221.pdf') }}"
                                        --}}
                                        href="https://cc-cms.online/cmsbuilder/page_preview/dist/76a58f7043709c0b96f3712d93fc03c1"
                                        target="_blank"><img src="{{ asset('sample_plugin/img/search-1.png') }}"></a>
                                    <a class="drop-down" href=""><img
                                            src="{{ asset('sample_plugin/img/arrow-1.png') }}"></a>
                                </div>
                            </div>

                            <div class="acc__panel  mr-left" style="font-weight: 600;">
                              Why it's important to monitor your patients' ALP and bilirubin levels, when to check them, and what the results mean.
                            </div>
                        </div>
                       <!--  <div class="acc__card" style="padding: 30px; padding-top: 0px; padding-bottom: 0px; color: #3D858D;">
                            <div class="acc__title">
                                <div class="form-group">
                                    <input type="checkbox" name="selector[]" class="checkbox" id="03"
                                        data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc"
                                        data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-print"
                                        data-title="PLENVU Co-Pay Cards: Codes and Processing Instructions ">
                                    <label for="03">Overview of Telehealth
                                    </label>
                                </div>
                                <div class="action-btn">
                                    <a class="preview"
                                        {{-- href="{{ asset('sample_plugin/sample_plugin_pdf/ACTonMS_1C_Overview_of_Telehealth_041221.pdf') }}"
                                        --}}
                                        href="https://cc-cms.online/cmsbuilder/page_preview/dist/058b10f501aa193e15741f527ab33ae1"
                                        target="_blank"><img src="{{ asset('sample_plugin/img/search-1.png') }}"></a>
                                    <a class="drop-down" href=""><img
                                            src="{{ asset('sample_plugin/img/arrow-1.png') }}"></a>
                                </div>
                            </div>

                            <div class="acc__panel  mr-left" style="font-weight: 600;">
                                Tips and resources for the implementation and optimization of digital health technology
                                in health care practice

                            </div>
                        </div>
                        <div class="acc__card" style="padding: 30px; padding-top: 0px; padding-bottom: 10px; color: #3D858D;">
                            <div class="acc__title">
                                <div class="form-group">
                                    <input type="checkbox" name="selector[]" class="checkbox" id="04"
                                        data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc"
                                        data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-print"
                                        data-title="PLENVU Co-Pay Cards: Codes and Processing Instructions ">
                                    <label for="04"><span>Understanding Barriers:
                                        </span><br>Understanding and Addressing Barriers to Patient Care in Multiple
                                        Sclerosis
                                    </label>
                                </div>
                                <div class="action-btn">
                                    <a class="preview"
                                        {{-- href="{{ asset('sample_plugin/sample_plugin_pdf/ACTonMS_2_Understanding_and_Addressing_Barriers_to_Patient_Care_in_MS_033121.pdf') }}"
                                        --}}
                                        href="https://cc-cms.online/cmsbuilder/page_preview/dist/65fcce6024c9fade1cef837374a85de8"
                                        target="_blank"><img src="{{ asset('sample_plugin/img/search-1.png') }}"></a>
                                    <a class="drop-down" href=""><img
                                            src="{{ asset('sample_plugin/img/arrow-1.png') }}"></a>
                                </div>
                            </div>

                            <div class="acc__panel  mr-left" style="font-weight: 600;">
                                A tool to help patients be active participants in treatment decision-making by helping
                                HCPs understand barriers to patient care and how they may be addressed

                            </div>
                        </div>
                    </div> -->
                </div>
        

 <!-- --------------------- -->






                    <div  class="sds" style="margin: 30px;" ></div>
                   


                         <div class="patient_parent_box">
<div class="patient_child_box">
    <div class="row provider_padding" style="padding-left: 70px;"><h2 class="plugin_patient"><strong>Patient</strong></h2>
    </div>
</div>
</div>
<style type="text/css">
    #box
    {

        width: 1000px;
        height: 55px;
       /* background-color: red;*/
    }
</style>












                   <!--  <div class="partition"></div> -->
                   <!-- <div class="nn" style="background-color: #e74a21; height: 55px; ">
                    <h5 class="plugin-head-inner" style="padding-top: 8px; padding-left:  40px; color: white;">Patient and Caregiver</h5>
                </div> -->
                    <div class="acc pd-left">
                        <div class="acc__card" style="padding: 30px; padding-bottom: 0px; color: #3D858D; ">
                            <div class="acc__title">
                                <div class="form-group">
                                    <input type="checkbox" name="selector[]" class="checkbox" id="05"
                                        data-tool-url="https://cc-cms.online/cmsbuilder/page_preview/dist/26260cc2460b33ddc95452d7d2281389"
                                        data-print-url="https://cc-cms.online/cmsbuilder/page_preview/dist/26260cc2460b33ddc95452d7d2281389"
                                        data-title="PLENVU Dosing Reference Guide">
                                    <label for="05">Face Your Fatigue (Tiredness)</label>

                                </div>
                                <div class="action-btn">
                                    <a class="preview"
                                        {{-- href="{{ asset('sample_plugin/sample_plugin_pdf/ACTonMS_3A_Understanding_Your_MS_Journey_032321.pdf') }}"
                                        --}}
                                        href="https://cc-cms.online/cmsbuilder/page_preview/dist/26260cc2460b33ddc95452d7d2281389"
                                        target="_blank"><img src="{{ asset('sample_plugin/img/search-1.png') }}"></a>
                                    <a class="drop-down" href=""><img
                                            src="{{ asset('sample_plugin/img/arrow-1.png') }}"></a>
                                </div>
                            </div>
                            <div class="acc__panel  mr-left" style="font-weight: 600;">
                               This tool provides information for your patients about approaches to relieve fatigue.
                            </div>
                        </div>
                        <div class="acc__card"  style="padding: 30px; padding-top: 0px; padding-bottom: 0px; color: #3D858D;">
                            <div class="acc__title">
                                <div class="form-group">
                                    <input type="checkbox" name="selector[]" class="checkbox" id="06"
                                        data-tool-url="https://cc-cms.online/cmsbuilder/page_preview/dist/0473f213973fc74b49498104ba9d005f"
                                        data-print-url="https://cc-cms.online/cmsbuilder/page_preview/dist/0473f213973fc74b49498104ba9d005f"
                                        data-title="PLENVU Dosing Reference Guide">
                                    <label for="06">Managing Pruritus (Itching)</label>


                                </div>
                                <div class="action-btn">
                                    <a class="preview"
                                        {{-- href="{{ asset('sample_plugin/sample_plugin_pdf/ACTonMS_3B_Members_of_Your_MS_Care_Team_032321.pdf') }}"
                                        --}}
                                        href="https://cc-cms.online/cmsbuilder/page_preview/dist/0473f213973fc74b49498104ba9d005f"
                                        target="_blank"><img src="{{ asset('sample_plugin/img/search-1.png') }}"></a>
                                    <a class="drop-down" href=""><img
                                            src="{{ asset('sample_plugin/img/arrow-1.png') }}"></a>
                                </div>
                            </div>
                            <div class="acc__panel  mr-left" style="font-weight: 600;">
                               This tool can assist your patients in managing itching.
                            </div>
                        </div>
                        <div class="acc__card"  style="padding: 30px; padding-top: 0px; padding-bottom: 0px; color: #3D858D; ">
                            <div class="acc__title">
                                <div class="form-group">
                                    <input type="checkbox" name="selector[]" class="checkbox" id="07"
                                        data-tool-url="https://cc-cms.online/cmsbuilder/page_preview/dist/8f4e1af0aad6d4c13bfb9167a95a273a"
                                        data-print-url="https://cc-cms.online/cmsbuilder/page_preview/dist/8f4e1af0aad6d4c13bfb9167a95a273a"
                                        data-title="PLENVU Dosing Reference Guide">
                                    <label for="07">PBC Discussion Guide</label>

                                </div>
                                <div class="action-btn">
                                    <a class="preview"
                                        {{-- href="{{ asset('sample_plugin/sample_plugin_pdf/ACTonMS_4A_A_Guide_to_Understanding_MS_and_Common_Phrases_041521.pdf') }}"
                                        --}}
                                        href="https://cc-cms.online/cmsbuilder/page_preview/dist/8f4e1af0aad6d4c13bfb9167a95a273a"
                                        target="_blank"><img src="{{ asset('sample_plugin/img/search-1.png') }}"></a>
                                    <a class="drop-down" href=""><img
                                            src="{{ asset('sample_plugin/img/arrow-1.png') }}"></a>
                                </div>
                            </div>
                            <div class="acc__panel  mr-left" style="font-weight: 600;">
                              Provide this tool to your patients so they can ask questions about their PBC journey.
                            </div>
                        </div>
                        <div class="acc__card"  style="padding: 30px; padding-top: 0px; padding-bottom: 0px; color: #3D858D;">
                            <div class="acc__title">
                                <div class="form-group">
                                    <input type="checkbox" name="selector[]" class="checkbox" id="08"
                                        data-tool-url="https://cc-cms.online/cmsbuilder/page_preview/dist/913e4ceaa3558704875e9a31c39b1a92"
                                        data-print-url="https://cc-cms.online/cmsbuilder/page_preview/dist/913e4ceaa3558704875e9a31c39b1a92"
                                        data-title="PLENVU Dosing Reference Guide">
                                    <label for="08">Tracking Your PBC</label>

                                </div>
                                <div class="action-btn">
                                    <a class="preview"
                                        {{-- href="{{ asset('sample_plugin/sample_plugin_pdf/ACTonMS_4B_What_to_Expect_in_a_Telemedicine_Visit_041221.pdf') }}"
                                        --}}
                                        href="https://cc-cms.online/cmsbuilder/page_preview/dist/913e4ceaa3558704875e9a31c39b1a92"
                                        target="_blank"><img src="{{ asset('sample_plugin/img/search-1.png') }}"></a>
                                    <a class="drop-down" href=""><img
                                            src="{{ asset('sample_plugin/img/arrow-1.png') }}"></a>
                                </div>
                            </div>
                            <div class="acc__panel  mr-left" style="font-weight: 600;">
                               This tool can help your patients monitor key markers of liver health to determine how treatment is working for them.
                            </div>
                        </div>
                        <div class="acc__card"  style=" padding-top: 0px; padding-bottom: px; color: #3D858D; ">
                            <div class="acc__titl">
                                <div class="form-grou">
                                   <!--  <input type="checkbox" name="selector[]" class="checkbox" id="09"
                                        data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unddfchc"
                                        data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-pridfdfnt"
                                        data-title="PLENVU Dosing Reference Guide"> -->
                                    <label for="09" style="color: #3D858D;"></label>

                                </div>
                                <div class="action-btn">
                                    <!-- <a class="preview"
                                        {{-- href="{{ asset('sample_plugin/sample_plugin_pdf/ACTonMS_4C_Talk_to_Your_Doctor_About_How_Medications_Can_Help_You_Manage_Your_MS_032321.pdf') }}"
                                        --}}
                                        href="https://cc-cms.online/cmsbuilder/page_preview/dist/f3f2fca4c6f04175b77e33bbf2f180fc"
                                        target="_blank"><img src="{{ asset('sample_plugin/img/search-1.png') }}"></a>
                                    <a class="drop-down" href=""><img
                                            src="{{ asset('sample_plugin/img/arrow-1.png') }}"></a> -->
                                </div>
                            </div>
                            <div class="acc__panel  mr-left" style="color: #3D858D; font-weight: 600;">
                               
                            </div>
                        </div>
                    </div>



   <a class="back-to-top meee"  >
<div style="color: black; font-size: 20px; font-weight: 300px; padding-bottom: 40px;  color: black;" >
         <img style="padding-bottom: px; " width="24px;" src="{{asset('img/top_arrow.png')}}"> Back to Top
     </div>
        </a>

     



<!-- 

        <style type="text/css">
            
             .meee{

               x
/*             font-size: 13px;*/

                        
                                    }
        </style> -->
                    <!-- --------------------- -->
                    <section class="content-accord-sec" style=" padding-top:  40px;">
                        <div class="button-groups d-flex  justify-content-between">
                            <a href="#" data-toggle="modal" data-target="#myModal" class="button email_btn stackup" >Email</a>
                            <a href="#" data-toggle="modal" data-target="#myModal2"
                                class="button print_btn stackup">View/Print</a>
                            <a href="#" data-toggle="modal" data-target="#myModal3" class="button copy_btn stackup">Copy
                                Link</a>
                        </div>
                    </section>
                </div>
            </div>


                <style>

</style>









            <!-- Footer Starts -->
            <div class="f-row">
                <footer>
                    <div class="innrContainer">
                        <div class="inner-wrap">
                            <div class="footer-dis">
                                <div class="foo-logo">
                                     <div class="logo"><img src="{{asset('img/logo.jpg')}}"> 
                                        <h5 class="inter_phar" style="margin-top:7%;">© 2021 Intercept Pharmaceuticals, Inc. US-NP-PB-1261 7/21</h5>
                            </div> 
                                </div>
                                <!-- <h5>© 2021 Intercept Pharmaceuticals, Inc. US-NP-PB-1281</h5> -->
                            </div>
                               <!--href="http://go.aventriahealth.com/ITPLiverLifeContactUsFormwGINA_NVSACTonMS-ContactFormwGINAWebPage.html"-->
                            <div class="inner-wrap-content">
                                <h5 class="contact_sample">Questions? <a
                              
                                     
                                      href=" http://go.aventriahealth.com/ITPLiverLifeContactUs.html" target="_blank">Contact Us</a> </h5>
                                 <!-- <h5 style="padding-right: 30px; padding-top: 0px;"><span>6/21</span> <span> US-NP-PB-1281</span></h5> -->
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- Footer Ends -->
        </section>
    </div>

    <!-- Modal Starts here-->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-box sample-modal">
            <div class="default-box">
                <div class="modal-header">
                    <h5 class="modal-title text-center">This button allows the user to email the selected resource link
                        to a patient.</h5>
                    <button type="button" class="close model-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Ends Here -->
    <!-- Modal Starts here-->
    <div id="myModal2" class="modal fade" role="dialog">
        <div class="modal-dialog modal-box sample-modal">
            <div class="default-box">
                <div class="modal-header">
                    <h5 class="modal-title text-center">This button allows the user to print the selected tool.</h5>
                    <button type="button" class="close model-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Ends Here -->
    <!-- Modal Starts here-->
    <div id="myModal3" class="modal fade" role="dialog">
        <div class="modal-dialog modal-box sample-modal">
            <div class="default-box">
                <div class="modal-header">
                    <h5 class="modal-title text-center">This button allows the user to copy the link for the selected
                        resource to email to a patient.</h5>
                    <button type="button" class="close model-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Ends Here -->

    <script type="text/javascript" src="{{ asset('sample_plugin/js/plugin.js') }}"></script> <!-- Plugin -->
    <script>




    var btn = $('.back-to-top');

    $(window).scroll(function() {
        if ($(window).scrollTop() > 300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });

    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, '300');
    });


    $('.pglink a').click(function (e) {
        e.preventDefault();
        var target = $($(this).attr('href'));
        if (target.length) {
            var scrollTo = target.offset().top;
            $('body, html').animate({ scrollTop: scrollTo + 'px' }, 800);
        }
    });











        var checkboxes = $(".mainContainer > input[type='checkbox']"),
            actions = $("html");
        checkboxes.click(function () {
            if (!checkboxes.is(":checked")) {
                actions.removeClass("hidden", );
                $('.hamburger-menu').removeClass('full');
                $('.menu').removeClass('cross');
            } else {
                actions.addClass("hidden", );
                $('.hamburger-menu').addClass('full');
                $('.menu').addClass('cross');

            }

        });


        $('.acc__title .drop-down ').click(function (j) {

            var dropDown = $(this).closest('.acc__card').find('.acc__panel');
            $(this).closest('.acc').find('.acc__panel').not(dropDown).slideUp();

            if ($(this).parents('.acc__title').hasClass('active')) {
                $(this).parents('.acc__title').removeClass('active');
            } else {
                $(this).closest('.acc').find('.acc__title.active').removeClass('active');
                $(this).parents('.acc__title').addClass('active');
            }

            dropDown.stop(false, true).slideToggle();
            j.preventDefault();
        });


        /* Copy Tool */
        $('#copy-tool').on('click', function (e) {


            var value = [];
            var value = $('input[type=checkbox]:checked').map(function (_, el) {
                return (' ' + $(el).data('tool-url'));
            }).get();

            if ($("input[type=checkbox]").is(":checked")) {
                console.log("Copied the text: " + value);
                var $temp = $("<input type='text'>");
                //alert($temp);
                $('body').append($temp);
                $temp.val(value).select();
                document.execCommand("copy");

                $('.copy-cbd').fadeIn();
                setTimeout(function () {
                    $('.copy-cbd').fadeOut();
                }, 3000);

            } else if ($("input[type=checkbox]").is(":not(:checked)")) {
                $('.uncopy-cbd').fadeIn();
                setTimeout(function () {
                    $('.uncopy-cbd').fadeOut();
                }, 3000);
            }

            e.preventDefault();
            //alert(sel);      

        });
    </script>
</body>

</html>