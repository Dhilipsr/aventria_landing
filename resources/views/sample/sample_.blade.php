<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="theme-color" content="#ff5069" />
    <title> </title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('sample_plugin/css/plugin.css') }}">
</head>

<body>



    <!-- Main Container Starts -->
    <div class="mainContainer sample-main">
        <h2 class="water-mark">Sample</h2>

        <!-- <input type="checkbox" />
        <div class="menu">
            <span></span>
            <span></span>
            <span></span>
        </div> -->

        <!-- hamburger  Starts -->
        <!-- <div class="f-row hamburger-menu">
            <nav role="navigation" class="navi">
                <div id="menuToggle">


                    <div id="menu">
                        <div class="strip"></div>
                        <ul>
                            <li class="head"><a href="#">Home</a></li>
                            <li><a href="#">Tool 1</a></li>
                            <li><a href="#">Tool 2</a></li>
                            <li><a href="#">Tool 3</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div> -->
        <!-- hamburger  Ends -->
        <section class="space-top sample-space-top">
            <!-- Header Starts -->
            <div class="f-row">
                <header>
                    <div class="plugin-head">Advanced Care Tools to Help Improve
                        Multiple Sclerosis (MS) Patient Engagement</div>
                </header>
            </div>
            <!-- Header Ends -->
            <div class="innrContainer d-flex  justify-content-end">
                <nav class="notification">
                    <ul>
                        <li><img src="{{ asset('sample_plugin/img/select.png') }}"> Select tool(s)</li>
                        <li><img src="{{ asset('sample_plugin/img/search-1.png') }}"> Preview tool</li>
                        <li><img src="{{ asset('sample_plugin/img/arrow-1.png') }} "> Description</li>
                    </ul>
                </nav>

            </div>

            <div class="innrContainer ">
                <div class="content-box">
                    <div class="partition"></div>
                    <h5 class="plugin-head-inner">Provider</h5>
                    <div class="acc pd-left">
                        <div class="acc__card">
                            <div class="acc__title">
                                <div class="form-group">
                                    <input type="checkbox" name="selector[]" class="checkbox" id="01"
                                        data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unddfchc"
                                        data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-pridfdfnt"
                                        data-title="PLENVU Dosing Reference Guide">
                                    <label for="01"><span>Early Treatment:</span>
                                        <br>Early Treatment in Multiple Sclerosis</label>

                                </div>
                                <div class="action-btn">
                                    <a class="preview"
                                        {{-- href="{{ asset('sample_plugin/sample_plugin_pdf/ACTonMS_1A_Early_treatment_in_MS_033121.pdf') }}"
                                        --}}
                                        href="https://cc-cms.online/cmsbuilder/page_preview/dist/21085ca3034894b9ac7dab95d9e9319e"
                                        target="_blank"><img src="{{ asset('sample_plugin/img/search-1.png') }}"></a>
                                    <a class="drop-down" href=""><img
                                            src="{{ asset('sample_plugin/img/arrow-1.png') }}"></a>
                                </div>
                            </div>
                            <div class="acc__panel  mr-left">
                                Understand how early treatment interventions may slow down the progression of MS and
                                modify the long-term disease course
                                in MS.
                            </div>
                        </div>
                        <div class="acc__card">
                            <div class="acc__title">
                                <div class="form-group">
                                    <input type="checkbox" name="selector[]" class="checkbox" id="02"
                                        data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc"
                                        data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-print"
                                        data-title="PLENVU Co-Pay Cards: Codes and Processing Instructions ">
                                    <label for="02"><span>Study Measures:</span>
                                        <br>Overview of Clinical Study Measures in Multiple Sclerosis
                                    </label>
                                </div>
                                <div class="action-btn">
                                    <a class="preview"
                                        {{-- href="{{ asset('sample_plugin/sample_plugin_pdf/ACTonMS_1B_Overview_of_Clinical_Study_Measures_041221.pdf') }}"
                                        --}}
                                        href="https://cc-cms.online/cmsbuilder/page_preview/dist/ead4f93444cebccbe812e42c02a30f1e"
                                        target="_blank"><img src="{{ asset('sample_plugin/img/search-1.png') }}"></a>
                                    <a class="drop-down" href=""><img
                                            src="{{ asset('sample_plugin/img/arrow-1.png') }}"></a>
                                </div>
                            </div>

                            <div class="acc__panel  mr-left">
                                Review various markers used in clinical trials to help evaluate a potential treatment
                                for a patient with MS.
                            </div>
                        </div>
                        <div class="acc__card">
                            <div class="acc__title">
                                <div class="form-group">
                                    <input type="checkbox" name="selector[]" class="checkbox" id="03"
                                        data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc"
                                        data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-print"
                                        data-title="PLENVU Co-Pay Cards: Codes and Processing Instructions ">
                                    <label for="03">Overview of Telehealth
                                    </label>
                                </div>
                                <div class="action-btn">
                                    <a class="preview"
                                        {{-- href="{{ asset('sample_plugin/sample_plugin_pdf/ACTonMS_1C_Overview_of_Telehealth_041221.pdf') }}"
                                        --}}
                                        href="https://cc-cms.online/cmsbuilder/page_preview/dist/058b10f501aa193e15741f527ab33ae1"
                                        target="_blank"><img src="{{ asset('sample_plugin/img/search-1.png') }}"></a>
                                    <a class="drop-down" href=""><img
                                            src="{{ asset('sample_plugin/img/arrow-1.png') }}"></a>
                                </div>
                            </div>

                            <div class="acc__panel  mr-left">
                                Tips and resources for the implementation and optimization of digital health technology
                                in health care practice

                            </div>
                        </div>
                        <div class="acc__card">
                            <div class="acc__title">
                                <div class="form-group">
                                    <input type="checkbox" name="selector[]" class="checkbox" id="04"
                                        data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc"
                                        data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-print"
                                        data-title="PLENVU Co-Pay Cards: Codes and Processing Instructions ">
                                    <label for="04"><span>Understanding Barriers:
                                        </span><br>Understanding and Addressing Barriers to Patient Care in Multiple
                                        Sclerosis
                                    </label>
                                </div>
                                <div class="action-btn">
                                    <a class="preview"
                                        {{-- href="{{ asset('sample_plugin/sample_plugin_pdf/ACTonMS_2_Understanding_and_Addressing_Barriers_to_Patient_Care_in_MS_033121.pdf') }}"
                                        --}}
                                        href="https://cc-cms.online/cmsbuilder/page_preview/dist/65fcce6024c9fade1cef837374a85de8"
                                        target="_blank"><img src="{{ asset('sample_plugin/img/search-1.png') }}"></a>
                                    <a class="drop-down" href=""><img
                                            src="{{ asset('sample_plugin/img/arrow-1.png') }}"></a>
                                </div>
                            </div>

                            <div class="acc__panel  mr-left">
                                A tool to help patients be active participants in treatment decision-making by helping
                                HCPs understand barriers to patient care and how they may be addressed

                            </div>
                        </div>
                    </div>
                    <!-- --------------------- -->
                    <div class="partition"></div>
                    <h5 class="plugin-head-inner">Patient and Caregiver</h5>
                    <div class="acc pd-left">
                        <div class="acc__card">
                            <div class="acc__title">
                                <div class="form-group">
                                    <input type="checkbox" name="selector[]" class="checkbox" id="05"
                                        data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unddfchc"
                                        data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-pridfdfnt"
                                        data-title="PLENVU Dosing Reference Guide">
                                    <label for="05"><span>MS Patient Journey:</span>
                                        <br>Understanding the Multiple Sclerosis Journey</label>

                                </div>
                                <div class="action-btn">
                                    <a class="preview"
                                        {{-- href="{{ asset('sample_plugin/sample_plugin_pdf/ACTonMS_3A_Understanding_Your_MS_Journey_032321.pdf') }}"
                                        --}}
                                        href="https://cc-cms.online/cmsbuilder/page_preview/dist/41a9197abef4360514d84d2118f1a584"
                                        target="_blank"><img src="{{ asset('sample_plugin/img/search-1.png') }}"></a>
                                    <a class="drop-down" href=""><img
                                            src="{{ asset('sample_plugin/img/arrow-1.png') }}"></a>
                                </div>
                            </div>
                            <div class="acc__panel  mr-left">
                                Help patients stay on track during the various stages of the MS journey and be
                                prepared to partner with their care team by knowing what questions to ask.
                            </div>
                        </div>
                        <div class="acc__card">
                            <div class="acc__title">
                                <div class="form-group">
                                    <input type="checkbox" name="selector[]" class="checkbox" id="06"
                                        data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unddfchc"
                                        data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-pridfdfnt"
                                        data-title="PLENVU Dosing Reference Guide">
                                    <label for="06"><span>MS Care Team:</span><br>Members of Your Multiple Sclerosis
                                        Care Team</label>


                                </div>
                                <div class="action-btn">
                                    <a class="preview"
                                        {{-- href="{{ asset('sample_plugin/sample_plugin_pdf/ACTonMS_3B_Members_of_Your_MS_Care_Team_032321.pdf') }}"
                                        --}}
                                        href="https://cc-cms.online/cmsbuilder/page_preview/dist/ab23eb15624dbcda6cabca25a275df92"
                                        target="_blank"><img src="{{ asset('sample_plugin/img/search-1.png') }}"></a>
                                    <a class="drop-down" href=""><img
                                            src="{{ asset('sample_plugin/img/arrow-1.png') }}"></a>
                                </div>
                            </div>
                            <div class="acc__panel  mr-left">
                                A tool to understand the role of each member of the MS care team and keep track
                                of contact information and questions to ask them
                            </div>
                        </div>
                        <div class="acc__card">
                            <div class="acc__title">
                                <div class="form-group">
                                    <input type="checkbox" name="selector[]" class="checkbox" id="07"
                                        data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unddfchc"
                                        data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-pridfdfnt"
                                        data-title="PLENVU Dosing Reference Guide">
                                    <label for="07"><span>MS Disease Course:</span><br>A Guide to Understanding
                                        Multiple
                                        Sclerosis and Common Phrases</label>

                                </div>
                                <div class="action-btn">
                                    <a class="preview"
                                        {{-- href="{{ asset('sample_plugin/sample_plugin_pdf/ACTonMS_4A_A_Guide_to_Understanding_MS_and_Common_Phrases_041521.pdf') }}"
                                        --}}
                                        href="https://cc-cms.online/cmsbuilder/page_preview/dist/c62dc703cfea5f2c57c3482a9abc252c"
                                        target="_blank"><img src="{{ asset('sample_plugin/img/search-1.png') }}"></a>
                                    <a class="drop-down" href=""><img
                                            src="{{ asset('sample_plugin/img/arrow-1.png') }}"></a>
                                </div>
                            </div>
                            <div class="acc__panel  mr-left">
                                A resource to define key terms related to the MS disease course including the MS disease
                                types

                            </div>
                        </div>
                        <div class="acc__card">
                            <div class="acc__title">
                                <div class="form-group">
                                    <input type="checkbox" name="selector[]" class="checkbox" id="08"
                                        data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unddfchc"
                                        data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-pridfdfnt"
                                        data-title="PLENVU Dosing Reference Guide">
                                    <label for="08"><span>Patient Telehealth:</span>
                                        <br>What to Expect in a Telemedicine Visit</label>

                                </div>
                                <div class="action-btn">
                                    <a class="preview"
                                        {{-- href="{{ asset('sample_plugin/sample_plugin_pdf/ACTonMS_4B_What_to_Expect_in_a_Telemedicine_Visit_041221.pdf') }}"
                                        --}}
                                        href="https://cc-cms.online/cmsbuilder/page_preview/dist/710598c891b18037e455179f91dd7c94"
                                        target="_blank"><img src="{{ asset('sample_plugin/img/search-1.png') }}"></a>
                                    <a class="drop-down" href=""><img
                                            src="{{ asset('sample_plugin/img/arrow-1.png') }}"></a>
                                </div>
                            </div>
                            <div class="acc__panel  mr-left">
                                A tool to prepare for telemedicine appointments with a checklist of what to expect and
                                how to prepare, and a printable
                                worksheet to track notes from the doctor’s visit
                            </div>
                        </div>
                        <div class="acc__card">
                            <div class="acc__title">
                                <div class="form-group">
                                    <input type="checkbox" name="selector[]" class="checkbox" id="09"
                                        data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unddfchc"
                                        data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-pridfdfnt"
                                        data-title="PLENVU Dosing Reference Guide">
                                    <label for="09"><span>Medications:</span><br>Talk to Your Doctor About How
                                        Medications Can
                                        Help You
                                        Manage Your Multiple Sclerosis</label>

                                </div>
                                <div class="action-btn">
                                    <a class="preview"
                                        {{-- href="{{ asset('sample_plugin/sample_plugin_pdf/ACTonMS_4C_Talk_to_Your_Doctor_About_How_Medications_Can_Help_You_Manage_Your_MS_032321.pdf') }}"
                                        --}}
                                        href="https://cc-cms.online/cmsbuilder/page_preview/dist/f3f2fca4c6f04175b77e33bbf2f180fc"
                                        target="_blank"><img src="{{ asset('sample_plugin/img/search-1.png') }}"></a>
                                    <a class="drop-down" href=""><img
                                            src="{{ asset('sample_plugin/img/arrow-1.png') }}"></a>
                                </div>
                            </div>
                            <div class="acc__panel  mr-left">
                                Understand the importance of MS medications and how to talk to your doctor about some
                                considerations in selecting a
                                treatment choice.
                            </div>
                        </div>
                    </div>
                    <!-- --------------------- -->
                    <section class="content-accord-sec">
                        <div class="button-groups d-flex  justify-content-between">
                            <a href="#" data-toggle="modal" data-target="#myModal" class="button  stackup">Email</a>
                            <a href="#" data-toggle="modal" data-target="#myModal2"
                                class="button  stackup">View/Print</a>
                            <a href="#" data-toggle="modal" data-target="#myModal3" class="button  stackup">Copy
                                Link</a>
                        </div>
                    </section>
                </div>
            </div>

            <!-- Footer Starts -->
            <div class="f-row">
                <footer>
                    <div class="innrContainer">
                        <div class="inner-wrap">
                            <div class="footer-dis">
                                <div class="foo-logo">
                                    <img src="img/logo.png">
                                </div>
                                <h5>© 2021 Novartis. </h5>
                            </div>
                            <div class="inner-wrap-content">
                                  <!--href="http://go.aventriahealth.com/NVSACTonMSContactFormwGinaWebPage.html"-->
                                <h5>Questions? <a href="http://go.aventriahealth.com/ITPLiverLifeContactUsFormwGINA_NVSACTonMS-ContactFormwGINAWebPage.html"
                                      
                                        target="_blank">Contact Us</a> </h5>
                                <h5><span>3/21</span> <span> T-NSF-1400562</span></h5>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- Footer Ends -->
        </section>
    </div>

    <!-- Modal Starts here-->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-box sample-modal">
            <div class="default-box">
                <div class="modal-header">
                    <h5 class="modal-title text-center">This button allows the user to email the selected resource link
                        to a patient.</h5>
                    <button type="button" class="close model-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Ends Here -->
    <!-- Modal Starts here-->
    <div id="myModal2" class="modal fade" role="dialog">
        <div class="modal-dialog modal-box sample-modal">
            <div class="default-box">
                <div class="modal-header">
                    <h5 class="modal-title text-center">This button allows the user to print the selected tool.</h5>
                    <button type="button" class="close model-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Ends Here -->
    <!-- Modal Starts here-->
    <div id="myModal3" class="modal fade" role="dialog">
        <div class="modal-dialog modal-box sample-modal">
            <div class="default-box">
                <div class="modal-header">
                    <h5 class="modal-title text-center">This button allows the user to copy the link for the selected
                        resource to email to a patient.</h5>
                    <button type="button" class="close model-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Ends Here -->

    <script type="text/javascript" src="{{ asset('sample_plugin/js/plugin.js') }}"></script> <!-- Plugin -->
    <script>
        var checkboxes = $(".mainContainer > input[type='checkbox']"),
            actions = $("html");
        checkboxes.click(function () {
            if (!checkboxes.is(":checked")) {
                actions.removeClass("hidden", );
                $('.hamburger-menu').removeClass('full');
                $('.menu').removeClass('cross');
            } else {
                actions.addClass("hidden", );
                $('.hamburger-menu').addClass('full');
                $('.menu').addClass('cross');

            }

        });


        $('.acc__title .drop-down ').click(function (j) {

            var dropDown = $(this).closest('.acc__card').find('.acc__panel');
            $(this).closest('.acc').find('.acc__panel').not(dropDown).slideUp();

            if ($(this).parents('.acc__title').hasClass('active')) {
                $(this).parents('.acc__title').removeClass('active');
            } else {
                $(this).closest('.acc').find('.acc__title.active').removeClass('active');
                $(this).parents('.acc__title').addClass('active');
            }

            dropDown.stop(false, true).slideToggle();
            j.preventDefault();
        });


        /* Copy Tool */
        $('#copy-tool').on('click', function (e) {


            var value = [];
            var value = $('input[type=checkbox]:checked').map(function (_, el) {
                return (' ' + $(el).data('tool-url'));
            }).get();

            if ($("input[type=checkbox]").is(":checked")) {
                console.log("Copied the text: " + value);
                var $temp = $("<input type='text'>");
                //alert($temp);
                $('body').append($temp);
                $temp.val(value).select();
                document.execCommand("copy");

                $('.copy-cbd').fadeIn();
                setTimeout(function () {
                    $('.copy-cbd').fadeOut();
                }, 3000);

            } else if ($("input[type=checkbox]").is(":not(:checked)")) {
                $('.uncopy-cbd').fadeIn();
                setTimeout(function () {
                    $('.uncopy-cbd').fadeOut();
                }, 3000);
            }

            e.preventDefault();
            //alert(sel);      

        });
    </script>
</body>

</html>