@section('title')
    customer-needs-assessment-tool
@endsection
@extends('layouts.layout')
@section('content')
    <!doctype html>
    <html class="no-js" lang="">



    <body>
        <!--[if lte IE 9]>
                                                                                                                                                                                                                                      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
                                                                                                                                                                                                                                      <![endif]-->
        {{-- <div class="mob-menu-up">
        <div class="mob-menu">
            <div class="mob-close-box"><span class="mob-close"><img src="img/close-white.png" alt="img" /></span>
            </div>
            <ul class="navigation">
                <li><a href="login.html">Log In</a></li>
                <li><a href="register.html">Register</a></li>
                <li><a href="#">Needs Assessment</a></li>
                <!--  <li><a href="#">Get Resources</a></li>
                <li><a href="#">Sample Plugin</a></li>
                <li><a href="#">References</a></li> -->
                <li><a href="#">Privacy Policy</a></li>
                <li>
                    <p>This site is intended for US health care professionals only.</p>
                </li>
            </ul>
        </div>
    </div> --}}
        <div class="searchContainer">
            <div class="srchBox">
                <div class="container">
                    <div class="srch">
                        <input type="text" placeholder="Search">
                        <span class="srchClose"></span>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Section -->
        <div class="banner-container">
            <div class="banner-container__top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Contact My Account Manager </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="login-box section">
            <div class="default-box">
                <h2>Need Help or Have Questions?</h2>
                <p>Simply fill out the information below and we’ll be in touch soon.</p>
            </div>
            <div class="container pad">
                <div class="row">
                    <div class="col-md-6 ml-auto  mr-auto  mt-4">
                        <p class="ind-txt float-right"><span>*</span> Required field</p>
                    </div>
                </div>
                <form method="POST" action="{{ route('contact-account-manager-store') }}">
                    <div class="row">
                        @csrf
                        <div class="m-auto col-md-6 form-box @error('email') error-box @enderror">

                            <label class="req">Name of your Novartis Account Manager</label>
                            <!--  <input type="text" class="form-field" name="salix_manager_name"> -->
                            @php
                                $salix_manager = App\SalixManager::get();
                                //$sources = App\Source::where(['status' => 1])->get();
                            @endphp
                            <select class="select2 form-field select2-clr" name="manager_id">
                                <option></option>
                                @foreach ($salix_manager as $salix_manager)
                                    <option value="{{ $salix_manager->id }}">{{ $salix_manager->salix_manager_name }}
                                    </option>
                                @endforeach
                            </select>
                            <div class="error">
                                <!-- <img src="{{ asset('img/error-arrow.png') }}" alt="error" /> --><img
                                    src="img/error-arrow.png" alt="error" /> @error('salix_manager_name')
                                {{ $message }} @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="m-auto col-md-6 form-box @error('email') error-box @enderror">

                            <label class="req">Type of support needed</label>
                            <!--  <input type="text" class="form-field" name="salix_manager_name"> -->
                            @php
                                $supports = App\Support::get();
                                // $sources = App\Source::where(['status' => 1])->get();
                            @endphp
                            <select class="select2 form-field select2-clr" name="support_id">
                                <option></option>
                                @foreach ($supports as $support)
                                    <option value="{{ $support->id }}">{{ $support->title }}</option>
                                @endforeach
                            </select>
                            <div class="error">
                                <!-- <img src="{{ asset('img/error-arrow.png') }}" alt="error" /> --><img
                                    src="img/error-arrow.png" alt="error" /> @error('support_id')
                                {{ $message }} @enderror
                            </div>
                        </div>
                    </div>
                    {{-- <div class="row">
                        <div class="m-auto col-md-6 form-box @error('email') error-box @enderror">
                            <label class="req">Email Address</label>
                            <input type="email" class="form-field" name="email" value="{{ old('email') }}">
                            <div class="error"><img src="{{ asset('img/error-arrow.png') }}" alt="error" />
                                @error('email')
                                    {{ $message }}
                                @enderror
                            </div>
                        </div>
                    </div> --}}
                    <div class="col-md-12 form-box login-register ">
                        <button class="line ">Submit</button>
                    </div>
                </form>
            </div>
            <div class="contact-msg" style="display: none;">
                <div class="container ">
                    <div class="default-box ">
                        <h2>Thank you for submitting your request.</h2>
                        <p>Someone will reach out to you within 24-48 hours.</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main Section Over-->




        <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
   

    </body>

    </html>
    
@endsection

@section('extra-js')

@endsection
<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
</body>

</html>
