@extends('layouts.layout')

@section('title')

Intercept Only

@endsection

@section('content')

<!-- Start Here -->

<div class="banner-container">

    <div class="banner-container__top">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2>Intercept Only</h2>

                </div>

            </div>

        </div>

    </div>

</div>

<div class="section intercept_section">

    <div class="container">

       <div class="row">

            <div class="col-lg-10 col-12 order_list">
                <h3 class="intercept_h3">Use these resources to :</h3>
               <ul>
                <h5><li>Create increased awareness around the need for better symptom management, early identification, and monitoring of PBC  progression.</li>
                <li>Collaborate with GI Supergroups and IDNs to identify and deliver innovative initiatives to help improve primary biliary cholangitis (PBC) patient care.</li>
                <li>Provide patient and provider education, including integration into health information technology systems such as EHR systems and telehealth systems.</li></h5>
               </ul>
                {{-- <h3><strong>GINA<sup>TM</sup></strong> (Guided INtegration Assistant) can help you integrate resources into your EHR system.</h3> --}}
            </div>
            

        </div>

    </div>

</div>



<div class="htContainer intercept_container">

    <div class="htContainerIn">

        @foreach( $intercepts as $intercept )

        <div class="htBox" style="width:100%;margin-bottom:31px;">

            <div class="htBoxIn" style="width:100%;height:auto"> 
                @if($intercept->id == 2)
                    <div class="htImg" style="width:60%;background:#bae2e2;"><img style="margin-bottom:8%;margin-top:8%;" src="{{asset('storage/'.$intercept->thumb_image)}}" alt="img" /></div>
                @endif
                @if($intercept->id == 3)
                    <div class="htImg" style="width:60%;background:#bae2e2;"><img style="margin-bottom:1%;margin-top:1%;" src="{{asset('storage/'.$intercept->thumb_image)}}" alt="img" /></div>
                @endif
                @if($intercept->id == 4)
                    <div class="htImg" style="width:60%;background:#bae2e2;"><img style="margin-bottom:8%;margin-top:8%;" src="{{asset('storage/'.$intercept->thumb_image)}}" alt="img" /></div>
                @endif
                @if($intercept->id == 5)
                    <div class="htImg" style="width:60%;background:#bae2e2;"><img style="margin-left:20%;width:60%;" src="{{asset('storage/'.$intercept->thumb_image)}}" alt="img" /></div>
                @endif                

                <div class="htCont">

                    <div class="htCont__in">

                        
                        <h3>{{$intercept->title}}</h3>
                        <p>{{$intercept->light_title}}</p>
                        <br>
                        <p>{!! $intercept->description !!}</p>

                    </div>

                    {{-- <a href="#" class="button click_check" data-toggle="modal" data-target="#guideModal_{{$intercept->id}}"
                    data-url="{{$intercept->pretty_link}}" >View/Print</a> --}}

                    <a style="width:35%;" href="{{route('viewintercept', ['intercept_id' => $intercept->id ] )}}"
                        class="button click_check" data-label="{{$intercept->title}}"
                        onClick="ga('send', 'event', 'View/Customized', 'Click', '{{$intercept->title}}');">View</a>

                </div>

            </div>

        </div>

        @endforeach

        <div class="clearfix"></div>

    </div>



    <div class="paginat">

        {{  $intercepts->links() }}

    </div>

</div>



{{-- <div class="footer-note">
        <p>Content contained in this educational disease-state resource is being provided by Novartis Pharmaceuticals for
        informational purposes only.
        Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p>
    </div> --}}
@endsection
