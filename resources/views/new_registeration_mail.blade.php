{{-- You have a new Order Inquiry <br>
Name : {{ $name }} , <br>
Email : {{ $email }} , <br>
organization : {{ $organization }} , <br>
Account Manager : {{ $account_manager }} , <br> --}}


<html>

<head></head>

<body>
    <table width="786px">
        <tr>
            <td style="font: 15px/1.5 'Open Sans', sans-serif;">
                <p>Thank you for registering with LiverLife. You will soon receive an account activation email that allows
                    you to log in to our website through the login page.</p>
            </td>
        </tr>
       
        <tr>
            <td style="font:300 14px/1.5 'Open Sans', sans-serif ;" height="10px">
                <p>Please do not reply to this message. Replies to this message are routed to an unmonitored mailbox.
                    If you have any questions, please contact us at <a href="mailto:support@liverlifepro.com" target="_blank"
                        style="color: #2B79AC;">support@LiverLifePro.com</a>.
                </p>
            </td>
        </tr>
        <tr>
            <td style="font:300 14px/1.5 'Open Sans', sans-serif;" height="25px">

            </td>
        </tr>
    </table>
    <table width="786px">
        <tr>
            <td style="font:  12px/1.5 'Open Sans', sans-serif;">
                <p>Content contained in <a href="{{env('APP_URL')}}" target="_blank"
                        style="color: #2B79AC;">LiverLifePro.com</a>
                    is being provided by Intercept Pharmaceuticals, Inc. for informational purposes
                    only.
                    Health care professionals should use their own clinical judgment in diagnosing, counseling, and advising patients.
                </p>
            </td>
        </tr>
        <tr>
            <td style="font:  12px/1.5 'Open Sans', sans-serif;" height="20">

            </td>
        </tr>

    </table>

    <table width="786px">

        <tr>
            <td height="20px" width="180px">
                <img style="width:30%;" src="https://liverlifeweb.myliverlife.online/img/foo-logo.png">
            </td>
            <td height="20px">

            </td>
        </tr>
        <tr>
            <td height="20px" style="font-size:12px;width:58%; color:#000;font-family:'Open Sans', sans-serif;line-height:22px">
                The INTERCEPT logo is a registered trademark of Intercept Pharmaceuticals, Inc.
                10 Hudson Yard, New York, NY 10001 T:844-782-4278 F:646-747-1001<br>
                © 2021 Intercept Pharmaceuticals, Inc. All rights reserved. US-NP-PB-1296 7/21
            </td>
            <!--<td height="20px" style="font-size:12px; color:#000;font-family:'Open Sans', sans-serif;">
                <div style=" margin-right:40px"> Copyright © 2021 Intercept Pharmaceuticals, Inc. All
                    rights reserved. </div>
            </td>
            <td height="20px" style="font-size:12px; color:#000;font-family:'Open Sans', sans-serif; text-align:right">
                {{-- <span style="disply:inline-block; margin-right:5px">3/21</span> --}}
                <span style="disply:inline-block; margin-left:5px"> 6/21 US-NP-PB-1281</span>
            </td>-->
        </tr>
    </table>

</body>

</html>