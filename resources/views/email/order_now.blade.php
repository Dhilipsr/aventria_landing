{{-- You have a new Order Inquiry <br>
Name : {{ $name }} , <br>
Email : {{ $email }} , <br>
organization : {{ $organization }} , <br>
Account Manager : {{ $account_manager }} , <br> --}}


<html>

<head></head>

<body>
    <table width="650" style="margin: auto;">
        <tr>
            <td style="font:  15px/1.5 arial;">
                <p>Dear {{ $name }}</p>

            </td>
        </tr>
        <tr>
            <td style="font:300 14px/1.5 arial" height="25px">
                <p>The ACTonMS plugin you ordered is here! Simply click this link for access:
                    <a href="https://actonms.com/" target="_blank" style="color: #2B79AC;">
                        https://www.plugin.actonms.com/customizedorganizationname</a></p>
            </td>
        </tr>
        <tr>
            <td style="font:  12px/1.5 arial;">
                <p>As part of our continued commitment to you, we are pleased to offer you support with integrating the
                    plugin into your EHR system. Try our Guided INtegration Assistant, <a
                        href="https://www.research.net/r/ACTonMS" target="_blank" style="color: #2B79AC;">GINA™.</a></p>
            </td>
        </tr>
        <tr>
            <td style="font:300 14px/1.5 arial" height="25px">

            </td>
        </tr>
        <tr>
            <td style="font: 12px/1.5 arial; text-align: center; ">
                {{-- <address style="font-style: normal;">Novartis Pharmaceuticals: 400 Somerset Corporate Blvd.,<br>
                    Bridgewater, NJ 08807.</address> --}}
            </td>
        </tr>
        <tr>
            <td style="font:300 14px/1.5 arial" height="25px">
                <p>We also offer monthly webinars to assist in the installation of the plugin. Join us for the next
                    webinar by
                    registering on ACTonMS today</p>
            </td>
        </tr>
        <tr>
            <td style="font:300 14px/1.5 arial" height="25px">
                <p>We hope you take advantage to help get the most out of the ACTonMS plugin.
                </p>
            </td>
        </tr>
        <tr>
            <td style="font:300 14px/1.5 arial" height="25px">
                <p>If you have any questions or need any assistance, please contact us a <a
                        href="mailto:support@actonms.com" target="_blank"
                        style="color: #2B79AC;">support@ACTonMS.com</a></p>
            </td>
        </tr>
        <tr>
            <td height="20px">

            </td>
        </tr>

    </table>

    <table>
        <tr>
            <td style="font:  12px/1.5 arial;">
                <p>Content contained in <a href="https://actonms.com/" target="_blank"
                        style="color: #2B79AC;">ACTonMS.com</a>
                    is being provided Intercept Pharmaceuticals, Inc. for informational purposes
                    only.
                   Health care professionals should use their own clinical judgment in diagnosing, counseling, and advising patients.
                </p>
            </td>
        </tr>
        <tr>
            <td height="20px" width=" 200px">
                <img src="https://actonms.org/storage/NovartisLogo.jpg">
            </td>
            <td height="20px">

            </td>
        </tr>
        <tr>
            <td height="20px" style="font-size:12px;width:58%; color:#000;font-family:'Open Sans', sans-serif;">
            The INTERCEPT logo is a registered trademark of Intercept Pharmaceuticals, Inc.
            10 Hudson Yard, New York, NY 10001 T:844-782-4278 F:646-747-1001<br>
            © 2021 Intercept Pharmaceuticals, Inc. All rights reserved. US-NP-PB-1296 7/21
        </td>
            <!--<td height="20px" style="font-size:12px; color:#000;font-family:arial;">
                <div style="disply:inline-block; margin-right:40px"> Copyright © 2021 Novartis Pharmaceuticals
                    Corporation. All rights reserved. </div>
            </td>
            <td height="20px" style="font-size:12px; color:#000;font-family:arial;">
                <span style="disply:inline-block; margin-right:5px">3/21</span> <span
                    style="disply:inline-block; margin-left:5px"> T-NSF-1400562</span>
            </td>-->
        </tr>
    </table>

</body>

</html>