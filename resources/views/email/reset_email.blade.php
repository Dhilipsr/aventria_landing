{{-- You have a new Order Inquiry <br>
Name : {{ $name }} , <br>
Email : {{ $email }} , <br>
organization : {{ $organization }} , <br>
Account Manager : {{ $account_manager }} , <br> --}}


<html>

<head></head>

<body>
  <table width="786px">
    <tr>
      <td style="font: 15px/1.5 'Open Sans', sans-serif;">
        <p>Reset your password by clicking the link below:</p>
        <a href="{{$link}}" style="line-break: anywhere;">{{$link}}</a>
      </td>
    </tr>
    <tr>
      <td style="font:300 14px/1.5 'Open Sans', sans-serif;" height="25px">
        <p>Please do not reply to this message. Replies to this message are routed to an unmonitored mailbox.
          If you have any questions, please contact us at <a href="mailto:support@liverlifepro.com" target="_blank"
            style="color: #2B79AC;">support@LiverLifePro.com</a>.
        </p>
      </td>
    </tr>
  </table>
  <table width="786px">
    <tr>
      <td style="font:  14px/1.5 'Open Sans', sans-serif;">
        <p>Content contained in <a href="{{env('APP_URL')}}" target="_blank" style="color: #2B79AC;">LiverLifePro.com</a>
          is being provided by Intercept Pharmaceuticals, Inc. for informational purposes
          only.
          Health care professionals should use their own clinical judgment in diagnosing, counseling, and advising patients.
        </p>
      </td>
    </tr>
    <tr>
      <td style="font:  12px/1.5 'Open Sans', sans-serif;" height="20">

      </td>
    </tr>
  </table>

  <table width="786px">

    <tr>
      <td height="20px" width="180px">
        <img style="width:30%;" src="https://liverlifeweb.myliverlife.online/img/foo-logo.png">
      </td>
      <td height="20px">

      </td>
    </tr>
    <tr>
         <td height="20px" style="font-size:12px;width:58%; color:#000;font-family:'Open Sans', sans-serif; line-height:22px">
            The INTERCEPT logo is a registered trademark of Intercept Pharmaceuticals, Inc.
            10 Hudson Yard, New York, NY 10001 T:844-782-4278 F:646-747-1001<br>
            © 2021 Intercept Pharmaceuticals, Inc. All rights reserved. US-NP-PB-1294 7/21
        </td>
    </tr>
  </table>

</body>

</html>