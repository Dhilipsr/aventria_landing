{{-- You have a new Order Inquiry <br>
Name : {{ $name }} , <br>
Email : {{ $email }} , <br>
organization : {{ $organization }} , <br>
Account Manager : {{ $account_manager }} , <br> --}}


<html>

<head></head>

<body>
    <table width="786px">
        <tr>
            <td style="font: 15px/1.5 arial;">
                <p>Your customer has requested account manager assistance from the ACTonMS portal. Please contact
                    your customer. Their registered email address is {{ $email }}.</p>
            </td>
        </tr>
        {{-- <tr>
            <td style="font:300 14px/1.5 arial; " height="25px">

            </td>
        </tr>
        <tr>
            <td style="font:300 14px/1.5 arial ;" height="25px">
                <p>Please do not reply to this message. Replies to this message are routed to an unmonitored mailbox.
                    If you have any questions or need any assistance, please contact us at <a
                        href="mailto:support@actonms.com" target="_blank"
                        style="color: #2B79AC;">support@ACTonMS.com</a>
                </p>
            </td>
        </tr> --}}
        <tr>
            <td style="font:300 14px/1.5 arial;" height="25px">

            </td>
        </tr>
    </table>
    <table width="786px">
        <tr>
            <td style="font:  12px/1.5 arial;">
                <p>Content contained in <a href="https://actonms.com/" target="_blank"
                        style="color: #2B79AC;">ACTonMS.com</a>
                    is being provided by Novartis Pharmaceuticals Corporation for informational purposes
                    only.
                    Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.
                </p>
            </td>
        </tr>
        <tr>
            <td style="font:  12px/1.5 arial;" height="20">

            </td>
        </tr>

    </table>

    <table width="786px">

        <tr>
            <td height="20px" width="180px">
                <img src="https://actonms.org/storage/NovartisLogo.jpg">
            </td>
            <td height="20px">

            </td>
        </tr>
        <tr>

            <td height="20px" style="font-size:12px; color:#000;font-family:arial;">
                <div style=" margin-right:40px"> Copyright © 2021 Novartis Pharmaceuticals Corporation. All
                    rights reserved. </div>
            </td>
            <td height="20px" style="font-size:12px; color:#000;font-family:arial; text-align:right">
                {{-- <span style="disply:inline-block; margin-right:5px">3/21</span>  --}}
                <span style="disply:inline-block; margin-left:5px"> 3/21 T-NSF-1400576</span>
            </td>
        </tr>
    </table>

</body>

</html>