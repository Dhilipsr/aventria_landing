@section('title')
quality care
@endsection
@extends('layouts.layout')
@section('content')
<div class="main_section">
    <div class="container">
        <div class="main-title margin-tb">
            <h2>{{$qualityCare->title}}</h2>
        </div>

        <div class="text-container">
            {!!$qualityCare->content_one!!}


        </div>
        <div class="list-two">
            {!! $qualityCare->content_two !!}
        </div>


        <hr>
        <div class="text-container two-line-res m-bottom">
           
            <p>
                <a href="{{route('healthtools')}}" class="blueBox--link">Get Resources </a>
            
            </p>
                <p class="gt-src">
                    @if( ! Auth::check() )
                    <a href="{{route('register')}}" class="plain-link">Register now</a>
                    to get resources aligned to quality care.
                    @endif
                    Integrate these resources into your workflow to help standardize care and improve quality.
                </p>
        </div>

       


    </div>
</div>
<div id="guideModal_1" class="modal fade" role="dialog">
        <div class="modal-dialog modal-box">
            <div class="model-close" data-dismiss="modal"><img src="{{asset('img/close-white.png')}}" alt="img" /></div>
            <div class="row guidePop">
                <div class="col-md-12">
                    <h3>Thank you for visiting the NOVARTIS website;<br> you are now leaving our site</h3>
                    <ul>
                        <li><a href="#"  data-dismiss="modal"><img src="{{asset('img/arrow-white-left.png')}}" alt="img" /> &nbsp;&nbsp; Go Back</a>
                        </li>
                        <li><a href="https://aasldpubs.onlinelibrary.wiley.com/doi/10.1002/hep.30489" target="_blank">Continue &nbsp;&nbsp; <img src="{{asset('img/arrow-white-right.png')}}" alt="img" /></a>
                        </li>
                    </ul>
                    <p class="text-center text-white">The website you are about to visit is not affiliated with Salix Pharmaceutical or its affiliated entities, and is not responsible for the content, format, maintenance,or policies of the website you are about to visit. Salix Pharmaceutical or its affiliated entities do not monitor non-affiliated websites for accuracy. This link does not imply endorsement or support of any program, products, or services associated with the website.</p>
                </div>
                <div class="col-md-12 text-center">
                    <div class="check-box">
                        <input type="checkbox" class="do-not-show"  id="chk_1">
                        <label for="chk_1">Don’t show me this message again.</label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="guideModal_2" class="modal fade" role="dialog">
        <div class="modal-dialog modal-box">
            <div class="model-close" data-dismiss="modal"><img src="{{asset('img/close-white.png')}}" alt="img" /></div>
            <div class="row guidePop">
                <div class="col-md-12">
                    <h3>Thank you for visiting the NOVARTIS website;<br> you are now leaving our site</h3>
                    <ul>
                        <li><a href="#"  data-dismiss="modal"><img src="{{asset('img/arrow-white-left.png')}}" alt="img" /> &nbsp;&nbsp; Go Back</a>
                        </li>
                        <li><a href="https://www.aasld.org/" target="_blank">Continue &nbsp;&nbsp; <img src="{{asset('img/arrow-white-right.png')}}" alt="img" /></a>
                        </li>
                    </ul>
                    <p class="text-center text-white">The website you are about to visit is not affiliated with Salix Pharmaceutical or its affiliated entities, and is not responsible for the content, format, maintenance,or policies of the website you are about to visit. Salix Pharmaceutical or its affiliated entities do not monitor non-affiliated websites for accuracy. This link does not imply endorsement or support of any program, products, or services associated with the website.</p>
                </div>
                <div class="col-md-12 text-center">
                    <div class="check-box">
                        <input type="checkbox" class="do-not-show"  id="chk_2">
                        <label for="chk_2">Don’t show me this message again.</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')
<script type="text/javascript">
    // donot ask again starts
        $('.redirectUrl').click(function(e){
            console.log($(this).attr('data-url'));
        });   
        $('.click_check').click(function(e){
           
            // console.log(test);
            var local_check = localStorage.getItem("donotshowhealthtool");
              if (local_check == 1) {
                console.log("test");
                var link = $(this).attr('data-url');
                window.location.href = link;
                 e.stopPropagation();
              }else{

              }
              
        });
        $(".do-not-show").click(function(){
        
          if($(this). prop("checked") == true) {
            localStorage.setItem("donotshowhealthtool", "1");
          }else{
            localStorage.removeItem("donotshowhealthtool"); 
            
          }
        });
        // donot ask again ends
</script>
@endsection
