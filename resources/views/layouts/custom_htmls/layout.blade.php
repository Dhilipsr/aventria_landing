<!doctype html>
<html lang="en">
    <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Liver Health</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="{{asset('icon.png')}}" type="image/x-icon">
        <link rel="icon" href="{{asset('icon.png')}}" type="image/gif" sizes="16x16">

        <link rel="stylesheet" href="{{asset('custom_htmls/css/main.css')}}"> <!-- main -->
    </head>

    <body>
        <div class="mainContainer">
            <div class="mainContainerIn">

                @yield('content')
                
                <!-- Footer Starts -->
                @include('partials.custom_htmls.footer')
                <!-- Footer Ends -->

            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="{{asset('custom_htmls/js/common.js')}}"></script>

    </body>

</html>
