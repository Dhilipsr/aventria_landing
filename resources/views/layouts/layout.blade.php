<!doctype html>

<html lang="en" ng-app="">

<!--<![endif]-->



<!doctype html>

<html class="no-js" lang="">



<head>

    <meta charset="utf-8">

    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>LiverLife - @yield('title')</title>

    <meta name="description" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <link rel="manifest" href="site.webmanifest"> -->

    <link rel="apple-touch-icon" href="{{ asset('icon.png') }}">


    <!-- <link rel="icon" href="{{ asset('icon.png') }}" type="image/gif" sizes="16x16"> -->


    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/swiper-bundle.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">

    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">


    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css"/> --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css"
        rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>


    {{-- <link rel="stylesheet" href="{{asset('css/swiper.min.css')}}"> --}}

    <!-- Google Tag Manager -->
   <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KMDGNBN');</script>
    <!-- End Google Tag Manager -->

</head>



<body>
    <!-- Google Tag Manager (noscript) -->
   <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KMDGNBN"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    {{-- header starts here --}}

    @include('partials.header')

    {{-- header ends here --}}



    <!-- Main Container Starts -->

    <div class="mainContainer">

        @yield('content')

    </div>

    <!-- Main Container Ends -->

    {{-- footer starts here --}}

    @include('partials.footer')

    {{-- footer ends here --}}


  

    <!-- jQuery -->

    <script src="{{ asset('js/vendor/modernizr-3.5.0.min.js') }}"></script>

    <script src="{{ asset('js/vendor/jquery-3.2.1.min.js') }}"></script>

    <script src="{{ asset('js/bootstrap.js') }}"></script>

    {{-- <script src="{{ asset('js/swiper-bundle.min.js') }}"></script> --}}

    <script src="{{ asset('js/plugins.js') }}"></script>

    <script src="{{ asset('js/main.js') }}"></script>

    <script>
        $(document).ready(function(){

         fetch_customer_data();

         function fetch_customer_data(query = '')
         {
          $.ajax({
           url:"{{ route('live_search.action') }}",
           method:'GET',
           data:{query:query}, 
           dataType:'json',
           success:function(data)
           {
            // console.log(data)
            $('#healthtools_ajax_results').html(data.table_data);
            $('#ebw_ajax_results').html(data.table_data_two);
            $('#guidelines_ajax_results').html(data.table_data_three);
            $('#total_records').text(data.total_data);
           }
          })
         }

         $(document).on('keyup', '#search', function(){
          var query = $(this).val();
          console.log(query);
          fetch_customer_data(query);
         });
        });
    </script>
    <script>
       $('.txtOnly_name').keypress(function (e) {
              // var regex = new RegExp("^[a-zA-Z]+$");
              var regex = /^[a-zA-Z\s]*$/;
              var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
              if (regex.test(str)) {
                return true;
              }
              else
              {
                e.preventDefault();
                $('.error_name').show();
                $('.error_name').text('Please enter letters only');
                
                return false;
              }
          });

        $('.txtOnly_lname').keypress(function (e) {
              // var regex = new RegExp("^[a-zA-Z]+$");
              var regex = /^[a-zA-Z\s]*$/;
              var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
              if (regex.test(str)) {
                return true;
              }
              else
              {
                e.preventDefault();
                
                $('.error_lname').show();
                $('.error_lname').text('Please enter letters only');
                return false;
              }
          });
          
          $('#contact').keypress(function (e) {    
    
                var charCode = (e.which) ? e.which : event.keyCode    
    
                if (String.fromCharCode(charCode).match(/[^0-9]/g)) 
                {
                    e.preventDefault();
                    $('.error_contact').show();
                    $('.error_contact').text('Please enter numbers only');
                    return false;    
                }
                else
                {
                    return true;
                }
    
            });   
    </script>

    @yield('extra-js')

</body>



</html>