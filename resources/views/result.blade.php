@section('title')

Health Tools

@endsection

@extends('layouts.layout')

@section('content')

<!-- Start Here -->

@php

if (!empty($_GET['targetaudience'])) {

$pre_target_audiences = $_GET['targetaudience'];

}



if (!empty($_GET['patienttype'])) {

$pre_patienttypes = $_GET['patienttype'];

}



if (!empty($_GET['sort'])) {

$sort = $_GET['sort'];

}



@endphp

<div class="banner-container">

    <div class="banner-container__top">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2>Resources</h2>

                </div>

            </div>

        </div>

    </div>

</div>

<div class="section">

    <div class="container">

        <div class="row">

            <div class="col-lg-10 col-12">

                @foreach( $healthtool_page_contents as $healthtool_page_content )

                <h3  class="align-left">{!! $healthtool_page_content->content !!}</h3>

                @endforeach

                {{-- <h3><strong>GINA<sup>TM</sup></strong> (Guided INtegration Assistant) can help you integrate resources into your EHR system.</h3> --}}
            </div>
             <div class="col-md-12 form-box">
                     {{-- <a href=" https://www.research.net/r/LiverLifePro" target="_blank" class="button">GINA<sup>TM</sup></a> --}}
                  </div> 
            

        </div>

    </div>

</div>



<div class="htFilter">

    <div class="container">

        <div class="htFilterRow">

            <div class="htFiltHead htFiltHead1">

                <span class="htFiltHdSpan" data-id="1">Target Audience <img src="{{asset('img/arrow.png')}}"
                        alt="img" /></span>

            </div>

          

            <div class="htFiltHead htFiltSearchBox">

                <form method="GET" action="#">

                    <input type="text" placeholder="Search" class="htFiltSearch" name="searchbar"
                        id="intermidiate_search_val" />

                    <button id="searchbar_click"></button>

                </form>

            </div>

            <div class="htFiltHead htFiltSortBox">

                <select class="select2 form-field sortSel" placeholder="Sort By" id="sort_decide">

                    <option value=" ">Sort By</option>

                    <option value="asc" @if(!empty($_GET['sort'])) @if( $_GET['sort']=='asc' ) selected @endif @endif>

                        A - Z

                    </option>

                    <option value="desc" @if(!empty($_GET['sort'])) @if( $_GET['sort']=='desc' ) selected @endif @endif>
                        Z - A</option>

                </select>

            </div>

        </div>

        <div class="filtIntBox">



            <form method="GET" action="{{route('healthtoolfilter')}}">

                <div class="filtInt">

                    {{-- target audience starts --}}

                    <ul class="htFiltBody htFiltBody1">

                        @foreach( $targetaudiences as $targetaudience )

                        <li>

                            <div class="check-box">

                                <input type="checkbox" id="targetaudience_{{$targetaudience->id}}"
                                    name="targetaudience[]" value="{{$targetaudience->id}}"
                                    @if(!empty($_GET['targetaudience'])))
                                    @foreach( $pre_target_audiences as
                                    $pre_target_audience ) @if( $pre_target_audience==$targetaudience->id)

                                checked

                                @endif

                                @endforeach

                                @endif

                                >

                                <label for="targetaudience_{{$targetaudience->id}}">{{$targetaudience->title}}</label>

                            </div>

                        </li>

                        @endforeach

                    </ul>

                    {{-- target audience ends --}}

                    {{-- Patient Type starts --}}

                    <ul class="htFiltBody htFiltBody2">

                        @foreach( $patienttypes as $patienttype )

                        <li>

                            <div class="check-box">

                                <input type="checkbox" id="patienttype_{{$patienttype->id}}" name="patienttype[]"
                                    value="{{$patienttype->id}}" @if (!empty($_GET['patienttype'])) @foreach(
                                    $pre_patienttypes as $pre_patienttype ) @if( $pre_patienttype==$patienttype->id)

                                checked

                                @endif

                                @endforeach

                                @endif

                                >

                                <label for="patienttype_{{$patienttype->id}}">{{$patienttype->type}}</label>

                            </div>

                        </li>

                        @endforeach

                    </ul>

                    <input type="hidden" name="sort" value="asc" id="sort">

                    <input type="hidden" placeholder="Search" class="htFiltSearch" id="searchbar_value"
                        name="searchbar" />

                    {{-- Patient Type ends --}}

                </div>

                <div class="filtBtns">

                    <button class="filtBtn" id="filter">Filter</button>

                    <a href="{{route('healthtools')}}">Clear Filter</a>

                </div>

            </form>

        </div>

        {{-- post filter starts --}}

        <p class="filtResult">Your Results {{$totalhealthtools}}</p>

        {{-- post filter starts --}}

    </div>

</div>



<div class="htContainer">

    <div class="htContainerIn">

        @foreach( $healthtools as $healthtool )

        <div class="htBox">

            <div class="htBoxIn">

                <div class="htImg"><img src="{{asset('storage/'.$healthtool->image)}}" alt="img" /></div>

                <div class="htCont">

                    <div class="htCont__in">

                        <p>{{$healthtool->light_title}}</p>

                        <h3>{{$healthtool->title}}</h3>

                        <p>{!! $healthtool->description !!}</p>

                    </div>

                    {{-- <a href="#" class="button click_check" data-toggle="modal" data-target="#guideModal_{{$healthtool->id}}"
                    data-url="{{$healthtool->pretty_link}}">View/Print</a> --}}

                    <a href="{{route('customizeHealthTools', ['healthtool_id' => $healthtool->id ] )}}"
                        class="button click_check">View/Customize</a>

                </div>

            </div>

        </div>

        @endforeach

        <div class="clearfix"></div>

    </div>

    @php



    // $test = (json_encode($healthtool_targetaudiences_array));

    @endphp



    {{ $healthtools->appends([

        'targetaudience' => Request::get('targetaudience') ,

        'patienttype' => Request::get('patienttype') ,

        'searchbar' => Request::get('searchbar') ,

        'sort' => Request::get('sort')

        ])->links() }}

</div>



{{-- Modal --}}

@foreach( $healthtools as $plugin )

<div id="guideModal_{{$plugin->id}}" class="modal fade" role="dialog">

    <div class="modal-dialog modal-box">

        <div class="model-close" data-dismiss="modal"><img src="{{asset('img/close-white.png')}}" alt="img" /></div>

        <div class="row guidePop">

            <div class="col-md-12">

                <h3>Thank you for visiting the ACE website;<br> you are now leaving our site</h3>

                <ul>

                    <li><a href="#" data-dismiss="modal"><img src="{{asset('img/arrow-white-left.png')}}" alt="img" />
                            &nbsp;&nbsp; Go Back</a>

                    </li>

                    <li><a href="{{$plugin->pretty_link}}" target="_blank">Continue &nbsp;&nbsp; <img
                                src="{{asset('img/arrow-white-right.png')}}" alt="img" /></a>

                    </li>

                </ul>

                <p class="text-center text-white">The website you are about to visit is not affiliated with Novartis
                    Pharmaceutical or its affiliated entities, and is not responsible for the content, format,
                    maintenance,or policies of the website you are about to visit. Salix Pharmaceutical or its
                    affiliated entities do not monitor non-affiliated websites for accuracy. This link does not imply
                    endorsement or support of any program, products, or services associated with the website.</p>

            </div>

            <div class="col-md-12 text-center">

                <div class="check-box">

                    <input type="checkbox" class="do-not-show" id="chk_{{$plugin->id}}">

                    <label for="chk_{{$plugin->id}}">Don’t show me this message again.</label>

                </div>

            </div>

        </div>

    </div>


</div>

@endforeach

{{-- modal ends --}}

<div class="section">
    <div class="container">
        <div class="text-center">
            <h3>GINA™ (Guided INtegration Assistant) can help you integrate resources into your EHR system.</h3>
            <a href="https://www.research.net/r/LiverLifePro" target="_blank" class="line button ml-auto mr-auto">
                GINA™
            </a>
        </div>
    </div>
</div>

@endsection




@section('extra-js')

<script type="text/javascript">
    $(document).ready(function(){

        $('#sort_decide').change(function(e){



            // window.location.href = window.location.href+'&sort='+$(this).val();

            $('#sort').val($(this).val());

            $('#filter').click();



        });



        $('#searchbar_click').click(function(e) {

            var search_val = $('#intermidiate_search_val').val();

            $('#searchbar_value').val(search_val);

            $('#filter').click();

            e.preventDefault();

        });



        // donot ask again starts

        $('.click_check').click(function(e){

            var local_check = localStorage.getItem("donotshowhealthtool");

              if (local_check == 1) {

                console.log("test");

                var link = $(this).attr('data-url');

                window.location.href = link;

                 e.stopPropagation();

              }else{



              }

        });

        $(".do-not-show").click(function(){



          if($(this). prop("checked") == true) {

            localStorage.setItem("donotshowhealthtool", "1");

          }else{

            localStorage.removeItem("donotshowhealthtool");



          }

        });

        // donot ask again ends

    });

</script>

@endsection