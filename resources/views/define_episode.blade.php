@section('title')
Define Episode
@endsection
@extends('layouts.layout')
@section('content')


    <!-- Main Section -->
    <div class="main_section">
        <div class="container">
            <div class="main-title margin-tb">
                <h2>{{ $content->title }}</h2>
            </div>

            @foreach( $content->sections as $section )
                    <div class="text-container">
                        {!! $section->content !!}
                    </div>
                @if($loop->first)
                    <div class="blue-box">
                        <div class="box-content">

                            {!!  $content->banner_content !!}
                        </div>
                    </div>
                @endif
                    

                @if( $loop->last)
                @else
                <hr>
                @endif
            @endforeach
                <div class="text-container">
                    <p>
                        You can add your organization logo to the resources and create favorite links of the tools.</sup>
                    </p>
                    <p>
                        <a href="{{route('healthtools')}}" class="blueBox--link">Get Resources </a>
                        @if( ! Auth::check() )
                        <a href="{{route('register')}}" class="plain-link">Register now</a>
                        to get access to the resources below.</sup>
                        @endif
                    </p>
                </div>
            <div class="yellow-head">
                <h4>Provider Resources</h4>
            </div>
            <div class="list-one">
                {!! $content->provider_tools !!}
            </div>

            <div class="yellow-head">
                <h4>Patient Resources</h4>
            </div>
            <div class="list-one">
                {!! $content->patient_tools !!}
            </div>


        </div>
    </div>
    <!-- Main Section Over-->
    <!-- References Section -->
    <section class="refrences_container margin-top1 margin-bottom1">
        <div class="refrences_sec">
            <h5>References </h5>
        </div>
        <div class="slider inside-page-slider">
            <div class="container">
                <ol>
                    <li>Vilstrup H, Amodio P, Bajaj J, et al. Hepatic encephalopathy in chronic liver disease: 2014
                        practice guideline by the
                        American Association
                        for the Study of Liver Diseases and the European Association for the Study of the Liver.
                        <i>Hepatology.</i> 2014;60(2):715-735.</li>
                </ol>
            </div>
        </div>
    </section>
    <!-- References Section Over-->

 @endsection

@section('page-level-js')
@endsection