@section('title')
Home
@endsection
@extends('layouts.layout')
@section('content')

{{-- <!doctype html>
    <html class="no-js" lang=""> --}}

{{-- <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="manifest" href="site.webmanifest">
        <link rel="apple-touch-icon" href="icon.png">
        <!-- Place favicon.ico in the root directory -->
    </head> --}}


<!--[if lte IE 9]>
                                      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
                                      <![endif]-->

<!-- Header Starts -->

<!-- <div class="mob-menu-up">
                                            <div class="mob-menu">
                                                <div class="mob-close-box"><span class="mob-close"><img src="img/close-white.png" alt="img" /></span>
                                                </div>
                                                <ul class="navigation">
                                                    <li><a href="login.html">Log In</a></li>
                                                    <li><a href="register.html">Register</a></li>
                                                    <li><a href="customer-needs-assessment-tool.html">Needs Assessment</a></li>

                                                    <li><a href="#">Privacy Policy</a></li>
                                                    <li>
                                                        <p>This site is intended for US health care professionals only.</p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> -->
{{-- <div class="searchContainer">
        <div class="srchBox">
            <div class="container">
                <div class="srch">
                    <input type="text" placeholder="Search">
                    <span class="srchClose"></span>
                </div>
            </div>
        </div>
    </div> --}}

<!-- Header Ends -->
<div class="main_section top-home-banner">
    <div class="container">
        <div class="row">
                        

                            <div class="swiper-container img-caro web_banner">
                               
                                <div class="swiper-wrapper">
                                    @foreach ($banner_contents as $banner)
                                        <div class="swiper-slide"><img src="{{ asset('storage/' . $banner->img) }}" alt=""></div>
                                    @endforeach

                                    {{-- <div class="swiper-slide"><img src="img/home-banner-01.png" alt=""></div>
                                        <div class="swiper-slide"><img src="img/home-banner-02.png" alt=""></div> --}}

                                </div>
                            </div>
                            
                            <div class="swiper-container img-caro mobile_banner">
                               
                                <div class="swiper-wrapper">
                                    @foreach ($mobile_banner_contents as $mobile_banner)
                                        <div class="swiper-slide"><img src="{{ asset('storage/' . $mobile_banner->img) }}" alt=""></div>
                                    @endforeach

                                    {{-- <div class="swiper-slide"><img src="img/home-banner-01.png" alt=""></div>
                                        <div class="swiper-slide"><img src="img/home-banner-02.png" alt=""></div> --}}

                                </div>
                            </div>

                       
                    
        </div>
    </div>
</div>


<div class="main_section">
    <div class="container" style="font-size:20px;">
        <div class="row resource_button_home">
            <a href="{{ route('healthtools') }}" class="button resource_class">Get Resources</a>
        </div>
        {!! $page_contents->subtitle !!}

    </div>
</div>


<!-- References Section -->
<div class="main_section contents-space">
    <div class="container">
        <div class="row">

            <div class="refrences_sec">
                <h5>References</h5>
            </div>
            <div class="slider ">
                {!! $page_contents->ref !!}

            </div>

        </div>

    </div>
</div>
@if(Session::has('success_contact_manager'))
<div id="contactManager" class="modal fade" role="dialog">
    <div class="modal-dialog modal-box">
        <div class="model-close" data-dismiss="modal"><img src="{{asset('img/close-white.png')}}" alt="img" /></div>
        <div class="default-box new_pass">
            <div class="model-img-align txt-left">
                <img src="{{asset('img/icon-01-white.png')}}" alt="img" />
                <p> LiverLife has alerted your account manager.
                    You will be contacted.</p>
            </div>
        </div>
        {{-- <div class="default-box" id='flash_mesg'>
                <h2> You will receive a  password reset LINK via email.</h2>
            <div> --}}
    </div>
</div>
@endif
@endsection

@section('extra-js')
<script type="text/javascript">
    @if(Session::has('success_contact_manager'))
        $('#contactManager').modal('show');
    @endif
</script>
@endsection

<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
</body>

</html>