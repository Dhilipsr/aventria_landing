@section('title')
customer-needs-assessment-tool
@endsection
@extends('layouts.layout')

@section('content')
<!doctype html>
<html class="no-js" lang="">

{{-- <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/responsive.css">
</head> --}}

<!--[if lte IE 9]>
                  <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
                  <![endif]-->


{{-- <div class="searchContainer">
        <div class="srchBox">
            <div class="container">
                <div class="srch">
                    <input type="text" placeholder="Search">
                    <span class="srchClose"></span>
                </div>
            </div>
        </div>
    </div> --}}

<!-- Header Ends -->

<!-- Main Section -->
<div class="banner-container">
    <div class="banner-container__top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Needs Assessment <br><span> For Internal Use Only</span></h2>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="reg-form section">
    <form method="POST" action="{{ route('customer-assessment-store') }}">
        @csrf
        <div class="container pad">
            <div class="row">
                <div class="col-md-12">
                    <p class="ind-txt float-right"><span>*</span> Required field</p>
                </div>
            </div>

            <div class="row">
                <!--  @csrf -->
                <div class="col-md-6 form-box @error('date') error-box @enderror">
                    <label class="req">Date</label>
                    <input type="text" id="date" class="form-field date" name="date"
                        value="{{Carbon\Carbon::now()->format('m-d-Y')}}">
                    <div class="error">
                        <!-- <img src="{{ asset('img/error-arrow.png') }}" alt="error" /> --><img
                            src="img/error-arrow.png" alt="error" /> @error('date') {{ $message }} @enderror
                    </div>
                </div>
                <div class="col-md-6 form-box @error('name') error-box @enderror"">
                                        <label class=" req">Customer Name</label>
                    <input type="text" class="form-field" name="name" value=''>
                    <div class="error">
                        <!-- <img src="{{ asset('img/error-arrow.png') }}" alt="error" /> --><img
                            src="img/error-arrow.png" alt="error" /> This field is required
                    </div>
                </div>
                <div class="col-md-6 form-box @error('job_title') error-box @enderror">
                    <label class="req">Title</label>
                    <input type="text" class="form-field" name="job_title" value=''>
                    <div class="error">
                        <!-- <img src="{{ asset('img/error-arrow.png') }}" alt="error" /> --><img
                            src="img/error-arrow.png" alt="error" /> @error('job_title') {{ $message }} @enderror
                    </div>
                </div>

                <div class="col-md-6 form-box @error('organization') error-box @enderror">
                    <label class="req">Organization</label>
                    <input type="text" class="form-field" name="organization" value=''>
                    <div class="error">
                        <!-- <img src="{{ asset('img/error-arrow.png') }}" alt="error" /> --><img
                            src="img/error-arrow.png" alt="error" /> @error('organization') {{ $message }}
                        @enderror
                    </div>
                </div>
                <div class="col-md-6 form-box @error('city') error-box @enderror">
                    <label class="req">City</label>
                    <input type="text" class="form-field" name="city" value="{{ old('city') }}">
                    <div class="error">
                        <!-- <img src="{{ asset('img/error-arrow.png') }}" alt="error" /> --><img
                            src="img/error-arrow.png" alt="error" /> @error('city') {{ $message }} @enderror
                    </div>
                </div>
                @php
                $states = App\State::get();
                $sources = App\Source::where(['status' => 1])->get();
                @endphp
                <div class="col-md-6 form-box @error('State') error-box @enderror">
                    <label class="req">State</label>
                    <select class="select2 form-field select2-clr" name="State">
                        <option></option>
                        @foreach ($states as $state)
                        <option value="{{ $state->id }}">{{ $state->state }}</option>
                        @endforeach
                    </select>
                    <div class="error">
                        <!-- <img src="{{ asset('img/error-arrow.png') }}" alt="error" /> --><img
                            src="img/error-arrow.png" alt="error" /> @error('State') {{ $message }} @enderror
                    </div>
                </div>
            </div>
        </div>
        <div class="container mr-top-02 data">
            <p>The Needs Assessment is designed to help stimulate discussions around health care provider
                thoughts about improving MS patient care. These discussions may help identify potential areas for
                collaboration.</p>
            <p>Use the Needs Assessment to capture health care provider insights on resource gaps that may be addressed
                with currently available resources within ACTonMS.</p>
            <p>
                The “Other Needs” box below is a free-form field you can use to note collaboration opportunities not
                covered under the current Support for Goals of Care.
                Any “Other Needs” must remain unbranded and relevant to what ACTonMS can provide.</p>
            <b>This tool should be completed by the MS Account Manager in discussion with health care
                providers.</b>
        </div>

        <div class="container mr-top-02">
            <div class="row tool-selection">
                <div class="col-md-3 ">
                    <h3 class="tool-title">MS Care Support </h3>
                </div>
                <div class="col-md-9 desk-title">
                    <h3 class="tool-title">Support for Goals of Care</h3>
                </div>
            </div>
            <ol class="care-sup-list">
                @foreach ($patient_journeys as $journeys)
                <li>
                    <div class="row tools mr-bottom">

                        <div class="col-md-3 tool-head toggle">
                            <h4>{{ $journeys->title }}</h4>
                        </div>
                        <div class="col-md-9 tool-list inner">
                            <section class="row">
                                <div class="col-12 mob-title">
                                    <h3 class="tool-title">Support for Goals of Care</h3>
                                </div>
                                @foreach ($journeys->GetSharedPriority as $shared_priority)
                                <div class="col-lg-4 col-md-6 col-sm-6">
                                    <label class="tool-checkbox data-title">{{ $shared_priority->title }}
                                        <input type="checkbox" class="gacheckbox" value="{{ $shared_priority->id }}"
                                            name="data[{{ $journeys->id }}][shared_priority][]">
                                        <span class="checkmark"></span>
                                        <div class="bg-clr"></div>
                                    </label>
                                </div>
                                @endforeach
                                <div class="col-lg-4 col-md-6 col-sm-6">
                                    <label class="tool-checkbox  other">Other needs:
                                        <input type="checkbox" class="otherneeds">
                                        <span class="checkmark"></span>
                                        <div class="bg-clr"></div>
                                        <textarea class="textarea" name='data[{{ $journeys->id }}][others]'
                                            value=''></textarea>
                                    </label>
                                </div>
                            </section>
                        </div>
                    </div>

                </li>
                @endforeach
            </ol>
            <div class="row m-auto">
                <div class="col-md-12 form-box ">
                    <button id="submit" class="line">Submit</button>
                    <!-- <br>
                                        <a href="#" id="login" class="line" data-toggle="modal" data-toggle="modal" data-target="#myModal2">Submit</a> -->
                </div>
            </div>
        </div>

    </form>
    @if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
    @endif
</div>

{{-- <div class="tes" data-toggle="modal" data-target="#myModal2">TEST</div> --}}

<div class="contact-msg" style="display: none;">
    <div class="container">
        <div class="default-box ">
            <h2>Thank you for registering with ACTonMS. </h2>
            <p>You will soon receive an account activation email that allows you to log in to our website.</p>
        </div>
    </div>
</div>

<div id="myModal2" class="modal fade" role="dialog">
    <div class="modal-dialog modal-box">
        <div class="model-close" data-dismiss="modal">
            <!-- <img src="{{ asset('img/close-white.png') }}" alt="img" /> --><img src="img/close-white.png"
                alt="img" />
        </div>
        <div class="default-box">
            <p> Please submit to save this needs assessment</p>
        </div>

    </div>
</div>
@if (Session::has('success'))
<div class="alert alert-success">

    {{ Session::get('success') }}
</div>
@endif
<!-- Modal Ends Here -->
<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->

@endsection

@section('extra-js')

<script type="text/javascript ">
    setTimeout(() => {
            console.log(123);
            $('#myModal2').modal('show');
        }, 50000);

        $(document).ready(function() {



            $('#login').click(function(e) {
                var email = $('#email').val();
                var password = $('#password').val();
                var email = $('#email').val();
                if (email == " ") {
                    $('#email').parents('.form-box').addClass('error-box');
                    e.preventDefault();
                }
                if (password == " ") {
                    $('#password').parents('.form-box').addClass('error-box');
                    e.preventDefault();
                }
            });
        });


        $('.submit').click(function(e) {
            if ($(this).is(':checked')) {
                console.log(1)
                var label = $(this).attr('data-title');
                var label = $(this).attr('data-title');
                // var test = ga('send', {
                //   hitType: 'event',
                //   eventCategory: checkBoxCategory,
                //   eventAction: 'Checked',
                //   eventLabel: 'Checked',
                //   eventLabel: label
                // });
            }
        });

        // $('.toggle').click(function(e) {
        //     e.preventDefault();

        //     let $this = $(this);

        //     if ($this.next().hasClass('show')) {
        //         $this.next().removeClass('show');
        //         $this.next().slideUp(350);
        //         $this.removeClass('active');
        //     } else {
        //         $this.parent().parent().find('.toggle').removeClass('active');
        //         $this.parent().parent().find('.tools .inner').removeClass('show');
        //         $this.parent().parent().find('.tools .inner').slideUp(350);
        //         $this.next().toggleClass('show');
        //         $this.next().slideToggle(350);
        //         $this.addClass('active');
        //     }
        // });


        function CheckColors(val) {
            var element = document.getElementById('color');
            if (val == 'pick a color' || val == 'others')
                element.style.display = 'block';
            else
                element.style.display = 'none';
        }




        $(document).ready(function() {

            $('.date').datepicker({
                format: 'mm-dd-yyyy'
            });
        });

</script>
@endsection