@extends('layouts.layout')

@section('title')

Resources

@endsection

@section('content')

<!-- Start Here -->

<div class="banner-container">

    <div class="banner-container__top">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2>Resources</h2>

                </div>

            </div>

        </div>

    </div>

</div>

<div class="section">

    <div class="container">

        <div class="row">

            <div class="col-lg-10 col-12">

                @foreach( $healthtool_page_contents as $healthtool_page_content )

                <h3 class="align-left"> {!! $healthtool_page_content->content !!}</h3>

                @endforeach
                {{-- <h3><strong>GINA<sup>TM</sup></strong> (Guided INtegration Assistant) can help you integrate resources into your EHR system.</h3> --}}
            </div>
            <div class="col-md-12 form-box">
                {{-- <a href="https://www.research.net/r/LiverLifePro" target="_blank" class="button">GINA<sup>TM</sup></a> --}}
            </div>

        </div>

    </div>

</div>



<div class="htFilter">

    <div class="container">

        <div class="htFilterRow">

            <div class="htFiltHead htFiltHead1">

                <span class="htFiltHdSpan" data-id="1">Target Audience <img src="{{asset('img/arrow.png')}}"
                        alt="img" /></span>
            </div>

           <!--  <div class="htFiltHead htFiltHead2">

                <span class="htFiltHdSpan" data-id="2">Patient Type <img src="{{asset('img/arrow.png')}}"
                        alt="img" /></span>

            </div> -->

            <div class="htFiltHead htFiltSearchBox">

                <form method="GET" action="{{route('healthtoolfilter')}}">

                    <input type="text" placeholder="Search" class="htFiltSearch" name="searchbar" />

                    <button></button>

                </form>

            </div>

            <div class="htFiltHead htFiltSortBox">

                <select class="select2 form-field sortSel" placeholder="Sort By" id="sort_decide">

                    <option value=" ">Sort By</option>

                    <option value="asc">A - Z</option>

                    <option value="desc">Z - A</option>

                </select>

            </div>

        </div>

        <div class="filtIntBox">

            <form method="GET" action="{{route('healthtoolfilter')}}">

                <div class="filtInt">

                    {{-- target audience starts --}}

                    <ul class="htFiltBody htFiltBody1">

                        @foreach( $targetaudiences as $targetaudience )

                        <li>

                            <div class="check-box">

                                <input type="checkbox" id="targetaudience_{{$targetaudience->id}}"
                                    name="targetaudience[]" value="{{$targetaudience->id}}" <?php
                                        if(isset($_GET['filter'])){
                                            if ($_GET['filter'] == "clear") {

                                            }else{
                                                echo "checked='checked'";
                                            }
                                        }else{
                                                 echo "checked='checked'";
                                            }
                                    ?>>

                                <label for="targetaudience_{{$targetaudience->id}}">{{$targetaudience->title}}</label>

                            </div>

                        </li>

                        @endforeach
                    </ul>
                    {{-- target audience ends --}}

                    {{-- Patient Type starts --}}

                    <ul class="htFiltBody htFiltBody2">
                        @foreach( $patienttypes as $patienttype )
                        <li>
                            <div class="check-box">
                                <input type="checkbox" id="patienttype_{{$patienttype->id}}" name="patienttype[]"
                                    value="{{$patienttype->id}}" <?php
                                        if(isset($_GET['filter'])){
                                            if ($_GET['filter'] == "clear") {

                                            }else{
                                                echo "checked='checked'";
                                            }
                                        }else{
                                                 echo "checked='checked'";
                                            }
                                    ?>>
                                <label for="patienttype_{{$patienttype->id}}">{{$patienttype->type}}</label>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                    <input type="hidden" name="sort" value="asc" id="sort">
                    {{-- Patient Type ends --}}
                </div>

                <div class="filtBtns">
                    <button class="filtBtn" id="filter">Filter</button>
                    <a href="{{route('healthtools')."?filter=clear"}}">Clear Filter</a>
                </div>
            </form>
        </div>

        {{-- post filter starts --}}

        {{-- <p class="filtResult">Your Results 10</p> --}}

        {{-- post filter starts --}}

    </div>

</div>



<div class="resour_container htContainer">

    <div class="htContainerIn">

        @foreach( $healthtools as $healthtool )

        <div class="htBox">

            <div class="htBoxIn">

                <div class="htImg"><img src="{{asset('storage/'.$healthtool->image)}}" alt="img" /></div>

                <div class="htCont">

                    <div class="htCont__in">

                        <p>{{$healthtool->light_title}}</p>
                        <h3>{{$healthtool->title}}</h3>

                        <p>{!! $healthtool->description !!}</p>

                    </div>

                    {{-- <a href="#" class="button click_check" data-toggle="modal" data-target="#guideModal_{{$healthtool->id}}"
                    data-url="{{$healthtool->pretty_link}}" >View/Print</a> --}}

                    <a href="{{route('customizeHealthTools', ['healthtool_id' => $healthtool->id ] )}}"
                        class="button click_check" data-label="{{$healthtool->title}}"
                        onClick="ga('send', 'event', 'View/Customized', 'Click', '{{$healthtool->title}}');">View/Customize</a>

                </div>

            </div>

        </div>

        @endforeach

        <div class="clearfix"></div>

    </div>



    <div class="paginat">

        {{  $healthtools->links() }}

    </div>

</div>

{{-- Modal --}}
@foreach( $healthtools as $plugin )
<div id="guideModal_{{$plugin->id}}" class="modal fade" role="dialog">
    <div class="modal-dialog modal-box">
        <div class="model-close" data-dismiss="modal"><img src="{{asset('img/close-white.png')}}" alt="img" /></div>
        <div class="row guidePop">
            <div class="col-md-12">
                <h3>Thank you for visiting the LiverHealth website;<br> you are now leaving our site</h3>
                <ul>
                    <li><a href="#" data-dismiss="modal"><img src="{{asset('img/arrow-white-left.png')}}" alt="img" />
                            &nbsp;&nbsp; Go Back</a>
                    </li>
                    <li><a href="{{$plugin->pretty_link}}" target="_blank">Continue &nbsp;&nbsp; <img
                                src="{{asset('img/arrow-white-right.png')}}" alt="img" /></a>
                    </li>
                </ul>
                <p class="text-center text-white">The website you are about to visit is not affiliated with Salix
                    Pharmaceutical or its affiliated entities, and is not responsible for the content, format,
                    maintenance,or policies of the website you are about to visit. Salix Pharmaceutical or its
                    affiliated entities do not monitor non-affiliated websites for accuracy. This link does not imply
                    endorsement or support of any program, products, or services associated with the website.</p>
            </div>
            <div class="col-md-12 text-center">
                <div class="check-box">
                    <input type="checkbox" class="do-not-show" id="chk_{{$plugin->id}}">
                    <label for="chk_{{$plugin->id}}">Don’t show me this message again.</label>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
<div class="section gina_contents_reso">
    <div class="container">
        <div class="text-center">
            <h3>GINA™ (Guided INtegration Assistant) can help you integrate resources into your EHR system.</h3>
            <a href="https://www.research.net/r/LiverLifePro" target="_blank" class="line button resources_gina ml-auto mr-auto">
                GINA™
            </a>
        </div>
    </div>
</div>
{{-- <div class="footer-note">
        <p>Content contained in this educational disease-state resource is being provided by Novartis Pharmaceuticals for
        informational purposes only.
        Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p>
    </div> --}}
@endsection



@section('extra-js')
<script type="text/javascript">
    $(document).ready(function(){
        $('#sort_decide').change(function(e){
            $('#sort').val($(this).val());
            $('#filter').click();
        });

        // donot ask again starts
        $('.click_check').click(function(e){
            // google analytics click functionality
           var label = $(this).attr('data-label');
           var test = ga('send', {
             hitType: 'event',
             eventCategory: 'View/Customized',
             eventAction: 'Click',
             eventLabel: label
           });
            // console.log(test);
            // var local_check = localStorage.getItem("donotshowhealthtool");
            //   if (local_check == 1) {
            //     console.log("test");
            //     var link = $(this).attr('data-url');
            //     window.location.href = link;
            //      e.stopPropagation();
            //   }else{

            //   }
            //   e.preventDefault();
        });
        $(".do-not-show").click(function(){

          if($(this). prop("checked") == true) {
            localStorage.setItem("donotshowhealthtool", "1");
          }else{
            localStorage.removeItem("donotshowhealthtool");

          }
        });
        // donot ask again ends




    });


</script>

<script type="text/javascript">



</script>

@endsection