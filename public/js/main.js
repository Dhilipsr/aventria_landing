$(document).ready(function () {
    
     

    stickyHeader();
    $(window).scroll(function () {
        stickyHeader();
    });


    var touch = $('#resp-menu');
    var menu = $('.navigation');

    $(touch).on('click', function (e) {
        e.preventDefault();
        menu.slideToggle();
    });

    $(window).resize(function () {
        var w = $(window).width();
        if (w > 767 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });






    /*  $('.select2').select2({
          placeholder: "Select an option",
      });
      $(".sortSel").select2({
          placeholder: "Sort By"
      });*/

    var dataIdRef;
    $(document).on('click', '.htFiltHdSpan', function (e) {
        e.stopPropagation();
        let dataId = $(this).data('id');
        // console.log(dataId);
        if (dataIdRef == undefined || dataIdRef != dataId) {
            $('.htFiltHdSpan').css('opacity', 0.5);
            $(this).css('opacity', 1);
            $('.filtIntBox').show();
            $('.htFiltBody').hide();
            $('.htFiltSearchBox').hide();
            $('.htFiltSortBox').hide();
            $('.htFiltBody' + dataId).show();
            dataIdRef = dataId;
        } else {
            filterClose();
        }
    })
    $(document).on('click', 'body', function () {
        filterClose();
    })
    
    $("#reset").click(function(e){
        e.preventDefault();
        var baseurl = window.location.origin;
        var email = $("#resetemail").val();
        if(email == "")
        {
            $(".forgot_email").html('<img src="'+baseurl+'/img/error-arrow-wht.png")}}" alt="error" />');
            $(".forgot_email").append(" This field is required.");
            $(".forgot_email").css({"display":"block", "color":"white"});
            $("#resetemail").addClass("resetPass");
            return false;
        }
        var token = $("#csrf_token1").val();
        $.ajax({
            url:baseurl+"/checkbeforereset",
            type:"post",
            data:{
                _token: token,
                email:email
            },
            success : function(response)
            {
                if(response == 0)
                {
                    $(".forgot_email").html('<img src="'+baseurl+'/img/error-arrow-wht.png")}}" alt="error" />');
                    $(".forgot_email").append(" We can't find a user with that email address.");
                    $(".forgot_email").css({"display":"block", "color":"white"});
                    $("#resetemail").addClass("resetPass");
                }
                else
                {
                    $(".new_pass").submit();
                }
            }
        });
    });

    function filterClose() {
        $('.htFiltHdSpan').css('opacity', 1);
        $('.filtIntBox').hide();
        $('.htFiltBody').show();
        $('.htFiltSearchBox').show();
        $('.htFiltSortBox').show();
        dataIdRef = undefined;
    }

    $(document).on('click', '.filtIntBox', function (e) {
        e.stopPropagation();
    })

    $(document).on('click', '.srchBtn', function (e) {
        $('.searchContainer').fadeIn(300);
        $('body').css('overflow', 'hidden');

    })
    $(document).on('click', '.srchClose, .searchContainer', function (e) {
        $('.searchContainer').fadeOut(300);
        $('body').css('overflow', 'auto');
    })
    $(document).on('click', '.srch', function (e) {
        e.stopPropagation();
    })

    /* Menu */
    $(document).on('click', '.menu-icon', function () {
        $('.mob-menu-up').addClass('slide');
        $(this).addClass('close');
    })
    $(document).on('click', '.close', function () {
        $('.mob-menu-up').removeClass('slide');
        $(this).removeClass('close');
    })
    /* //Menu */




    var button = $('.refrences_sec h5');
    var slider = $('.slider');

    button.on('click', function (e) {

        if (slider.hasClass('open')) {
            button.toggleClass('open');
            slider.toggleClass('open');
        } else {
            slider.toggleClass('open');
            button.toggleClass('open');
        }

    });

    /* SWIPER SLIDER */
    var swiper = new Swiper('.img-caro', {
        loop: true,
        spaceBetween: 30,
        effect: 'fade',
        fadeEffect: {
            crossFade: true
        },
        autoplay: {
            delay: 3500,
            disableOnInteraction: false,
        },
        speed: 2000,
    });
    var swiper = new Swiper('.text-caro', {
        loop: true,
        spaceBetween: 30,
        pagination: {
            el: '.swiper-pagination',
            clickable: false,
        },
        autoplay: {
            delay: 3500,
            disableOnInteraction: false,
        },
        speed: 2000,

    });



    /*  $("form").on("change", ".file-upload-field", function() {
         $(this).parent(".file-upload-wrapper").attr("data-text", $(this).val().replace(/.*(\/|\\)/, ''));
     }); */



    /*   $('.favrite-btn').click(function(event) {
          $(this).html("<i class='icon-img'><img src='img/tick.svg'></i> Added to your profile page");
          event.preventDefault();
      }); */



    /* Click checkbox */
    $('.otherneeds').change(function () {
        //alert('changed');
        if ($(this).is(":checked")) {
            $(".answer").show();
            $(this).parents('.col-lg-4').addClass('w100');
            $(this).parent('.tool-checkbox').children('.textarea').show();
            //alert('changed');
        } else {
            $(".answer").hide();
            $(this).parents('.col-lg-4').removeClass('w100');
            $(this).parent('.tool-checkbox').children('.textarea').hide();
            // alert('close');
        }
    });






    $('.toggle').click(function (e) {
        e.preventDefault();

        let $this = $(this);

        if ($this.next().hasClass('show')) {
            $this.next().removeClass('show');
            $this.next().slideUp(350);
            $this.removeClass('active');
        } else {
            $this.parent().parent().find('.toggle').removeClass('active');
            $this.parent().parent().find('.tools .inner').removeClass('show');
            $this.parent().parent().find('.tools .inner').slideUp(350);
            $this.next().toggleClass('show');
            $this.next().slideToggle(350);
            $this.addClass('active');
        }
    });







});

function stickyHeader() {
    var sticky = $('header'),
        scroll = $(window).scrollTop();

    if (scroll < 40) sticky.addClass('fixHeader');
    else sticky.removeClass('fixHeader');
}



function copyT() {
    /* Get the text field */
    var copyText = document.getElementById("myInput");

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/

    /* Copy the text inside the text field */
    document.execCommand("copy");



    /* Alert the copied text */
    alert("Copied the text: " + copyText.value);
}