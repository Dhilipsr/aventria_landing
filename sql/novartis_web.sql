-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 05, 2021 at 09:58 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `novartis_web`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(2, 1, 'name', 'text', 'First Name', 1, 1, 1, 1, 1, 1, '{}', 3),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 4),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '{}', 18),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '{}', 19),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 20),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 22),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '{}', 23),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 25),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 26),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '{}', 29),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 0, 1, 1, 1, 1, 1, '{}', 24),
(22, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'type', 'text', 'Type', 0, 1, 1, 1, 1, 1, '{}', 2),
(24, 4, 'status', 'select_dropdown', 'Active', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}', 3),
(25, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(26, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(27, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(28, 5, 'sources', 'text', 'Sources', 0, 1, 1, 1, 1, 1, '{}', 3),
(29, 5, 'status', 'select_dropdown', 'Active', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}', 4),
(30, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(31, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(32, 1, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 1, 1, 1, 1, 1, '{}', 21),
(33, 1, 'lname', 'text', 'Last name', 0, 1, 1, 1, 1, 1, '{}', 5),
(34, 1, 'organization', 'text', 'Organization', 0, 1, 1, 1, 1, 1, '{}', 6),
(35, 1, 'organization_id', 'text', 'Organization Id', 0, 1, 1, 1, 1, 1, '{}', 32),
(36, 1, 'job_title', 'text', 'Job Title', 0, 1, 1, 1, 1, 1, '{}', 8),
(37, 1, 'contact', 'text', 'Contact', 0, 1, 1, 1, 1, 1, '{}', 9),
(38, 1, 'address', 'text', 'Address', 0, 1, 1, 1, 1, 1, '{}', 10),
(39, 1, 'salix_manager_name', 'text', 'Novartis Manager Name', 0, 0, 0, 0, 0, 0, '{}', 11),
(40, 1, 'source_id', 'text', 'Source Id', 0, 1, 1, 1, 1, 1, '{}', 14),
(41, 1, 'update', 'select_dropdown', 'Update Check', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}', 16),
(42, 1, 'agreement_check', 'select_dropdown', 'Agreement Check', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}', 17),
(43, 1, 'user_belongsto_organizationtype_relationship', 'relationship', 'Organization Types', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Organizationtype\",\"table\":\"organizationtypes\",\"type\":\"belongsTo\",\"column\":\"organization_id\",\"key\":\"id\",\"label\":\"type\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(44, 1, 'user_belongsto_source_relationship', 'relationship', 'Source', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Source\",\"table\":\"sources\",\"type\":\"belongsTo\",\"column\":\"source_id\",\"key\":\"id\",\"label\":\"sources\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 12),
(45, 1, 'status', 'select_dropdown', 'Active', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}', 2),
(61, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(62, 8, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(63, 8, 'status', 'select_dropdown', 'Active', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}', 3),
(64, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(65, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(66, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(67, 9, 'type', 'text', 'Type', 0, 1, 1, 1, 1, 1, '{}', 2),
(68, 9, 'status', 'select_dropdown', 'Active', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}', 3),
(69, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(70, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(71, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(72, 10, 'item_code', 'text', 'Item Code', 0, 1, 1, 1, 1, 1, '{}', 2),
(73, 10, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 4),
(74, 10, 'description', 'text_area', 'Description', 0, 1, 1, 1, 1, 1, '{}', 5),
(75, 10, 'pretty_link', 'text', 'Pretty Link', 0, 1, 1, 1, 1, 1, '{}', 6),
(76, 10, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 11),
(77, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 13),
(78, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 14),
(79, 10, 'status', 'select_dropdown', 'Active', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}', 12),
(80, 10, 'healthtool_belongstomany_targetaudience_relationship', 'relationship', 'Target Audiences', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Targetaudience\",\"table\":\"targetaudiences\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"healthtool_targetaudiences\",\"pivot\":\"1\",\"taggable\":\"on\"}', 7),
(81, 10, 'healthtool_belongstomany_patienttype_relationship', 'relationship', 'Topic', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Patienttype\",\"table\":\"patienttypes\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"type\",\"pivot_table\":\"healthtool_paitienttype\",\"pivot\":\"1\",\"taggable\":\"on\"}', 8),
(82, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(83, 11, 'content', 'text_area', 'Content', 0, 1, 1, 1, 1, 1, '{}', 2),
(84, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(85, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(86, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(87, 13, 'content', 'text_area', 'Content', 0, 1, 1, 1, 1, 1, '{}', 2),
(88, 13, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(89, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(121, 20, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(122, 20, 'content', 'rich_text_box', 'Content', 0, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 4),
(123, 20, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(124, 20, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(125, 21, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(126, 21, 'title', 'text_area', 'Title', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required | max:255\"}}', 3),
(129, 21, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 10),
(130, 21, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(139, 27, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(140, 27, 'address', 'text', 'Address', 0, 1, 1, 1, 1, 1, '{}', 2),
(141, 27, 'copyright', 'text', 'Copyright', 0, 1, 1, 1, 1, 1, '{}', 3),
(142, 27, 'code_item', 'text', 'Code Item', 0, 1, 1, 1, 1, 1, '{}', 4),
(143, 27, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(144, 27, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(145, 29, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(146, 29, 'ref', 'rich_text_box', 'Ref', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 3),
(147, 29, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(148, 29, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(149, 29, 'subtitle', 'rich_text_box', 'Subtitle', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 4),
(150, 29, 'description', 'rich_text_box', 'Description', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 5),
(151, 20, 'img', 'image', 'Img', 0, 1, 1, 1, 1, 1, '{}', 5),
(152, 20, 'alt_tag', 'text', 'Alt Tag', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 6),
(153, 20, 'sq_no', 'number', 'Sq No', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 3),
(154, 20, 'homebannercontent_belongsto_home_page_content_relationship', 'relationship', 'home_page_contents', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\HomePageContent\",\"table\":\"home_page_contents\",\"type\":\"belongsTo\",\"column\":\"home_page_contents_id\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(155, 20, 'home_page_contents_id', 'text', 'Home Page Contents Id', 0, 1, 1, 1, 1, 1, '{}', 9),
(156, 29, 'home_page_content_hasmany_homebannercontent_relationship', 'relationship', 'homebannercontents', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Homebannercontent\",\"table\":\"homebannercontents\",\"type\":\"hasMany\",\"column\":\"home_page_contents_id\",\"key\":\"id\",\"label\":\"content\",\"pivot_table\":\"abouts\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(157, 21, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required | max:150\"}}', 2),
(158, 21, 'banner_content', 'rich_text_box', 'Banner Content', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 4),
(159, 21, 'provider_tools', 'rich_text_box', 'Provider Tools', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 5),
(160, 21, 'patient_tools', 'rich_text_box', 'Patient Tools', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 6),
(161, 21, 'slug', 'text', 'Slug', 0, 1, 1, 0, 1, 0, '{\"slugify\":{\"origin\":\"name\",\"forceUpdate\":true}}', 7),
(162, 30, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(163, 30, 'service_id', 'select_dropdown', 'Service Id', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 3),
(164, 30, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 4),
(165, 30, 'seq_no', 'number', 'Seq No', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 5),
(166, 30, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(167, 30, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(168, 30, 'service_section_belongsto_service_relationship', 'relationship', 'services', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Service\",\"table\":\"services\",\"type\":\"belongsTo\",\"column\":\"service_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"abouts\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(169, 21, 'service_hasmany_service_section_relationship', 'relationship', 'service_sections', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\ServiceSection\",\"table\":\"service_sections\",\"type\":\"hasMany\",\"column\":\"service_id\",\"key\":\"id\",\"label\":\"seq_no\",\"pivot_table\":\"abouts\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(170, 21, 'ref', 'rich_text_box', 'Ref', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 9),
(171, 10, 'customize_image', 'image', 'Customize Image', 0, 1, 1, 1, 1, 1, '{}', 9),
(172, 10, 'html', 'rich_text_box', 'Html', 0, 1, 1, 1, 1, 1, '{}', 10),
(173, 31, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(174, 31, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(175, 31, 'content_one', 'rich_text_box', 'Content One', 0, 1, 1, 1, 1, 1, '{}', 3),
(176, 31, 'content_two', 'rich_text_box', 'Content Two', 0, 1, 1, 1, 1, 1, '{}', 4),
(177, 31, 'content_three', 'rich_text_box', 'Content Three', 0, 1, 1, 1, 1, 1, '{}', 5),
(178, 31, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(179, 31, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(180, 10, 'api_title', 'text', 'Api Title', 0, 1, 1, 1, 1, 1, '{}', 15),
(181, 1, 'created_at_time_zone', 'text', 'Created At Time Zone', 0, 0, 0, 0, 0, 0, '{}', 13),
(182, 1, 'updated_at_timezone', 'text', 'Updated At Timezone', 0, 1, 1, 1, 1, 1, '{}', 15),
(183, 1, 'timezone', 'text', 'Timezone', 0, 1, 1, 1, 1, 1, '{}', 27),
(184, 1, 'last_login', 'text', 'Last Login', 0, 1, 1, 1, 1, 1, '{}', 28),
(185, 1, 'last_login_timezone', 'text', 'Last Login Timezone', 0, 1, 1, 1, 1, 1, '{}', 30),
(186, 1, 'code', 'text', 'Code', 0, 1, 1, 1, 1, 1, '{}', 31),
(187, 5, 'sr_no', 'text', 'Sr No', 0, 1, 1, 1, 1, 1, '{}', 2),
(188, 32, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(189, 32, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 2),
(190, 32, 'code', 'text', 'Code', 0, 1, 1, 1, 1, 1, '{}', 3),
(191, 32, 'department', 'text', 'Department', 0, 1, 1, 1, 1, 1, '{}', 4),
(192, 32, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(193, 32, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(195, 32, 'department_belongstomany_healthtool_relationship', 'relationship', 'healthtools', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Healthtool\",\"table\":\"healthtools\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"departments_healthtools\",\"pivot\":\"1\",\"taggable\":\"0\"}', 8),
(197, 1, 'department_id', 'text', 'Department Id', 0, 0, 0, 0, 0, 0, '{}', 29),
(200, 33, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(201, 33, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 2),
(202, 33, 'title', 'rich_text_box', 'Title', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 3),
(203, 33, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(204, 33, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(205, 27, 'subtext', 'text', 'Subtext', 0, 0, 1, 1, 1, 1, '{}', 7),
(206, 40, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(207, 40, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(208, 40, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 3),
(209, 40, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(210, 41, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(212, 41, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 3),
(213, 41, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 4),
(214, 41, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(217, 40, 'shared_prioritie', 'multiple_checkbox', 'Shared Prioritie', 0, 1, 1, 1, 1, 1, '{}', 5),
(218, 40, 'patient_journey_belongstomany_shared_priority_relationship', 'relationship', 'shared_priorities', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\SharedPriority\",\"table\":\"shared_priorities\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"patient_prioritey\",\"pivot\":\"1\",\"taggable\":\"on\"}', 6),
(219, 41, 'shared_priority_belongstomany_patient_journey_relationship', 'relationship', 'patient_journeys', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\PatientJourney\",\"table\":\"patient_journeys\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"patient_prioritey\",\"pivot\":\"1\",\"taggable\":\"0\"}', 6),
(220, 47, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(221, 47, 'salix_manager_name', 'text', 'Novartis Manager Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(222, 47, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 4),
(223, 47, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(224, 50, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(225, 50, 'user_id', 'text', 'User Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(226, 50, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 3),
(227, 50, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(228, 50, 'user_assessment_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5),
(229, 52, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(230, 52, 'state', 'text', 'State', 0, 1, 1, 1, 1, 1, '{}', 2),
(231, 52, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 3),
(232, 52, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(233, 47, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 3),
(234, 55, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(235, 55, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(236, 55, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(237, 55, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(238, 56, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(239, 56, 'main_content', 'rich_text_box', 'Main Content', 0, 1, 1, 1, 1, 1, '{}', 2),
(240, 56, 'contact_acc_manager', 'text', 'Contact Acc Manager', 0, 1, 1, 1, 1, 1, '{}', 3),
(241, 56, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(242, 56, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(243, 57, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(244, 57, 'manager_name', 'text', 'Manager Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(245, 57, 'support_type', 'text', 'Support Type', 0, 1, 1, 1, 1, 1, '{}', 3),
(246, 57, 'first_name', 'text', 'First Name', 0, 1, 1, 1, 1, 1, '{}', 4),
(247, 57, 'last_name', 'text', 'Last Name', 0, 1, 1, 1, 1, 1, '{}', 5),
(248, 57, 'organization', 'text', 'Organization', 0, 1, 1, 1, 1, 1, '{}', 6),
(249, 57, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 7),
(250, 57, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(251, 57, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(252, 58, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(253, 58, 'customer_name', 'text', 'Customer Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(254, 58, 'organization', 'text', 'Organization', 0, 1, 1, 1, 1, 1, '{}', 3),
(255, 58, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 4),
(256, 58, 'manager_name', 'text', 'Manager Name', 0, 1, 1, 1, 1, 1, '{}', 5),
(257, 58, 'customer_lname', 'text', 'Customer Lname', 0, 1, 1, 1, 1, 1, '{}', 6),
(258, 58, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(259, 58, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(260, 1, 'salix_manager_id', 'text', 'Salix Manager Id', 0, 1, 1, 1, 1, 1, '{}', 30),
(261, 1, 'user_belongsto_salix_manager_relationship', 'relationship', 'Novartis Manager', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\SalixManager\",\"table\":\"salix_managers\",\"type\":\"belongsTo\",\"column\":\"salix_manager_id\",\"key\":\"id\",\"label\":\"salix_manager_name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 33),
(262, 56, 'sample_page', 'image', 'Sample Page', 0, 1, 1, 1, 1, 1, '{}', 4),
(263, 1, 'need_assessment', 'select_dropdown', 'Need Assessment', 0, 1, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}', 31),
(264, 50, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 6),
(265, 50, 'city', 'text', 'City', 0, 1, 1, 1, 1, 1, '{}', 8),
(266, 50, 'state', 'text', 'State', 0, 1, 1, 1, 1, 1, '{}', 9),
(267, 50, 'customer_name', 'text', 'Customer Name', 0, 1, 1, 1, 1, 1, '{}', 10),
(268, 50, 'date', 'text', 'Date', 0, 1, 1, 1, 1, 1, '{}', 11),
(269, 50, 'organization', 'text', 'Organization', 0, 1, 1, 1, 1, 1, '{}', 7),
(270, 10, 'light_title', 'text', 'Light Title', 0, 1, 1, 1, 1, 1, '{}', 3);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'App\\Http\\Controllers\\CustomUserController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2019-05-08 03:24:31', '2021-04-23 05:34:40'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2019-05-08 03:24:31', '2019-05-08 03:24:31'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2019-05-08 03:24:31', '2019-05-08 03:24:31'),
(4, 'organizationtypes', 'organizationtypes', 'Organization Type', 'Organization Types', 'voyager-list', 'App\\Organizationtype', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-05-08 04:57:31', '2019-05-08 04:57:31'),
(5, 'sources', 'sources', 'Source', 'Sources', 'voyager-list', 'App\\Source', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-05-08 05:22:00', '2020-10-13 14:53:01'),
(8, 'targetaudiences', 'targetaudiences', 'Target Audience', 'Target Audiences', 'voyager-megaphone', 'App\\Targetaudience', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-05-13 03:47:23', '2019-05-13 03:47:23'),
(9, 'patienttypes', 'patienttypes', 'Patient Type', 'Patient Types', 'voyager-receipt', 'App\\Patienttype', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-05-13 03:50:11', '2019-05-13 03:50:11'),
(10, 'healthtools', 'healthtools', 'Health Tool', 'Health Tools', 'voyager-tools', 'App\\Healthtool', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-05-13 04:05:08', '2021-05-19 05:56:27'),
(11, 'healthtoolstaticcontents', 'healthtoolstaticcontents', 'Health Tool Static Content', 'Health Tool Static Contents', 'voyager-pen', 'App\\Healthtoolstaticcontent', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-05-13 05:52:50', '2019-05-13 05:52:50'),
(12, 'evidencebasedworkflowscontent', 'evidencebasedworkflowscontent', 'Evidence based workflows content', 'Evidence based workflows contents', 'voyager-pen', 'App\\Evidencebasedworkflowscontent', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-05-13 06:38:08', '2019-05-13 06:38:08'),
(13, 'evidencebasedworkflowscontents', 'evidencebasedworkflowscontents', 'Evidence Based workflows Content', 'Evidence Based workflows Contents', 'voyager-pen', 'App\\Evidencebasedworkflowscontent', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-05-13 06:40:27', '2019-05-13 06:40:49'),
(17, 'guidelineandlinkscontent', 'guidelineandlinkscontent', 'Guidelines and Links Content', 'Guidelines and Links Contents', 'voyager-pen', 'App\\Guidelineandlinkscontent', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-05-13 09:46:05', '2019-05-13 09:46:05'),
(20, 'homebannercontents', 'homebannercontents', 'Home Banner Content', 'Home Banner Contents', 'voyager-window-list', 'App\\Homebannercontent', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-05-13 12:24:40', '2020-12-15 18:38:12'),
(21, 'services', 'services', 'Service', 'Services', 'voyager-medal-rank-star', 'App\\Service', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-05-13 12:45:00', '2020-02-29 13:59:22'),
(27, 'footers', 'footers', 'Footer', 'Footers', 'voyager-hook', 'App\\Footer', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-05-13 13:18:56', '2020-11-25 05:33:11'),
(29, 'home_page_contents', 'home-page-contents', 'Home Page Content', 'Home Page Contents', NULL, 'App\\HomePageContent', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-29 10:55:37', '2020-02-29 13:05:43'),
(30, 'service_sections', 'service-sections', 'Service Section', 'Service Sections', NULL, 'App\\ServiceSection', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-29 13:42:53', '2020-02-29 13:43:58'),
(31, 'quality_cares', 'quality-cares', 'Quality Care', 'Quality Cares', NULL, 'App\\QualityCare', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-04-28 16:02:17', '2020-04-28 16:02:17'),
(32, 'departments', 'departments', 'Department', 'Departments', NULL, 'App\\Department', NULL, 'App\\Http\\Controllers\\VoyagerDepartmentController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-11-10 06:38:03', '2020-11-11 03:32:11'),
(33, 'homesliders', 'homesliders', 'Homeslider', 'Homesliders', NULL, 'App\\Homeslider', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-11-25 04:39:29', '2021-04-26 10:59:18'),
(39, 'patient_journey', 'patient-journey', 'Patient Journey', 'Patient Journeys', NULL, 'App\\PatientJourney', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-12-02 01:35:52', '2020-12-02 01:35:52'),
(40, 'patient_journeys', 'patient-journeys', 'Patient Journey', 'Patient Journeys', NULL, 'App\\PatientJourney', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-12-02 01:41:33', '2020-12-18 12:55:38'),
(41, 'shared_priorities', 'shared-priorities', 'Shared Priority', 'Shared Priorities', NULL, 'App\\SharedPriority', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-12-02 01:52:50', '2020-12-18 14:24:30'),
(47, 'salix_managers', 'novartis-managers', 'Novartis Manager', 'Novartis Managers', NULL, 'App\\SalixManager', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-01-06 14:02:10', '2021-04-28 16:16:13'),
(50, 'user_assessments', 'user-assessments', 'User Assessment', 'User Assessments', NULL, 'App\\UserAssessment', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-01-11 06:01:02', '2021-04-23 07:27:52'),
(52, 'states', 'states', 'State', 'States', NULL, 'App\\State', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-01-11 15:48:50', '2021-01-11 15:48:50'),
(55, 'supports', 'supports', 'Support', 'Supports', NULL, 'App\\Support', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-07 06:43:32', '2021-04-07 06:43:32'),
(56, 'ehr_homes', 'ehr-homes', 'Ehr Home', 'Ehr Homes', NULL, 'App\\EhrHome', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-07 07:00:43', '2021-04-20 11:31:08'),
(57, 'support_requests', 'support-requests', 'Support Request', 'Support Requests', NULL, 'App\\SupportRequest', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-07 10:21:09', '2021-04-07 10:21:09'),
(58, 'order_requests', 'order-requests', 'Order Request', 'Order Requests', NULL, 'App\\OrderRequest', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-07 10:22:33', '2021-04-07 10:22:33');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `image`, `code`, `department`, `created_at`, `updated_at`) VALUES
(6, 'departments\\November2020\\krBxapClDc0VDz8cYzRu.jpg', 'A1B2C3', 'Leo9', '2020-11-11 03:56:00', '2020-11-11 04:02:32');

-- --------------------------------------------------------

--
-- Table structure for table `departments_healthtools`
--

CREATE TABLE `departments_healthtools` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `healthtool_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments_healthtools`
--

INSERT INTO `departments_healthtools` (`id`, `department_id`, `healthtool_id`, `created_at`, `updated_at`) VALUES
(1, '1', 19, NULL, NULL),
(2, '1', 20, NULL, NULL),
(3, '1', 21, NULL, NULL),
(4, '2', 19, NULL, NULL),
(5, '2', 20, NULL, NULL),
(6, '2', 21, NULL, NULL),
(7, '3', 19, NULL, NULL),
(8, '3', 20, NULL, NULL),
(9, '3', 21, NULL, NULL),
(10, '4', 19, NULL, NULL),
(11, '4', 20, NULL, NULL),
(12, '4', 21, NULL, NULL),
(13, '5', 19, NULL, NULL),
(14, '5', 20, NULL, NULL),
(15, '5', 21, NULL, NULL),
(16, '6', 19, NULL, NULL),
(17, '6', 20, NULL, NULL),
(18, '6', 21, NULL, NULL),
(19, '6', 22, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `departments_users`
--

CREATE TABLE `departments_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_code` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments_users`
--

INSERT INTO `departments_users` (`id`, `department_code`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 12, NULL, NULL),
(2, 1, 30, NULL, NULL),
(3, 2, 558, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ehr_homes`
--

CREATE TABLE `ehr_homes` (
  `id` int(10) UNSIGNED NOT NULL,
  `main_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_acc_manager` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sample_page` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ehr_homes`
--

INSERT INTO `ehr_homes` (`id`, `main_content`, `contact_acc_manager`, `created_at`, `updated_at`, `sample_page`) VALUES
(1, '<h3>A plugin is a way to integrate customized provider, patient, and caregiver tools into your workflow within your existing information technology system.</h3>\r\n<h3>Once installed, resources or tools on the plugin can be <strong>viewed, printed, or sent to patients</strong>. The plugin is EHR-agnostic and also can be used as a mobile application or integrated into a website.</h3>\r\n<h3>The LiverLife plugin does not access any patient health information or data from your EHR. To learn how to integrate the LiverLife plugin into your existing EHR, click on the GINA&trade; button at the bottom of this page.</h3>', '<span>To order a customized plugin</span>, click Order Now or contact your Account Manager for more information.', '2021-04-07 07:01:00', '2021-07-05 01:12:32', 'ehr-homes\\July2021\\2R4nBNW0CpYrEKmgVCuW.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `evidencebasedworkflowscontents`
--

CREATE TABLE `evidencebasedworkflowscontents` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `evidencebasedworkflowscontents`
--

INSERT INTO `evidencebasedworkflowscontents` (`id`, `content`, `created_at`, `updated_at`) VALUES
(1, 'The American Association for the Study of Liver Disease (AASLD) and the European Association of the Study of the Liver (EASL) Practice Guideline recommends ways to reduce the risk of OHE recurrence and OHE-related hospitalizations.', '2019-05-13 06:40:00', '2019-05-23 15:55:28'),
(2, 'Incorporating these guideline recommendations into health system workflows is one way to support standardized care.', '2019-05-13 06:41:04', '2019-05-13 06:41:04'),
(3, '<span>The ACE platform</span> can help standardize evidence-based OHE care within the EHR via care  pathways and health tools that align with practice guidelines.', '2019-05-13 06:41:00', '2019-05-23 15:53:32');

-- --------------------------------------------------------

--
-- Table structure for table `footers`
--

CREATE TABLE `footers` (
  `id` int(10) UNSIGNED NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `copyright` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_item` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `subtext` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `footers`
--

INSERT INTO `footers` (`id`, `address`, `copyright`, `code_item`, `created_at`, `updated_at`, `subtext`) VALUES
(1, NULL, NULL, '6/21 US-NP-PB-1281', '2019-05-13 13:19:00', '2021-06-30 02:28:16', 'Content  provided by Novartis Pharmaceuticals Corporation for HCPs to use with patients for  educational and informational purposes only. <br>Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.');

-- --------------------------------------------------------

--
-- Table structure for table `healthtools`
--

CREATE TABLE `healthtools` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pretty_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customize_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `html` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `light_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `healthtools`
--

INSERT INTO `healthtools` (`id`, `item_code`, `title`, `description`, `pretty_link`, `image`, `customize_image`, `html`, `created_at`, `updated_at`, `status`, `api_title`, `light_title`) VALUES
(46, 'Tool 1A', 'Level Check -- PBC Predictive Biomarkers', 'Why it\'s important to monitor your patients ALP  and bilirubin levels, when to check them, and what the results mean.', 'https://cc-oge.online/actonms/level-check-pbc-predictive-biomarkers', 'healthtools\\July2021\\s00689fi9bYGBRRlGEci.jpg', 'healthtools\\July2021\\gasM7uhITkcDiLMx3KAl.jpg', NULL, '2021-04-07 05:23:00', '2021-07-02 08:07:52', '1', 'ACTonMS 1A Early Treatment in MS', NULL),
(48, 'Tool 1B', 'Managing Pruritus (Itchhing)', 'This tool can assist your patients in managing itching.', 'https://cc-oge.online/actonms/managing-pruritus', 'healthtools\\July2021\\WebwbnGHekUZO7q3oYq2.jpg', 'healthtools\\July2021\\GLDvaCz2KFEo1uZrRPd0.jpg', NULL, '2021-04-07 05:42:00', '2021-07-02 08:16:32', '1', 'ACTonMS 1B Overview of Clinical Study Measures in MS', NULL),
(49, 'Tool 1C', 'PBC Discussion Guide', 'Provide this tool to your patients so they can ask questions about their PBC journey.', 'https://cc-oge.online/actonms/pbc-discussion-guide', 'healthtools\\July2021\\nemCHAu9c9cJU0YktrzQ.jpg', 'healthtools\\July2021\\02EiRRNI375iNDbI4o3k.jpg', NULL, '2021-04-07 05:43:00', '2021-07-02 08:19:07', '1', 'ACTonMS 1C Overview of Telehealth', NULL),
(52, 'Tool 3B', 'Face Your Fatigue (Tiredness)', 'This tool provides information for your patients about approaches to relieve fatigues.', 'https://cc-oge.online/actonms/face-your-fatigue', 'healthtools\\July2021\\zHZUVTsaWe7Ma0OZpav8.jpg', 'healthtools\\July2021\\XKYa243rXYwsdBaWOZZY.jpg', NULL, '2021-04-07 06:02:00', '2021-07-05 00:50:00', '1', 'ACTonMS 3B Members of Your MS Care Team', NULL),
(53, 'Tool 4A', 'Primary Biliary Cholangitis AASLD Guidance Snapshot', 'Guidance statements from the AASLD  2018 PBC Guidance  to help you optimize the care of your patients.', 'https://cc-oge.online/liverlife/primary-biliary-cholangitis-aasld-guidance-snapshot', 'healthtools\\July2021\\oB1FKZpApDUzMXrAzlni.jpg', 'healthtools\\July2021\\oFEmsNvkiOPPLJLWOvEJ.jpg', NULL, '2021-04-07 06:03:00', '2021-07-02 03:51:21', '1', 'ACTonMS 4A A Guide to Understanding MS and Common Phrases', NULL),
(55, 'Tool 4C', 'Tracking your PBC', 'This tool can help your patients monitor key markers of liver health to determine how treatment is working  for them.', 'https://cc-oge.online/actonms/tracking-your-pbc', 'healthtools\\July2021\\HvpVQhLqp8PvSbxXGrCJ.jpg', 'healthtools\\July2021\\gRpDl8cgZB4CvJkuxFOS.jpg', NULL, '2021-04-07 06:07:00', '2021-07-02 08:21:34', '1', 'ACTonMS 4C Talk to Your Doctor About How Medications Can Help You Manage Your MS', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `healthtoolstaticcontents`
--

CREATE TABLE `healthtoolstaticcontents` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `healthtoolstaticcontents`
--

INSERT INTO `healthtoolstaticcontents` (`id`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Find health tools for your provider teams and patients quickly and easily by using the filters below.', '2019-05-13 05:53:00', '2021-07-02 03:41:21');

-- --------------------------------------------------------

--
-- Table structure for table `healthtool_paitienttype`
--

CREATE TABLE `healthtool_paitienttype` (
  `id` int(10) UNSIGNED NOT NULL,
  `healthtool_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patienttype_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `healthtool_paitienttype`
--

INSERT INTO `healthtool_paitienttype` (`id`, `healthtool_id`, `patienttype_id`, `created_at`, `updated_at`) VALUES
(1, '1', '1', NULL, NULL),
(2, '2', '1', NULL, NULL),
(3, '3', '1', NULL, NULL),
(4, '4', '1', NULL, NULL),
(5, '5', '1', NULL, NULL),
(6, '6', '1', NULL, NULL),
(7, '7', '2', NULL, NULL),
(8, '8', '2', NULL, NULL),
(9, '9', '2', NULL, NULL),
(10, '10', '2', NULL, NULL),
(11, '11', '2', NULL, NULL),
(12, '12', '2', NULL, NULL),
(13, '13', '3', NULL, NULL),
(14, '14', '3', NULL, NULL),
(15, '15', '3', NULL, NULL),
(16, '16', '3', NULL, NULL),
(17, '17', '3', NULL, NULL),
(18, '9', '1', NULL, NULL),
(19, '39', '1', NULL, NULL),
(20, '20', '1', NULL, NULL),
(21, '41', '1', NULL, NULL),
(22, '40', '1', NULL, NULL),
(23, '30', '1', NULL, NULL),
(24, '30', '3', NULL, NULL),
(25, '28', '1', NULL, NULL),
(26, '28', '3', NULL, NULL),
(27, '37', '1', NULL, NULL),
(28, '36', '1', NULL, NULL),
(29, '42', '2', NULL, NULL),
(30, '24', '2', NULL, NULL),
(31, '19', '2', NULL, NULL),
(32, '27', '2', NULL, NULL),
(33, '43', '2', NULL, NULL),
(34, '26', '2', NULL, NULL),
(35, '23', '2', NULL, NULL),
(36, '29', '2', NULL, NULL),
(37, '35', '2', NULL, NULL),
(38, '32', '3', NULL, NULL),
(39, '44', '3', NULL, NULL),
(40, '22', '3', NULL, NULL),
(41, '25', '3', NULL, NULL),
(42, '38', '3', NULL, NULL),
(43, '21', '1', NULL, NULL),
(44, '45', '1', NULL, NULL),
(46, '47', '1', NULL, NULL),
(47, '53', '4', NULL, NULL),
(49, '55', '4', NULL, NULL),
(51, '54', '4', NULL, NULL),
(52, '54', '5', NULL, NULL),
(53, '52', '4', NULL, NULL),
(55, '51', '4', NULL, NULL),
(56, '51', '5', NULL, NULL),
(57, '50', '6', NULL, NULL),
(58, '49', '6', NULL, NULL),
(59, '48', '6', NULL, NULL),
(60, '46', '6', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `healthtool_targetaudiences`
--

CREATE TABLE `healthtool_targetaudiences` (
  `id` int(10) UNSIGNED NOT NULL,
  `healthtool_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `targetaudience_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `healthtool_targetaudiences`
--

INSERT INTO `healthtool_targetaudiences` (`id`, `healthtool_id`, `targetaudience_id`, `created_at`, `updated_at`) VALUES
(1, '1', '3', NULL, NULL),
(2, '2', '3', NULL, NULL),
(3, '3', '3', NULL, NULL),
(4, '4', '3', NULL, NULL),
(5, '5', '3', NULL, NULL),
(6, '6', '1', NULL, NULL),
(7, '6', '2', NULL, NULL),
(8, '7', '3', NULL, NULL),
(9, '8', '3', NULL, NULL),
(10, '9', '1', NULL, NULL),
(11, '9', '2', NULL, NULL),
(12, '10', '1', NULL, NULL),
(13, '10', '2', NULL, NULL),
(14, '11', '1', NULL, NULL),
(15, '11', '2', NULL, NULL),
(16, '12', '1', NULL, NULL),
(17, '12', '2', NULL, NULL),
(18, '13', '3', NULL, NULL),
(19, '14', '1', NULL, NULL),
(20, '14', '2', NULL, NULL),
(21, '15', '1', NULL, NULL),
(22, '15', '2', NULL, NULL),
(23, '16', '1', NULL, NULL),
(24, '16', '2', NULL, NULL),
(25, '17', '1', NULL, NULL),
(26, '17', '2', NULL, NULL),
(27, '18', '3', NULL, NULL),
(28, '39', '3', NULL, NULL),
(29, '20', '3', NULL, NULL),
(30, '41', '3', NULL, NULL),
(31, '40', '1', NULL, NULL),
(32, '40', '2', NULL, NULL),
(33, '30', '1', NULL, NULL),
(34, '30', '2', NULL, NULL),
(35, '28', '1', NULL, NULL),
(36, '28', '2', NULL, NULL),
(37, '37', '1', NULL, NULL),
(38, '37', '2', NULL, NULL),
(39, '36', '1', NULL, NULL),
(40, '36', '2', NULL, NULL),
(41, '42', '3', NULL, NULL),
(42, '24', '3', NULL, NULL),
(43, '19', '3', NULL, NULL),
(44, '27', '1', NULL, NULL),
(45, '27', '2', NULL, NULL),
(46, '43', '1', NULL, NULL),
(47, '43', '2', NULL, NULL),
(48, '26', '1', NULL, NULL),
(49, '26', '2', NULL, NULL),
(50, '23', '1', NULL, NULL),
(51, '23', '2', NULL, NULL),
(52, '29', '1', NULL, NULL),
(53, '29', '2', NULL, NULL),
(54, '35', '1', NULL, NULL),
(55, '35', '2', NULL, NULL),
(56, '32', '3', NULL, NULL),
(57, '44', '3', NULL, NULL),
(58, '22', '3', NULL, NULL),
(59, '25', '1', NULL, NULL),
(60, '25', '2', NULL, NULL),
(61, '38', '1', NULL, NULL),
(62, '38', '2', NULL, NULL),
(63, '21', '3', NULL, NULL),
(64, '45', '3', NULL, NULL),
(66, '47', '1', NULL, NULL),
(67, '53', '1', NULL, NULL),
(69, '55', '1', NULL, NULL),
(71, '54', '1', NULL, NULL),
(72, '54', '2', NULL, NULL),
(73, '52', '1', NULL, NULL),
(75, '51', '1', NULL, NULL),
(76, '51', '2', NULL, NULL),
(77, '50', '3', NULL, NULL),
(78, '49', '3', NULL, NULL),
(79, '48', '3', NULL, NULL),
(80, '46', '3', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `health_tools_favs`
--

CREATE TABLE `health_tools_favs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `healthtool_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `healthtool_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_fav` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `org_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `health_tools_favs`
--

INSERT INTO `health_tools_favs` (`id`, `user_id`, `healthtool_id`, `healthtool_slug`, `link`, `avatar`, `is_fav`, `org_image`, `created_at`, `updated_at`) VALUES
(145, '37', NULL, '19', NULL, NULL, '1', NULL, '2020-05-08 14:16:08', '2020-05-08 14:16:08'),
(146, '37', NULL, '20', NULL, NULL, '1', NULL, '2020-05-08 14:56:41', '2020-05-08 14:56:41'),
(147, '50', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200508191240.jpeg', NULL, NULL, '2020-05-08 19:12:40', '2020-05-08 19:12:40'),
(148, '50', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200508191318.jpeg', NULL, NULL, '2020-05-08 19:13:18', '2020-05-08 19:13:18'),
(154, '40', NULL, '26', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'userstemplogos/20200508200636.jpeg', '1', NULL, '2020-05-08 20:06:36', '2020-05-08 20:06:54'),
(155, '126', NULL, '19', NULL, NULL, '1', NULL, '2020-05-11 13:40:49', '2020-05-11 13:40:49'),
(156, '112', NULL, '25', 'i-have-he', 'userstemplogos/20200511134314.jpeg', NULL, NULL, '2020-05-11 13:43:14', '2020-05-11 13:43:14'),
(161, '67', NULL, '22', NULL, NULL, '1', '', '2020-05-12 14:41:57', '2020-05-12 14:41:57'),
(163, '42', NULL, '27', NULL, NULL, '1', NULL, '2020-05-12 19:27:03', '2020-05-12 19:27:03'),
(164, '42', NULL, '32', NULL, NULL, '1', NULL, '2020-05-12 19:27:51', '2020-05-12 19:27:51'),
(166, '102', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200514134905.png', NULL, NULL, '2020-05-14 13:49:05', '2020-05-14 13:49:05'),
(167, '102', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200514141304.png', NULL, NULL, '2020-05-14 14:13:04', '2020-05-14 14:13:04'),
(169, '47', NULL, '41', NULL, NULL, '1', NULL, '2020-05-14 19:40:30', '2020-05-14 19:40:30'),
(170, '36', NULL, '27', NULL, NULL, '1', NULL, '2020-05-15 13:31:10', '2020-05-15 13:31:10'),
(175, '47', NULL, '29', NULL, NULL, '1', NULL, '2020-05-15 17:35:30', '2020-05-15 17:35:30'),
(178, '66', NULL, '41', NULL, NULL, '1', '', '2020-05-18 18:24:06', '2020-05-18 18:24:06'),
(179, '138', NULL, '32', NULL, NULL, '1', NULL, '2020-05-20 05:15:18', '2020-05-20 05:15:18'),
(180, '138', NULL, '40', NULL, NULL, '1', NULL, '2020-05-20 05:17:14', '2020-05-20 05:17:14'),
(181, '138', NULL, '39', NULL, NULL, '1', NULL, '2020-05-20 05:17:32', '2020-05-20 05:17:32'),
(184, '95', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'userstemplogos/20200520135142.html', NULL, NULL, '2020-05-20 13:51:42', '2020-05-20 13:51:42'),
(185, '95', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200520135639.png', NULL, NULL, '2020-05-20 13:56:39', '2020-05-20 13:56:39'),
(186, '119', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200520140721.png', NULL, NULL, '2020-05-20 14:07:21', '2020-05-20 14:07:21'),
(187, '50', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200520141329.jpeg', NULL, NULL, '2020-05-20 14:13:29', '2020-05-20 14:13:29'),
(188, '119', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200520141329.png', NULL, NULL, '2020-05-20 14:13:29', '2020-05-20 14:13:29'),
(190, '95', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200520141528.png', NULL, NULL, '2020-05-20 14:15:28', '2020-05-20 14:15:28'),
(191, '95', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200520144033.jpeg', NULL, NULL, '2020-05-20 14:40:33', '2020-05-20 14:40:33'),
(192, '68', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'userstemplogos/20200520191555.jpeg', NULL, NULL, '2020-05-20 19:15:55', '2020-05-20 19:15:55'),
(193, '68', NULL, '22', 'discuss-patient-journaling-with-your-patients', 'userstemplogos/20200520191623.jpeg', NULL, NULL, '2020-05-20 19:16:23', '2020-05-20 19:16:23'),
(194, '50', NULL, '25', 'i-have-he', 'userstemplogos/20200520192130.jpeg', NULL, NULL, '2020-05-20 19:21:30', '2020-05-20 19:21:30'),
(195, '155', NULL, '20', NULL, NULL, '1', '', '2020-05-20 19:54:55', '2020-05-20 19:54:55'),
(196, '66', NULL, '23', NULL, NULL, '1', '', '2020-05-21 16:15:38', '2020-05-21 16:15:38'),
(197, '119', NULL, '23', 'goal-setting-when-you-have-chronic-liver-disease', 'userstemplogos/20200521202500.png', NULL, NULL, '2020-05-21 20:25:00', '2020-05-21 20:25:00'),
(198, '95', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200522161215.png', NULL, NULL, '2020-05-22 16:12:15', '2020-05-22 16:12:15'),
(199, '95', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200522161235.png', NULL, NULL, '2020-05-22 16:12:35', '2020-05-22 16:12:35'),
(200, '95', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'userstemplogos/20200522161916.png', NULL, NULL, '2020-05-22 16:19:16', '2020-05-22 16:19:16'),
(207, '102', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200526135029.png', NULL, NULL, '2020-05-26 13:50:29', '2020-05-26 13:50:29'),
(216, '152', NULL, '20', NULL, NULL, '1', NULL, '2020-05-26 16:34:25', '2020-05-26 16:34:25'),
(217, '152', NULL, '41', NULL, NULL, '1', NULL, '2020-05-26 16:38:04', '2020-05-26 16:38:04'),
(218, '152', NULL, '21', NULL, NULL, '1', NULL, '2020-05-26 16:38:13', '2020-05-26 16:38:13'),
(219, '152', NULL, '27', NULL, NULL, '1', NULL, '2020-05-26 16:38:57', '2020-05-26 16:38:57'),
(220, '152', NULL, '26', NULL, NULL, '1', NULL, '2020-05-26 16:40:29', '2020-05-26 16:40:29'),
(221, '102', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200526181315.png', NULL, NULL, '2020-05-26 18:13:15', '2020-05-26 18:13:15'),
(222, '102', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200526181330.png', NULL, NULL, '2020-05-26 18:13:30', '2020-05-26 18:13:30'),
(223, '95', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'userstemplogos/20200528191730.jpeg', NULL, NULL, '2020-05-28 19:17:30', '2020-05-28 19:17:30'),
(227, '112', NULL, '21', 'assess-he-with-the-stroop-test', 'users/QXZlbnRyaWFfTE9HT19mdWxsIGNvbG9yIEhvcml6b250YWwucG5n20200511163710.png', NULL, NULL, '2020-05-29 05:35:25', '2020-05-29 05:35:25'),
(242, '109', NULL, '41', 'cirrhosis-and-its-complications', 'users/U2V0dGluZyBVcCBNZWRpY2FsIEFsZXJ0cy5qcGc=20200508054451.jpeg', '1', NULL, '2020-05-29 16:28:01', '2020-05-29 16:28:02'),
(243, '109', NULL, '41', 'cirrhosis-and-its-complications', 'users/U2V0dGluZyBVcCBNZWRpY2FsIEFsZXJ0cy5qcGc=20200508054451.jpeg', '1', NULL, '2020-05-29 16:29:19', '2020-05-29 16:29:21'),
(244, '109', NULL, '41', 'cirrhosis-and-its-complications', 'users/U2V0dGluZyBVcCBNZWRpY2FsIEFsZXJ0cy5qcGc=20200508054451.jpeg', NULL, NULL, '2020-05-29 16:31:02', '2020-05-29 16:31:02'),
(245, '109', NULL, '41', 'cirrhosis-and-its-complications', 'users/U2V0dGluZyBVcCBNZWRpY2FsIEFsZXJ0cy5qcGc=20200508054451.jpeg', '1', NULL, '2020-05-29 16:31:02', '2020-05-29 16:31:08'),
(246, '109', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200529163221.jpeg', '1', NULL, '2020-05-29 16:32:21', '2020-05-29 16:32:23'),
(247, '109', NULL, '20', NULL, NULL, '1', NULL, '2020-05-29 16:34:47', '2020-05-29 16:34:47'),
(248, '95', NULL, '21', 'assess-he-with-the-stroop-test', 'users/QlNXIGxvZ28ucG5n20200526145251.png', '1', NULL, '2020-05-29 17:25:34', '2020-05-29 17:25:55'),
(249, '95', NULL, '39', 'west-haven-criteria', 'users/QlNXIGxvZ28ucG5n20200526145251.png', '1', NULL, '2020-05-29 17:26:31', '2020-05-29 17:26:37'),
(250, '95', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/QlNXIGxvZ28ucG5n20200526145251.png', '1', NULL, '2020-05-29 17:27:08', '2020-05-29 17:27:14'),
(251, '95', NULL, '25', 'i-have-he', 'users/QlNXIGxvZ28ucG5n20200526145251.png', '1', NULL, '2020-05-29 17:27:55', '2020-05-29 17:28:00'),
(252, '95', NULL, '32', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'users/QlNXIGxvZ28ucG5n20200526145251.png', '1', NULL, '2020-05-29 17:28:31', '2020-05-29 17:28:35'),
(253, '95', NULL, '40', 'what-is-hepatic-encephalopathy', 'users/QlNXIGxvZ28ucG5n20200526145251.png', '1', NULL, '2020-05-29 17:29:07', '2020-05-29 17:29:11'),
(254, '95', NULL, '26', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'users/QlNXIGxvZ28ucG5n20200526145251.png', '1', NULL, '2020-05-29 17:29:48', '2020-05-29 17:29:52'),
(255, '114', NULL, '30', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'users/default.png', '1', NULL, '2020-05-29 18:37:50', '2020-05-29 18:40:29'),
(256, '114', NULL, '30', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'users/default.png', NULL, NULL, '2020-05-29 18:38:41', '2020-05-29 18:38:41'),
(257, '95', NULL, '23', 'goal-setting-when-you-have-chronic-liver-disease', 'users/QlNXIGxvZ28ucG5n20200526145251.png', NULL, NULL, '2020-05-29 18:56:11', '2020-05-29 18:56:11'),
(258, '114', NULL, '27', NULL, NULL, '1', NULL, '2020-05-29 19:14:20', '2020-05-29 19:14:20'),
(259, '119', NULL, '37', 'symptoms-complications-and-management-of-cirrhosis', 'userstemplogos/20200601033935.jpeg', NULL, NULL, '2020-06-01 03:39:35', '2020-06-01 03:39:35'),
(260, '119', NULL, '37', 'symptoms-complications-and-management-of-cirrhosis', 'userstemplogos/20200601034204.jpeg', NULL, NULL, '2020-06-01 03:42:04', '2020-06-01 03:42:04'),
(261, '119', NULL, '25', 'i-have-he', 'userstemplogos/20200601034619.jpeg', NULL, NULL, '2020-06-01 03:46:19', '2020-06-01 03:46:19'),
(262, '119', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'userstemplogos/20200601034708.jpeg', NULL, NULL, '2020-06-01 03:47:08', '2020-06-01 03:47:08'),
(263, '119', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'userstemplogos/20200601034900.jpeg', NULL, NULL, '2020-06-01 03:49:00', '2020-06-01 03:49:00'),
(264, '119', NULL, '35', 'setting-up-medical-alerts-on-digital-devices', 'userstemplogos/20200601042453.jpeg', NULL, NULL, '2020-06-01 04:24:53', '2020-06-01 04:24:53'),
(265, '204', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200602164707.png', NULL, NULL, '2020-06-02 16:47:07', '2020-06-02 16:47:07'),
(266, '204', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200602164728.png', '1', NULL, '2020-06-02 16:47:28', '2020-06-02 16:47:38'),
(267, '102', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200602180742.jpeg', NULL, NULL, '2020-06-02 18:07:42', '2020-06-02 18:07:42'),
(269, '66', NULL, '35', 'setting-up-medical-alerts-on-digital-devices', 'users/983.jpg', '1', NULL, '2020-06-03 18:14:26', '2020-06-03 18:14:31'),
(270, '66', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/983.jpg', '1', NULL, '2020-06-03 18:24:51', '2020-06-03 18:24:59'),
(271, '40', NULL, '37', NULL, NULL, '1', NULL, '2020-06-03 18:25:21', '2020-06-03 18:25:21'),
(272, '66', NULL, '25', 'i-have-he', 'users/983.jpg', NULL, NULL, '2020-06-03 18:27:08', '2020-06-03 18:27:08'),
(273, '112', NULL, '35', 'setting-up-medical-alerts-on-digital-devices', 'users/QXZlbnRyaWFfTE9HT19mdWxsIGNvbG9yIEhvcml6b250YWwucG5n20200511163710.png', NULL, NULL, '2020-06-03 18:38:48', '2020-06-03 18:38:48'),
(274, '68', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'userstemplogos/20200603204735.jpeg', NULL, NULL, '2020-06-03 20:47:35', '2020-06-03 20:47:35'),
(275, '112', NULL, '41', 'cirrhosis-and-its-complications', 'users/QXZlbnRyaWFfTE9HT19mdWxsIGNvbG9yIEhvcml6b250YWwucG5n20200511163710.png', NULL, NULL, '2020-06-03 20:51:01', '2020-06-03 20:51:01'),
(276, '112', NULL, '21', 'assess-he-with-the-stroop-test', 'users/UHJvZmlsZSBsb2dvLmpwZw==20200604043654.jpeg', NULL, NULL, '2020-06-04 04:37:35', '2020-06-04 04:37:35'),
(277, '112', NULL, '21', 'assess-he-with-the-stroop-test', 'users/bmFtZS5wbmc=20200604044100.png', NULL, NULL, '2020-06-04 04:41:23', '2020-06-04 04:41:23'),
(278, '112', NULL, '21', 'assess-he-with-the-stroop-test', 'users/bmFtZS5wbmc=20200604044404.png', NULL, NULL, '2020-06-04 04:44:19', '2020-06-04 04:44:19'),
(279, '68', NULL, '41', 'cirrhosis-and-its-complications', 'users/656.jpg', NULL, NULL, '2020-06-04 15:48:01', '2020-06-04 15:48:01'),
(280, '213', NULL, '21', 'assess-he-with-the-stroop-test', 'users/default.png', NULL, NULL, '2020-06-04 21:43:38', '2020-06-04 21:43:38'),
(281, '213', NULL, '21', 'assess-he-with-the-stroop-test', 'users/default.png', NULL, NULL, '2020-06-04 21:43:53', '2020-06-04 21:43:53'),
(284, '109', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200605120934.jpeg', '1', NULL, '2020-06-05 12:09:34', '2020-06-05 12:12:23'),
(285, '109', NULL, '21', 'assess-he-with-the-stroop-test', 'users/U2V0dGluZyBVcCBNZWRpY2FsIEFsZXJ0cy5qcGc=20200508054451.jpeg', NULL, NULL, '2020-06-05 12:12:11', '2020-06-05 12:12:11'),
(286, '109', NULL, '21', 'assess-he-with-the-stroop-test', 'users/U2V0dGluZyBVcCBNZWRpY2FsIEFsZXJ0cy5qcGc=20200508054451.jpeg', NULL, NULL, '2020-06-05 12:12:14', '2020-06-05 12:12:14'),
(287, '109', NULL, '21', 'assess-he-with-the-stroop-test', 'users/U2V0dGluZyBVcCBNZWRpY2FsIEFsZXJ0cy5qcGc=20200508054451.jpeg', '1', NULL, '2020-06-05 12:13:03', '2020-06-05 12:13:07'),
(288, '112', NULL, '21', 'assess-he-with-the-stroop-test', 'users/bmFtZS5wbmc=20200604044404.png', NULL, NULL, '2020-06-05 13:00:22', '2020-06-05 13:00:22'),
(289, '112', NULL, '41', 'cirrhosis-and-its-complications', 'users/bmFtZS5wbmc=20200604044404.png', NULL, NULL, '2020-06-05 13:28:30', '2020-06-05 13:28:30'),
(290, '160', NULL, '23', 'goal-setting-when-you-have-chronic-liver-disease', 'users/default.png', NULL, NULL, '2020-06-05 13:39:15', '2020-06-05 13:39:15'),
(291, '160', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', '1', NULL, '2020-06-05 13:40:32', '2020-06-05 13:40:45'),
(292, '160', NULL, '44', NULL, NULL, '1', NULL, '2020-06-05 13:41:11', '2020-06-05 13:41:11'),
(293, '160', NULL, '25', 'i-have-he', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', '1', NULL, '2020-06-05 13:42:13', '2020-06-05 13:42:22'),
(294, '68', NULL, '24', 'help-patients-set-goals-using-shared-decision-making', 'users/656.jpg', NULL, NULL, '2020-06-05 13:47:58', '2020-06-05 13:47:58'),
(295, '68', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'users/656.jpg', NULL, NULL, '2020-06-05 13:48:37', '2020-06-05 13:48:37'),
(296, '119', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200608142652.png', NULL, NULL, '2020-06-08 14:26:52', '2020-06-08 14:26:52'),
(297, '119', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200608151023.png', NULL, NULL, '2020-06-08 15:10:23', '2020-06-08 15:10:23'),
(298, '119', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'userstemplogos/20200608151352.png', NULL, NULL, '2020-06-08 15:13:52', '2020-06-08 15:13:52'),
(299, '114', NULL, '25', NULL, NULL, '1', NULL, '2020-06-08 18:32:29', '2020-06-08 18:32:29'),
(300, '119', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'userstemplogos/20200611044826.png', NULL, NULL, '2020-06-11 04:48:26', '2020-06-11 04:48:26'),
(301, '119', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'userstemplogos/20200611044839.png', NULL, NULL, '2020-06-11 04:48:39', '2020-06-11 04:48:39'),
(302, '119', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'userstemplogos/20200611045005.png', NULL, NULL, '2020-06-11 04:50:05', '2020-06-11 04:50:05'),
(303, '119', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'userstemplogos/20200611045637.png', NULL, NULL, '2020-06-11 04:56:37', '2020-06-11 04:56:37'),
(304, '119', NULL, '42', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'userstemplogos/20200611045907.png', NULL, NULL, '2020-06-11 04:59:07', '2020-06-11 04:59:07'),
(305, '119', NULL, '32', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'userstemplogos/20200611045958.png', NULL, NULL, '2020-06-11 04:59:58', '2020-06-11 04:59:58'),
(306, '119', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200611050352.png', NULL, NULL, '2020-06-11 05:03:52', '2020-06-11 05:03:52'),
(307, '37', NULL, '25', 'i-have-he', 'userstemplogos/20200611150353.png', '1', NULL, '2020-06-11 15:03:53', '2020-06-11 15:04:00'),
(308, '66', NULL, '23', 'goal-setting-when-you-have-chronic-liver-disease', 'users/983.jpg', NULL, NULL, '2020-06-12 18:41:13', '2020-06-12 18:41:13'),
(309, '66', NULL, '32', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'users/983.jpg', NULL, NULL, '2020-06-12 18:43:57', '2020-06-12 18:43:57'),
(310, '66', NULL, '23', 'goal-setting-when-you-have-chronic-liver-disease', 'users/983.jpg', NULL, NULL, '2020-06-12 19:10:15', '2020-06-12 19:10:15'),
(311, '66', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/983.jpg', NULL, NULL, '2020-06-12 19:11:37', '2020-06-12 19:11:37'),
(312, '37', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200615160746.png', '1', NULL, '2020-06-15 16:07:46', '2020-06-15 16:07:53'),
(313, '66', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/983.jpg', '1', NULL, '2020-06-15 20:20:21', '2020-06-15 20:20:27'),
(314, '66', NULL, '23', 'goal-setting-when-you-have-chronic-liver-disease', 'users/983.jpg', '1', NULL, '2020-06-15 20:23:02', '2020-06-15 20:23:10'),
(315, '244', NULL, '21', 'assess-he-with-the-stroop-test', 'users/default.png', NULL, NULL, '2020-06-18 09:42:53', '2020-06-18 09:42:53'),
(316, '244', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200618094357.png', NULL, NULL, '2020-06-18 09:43:57', '2020-06-18 09:43:57'),
(317, '244', NULL, '21', 'assess-he-with-the-stroop-test', 'users/default.png', NULL, NULL, '2020-06-18 09:47:25', '2020-06-18 09:47:25'),
(320, '50', NULL, '25', 'i-have-he', 'userstemplogos/20200622131445.png', '1', NULL, '2020-06-22 13:14:45', '2020-06-22 13:17:01'),
(321, '112', NULL, '21', 'assess-he-with-the-stroop-test', 'users/bmFtZS5wbmc=20200604044404.png', NULL, NULL, '2020-06-22 16:43:17', '2020-06-22 16:43:17'),
(322, '112', NULL, '21', 'assess-he-with-the-stroop-test', 'users/bmFtZS5wbmc=20200604044404.png', NULL, NULL, '2020-06-22 16:43:36', '2020-06-22 16:43:36'),
(323, '112', NULL, '21', 'assess-he-with-the-stroop-test', 'users/bmFtZS5wbmc=20200604044404.png', NULL, NULL, '2020-06-22 16:43:48', '2020-06-22 16:43:48'),
(324, '112', NULL, '41', 'cirrhosis-and-its-complications', 'users/bmFtZS5wbmc=20200604044404.png', NULL, NULL, '2020-06-22 16:43:55', '2020-06-22 16:43:55'),
(325, '112', NULL, '41', 'cirrhosis-and-its-complications', 'users/bmFtZS5wbmc=20200604044404.png', NULL, NULL, '2020-06-22 17:05:01', '2020-06-22 17:05:01'),
(327, '50', NULL, '37', 'symptoms-complications-and-management-of-cirrhosis', 'userstemplogos/20200622182212.png', NULL, NULL, '2020-06-22 18:22:12', '2020-06-22 18:22:12'),
(328, '50', NULL, '37', 'symptoms-complications-and-management-of-cirrhosis', 'userstemplogos/20200622182235.png', '1', NULL, '2020-06-22 18:22:35', '2020-06-22 18:23:34'),
(329, '246', NULL, '24', NULL, NULL, '1', NULL, '2020-06-24 16:36:14', '2020-06-24 16:36:14'),
(330, '66', NULL, '41', 'cirrhosis-and-its-complications', 'users/983.jpg', '1', NULL, '2020-06-24 20:21:10', '2020-06-24 20:21:29'),
(331, '66', NULL, '41', 'cirrhosis-and-its-complications', 'users/983.jpg', NULL, NULL, '2020-06-24 20:46:17', '2020-06-24 20:46:17'),
(332, '187', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'users/default.png', NULL, NULL, '2020-06-25 02:53:43', '2020-06-25 02:53:43'),
(333, '187', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'users/default.png', NULL, NULL, '2020-06-25 02:53:44', '2020-06-25 02:53:44'),
(334, '112', NULL, '41', 'cirrhosis-and-its-complications', 'users/bmFtZS5wbmc=20200604044404.png', NULL, NULL, '2020-06-25 03:42:12', '2020-06-25 03:42:12'),
(335, '112', NULL, '41', 'cirrhosis-and-its-complications', 'users/bmFtZS5wbmc=20200604044404.png', NULL, NULL, '2020-06-29 13:32:06', '2020-06-29 13:32:06'),
(336, '112', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'users/bmFtZS5wbmc=20200604044404.png', NULL, NULL, '2020-06-29 13:33:19', '2020-06-29 13:33:19'),
(337, '115', NULL, '42', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'userstemplogos/20200629182358.gif', NULL, NULL, '2020-06-29 18:23:58', '2020-06-29 18:23:58'),
(339, '50', NULL, '43', 'prevent-another-attack-from-overt-hepatic-encephalopathy', 'userstemplogos/20200630144116.png', '1', NULL, '2020-06-30 14:41:16', '2020-06-30 14:42:17'),
(340, '165', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200630154903.png', NULL, NULL, '2020-06-30 15:49:03', '2020-06-30 15:49:03'),
(341, '165', NULL, '41', 'cirrhosis-and-its-complications', 'users/default.png', NULL, NULL, '2020-06-30 15:49:25', '2020-06-30 15:49:25'),
(342, '165', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200630154952.png', NULL, NULL, '2020-06-30 15:49:52', '2020-06-30 15:49:52'),
(343, '165', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200630155229.png', NULL, NULL, '2020-06-30 15:52:29', '2020-06-30 15:52:29'),
(344, '165', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200630160719.png', '1', NULL, '2020-06-30 16:07:19', '2020-06-30 16:20:50'),
(345, '165', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200630162337.png', '1', NULL, '2020-06-30 16:23:37', '2020-06-30 16:24:18'),
(347, '165', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200630164118.png', NULL, NULL, '2020-06-30 16:41:18', '2020-06-30 16:41:18'),
(348, '165', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200630164227.png', NULL, NULL, '2020-06-30 16:42:27', '2020-06-30 16:42:27'),
(349, '165', NULL, '41', 'cirrhosis-and-its-complications', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-06-30 16:43:44', '2020-06-30 16:43:44'),
(350, '66', NULL, '41', 'cirrhosis-and-its-complications', 'users/983.jpg', NULL, NULL, '2020-06-30 20:03:29', '2020-06-30 20:03:29'),
(351, '66', NULL, '41', 'cirrhosis-and-its-complications', 'users/983.jpg', '1', NULL, '2020-06-30 20:05:51', '2020-06-30 20:05:58'),
(352, '66', NULL, '21', 'assess-he-with-the-stroop-test', 'users/983.jpg', NULL, NULL, '2020-06-30 20:14:51', '2020-06-30 20:14:51'),
(353, '165', NULL, '41', 'cirrhosis-and-its-complications', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-07-02 17:04:20', '2020-07-02 17:04:20'),
(354, '50', NULL, '36', 'stages-and-types-of-liver-disease', 'userstemplogos/20200702201127.png', '1', NULL, '2020-07-02 20:11:27', '2020-07-02 20:15:24'),
(357, '102', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200708134235.jpeg', '1', NULL, '2020-07-08 13:42:35', '2020-07-08 13:44:22'),
(358, '142', NULL, '41', NULL, NULL, '1', NULL, '2020-07-08 17:22:16', '2020-07-08 17:22:16'),
(359, '142', NULL, '28', NULL, NULL, '1', NULL, '2020-07-08 17:23:22', '2020-07-08 17:23:22'),
(360, '142', NULL, '30', NULL, NULL, '1', NULL, '2020-07-08 17:23:48', '2020-07-08 17:23:48'),
(361, '142', NULL, '32', NULL, NULL, '1', NULL, '2020-07-08 17:23:57', '2020-07-08 17:23:57'),
(362, '142', NULL, '38', NULL, NULL, '1', NULL, '2020-07-08 17:24:58', '2020-07-08 17:24:58'),
(363, '36', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200709125521.jpeg', '1', NULL, '2020-07-09 12:55:21', '2020-07-09 12:56:29'),
(364, '36', NULL, '42', NULL, NULL, '1', NULL, '2020-07-09 16:13:12', '2020-07-09 16:13:12'),
(365, '36', NULL, '39', 'west-haven-criteria', 'userstemplogos/20200709175000.jpeg', NULL, NULL, '2020-07-09 17:50:00', '2020-07-09 17:50:00'),
(366, '66', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/983.jpg', NULL, NULL, '2020-07-09 20:20:26', '2020-07-09 20:20:26'),
(367, '156', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'userstemplogos/20200710131347.png', '1', NULL, '2020-07-10 13:13:47', '2020-08-21 13:31:23'),
(368, '156', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'userstemplogos/20200710131458.png', NULL, NULL, '2020-07-10 13:14:58', '2020-07-10 13:14:58'),
(369, '156', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/default.png', NULL, NULL, '2020-07-10 13:15:03', '2020-07-10 13:15:03'),
(370, '156', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/default.png', NULL, NULL, '2020-07-10 13:15:20', '2020-07-10 13:15:20'),
(371, '156', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'userstemplogos/20200710131635.png', NULL, NULL, '2020-07-10 13:16:35', '2020-07-10 13:16:35'),
(372, '36', NULL, '32', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'userstemplogos/20200710144430.png', NULL, NULL, '2020-07-10 14:44:30', '2020-07-10 14:44:30'),
(373, '36', NULL, '39', 'west-haven-criteria', 'userstemplogos/20200710144543.png', NULL, NULL, '2020-07-10 14:45:43', '2020-07-10 14:45:43'),
(374, '36', NULL, '42', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'userstemplogos/20200710144632.png', NULL, NULL, '2020-07-10 14:46:32', '2020-07-10 14:46:32'),
(375, '36', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'userstemplogos/20200710144735.png', NULL, NULL, '2020-07-10 14:47:35', '2020-07-10 14:47:35'),
(376, '156', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'userstemplogos/20200710145000.png', NULL, NULL, '2020-07-10 14:50:00', '2020-07-10 14:50:00'),
(377, '201', NULL, '21', NULL, NULL, '1', NULL, '2020-07-13 16:37:59', '2020-07-13 16:37:59'),
(378, '163', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200713214645.png', NULL, NULL, '2020-07-13 21:46:45', '2020-07-13 21:46:45'),
(379, '163', NULL, '41', 'cirrhosis-and-its-complications', 'users/default.png', NULL, NULL, '2020-07-13 21:47:07', '2020-07-13 21:47:07'),
(380, '163', NULL, '41', 'cirrhosis-and-its-complications', 'users/default.png', NULL, NULL, '2020-07-13 21:47:08', '2020-07-13 21:47:08'),
(381, '163', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200713214729.png', NULL, NULL, '2020-07-13 21:47:29', '2020-07-13 21:47:29'),
(382, '163', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'userstemplogos/20200713215148.png', NULL, NULL, '2020-07-13 21:51:48', '2020-07-13 21:51:48'),
(383, '163', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'userstemplogos/20200713215200.png', NULL, NULL, '2020-07-13 21:52:00', '2020-07-13 21:52:00'),
(384, '266', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200714154004.jpeg', NULL, NULL, '2020-07-14 15:40:04', '2020-07-14 15:40:04'),
(385, '266', NULL, '21', 'assess-he-with-the-stroop-test', 'users/default.png', NULL, NULL, '2020-07-14 15:40:14', '2020-07-14 15:40:14'),
(387, '266', NULL, '21', 'assess-he-with-the-stroop-test', 'users/default.png', NULL, NULL, '2020-07-14 15:42:18', '2020-07-14 15:42:18'),
(388, '115', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200715173733.jpeg', NULL, NULL, '2020-07-15 17:37:33', '2020-07-15 17:37:33'),
(391, '163', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200715232049.png', NULL, NULL, '2020-07-15 23:20:49', '2020-07-15 23:20:49'),
(392, '163', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'userstemplogos/20200715232221.png', NULL, NULL, '2020-07-15 23:22:21', '2020-07-15 23:22:21'),
(393, '138', NULL, '25', NULL, NULL, '1', NULL, '2020-07-15 23:59:12', '2020-07-15 23:59:12'),
(394, '138', NULL, '26', NULL, NULL, '1', NULL, '2020-07-15 23:59:40', '2020-07-15 23:59:40'),
(395, '138', NULL, '27', NULL, NULL, '1', NULL, '2020-07-16 00:00:11', '2020-07-16 00:00:11'),
(396, '138', NULL, '28', NULL, NULL, '1', NULL, '2020-07-16 00:00:59', '2020-07-16 00:00:59'),
(397, '138', NULL, '30', NULL, NULL, '1', NULL, '2020-07-16 00:02:06', '2020-07-16 00:02:06'),
(398, '138', NULL, '42', NULL, NULL, '1', NULL, '2020-07-16 00:02:43', '2020-07-16 00:02:43'),
(399, '138', NULL, '44', NULL, NULL, '1', NULL, '2020-07-16 00:06:08', '2020-07-16 00:06:08'),
(400, '138', NULL, '43', NULL, NULL, '1', NULL, '2020-07-16 00:06:57', '2020-07-16 00:06:57'),
(401, '138', NULL, '35', NULL, NULL, '1', NULL, '2020-07-16 00:07:19', '2020-07-16 00:07:19'),
(402, '50', NULL, '28', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'userstemplogos/20200716124745.png', '1', NULL, '2020-07-16 12:47:45', '2020-10-14 17:16:12'),
(403, '50', NULL, '28', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'userstemplogos/20200716124817.png', NULL, NULL, '2020-07-16 12:48:17', '2020-07-16 12:48:17'),
(404, '50', NULL, '32', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'userstemplogos/20200716133530.png', '1', NULL, '2020-07-16 13:35:30', '2020-07-16 13:36:54'),
(406, '95', NULL, '30', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'userstemplogos/20200716185843.png', NULL, NULL, '2020-07-16 18:58:43', '2020-07-16 18:58:43'),
(407, '95', NULL, '44', NULL, NULL, '1', NULL, '2020-07-23 13:55:10', '2020-07-23 13:55:10'),
(408, '36', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'userstemplogos/20200728161937.jpeg', NULL, NULL, '2020-07-28 16:19:37', '2020-07-28 16:19:37'),
(409, '50', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200729132400.png', '1', NULL, '2020-07-29 13:24:00', '2020-07-29 13:24:25'),
(410, '112', NULL, '42', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'users/bmFtZS5wbmc=20200604044404.png', NULL, NULL, '2020-07-30 14:31:41', '2020-07-30 14:31:41'),
(411, '112', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/bmFtZS5wbmc=20200604044404.png', NULL, NULL, '2020-07-30 15:21:11', '2020-07-30 15:21:11'),
(412, '160', NULL, '21', 'assess-he-with-the-stroop-test', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 20:14:40', '2020-07-30 20:14:40'),
(413, '160', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200730202122.jpeg', NULL, NULL, '2020-07-30 20:21:22', '2020-07-30 20:21:22'),
(414, '160', NULL, '21', 'assess-he-with-the-stroop-test', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 20:22:59', '2020-07-30 20:22:59'),
(415, '160', NULL, '41', 'cirrhosis-and-its-complications', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 20:23:16', '2020-07-30 20:23:16'),
(416, '160', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 20:25:46', '2020-07-30 20:25:46'),
(417, '160', NULL, '41', 'cirrhosis-and-its-complications', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 20:30:05', '2020-07-30 20:30:05'),
(418, '160', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'userstemplogos/20200730203435.jpeg', NULL, NULL, '2020-07-30 20:34:35', '2020-07-30 20:34:35'),
(419, '160', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 21:09:47', '2020-07-30 21:09:47'),
(420, '160', NULL, '22', 'discuss-patient-journaling-with-your-patients', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 21:10:56', '2020-07-30 21:10:56'),
(421, '160', NULL, '23', 'goal-setting-when-you-have-chronic-liver-disease', 'userstemplogos/20200730211206.jpeg', NULL, NULL, '2020-07-30 21:12:06', '2020-07-30 21:12:06'),
(422, '160', NULL, '24', 'help-patients-set-goals-using-shared-decision-making', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 21:13:08', '2020-07-30 21:13:08'),
(423, '160', NULL, '25', 'i-have-he', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 21:16:08', '2020-07-30 21:16:08'),
(424, '160', NULL, '26', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'userstemplogos/20200730211750.jpeg', NULL, NULL, '2020-07-30 21:17:50', '2020-07-30 21:17:50'),
(425, '160', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 21:18:31', '2020-07-30 21:18:31'),
(426, '160', NULL, '28', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'userstemplogos/20200730211938.jpeg', '1', NULL, '2020-07-30 21:19:38', '2020-07-30 21:19:57'),
(427, '160', NULL, '29', 'living-with-diabetes-and-liver-disease', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 21:20:12', '2020-07-30 21:20:12'),
(428, '160', NULL, '30', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 21:21:14', '2020-07-30 21:21:14'),
(429, '160', NULL, '42', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 21:21:57', '2020-07-30 21:21:57'),
(430, '160', NULL, '32', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'userstemplogos/20200730212235.jpeg', NULL, NULL, '2020-07-30 21:22:35', '2020-07-30 21:22:35'),
(431, '160', NULL, '32', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 21:23:24', '2020-07-30 21:23:24'),
(432, '160', NULL, '44', 'nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 21:23:44', '2020-07-30 21:23:44'),
(433, '160', NULL, '43', 'prevent-another-attack-from-overt-hepatic-encephalopathy', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 21:24:24', '2020-07-30 21:24:24'),
(434, '160', NULL, '35', 'setting-up-medical-alerts-on-digital-devices', 'userstemplogos/20200730212507.jpeg', NULL, NULL, '2020-07-30 21:25:07', '2020-07-30 21:25:07'),
(435, '160', NULL, '36', 'stages-and-types-of-liver-disease', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 21:26:08', '2020-07-30 21:26:08'),
(436, '160', NULL, '37', 'symptoms-complications-and-management-of-cirrhosis', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 21:27:18', '2020-07-30 21:27:18'),
(437, '160', NULL, '38', 'the-importance-of-patient-journaling', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 21:27:53', '2020-07-30 21:27:53'),
(438, '160', NULL, '39', 'west-haven-criteria', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 21:28:41', '2020-07-30 21:28:41'),
(439, '160', NULL, '40', 'what-is-hepatic-encephalopathy', 'userstemplogos/20200730212930.jpeg', NULL, NULL, '2020-07-30 21:29:30', '2020-07-30 21:29:30'),
(440, '160', NULL, '40', 'what-is-hepatic-encephalopathy', 'userstemplogos/20200730212944.jpeg', NULL, NULL, '2020-07-30 21:29:44', '2020-07-30 21:29:44'),
(441, '160', NULL, '28', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-30 21:30:32', '2020-07-30 21:30:32'),
(442, '160', NULL, '21', 'assess-he-with-the-stroop-test', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-31 13:32:56', '2020-07-31 13:32:56'),
(443, '160', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'userstemplogos/20200731133433.jpeg', NULL, NULL, '2020-07-31 13:34:33', '2020-07-31 13:34:33'),
(444, '160', NULL, '22', 'discuss-patient-journaling-with-your-patients', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-07-31 13:35:12', '2020-07-31 13:35:12'),
(445, '160', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'userstemplogos/20200731133904.jpeg', NULL, NULL, '2020-07-31 13:39:04', '2020-07-31 13:39:04'),
(446, '280', NULL, '39', 'west-haven-criteria', 'userstemplogos/20200731175634.docx', NULL, NULL, '2020-07-31 17:56:34', '2020-07-31 17:56:34'),
(447, '280', NULL, '39', 'west-haven-criteria', 'users/default.png', NULL, NULL, '2020-07-31 17:57:08', '2020-07-31 17:57:08'),
(448, '138', NULL, '41', NULL, NULL, '1', NULL, '2020-07-31 23:00:35', '2020-07-31 23:00:35'),
(449, '138', NULL, '20', NULL, NULL, '1', NULL, '2020-07-31 23:00:51', '2020-07-31 23:00:51'),
(450, '138', NULL, '23', NULL, NULL, '1', NULL, '2020-07-31 23:01:05', '2020-07-31 23:01:05'),
(451, '138', NULL, '36', NULL, NULL, '1', NULL, '2020-07-31 23:02:44', '2020-07-31 23:02:44'),
(452, '138', NULL, '37', NULL, NULL, '1', NULL, '2020-07-31 23:02:55', '2020-07-31 23:02:55'),
(453, '115', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200804194339.jpeg', NULL, NULL, '2020-08-04 19:43:39', '2020-08-04 19:43:39'),
(454, '115', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200805000819.jpeg', NULL, NULL, '2020-08-05 00:08:19', '2020-08-05 00:08:19'),
(455, '115', NULL, '41', 'cirrhosis-and-its-complications', 'users/2.gif', NULL, NULL, '2020-08-05 00:08:26', '2020-08-05 00:08:26'),
(456, '115', NULL, '41', 'cirrhosis-and-its-complications', 'users/2.gif', NULL, NULL, '2020-08-05 00:08:35', '2020-08-05 00:08:35'),
(457, '115', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/571.jpg', NULL, NULL, '2020-08-05 00:09:19', '2020-08-05 00:09:19'),
(458, '115', NULL, '25', 'i-have-he', 'users/571.jpg', NULL, NULL, '2020-08-05 13:55:13', '2020-08-05 13:55:13'),
(461, '275', NULL, '41', 'cirrhosis-and-its-complications', 'users/default.png', NULL, NULL, '2020-08-06 16:17:01', '2020-08-06 16:17:01'),
(462, '275', NULL, '41', 'cirrhosis-and-its-complications', 'users/default.png', NULL, NULL, '2020-08-06 16:19:55', '2020-08-06 16:19:55'),
(463, '275', NULL, '41', 'cirrhosis-and-its-complications', 'users/default.png', NULL, NULL, '2020-08-06 16:19:57', '2020-08-06 16:19:57'),
(464, '275', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200806162932.docx', NULL, NULL, '2020-08-06 16:29:32', '2020-08-06 16:29:32'),
(465, '275', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200806163029.docx', NULL, NULL, '2020-08-06 16:30:29', '2020-08-06 16:30:29'),
(466, '275', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200806163134.jpeg', NULL, NULL, '2020-08-06 16:31:34', '2020-08-06 16:31:34'),
(467, '50', NULL, '23', 'goal-setting-when-you-have-chronic-liver-disease', 'userstemplogos/20200806185824.png', '1', NULL, '2020-08-06 18:58:24', '2020-08-06 18:59:14'),
(468, '50', NULL, '35', 'setting-up-medical-alerts-on-digital-devices', 'userstemplogos/20200806190131.png', NULL, NULL, '2020-08-06 19:01:31', '2020-08-06 19:01:31'),
(469, '50', NULL, '35', 'setting-up-medical-alerts-on-digital-devices', 'userstemplogos/20200806190154.png', '1', NULL, '2020-08-06 19:01:54', '2020-08-06 19:02:56'),
(470, '50', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'userstemplogos/20200806190648.png', '1', NULL, '2020-08-06 19:06:48', '2020-08-06 19:07:09'),
(471, '50', NULL, '26', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'userstemplogos/20200806190751.png', '1', NULL, '2020-08-06 19:07:51', '2020-08-06 19:08:11'),
(472, '50', NULL, '25', 'i-have-he', 'userstemplogos/20200806190924.png', '1', NULL, '2020-08-06 19:09:24', '2020-08-06 19:09:35'),
(473, '112', NULL, '45', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'users/bmFtZS5wbmc=20200604044404.png', NULL, NULL, '2020-08-06 20:08:09', '2020-08-06 20:08:09'),
(474, '50', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'userstemplogos/20200806201639.png', '1', NULL, '2020-08-06 20:16:39', '2020-08-06 20:18:29'),
(475, '50', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'userstemplogos/20200806202304.png', '1', NULL, '2020-08-06 20:23:04', '2020-08-06 20:23:31'),
(476, '50', NULL, '35', 'setting-up-medical-alerts-on-digital-devices', 'userstemplogos/20200806202505.png', NULL, NULL, '2020-08-06 20:25:05', '2020-08-06 20:25:05'),
(477, '112', NULL, '45', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'users/bmFtZS5wbmc=20200604044404.png', NULL, NULL, '2020-08-09 02:41:21', '2020-08-09 02:41:21'),
(478, '275', NULL, '37', 'symptoms-complications-and-management-of-cirrhosis', 'userstemplogos/20200810164211.docx', NULL, NULL, '2020-08-10 16:42:11', '2020-08-10 16:42:11'),
(479, '112', NULL, '21', 'assess-he-with-the-stroop-test', 'users/bmFtZS5wbmc=20200604044404.png', NULL, NULL, '2020-08-10 19:26:26', '2020-08-10 19:26:26'),
(480, '112', NULL, '41', 'cirrhosis-and-its-complications', 'users/VGV4YXMgSGVhbHRoLmpwZw==20200810192648.jpeg', NULL, NULL, '2020-08-10 19:26:57', '2020-08-10 19:26:57'),
(481, '325', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200811131919.jpeg', NULL, NULL, '2020-08-11 13:19:19', '2020-08-11 13:19:19'),
(482, '325', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200811132056.jpeg', NULL, NULL, '2020-08-11 13:20:56', '2020-08-11 13:20:56'),
(483, '325', NULL, '23', 'goal-setting-when-you-have-chronic-liver-disease', 'userstemplogos/20200811132154.jpeg', NULL, NULL, '2020-08-11 13:21:54', '2020-08-11 13:21:54'),
(484, '325', NULL, '22', 'discuss-patient-journaling-with-your-patients', 'userstemplogos/20200811132259.jpeg', NULL, NULL, '2020-08-11 13:22:59', '2020-08-11 13:22:59'),
(485, '325', NULL, '25', 'i-have-he', 'userstemplogos/20200811132403.jpeg', NULL, NULL, '2020-08-11 13:24:03', '2020-08-11 13:24:03'),
(486, '325', NULL, '35', 'setting-up-medical-alerts-on-digital-devices', 'userstemplogos/20200811132547.jpeg', NULL, NULL, '2020-08-11 13:25:47', '2020-08-11 13:25:47'),
(487, '325', NULL, '35', 'setting-up-medical-alerts-on-digital-devices', 'userstemplogos/20200811134151.png', NULL, NULL, '2020-08-11 13:41:51', '2020-08-11 13:41:51'),
(488, '325', NULL, '35', 'setting-up-medical-alerts-on-digital-devices', 'userstemplogos/20200811134253.jpeg', NULL, NULL, '2020-08-11 13:42:53', '2020-08-11 13:42:53'),
(489, '325', NULL, '35', 'setting-up-medical-alerts-on-digital-devices', 'userstemplogos/20200811134431.png', NULL, NULL, '2020-08-11 13:44:31', '2020-08-11 13:44:31'),
(490, '325', NULL, '35', 'setting-up-medical-alerts-on-digital-devices', 'userstemplogos/20200811134616.png', NULL, NULL, '2020-08-11 13:46:16', '2020-08-11 13:46:16'),
(491, '325', NULL, '35', 'setting-up-medical-alerts-on-digital-devices', 'userstemplogos/20200811134912.jpeg', NULL, NULL, '2020-08-11 13:49:12', '2020-08-11 13:49:12'),
(492, '325', NULL, '30', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'userstemplogos/20200811135004.jpeg', NULL, NULL, '2020-08-11 13:50:04', '2020-08-11 13:50:04'),
(493, '160', NULL, '45', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-08-11 18:40:05', '2020-08-11 18:40:05'),
(494, '160', NULL, '45', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', '1', NULL, '2020-08-11 18:51:22', '2020-08-11 18:56:17'),
(495, '160', NULL, '45', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-08-11 18:56:23', '2020-08-11 18:56:23'),
(496, '160', NULL, '45', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'userstemplogos/20200811185633.jpeg', '1', NULL, '2020-08-11 18:56:33', '2020-08-11 18:56:47'),
(497, '160', NULL, '45', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-08-11 19:06:36', '2020-08-11 19:06:36'),
(498, '160', NULL, '45', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-08-11 19:08:48', '2020-08-11 19:08:48'),
(499, '160', NULL, '23', 'goal-setting-when-you-have-chronic-liver-disease', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-08-11 21:11:27', '2020-08-11 21:11:27'),
(500, '112', NULL, '45', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'users/UGl5dXNoLmpwZw==20200810193049.jpeg', NULL, NULL, '2020-08-12 01:40:28', '2020-08-12 01:40:28'),
(501, '160', NULL, '45', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-08-12 11:43:10', '2020-08-12 11:43:10'),
(502, '304', NULL, '23', 'goal-setting-when-you-have-chronic-liver-disease', 'users/default.png', NULL, NULL, '2020-08-13 14:51:22', '2020-08-13 14:51:22'),
(503, '304', NULL, '23', 'goal-setting-when-you-have-chronic-liver-disease', 'users/default.png', NULL, NULL, '2020-08-13 14:51:32', '2020-08-13 14:51:32'),
(504, '112', NULL, '41', 'cirrhosis-and-its-complications', 'users/UGl5dXNoLmpwZw==20200810193049.jpeg', NULL, NULL, '2020-08-13 16:15:57', '2020-08-13 16:15:57'),
(505, '323', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200814191114.jpeg', NULL, NULL, '2020-08-14 19:11:14', '2020-08-14 19:11:14'),
(506, '323', NULL, '38', 'the-importance-of-patient-journaling', 'userstemplogos/20200814201749.jpeg', NULL, NULL, '2020-08-14 20:17:49', '2020-08-14 20:17:49'),
(508, '351', NULL, '21', 'assess-he-with-the-stroop-test', 'users/default.png', NULL, NULL, '2020-08-19 02:57:50', '2020-08-19 02:57:50'),
(509, '112', NULL, '21', 'assess-he-with-the-stroop-test', 'users/UGl5dXNoLmpwZw==20200810193049.jpeg', NULL, NULL, '2020-08-19 14:12:02', '2020-08-19 14:12:02'),
(510, '167', NULL, '39', NULL, NULL, '1', NULL, '2020-08-19 15:56:23', '2020-08-19 15:56:23'),
(511, '294', NULL, '22', NULL, NULL, '1', NULL, '2020-08-19 16:47:18', '2020-08-19 16:47:18'),
(512, '294', NULL, '20', NULL, NULL, '1', NULL, '2020-08-19 16:49:13', '2020-08-19 16:49:13'),
(513, '331', NULL, '23', NULL, NULL, '1', NULL, '2020-08-19 20:45:58', '2020-08-19 20:45:58'),
(514, '331', NULL, '21', 'assess-he-with-the-stroop-test', 'users/default.png', NULL, NULL, '2020-08-19 20:47:11', '2020-08-19 20:47:11'),
(515, '366', NULL, '28', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'users/default.png', NULL, NULL, '2020-08-20 11:18:01', '2020-08-20 11:18:01'),
(516, '366', NULL, '28', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'users/default.png', NULL, NULL, '2020-08-20 11:18:08', '2020-08-20 11:18:08'),
(518, '112', NULL, '41', 'cirrhosis-and-its-complications', 'users/UGl5dXNoLmpwZw==20200810193049.jpeg', NULL, NULL, '2020-08-20 13:50:50', '2020-08-20 13:50:50'),
(519, '152', NULL, '40', NULL, NULL, '1', NULL, '2020-08-20 15:27:02', '2020-08-20 15:27:02'),
(520, '152', NULL, '45', NULL, NULL, '1', NULL, '2020-08-20 22:35:59', '2020-08-20 22:35:59'),
(521, '152', NULL, '39', NULL, NULL, '1', NULL, '2020-08-20 22:36:32', '2020-08-20 22:36:32'),
(522, '152', NULL, '30', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'users/default.png', NULL, NULL, '2020-08-21 00:01:41', '2020-08-21 00:01:41'),
(523, '370', NULL, '30', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'users/default.png', NULL, NULL, '2020-08-21 15:03:12', '2020-08-21 15:03:12'),
(524, '370', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'users/default.png', NULL, NULL, '2020-08-21 15:07:49', '2020-08-21 15:07:49'),
(525, '370', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/default.png', NULL, NULL, '2020-08-21 15:10:53', '2020-08-21 15:10:53'),
(526, '267', NULL, '21', 'assess-he-with-the-stroop-test', 'users/default.png', NULL, NULL, '2020-08-21 16:11:49', '2020-08-21 16:11:49'),
(527, '326', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200821163811.jpeg', NULL, NULL, '2020-08-21 16:38:11', '2020-08-21 16:38:11'),
(528, '295', NULL, '19', NULL, NULL, '1', NULL, '2020-08-23 14:49:10', '2020-08-23 14:49:10'),
(529, '286', NULL, '39', 'west-haven-criteria', 'users/default.png', NULL, NULL, '2020-08-24 15:18:28', '2020-08-24 15:18:28'),
(530, '286', NULL, '39', 'west-haven-criteria', 'users/default.png', NULL, NULL, '2020-08-24 15:18:33', '2020-08-24 15:18:33');
INSERT INTO `health_tools_favs` (`id`, `user_id`, `healthtool_id`, `healthtool_slug`, `link`, `avatar`, `is_fav`, `org_image`, `created_at`, `updated_at`) VALUES
(531, '160', NULL, '45', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-08-25 13:22:24', '2020-08-25 13:22:24'),
(532, '178', NULL, '30', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'userstemplogos/20200826191853.html', NULL, NULL, '2020-08-26 19:18:53', '2020-08-26 19:18:53'),
(533, '178', NULL, '30', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'users/default.png', NULL, NULL, '2020-08-26 19:24:14', '2020-08-26 19:24:14'),
(534, '178', NULL, '30', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'userstemplogos/20200826192443.jpeg', NULL, NULL, '2020-08-26 19:24:43', '2020-08-26 19:24:43'),
(535, '178', NULL, '30', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'users/default.png', NULL, NULL, '2020-08-26 19:24:59', '2020-08-26 19:24:59'),
(536, '178', NULL, '30', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'userstemplogos/20200826192739.jpeg', '1', NULL, '2020-08-26 19:27:39', '2020-08-26 19:27:42'),
(537, '178', NULL, '30', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'users/default.png', NULL, NULL, '2020-08-26 19:28:03', '2020-08-26 19:28:03'),
(538, '399', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'users/default.png', NULL, NULL, '2020-08-31 15:16:50', '2020-08-31 15:16:50'),
(539, '399', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'users/default.png', NULL, NULL, '2020-08-31 15:16:54', '2020-08-31 15:16:54'),
(540, '399', NULL, '29', NULL, NULL, '1', NULL, '2020-08-31 15:17:13', '2020-08-31 15:17:13'),
(541, '359', NULL, '21', 'assess-he-with-the-stroop-test', 'users/default.png', NULL, NULL, '2020-08-31 15:44:14', '2020-08-31 15:44:14'),
(542, '359', NULL, '21', 'assess-he-with-the-stroop-test', 'users/default.png', NULL, NULL, '2020-08-31 15:44:16', '2020-08-31 15:44:16'),
(543, '112', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/UGl5dXNoLmpwZw==20200810193049.jpeg', NULL, NULL, '2020-09-01 03:14:22', '2020-09-01 03:14:22'),
(544, '112', NULL, '45', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'users/UGl5dXNoLmpwZw==20200810193049.jpeg', NULL, NULL, '2020-09-01 03:22:10', '2020-09-01 03:22:10'),
(545, '244', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/default.png', NULL, NULL, '2020-09-01 13:46:36', '2020-09-01 13:46:36'),
(546, '244', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/default.png', NULL, NULL, '2020-09-01 13:46:36', '2020-09-01 13:46:36'),
(547, '244', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/default.png', NULL, NULL, '2020-09-01 13:46:37', '2020-09-01 13:46:37'),
(548, '244', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/default.png', NULL, NULL, '2020-09-01 13:46:37', '2020-09-01 13:46:37'),
(549, '244', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/default.png', NULL, NULL, '2020-09-01 13:46:38', '2020-09-01 13:46:38'),
(550, '244', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/default.png', NULL, NULL, '2020-09-01 13:46:43', '2020-09-01 13:46:43'),
(551, '391', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'userstemplogos/20200901153533.docx', NULL, NULL, '2020-09-01 15:35:33', '2020-09-01 15:35:33'),
(552, '112', NULL, '21', 'assess-he-with-the-stroop-test', 'users/UGl5dXNoLmpwZw==20200810193049.jpeg', NULL, NULL, '2020-09-01 17:06:26', '2020-09-01 17:06:26'),
(553, '112', NULL, '37', 'symptoms-complications-and-management-of-cirrhosis', 'users/UGl5dXNoLmpwZw==20200810193049.jpeg', NULL, NULL, '2020-09-01 17:07:51', '2020-09-01 17:07:51'),
(554, '116', NULL, '32', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'userstemplogos/20200903162949.png', NULL, NULL, '2020-09-03 16:29:49', '2020-09-03 16:29:49'),
(555, '112', NULL, '21', 'assess-he-with-the-stroop-test', 'users/TmVlcmFsIFNoYWguanBn20200903165547.jpeg', NULL, NULL, '2020-09-03 16:56:10', '2020-09-03 16:56:10'),
(556, '116', NULL, '32', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'userstemplogos/20200903165732.jpeg', NULL, NULL, '2020-09-03 16:57:32', '2020-09-03 16:57:32'),
(557, '112', NULL, '21', 'assess-he-with-the-stroop-test', 'users/TmVlcmFsIFNoYWgxLmpwZw==20200903165818.jpeg', NULL, NULL, '2020-09-03 16:58:34', '2020-09-03 16:58:34'),
(558, '112', NULL, '21', 'assess-he-with-the-stroop-test', 'users/TmVlcmFsIFNoYWguanBn20200903202121.jpeg', NULL, NULL, '2020-09-03 20:21:33', '2020-09-03 20:21:33'),
(559, '115', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200904143848.jpeg', NULL, NULL, '2020-09-04 14:38:48', '2020-09-04 14:38:48'),
(560, '115', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200904144033.png', '1', NULL, '2020-09-04 14:40:33', '2020-09-04 14:40:50'),
(561, '115', NULL, '25', 'i-have-he', 'userstemplogos/20200904144158.png', '1', NULL, '2020-09-04 14:41:58', '2020-09-04 14:42:07'),
(562, '356', NULL, '22', NULL, NULL, '1', NULL, '2020-09-09 23:51:57', '2020-09-09 23:51:57'),
(563, '356', NULL, '21', NULL, NULL, '1', NULL, '2020-09-09 23:52:58', '2020-09-09 23:52:58'),
(564, '294', NULL, '21', 'assess-he-with-the-stroop-test', 'users/default.png', NULL, NULL, '2020-09-14 16:35:21', '2020-09-14 16:35:21'),
(565, '294', NULL, '21', 'assess-he-with-the-stroop-test', 'users/default.png', NULL, NULL, '2020-09-14 16:35:23', '2020-09-14 16:35:23'),
(566, '231', NULL, '21', 'assess-he-with-the-stroop-test', 'users/default.png', NULL, NULL, '2020-09-15 13:53:22', '2020-09-15 13:53:22'),
(567, '231', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3191', 'userstemplogos/20200915164657.jpeg', NULL, NULL, '2020-09-15 16:47:18', '2020-09-15 16:47:18'),
(568, '231', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3192', 'users/default.png', NULL, NULL, '2020-09-15 16:48:08', '2020-09-15 16:48:08'),
(569, '231', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3193', 'users/default.png', NULL, NULL, '2020-09-15 16:49:40', '2020-09-15 16:49:40'),
(570, '404', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3194', 'users/default.png', NULL, NULL, '2020-09-15 18:01:32', '2020-09-15 18:01:32'),
(571, '404', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3195', 'userstemplogos/20200915180301.jpeg', NULL, NULL, '2020-09-15 18:03:17', '2020-09-15 18:03:17'),
(572, '40', NULL, '22', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3196', 'userstemplogos/20200915182100.jpeg', '1', NULL, '2020-09-15 18:21:21', '2020-09-15 18:21:56'),
(574, '40', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3197', 'users/744.png', '1', NULL, '2020-09-15 18:23:36', '2020-09-16 17:31:52'),
(575, '331', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3198', 'users/default.png', NULL, NULL, '2020-09-15 18:29:10', '2020-09-15 18:29:10'),
(576, '40', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3199', 'userstemplogos/20200916173123.jpeg', NULL, NULL, '2020-09-16 17:31:58', '2020-09-16 17:31:58'),
(577, '266', NULL, '22', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3200', 'userstemplogos/20200916184104.png', NULL, NULL, '2020-09-16 18:41:28', '2020-09-16 18:41:28'),
(578, '266', NULL, '22', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3201', 'userstemplogos/20200916184343.jpeg', NULL, NULL, '2020-09-16 18:43:59', '2020-09-16 18:43:59'),
(579, '266', NULL, '22', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3202', 'userstemplogos/20200916184516.jpeg', NULL, NULL, '2020-09-16 18:45:33', '2020-09-16 18:45:33'),
(580, '266', NULL, '22', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3203', 'userstemplogos/20200916184733.jpeg', NULL, NULL, '2020-09-16 18:47:49', '2020-09-16 18:47:49'),
(581, '266', NULL, '22', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3204', 'userstemplogos/20200916184804.jpeg', NULL, NULL, '2020-09-16 18:48:20', '2020-09-16 18:48:20'),
(582, '266', NULL, '22', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3205', 'userstemplogos/20200916185010.jpeg', NULL, NULL, '2020-09-16 18:50:27', '2020-09-16 18:50:27'),
(583, '266', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3206', 'userstemplogos/20200916185137.jpeg', NULL, NULL, '2020-09-16 18:51:52', '2020-09-16 18:51:52'),
(584, '266', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3207', 'users/default.png', NULL, NULL, '2020-09-16 18:52:54', '2020-09-16 18:52:54'),
(585, '266', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3208', 'users/default.png', NULL, NULL, '2020-09-16 18:53:26', '2020-09-16 18:53:26'),
(586, '266', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3209', 'userstemplogos/20200916185412.jpeg', NULL, NULL, '2020-09-16 18:54:27', '2020-09-16 18:54:27'),
(587, '266', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3210', 'userstemplogos/20200916185516.jpeg', NULL, NULL, '2020-09-16 18:55:33', '2020-09-16 18:55:33'),
(588, '266', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3211', 'users/350.jpg', NULL, NULL, '2020-09-16 19:23:44', '2020-09-16 19:23:44'),
(589, '266', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3212', 'users/350.jpg', NULL, NULL, '2020-09-16 19:23:46', '2020-09-16 19:23:46'),
(590, '266', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3213', 'userstemplogos/20200916192357.jpeg', NULL, NULL, '2020-09-16 19:24:11', '2020-09-16 19:24:11'),
(591, '266', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3214', 'users/350.jpg', NULL, NULL, '2020-09-16 19:24:53', '2020-09-16 19:24:53'),
(592, '266', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3215', 'users/350.jpg', NULL, NULL, '2020-09-16 19:26:03', '2020-09-16 19:26:03'),
(593, '266', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3216', 'userstemplogos/20200916192738.jpeg', NULL, NULL, '2020-09-16 19:27:53', '2020-09-16 19:27:53'),
(594, '266', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3217', 'userstemplogos/20200916192752.jpeg', NULL, NULL, '2020-09-16 19:28:08', '2020-09-16 19:28:08'),
(595, '266', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3218', 'userstemplogos/20200917145344.jpeg', NULL, NULL, '2020-09-17 14:54:10', '2020-09-17 14:54:10'),
(596, '266', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3219', 'users/350.jpg', NULL, NULL, '2020-09-17 14:54:28', '2020-09-17 14:54:28'),
(597, '266', NULL, '41', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3220', 'userstemplogos/20200917145636.jpeg', NULL, NULL, '2020-09-17 14:56:51', '2020-09-17 14:56:51'),
(598, '266', NULL, '21', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3221', 'userstemplogos/20200917150125.jpeg', NULL, NULL, '2020-09-17 15:01:41', '2020-09-17 15:01:41'),
(599, '40', NULL, '19', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3222', 'userstemplogos/20200917153432.jpeg', NULL, NULL, '2020-09-17 15:34:57', '2020-09-17 15:34:57'),
(600, '40', NULL, '19', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3223', 'users/744.png', '1', NULL, '2020-09-17 15:36:14', '2020-09-17 15:37:49'),
(601, '40', NULL, '41', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3224', 'userstemplogos/20200917154425.jpeg', '1', NULL, '2020-09-17 15:44:43', '2020-09-17 15:46:44'),
(602, '40', NULL, '23', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3225', 'userstemplogos/20200917171228.jpeg', NULL, NULL, '2020-09-17 17:12:51', '2020-09-17 17:12:51'),
(603, '40', NULL, '23', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3226', 'userstemplogos/20200917171334.jpeg', '1', NULL, '2020-09-17 17:13:48', '2020-09-17 17:15:08'),
(604, '231', NULL, '36', NULL, NULL, '1', NULL, '2020-09-21 14:32:15', '2020-09-21 14:32:15'),
(605, '231', NULL, '45', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3227', 'users/default.png', NULL, NULL, '2020-09-21 14:40:46', '2020-09-21 14:40:46'),
(606, '231', NULL, '45', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3228', 'users/default.png', NULL, NULL, '2020-09-21 14:41:34', '2020-09-21 14:41:34'),
(607, '231', NULL, '45', 'https://pretty-link-test.azurewebsites.net/ace-toc-ohiostateumc/what-is-osteoarthritis-3229', 'users/default.png', NULL, NULL, '2020-09-21 14:42:21', '2020-09-21 14:42:21'),
(608, '266', NULL, '41', NULL, NULL, '1', '1', '2020-09-21 15:55:01', '2020-09-21 15:55:01'),
(609, '266', NULL, '21', NULL, NULL, '1', '1', '2020-09-21 19:49:44', '2020-09-21 19:49:44'),
(610, '266', NULL, '19', NULL, NULL, '1', '1', '2020-09-21 19:51:10', '2020-09-21 19:51:10'),
(611, '244', NULL, '41', NULL, NULL, '1', NULL, '2020-09-22 05:12:37', '2020-09-22 05:12:37'),
(612, '231', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200922065134.jpeg', NULL, NULL, '2020-09-22 06:51:34', '2020-09-22 06:51:34'),
(613, '231', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200922070219.jpeg', NULL, NULL, '2020-09-22 07:02:20', '2020-09-22 07:02:20'),
(614, '231', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200922070303.jpeg', NULL, NULL, '2020-09-22 07:03:03', '2020-09-22 07:03:03'),
(615, '231', NULL, '21', 'assess-he-with-the-stroop-test', 'users/default.png', NULL, NULL, '2020-09-22 07:15:04', '2020-09-22 07:15:04'),
(616, '231', NULL, '21', 'assess-he-with-the-stroop-test', 'users/default.png', NULL, NULL, '2020-09-22 07:16:16', '2020-09-22 07:16:16'),
(617, '231', NULL, '21', 'assess-he-with-the-stroop-test', 'users/default.png', NULL, NULL, '2020-09-22 07:16:32', '2020-09-22 07:16:32'),
(618, '244', NULL, '41', 'cirrhosis-and-its-complications', 'users/default.png', NULL, NULL, '2020-09-22 07:17:11', '2020-09-22 07:17:11'),
(619, '244', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200922071749.svg', NULL, NULL, '2020-09-22 07:17:49', '2020-09-22 07:17:49'),
(620, '244', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200922071816.png', NULL, NULL, '2020-09-22 07:18:16', '2020-09-22 07:18:16'),
(621, '266', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'users/920.jpg', NULL, NULL, '2020-09-22 11:31:19', '2020-09-22 11:31:19'),
(622, '266', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'users/920.jpg', NULL, NULL, '2020-09-22 11:31:20', '2020-09-22 11:31:20'),
(623, '266', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'userstemplogos/20200922113128.jpeg', NULL, NULL, '2020-09-22 11:31:28', '2020-09-22 11:31:28'),
(624, '266', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'users/920.jpg', NULL, NULL, '2020-09-22 11:48:37', '2020-09-22 11:48:37'),
(625, '266', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'users/920.jpg', NULL, NULL, '2020-09-22 11:48:43', '2020-09-22 11:48:43'),
(626, '266', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'userstemplogos/20200922114849.jpeg', NULL, NULL, '2020-09-22 11:48:49', '2020-09-22 11:48:49'),
(627, '266', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'users/920.jpg', NULL, NULL, '2020-09-22 11:54:36', '2020-09-22 11:54:36'),
(628, '266', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'users/920.jpg', NULL, NULL, '2020-09-22 11:54:41', '2020-09-22 11:54:41'),
(629, '266', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'userstemplogos/20200922115448.jpeg', NULL, NULL, '2020-09-22 11:54:48', '2020-09-22 11:54:48'),
(630, '266', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'users/920.jpg', NULL, NULL, '2020-09-22 12:46:06', '2020-09-22 12:46:06'),
(631, '266', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'users/920.jpg', NULL, NULL, '2020-09-22 12:46:15', '2020-09-22 12:46:15'),
(632, '266', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'userstemplogos/20200922124624.jpeg', NULL, NULL, '2020-09-22 12:46:24', '2020-09-22 12:46:24'),
(633, '112', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200922130810.png', NULL, NULL, '2020-09-22 13:08:10', '2020-09-22 13:08:10'),
(634, '455', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200922145206.png', NULL, NULL, '2020-09-22 14:52:06', '2020-09-22 14:52:06'),
(635, '455', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200922145246.png', NULL, NULL, '2020-09-22 14:52:46', '2020-09-22 14:52:46'),
(636, '455', NULL, '37', 'symptoms-complications-and-management-of-cirrhosis', 'userstemplogos/20200922150326.png', NULL, NULL, '2020-09-22 15:03:26', '2020-09-22 15:03:26'),
(637, '455', NULL, '37', 'symptoms-complications-and-management-of-cirrhosis', 'userstemplogos/20200922153602.png', NULL, NULL, '2020-09-22 15:36:02', '2020-09-22 15:36:02'),
(638, '286', NULL, '24', 'help-patients-set-goals-using-shared-decision-making', 'users/default.png', NULL, NULL, '2020-09-22 16:27:18', '2020-09-22 16:27:18'),
(639, '286', NULL, '24', 'help-patients-set-goals-using-shared-decision-making', 'users/default.png', NULL, NULL, '2020-09-22 16:27:19', '2020-09-22 16:27:19'),
(640, '396', NULL, '43', NULL, NULL, '1', NULL, '2020-09-22 16:40:11', '2020-09-22 16:40:11'),
(641, '396', NULL, '27', NULL, NULL, '1', NULL, '2020-09-22 19:52:56', '2020-09-22 19:52:56'),
(642, '165', NULL, '21', 'assess-he-with-the-stroop-test', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', '1', NULL, '2020-09-23 15:48:37', '2020-09-23 15:55:01'),
(643, '396', NULL, '25', NULL, NULL, '1', NULL, '2020-09-23 15:58:10', '2020-09-23 15:58:10'),
(644, '165', NULL, '39', 'west-haven-criteria', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-23 15:58:26', '2020-09-23 15:58:26'),
(645, '165', NULL, '21', 'assess-he-with-the-stroop-test', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-23 16:06:11', '2020-09-23 16:06:11'),
(646, '165', NULL, '41', 'cirrhosis-and-its-complications', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-23 16:07:11', '2020-09-23 16:07:11'),
(647, '165', NULL, '41', 'cirrhosis-and-its-complications', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-24 17:40:40', '2020-09-24 17:40:40'),
(648, '156', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200924174100.png', NULL, NULL, '2020-09-24 17:41:00', '2020-09-24 17:41:00'),
(649, '156', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200924174241.png', NULL, NULL, '2020-09-24 17:42:41', '2020-09-24 17:42:41'),
(650, '156', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200924174255.png', NULL, NULL, '2020-09-24 17:42:55', '2020-09-24 17:42:55'),
(651, '165', NULL, '41', 'cirrhosis-and-its-complications', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-24 17:47:06', '2020-09-24 17:47:06'),
(652, '160', NULL, '41', 'cirrhosis-and-its-complications', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-09-24 19:45:35', '2020-09-24 19:45:35'),
(653, '112', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200924194544.png', NULL, NULL, '2020-09-24 19:45:44', '2020-09-24 19:45:44'),
(654, '165', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 14:29:50', '2020-09-25 14:29:50'),
(655, '165', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 14:49:41', '2020-09-25 14:49:41'),
(656, '165', NULL, '21', 'assess-he-with-the-stroop-test', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 14:50:55', '2020-09-25 14:50:55'),
(657, '165', NULL, '41', 'cirrhosis-and-its-complications', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 14:52:19', '2020-09-25 14:52:19'),
(658, '165', NULL, '22', 'discuss-patient-journaling-with-your-patients', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 14:52:47', '2020-09-25 14:52:47'),
(659, '165', NULL, '23', 'goal-setting-when-you-have-chronic-liver-disease', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 14:53:07', '2020-09-25 14:53:07'),
(660, '165', NULL, '24', 'help-patients-set-goals-using-shared-decision-making', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 14:54:22', '2020-09-25 14:54:22'),
(661, '165', NULL, '24', 'help-patients-set-goals-using-shared-decision-making', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 14:54:23', '2020-09-25 14:54:23'),
(662, '165', NULL, '24', 'help-patients-set-goals-using-shared-decision-making', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 14:54:27', '2020-09-25 14:54:27'),
(663, '165', NULL, '25', 'i-have-he', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 14:54:50', '2020-09-25 14:54:50'),
(664, '165', NULL, '26', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 14:55:22', '2020-09-25 14:55:22'),
(665, '165', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 14:55:46', '2020-09-25 14:55:46'),
(666, '165', NULL, '28', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 14:56:14', '2020-09-25 14:56:14'),
(667, '165', NULL, '29', 'living-with-diabetes-and-liver-disease', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 14:56:38', '2020-09-25 14:56:38'),
(668, '165', NULL, '30', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 15:20:50', '2020-09-25 15:20:50'),
(669, '165', NULL, '42', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 15:21:41', '2020-09-25 15:21:41'),
(670, '165', NULL, '32', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 15:22:14', '2020-09-25 15:22:14'),
(671, '165', NULL, '44', 'nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 15:26:12', '2020-09-25 15:26:12'),
(672, '165', NULL, '43', 'prevent-another-attack-from-overt-hepatic-encephalopathy', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 15:27:00', '2020-09-25 15:27:00'),
(673, '165', NULL, '35', 'setting-up-medical-alerts-on-digital-devices', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 15:27:30', '2020-09-25 15:27:30'),
(674, '165', NULL, '36', 'stages-and-types-of-liver-disease', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 15:28:46', '2020-09-25 15:28:46'),
(675, '165', NULL, '37', 'symptoms-complications-and-management-of-cirrhosis', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 15:29:10', '2020-09-25 15:29:10'),
(676, '165', NULL, '38', 'the-importance-of-patient-journaling', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 15:30:09', '2020-09-25 15:30:09'),
(677, '165', NULL, '45', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 15:30:31', '2020-09-25 15:30:31'),
(678, '165', NULL, '39', 'west-haven-criteria', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 15:31:54', '2020-09-25 15:31:54'),
(679, '165', NULL, '40', 'what-is-hepatic-encephalopathy', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-25 15:32:21', '2020-09-25 15:32:21'),
(680, '383', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200928114112.png', NULL, NULL, '2020-09-28 11:41:12', '2020-09-28 11:41:12'),
(681, '383', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20200928114124.png', NULL, NULL, '2020-09-28 11:41:24', '2020-09-28 11:41:24'),
(682, '383', NULL, '28', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'userstemplogos/20200928115934.png', NULL, NULL, '2020-09-28 11:59:34', '2020-09-28 11:59:34'),
(683, '383', NULL, '43', 'prevent-another-attack-from-overt-hepatic-encephalopathy', 'userstemplogos/20200928120016.png', NULL, NULL, '2020-09-28 12:00:16', '2020-09-28 12:00:16'),
(684, '383', NULL, '35', 'setting-up-medical-alerts-on-digital-devices', 'userstemplogos/20200928120103.png', NULL, NULL, '2020-09-28 12:01:03', '2020-09-28 12:01:03'),
(685, '383', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'userstemplogos/20200928120304.png', NULL, NULL, '2020-09-28 12:03:04', '2020-09-28 12:03:04'),
(686, '165', NULL, '21', 'assess-he-with-the-stroop-test', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-09-28 15:27:07', '2020-09-28 15:27:07'),
(687, '383', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'userstemplogos/20200928170034.png', NULL, NULL, '2020-09-28 17:00:34', '2020-09-28 17:00:34'),
(688, '383', NULL, '35', 'setting-up-medical-alerts-on-digital-devices', 'userstemplogos/20200928170201.png', NULL, NULL, '2020-09-28 17:02:01', '2020-09-28 17:02:01'),
(689, '383', NULL, '21', 'assess-he-with-the-stroop-test', 'userstemplogos/20200928170253.png', NULL, NULL, '2020-09-28 17:02:53', '2020-09-28 17:02:53'),
(690, '383', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'userstemplogos/20200928170441.png', NULL, NULL, '2020-09-28 17:04:41', '2020-09-28 17:04:41'),
(691, '66', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/983.jpg', '1', NULL, '2020-09-30 13:06:35', '2020-09-30 13:06:55'),
(692, '355', NULL, '35', 'setting-up-medical-alerts-on-digital-devices', 'users/default.png', NULL, NULL, '2020-09-30 17:55:49', '2020-09-30 17:55:49'),
(693, '383', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'userstemplogos/20201001160207.jpeg', NULL, NULL, '2020-10-01 16:02:07', '2020-10-01 16:02:07'),
(694, '383', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'userstemplogos/20201001160342.jpeg', NULL, NULL, '2020-10-01 16:03:42', '2020-10-01 16:03:42'),
(695, '302', NULL, '41', NULL, NULL, '1', NULL, '2020-10-01 16:05:17', '2020-10-01 16:05:17'),
(696, '383', NULL, '42', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'userstemplogos/20201001161943.jpeg', NULL, NULL, '2020-10-01 16:19:43', '2020-10-01 16:19:43'),
(697, '383', NULL, '25', 'i-have-he', 'userstemplogos/20201001211147.png', NULL, NULL, '2020-10-01 21:11:47', '2020-10-01 21:11:47'),
(698, '383', NULL, '25', 'i-have-he', 'userstemplogos/20201001211340.jpeg', NULL, NULL, '2020-10-01 21:13:40', '2020-10-01 21:13:40'),
(699, '383', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'userstemplogos/20201001211432.jpeg', NULL, NULL, '2020-10-01 21:14:32', '2020-10-01 21:14:32'),
(700, '383', NULL, '29', 'living-with-diabetes-and-liver-disease', 'userstemplogos/20201001211522.jpeg', NULL, NULL, '2020-10-01 21:15:22', '2020-10-01 21:15:22'),
(701, '383', NULL, '30', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'userstemplogos/20201001211558.jpeg', NULL, NULL, '2020-10-01 21:15:58', '2020-10-01 21:15:58'),
(702, '383', NULL, '43', 'prevent-another-attack-from-overt-hepatic-encephalopathy', 'userstemplogos/20201001211635.jpeg', NULL, NULL, '2020-10-01 21:16:35', '2020-10-01 21:16:35'),
(703, '383', NULL, '35', 'setting-up-medical-alerts-on-digital-devices', 'userstemplogos/20201001211653.jpeg', NULL, NULL, '2020-10-01 21:16:53', '2020-10-01 21:16:53'),
(704, '383', NULL, '36', 'stages-and-types-of-liver-disease', 'userstemplogos/20201001211743.jpeg', NULL, NULL, '2020-10-01 21:17:43', '2020-10-01 21:17:43'),
(705, '383', NULL, '37', 'symptoms-complications-and-management-of-cirrhosis', 'userstemplogos/20201001211813.jpeg', NULL, NULL, '2020-10-01 21:18:13', '2020-10-01 21:18:13'),
(706, '383', NULL, '39', 'west-haven-criteria', 'userstemplogos/20201001211846.jpeg', NULL, NULL, '2020-10-01 21:18:46', '2020-10-01 21:18:46'),
(707, '391', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'userstemplogos/20201005172440.', NULL, NULL, '2020-10-05 17:24:40', '2020-10-05 17:24:40'),
(708, '163', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20201005205159.png', NULL, NULL, '2020-10-05 20:51:59', '2020-10-05 20:51:59'),
(709, '163', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20201005205221.png', NULL, NULL, '2020-10-05 20:52:21', '2020-10-05 20:52:21'),
(710, '255', NULL, '41', 'cirrhosis-and-its-complications', 'users/default.png', NULL, NULL, '2020-10-05 23:22:31', '2020-10-05 23:22:31'),
(711, '255', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20201005232249.png', NULL, NULL, '2020-10-05 23:22:49', '2020-10-05 23:22:49'),
(712, '255', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20201005233641.png', NULL, NULL, '2020-10-05 23:36:41', '2020-10-05 23:36:41'),
(713, '255', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20201005234133.png', NULL, NULL, '2020-10-05 23:41:33', '2020-10-05 23:41:33'),
(714, '255', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20201005234527.png', NULL, NULL, '2020-10-05 23:45:27', '2020-10-05 23:45:27'),
(715, '255', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20201005234921.jpeg', NULL, NULL, '2020-10-05 23:49:21', '2020-10-05 23:49:21'),
(716, '255', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20201006002001.png', '1', NULL, '2020-10-06 00:20:01', '2020-10-06 00:20:20'),
(717, '272', NULL, '37', NULL, NULL, '1', NULL, '2020-10-07 15:12:29', '2020-10-07 15:12:29'),
(718, '165', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-10-07 16:43:02', '2020-10-07 16:43:02'),
(719, '165', NULL, '27', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-10-07 16:43:58', '2020-10-07 16:43:58'),
(720, '165', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL, NULL, '2020-10-07 16:47:01', '2020-10-07 16:47:01'),
(721, '306', NULL, '19', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'userstemplogos/20201008172855.jpeg', NULL, NULL, '2020-10-08 17:28:55', '2020-10-08 17:28:55'),
(722, '306', NULL, '41', 'cirrhosis-and-its-complications', 'userstemplogos/20201009163808.jpeg', NULL, NULL, '2020-10-09 16:38:08', '2020-10-09 16:38:08'),
(723, '302', NULL, '30', NULL, NULL, '1', NULL, '2020-10-13 17:31:14', '2020-10-13 17:31:14'),
(724, '217', NULL, '28', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'users/default.png', NULL, NULL, '2020-10-14 13:25:20', '2020-10-14 13:25:20'),
(725, '217', NULL, '28', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'users/default.png', NULL, NULL, '2020-10-14 13:25:23', '2020-10-14 13:25:23'),
(726, '217', NULL, '28', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'userstemplogos/20201014132533.jpeg', NULL, NULL, '2020-10-14 13:25:33', '2020-10-14 13:25:33'),
(727, '217', NULL, '28', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'users/default.png', '1', NULL, '2020-10-14 13:25:50', '2020-10-14 13:26:00'),
(728, '217', NULL, '28', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'userstemplogos/20201014132629.png', NULL, NULL, '2020-10-14 13:26:29', '2020-10-14 13:26:29'),
(729, '50', NULL, '26', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'userstemplogos/20201014143724.jpeg', '1', NULL, '2020-10-14 14:37:24', '2020-10-14 14:40:36'),
(730, '50', NULL, '28', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'userstemplogos/20201014171531.jpeg', NULL, NULL, '2020-10-14 17:15:31', '2020-10-14 17:15:31'),
(731, '50', NULL, '35', 'setting-up-medical-alerts-on-digital-devices', 'userstemplogos/20201014171753.jpeg', '1', NULL, '2020-10-14 17:17:53', '2020-10-14 17:19:59'),
(732, '50', NULL, '43', 'prevent-another-attack-from-overt-hepatic-encephalopathy', 'userstemplogos/20201014172133.jpeg', NULL, NULL, '2020-10-14 17:21:33', '2020-10-14 17:21:33'),
(733, '112', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/QXZlbnRyaWFMb2dvX3dpZGVfM18wOV8xNyBjb3B5LnBuZw==20201015193106.png', NULL, NULL, '2020-10-15 19:31:47', '2020-10-15 19:31:47'),
(734, '112', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/QXZlbnRyaWFMb2dvX3dpZGVfM18wOV8xNyBjb3B5LnBuZw==20201015193106.png', '1', NULL, '2020-10-15 19:31:57', '2020-10-15 19:32:05'),
(735, '112', NULL, '23', 'goal-setting-when-you-have-chronic-liver-disease', 'users/QXZlbnRyaWFMb2dvX3dpZGVfM18wOV8xNyBjb3B5LnBuZw==20201015193106.png', '1', NULL, '2020-10-15 19:35:03', '2020-10-15 19:35:08'),
(736, '306', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'userstemplogos/20201019155937.jpeg', NULL, NULL, '2020-10-19 15:59:37', '2020-10-19 15:59:37'),
(737, '306', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'userstemplogos/20201019160007.jpeg', NULL, NULL, '2020-10-19 16:00:07', '2020-10-19 16:00:07'),
(738, '160', NULL, '41', 'cirrhosis-and-its-complications', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-10-20 14:51:30', '2020-10-20 14:51:30'),
(739, '160', NULL, '41', 'cirrhosis-and-its-complications', 'users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL, NULL, '2020-10-20 14:51:41', '2020-10-20 14:51:41'),
(740, '167', NULL, '27', NULL, NULL, '1', NULL, '2020-10-20 18:55:59', '2020-10-20 18:55:59'),
(741, '306', NULL, '37', 'symptoms-complications-and-management-of-cirrhosis', 'userstemplogos/20201028144252.jpeg', NULL, NULL, '2020-10-28 14:42:52', '2020-10-28 14:42:52'),
(742, '66', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/983.jpg', NULL, NULL, '2020-10-28 19:56:51', '2020-10-28 19:56:51'),
(743, '187', NULL, '25', 'i-have-he', 'users/default.png', NULL, NULL, '2020-10-28 21:48:10', '2020-10-28 21:48:10'),
(744, '187', NULL, '25', 'i-have-he', 'userstemplogos/20201028215123.docx', NULL, NULL, '2020-10-28 21:51:23', '2020-10-28 21:51:23'),
(745, '187', NULL, '25', 'i-have-he', 'users/default.png', NULL, NULL, '2020-10-28 21:51:59', '2020-10-28 21:51:59'),
(746, '66', NULL, '20', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'users/983.jpg', NULL, NULL, '2020-10-29 15:35:43', '2020-10-29 15:35:43'),
(747, '531', NULL, '29', 'living-with-diabetes-and-liver-disease', 'users/default.png', NULL, NULL, '2020-10-30 15:25:29', '2020-10-30 15:25:29'),
(748, '531', NULL, '29', 'living-with-diabetes-and-liver-disease', 'users/default.png', NULL, NULL, '2020-10-30 15:25:29', '2020-10-30 15:25:29'),
(749, '187', NULL, '29', 'living-with-diabetes-and-liver-disease', 'users/default.png', NULL, NULL, '2020-11-05 16:11:39', '2020-11-05 16:11:39'),
(750, '187', NULL, '29', 'living-with-diabetes-and-liver-disease', 'users/default.png', NULL, NULL, '2020-11-05 16:11:45', '2020-11-05 16:11:45'),
(751, '355', NULL, '19', NULL, NULL, '1', NULL, '2020-11-06 14:20:54', '2020-11-06 14:20:54'),
(752, '95', NULL, '26', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'userstemplogos/20201106150746.png', '1', NULL, '2020-11-06 15:07:46', '2020-11-06 15:08:21'),
(753, '95', NULL, '30', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'userstemplogos/20201108192556.png', '1', NULL, '2020-11-08 19:25:56', '2020-11-08 19:26:38'),
(762, '1', '46', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-1a-early-treatment-in-ms-4454', NULL, NULL, NULL, '2021-04-27 04:54:40', '2021-04-27 04:54:40'),
(763, '568', '52', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-3b-members-of-your-ms-care-team-4455', NULL, NULL, NULL, '2021-04-27 12:36:56', '2021-04-27 12:36:56'),
(764, '568', '49', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-1c-overview-of-telehealth-4456', NULL, NULL, NULL, '2021-04-27 12:37:48', '2021-04-27 12:37:48'),
(765, '591', '53', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-4a-a-guide-to-understanding-ms-and-common-phrases-4457', NULL, NULL, NULL, '2021-04-27 17:10:33', '2021-04-27 17:10:33'),
(767, '591', '48', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-1b-overview-of-clinical-study-measures-in-ms-4463', NULL, NULL, NULL, '2021-04-29 15:09:13', '2021-04-29 15:09:13'),
(772, '574', '53', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-4a-a-guide-to-understanding-ms-and-common-phrases-4486', NULL, NULL, NULL, '2021-05-03 18:19:15', '2021-05-03 18:19:15'),
(774, '573', '53', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-4a-a-guide-to-understanding-ms-and-common-phrases-4487', NULL, NULL, NULL, '2021-05-03 18:20:03', '2021-05-03 18:20:03'),
(775, '574', '50', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-2-understanding-and-addressing-barriers-to-patient-care-in-ms-4490', NULL, NULL, NULL, '2021-05-03 20:19:00', '2021-05-03 20:19:00'),
(780, '593', '46', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-1a-early-treatment-in-ms-4500', NULL, NULL, NULL, '2021-05-06 02:03:49', '2021-05-06 02:03:49'),
(781, '593', '54', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-4b-what-to-expect-in-a-telemedicine-visit-4501', NULL, NULL, NULL, '2021-05-06 02:07:26', '2021-05-06 02:07:26'),
(782, '577', '53', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-4a-a-guide-to-understanding-ms-and-common-phrases-4502', NULL, NULL, NULL, '2021-05-06 05:34:43', '2021-05-06 05:34:43'),
(783, '577', '46', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-1a-early-treatment-in-ms-4503', NULL, NULL, NULL, '2021-05-06 06:25:30', '2021-05-06 06:25:30'),
(784, '568', '55', NULL, 'https://tools.actonms.org/actonms/actonms-4c-talk-to-your-doctor-about-how-medications-can-help-you-manage-your-ms-4508', NULL, NULL, NULL, '2021-05-06 15:58:27', '2021-05-06 15:58:27'),
(787, '591', '46', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-1a-early-treatment-in-ms-4514', NULL, NULL, NULL, '2021-05-19 17:06:56', '2021-05-19 17:06:56'),
(788, '568', '52', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-3b-members-of-your-ms-care-team-4515', NULL, NULL, NULL, '2021-05-19 17:07:08', '2021-05-19 17:07:08'),
(789, '591', '46', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-1a-early-treatment-in-ms-4516', NULL, NULL, NULL, '2021-05-19 17:07:37', '2021-05-19 17:07:37'),
(790, '585', '52', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-3b-members-of-your-ms-care-team-4524', NULL, NULL, NULL, '2021-05-25 15:07:26', '2021-05-25 15:07:26'),
(792, '574', '46', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-1a-early-treatment-in-ms-4528', NULL, NULL, NULL, '2021-05-25 19:07:31', '2021-05-25 19:07:31'),
(793, '589', '53', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-4a-a-guide-to-understanding-ms-and-common-phrases-4532', NULL, NULL, NULL, '2021-05-25 20:07:09', '2021-05-25 20:07:09'),
(794, '574', '53', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-4a-a-guide-to-understanding-ms-and-common-phrases-4536', NULL, NULL, NULL, '2021-05-26 18:45:38', '2021-05-26 18:45:38'),
(796, '574', '52', NULL, 'https://tools.actonms.org/actonms/actonms-3b-members-of-your-ms-care-team-4543', NULL, NULL, NULL, '2021-06-01 16:56:38', '2021-06-01 16:56:38'),
(797, '574', '52', NULL, 'https://tools.actonms.org/actonms/actonms-3b-members-of-your-ms-care-team-4544', NULL, NULL, NULL, '2021-06-01 16:58:07', '2021-06-01 16:58:07'),
(798, '574', '53', NULL, 'https://tools.actonms.org/actonms/actonms-4a-a-guide-to-understanding-ms-and-common-phrases-4557', NULL, NULL, NULL, '2021-06-01 17:52:23', '2021-06-01 17:52:23'),
(799, '568', '52', NULL, 'https://tools.actonms.org/actonms/actonms-3b-members-of-your-ms-care-team-4578', NULL, NULL, NULL, '2021-06-01 21:45:29', '2021-06-01 21:45:29'),
(800, '568', '51', NULL, 'https://tools.actonms.org/actonms/actonms-3a-understanding-the-ms-journey-4581', NULL, NULL, NULL, '2021-06-01 21:51:52', '2021-06-01 21:51:52'),
(802, '567', '52', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-3b-members-of-your-ms-care-team-4611', NULL, NULL, NULL, '2021-06-04 14:57:19', '2021-06-04 14:57:19'),
(803, '567', '46', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-1a-early-treatment-in-ms-4612', NULL, NULL, NULL, '2021-06-04 14:59:21', '2021-06-04 14:59:21'),
(805, '567', '46', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-1a-early-treatment-in-ms-4616', NULL, NULL, NULL, '2021-06-07 14:37:43', '2021-06-07 14:37:43'),
(806, '611', '53', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-4a-a-guide-to-understanding-ms-and-common-phrases-4682', NULL, NULL, NULL, '2021-06-12 12:10:17', '2021-06-12 12:10:17'),
(807, '569', '53', NULL, 'https://tools.actonms.org/ace-iohe-mohe-toc-cch/actonms-4a-a-guide-to-understanding-ms-and-common-phrases-4704', NULL, NULL, NULL, '2021-06-18 06:17:45', '2021-06-18 06:17:45');

-- --------------------------------------------------------

--
-- Table structure for table `homebannercontents`
--

CREATE TABLE `homebannercontents` (
  `id` int(10) UNSIGNED NOT NULL,
  `img` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alt_tag` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sq_no` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `home_page_contents_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `homebannercontents`
--

INSERT INTO `homebannercontents` (`id`, `img`, `alt_tag`, `content`, `sq_no`, `created_at`, `updated_at`, `home_page_contents_id`) VALUES
(1, 'homebannercontents\\July2021\\2PEXj2FUcCNLnPTfu9LR.png', 'slider_1', '<p>.</p>', 1, '2020-02-29 12:14:00', '2021-07-04 23:39:37', NULL),
(2, 'homebannercontents\\June2021\\ScdFk030XAC8luuBUfdM.png', 'slider_2', '<p>.</p>', 2, '2020-02-29 12:26:00', '2021-06-29 01:58:20', 2);

-- --------------------------------------------------------

--
-- Table structure for table `homesliders`
--

CREATE TABLE `homesliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `homesliders`
--

INSERT INTO `homesliders` (`id`, `image`, `title`, `created_at`, `updated_at`) VALUES
(1, 'homesliders/December2020/pO5XKxrJ4qPSLYs3sWSE.png', '<p>Patient engagement in MS has been shown to improve patient and health system outcomes.<sup>2</sup></p>', '2020-11-25 04:43:00', '2020-12-14 16:04:44'),
(2, 'homesliders/December2020/Blx291d02FltFwT8575n.png', '<p>A patient-reported survey reported that people living with MS who engage health care providers (HCPs) during relapses reported fewer relapses than those who do not engage HCPs.<sup>3</sup></p>', '2020-11-25 04:44:00', '2021-04-26 10:59:24'),
(3, 'homesliders/December2020/iyp2mBwBXvhCoJstaj3g.png', '<p>Patients&rsquo; individual care goals, including care of symptoms and disability modification, may be used to help guide treatment strategies.<sup>4</sup></p>', '2020-11-25 04:44:00', '2020-12-15 05:29:40');

-- --------------------------------------------------------

--
-- Table structure for table `home_page_contents`
--

CREATE TABLE `home_page_contents` (
  `id` int(10) UNSIGNED NOT NULL,
  `ref` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitle` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_page_contents`
--

INSERT INTO `home_page_contents` (`id`, `ref`, `subtitle`, `description`, `created_at`, `updated_at`) VALUES
(2, '<ol class=\"order-list\">\r\n<li>Rae-Grant A, Day GS, Marrie RA, et al. Practice guideline recommendations summary: disease-modifying therapies for adults with multiple sclerosis: report of the Guideline Development, Dissemination, and Implementation Subcommittee of the American Academy of Neurology. <em>Neurology</em>. 2018;90:777-788. doi:10.1212/WNL.0000000000005347</li>\r\n<li>Rieckmann P, Boyko A, Centonze D, et al. Achieving patient engagement in multiple sclerosis: a perspective from the multiple sclerosis in the 21st Century Steering Group. <em>Mult Scler Relat Disord</em>. 2015;4(3):202-218.</li>\r\n<li>Nazareth TA, Ravaa AR, Polyakova JL, et al. Relapse prevalence, symptoms, and health care engagement: patient insights from the Multiple Sclerosis in America 2017 survey. <em>Mult Scler Relat Disord</em>. 2018;26:219-234.</li>\r\n<li>Day GS, Rae-Grant A, Armstrong MJ, et al. Identifying priority outcomes that influence selection of disease-modifying therapies in <em>MS. Neurol Clin Pract</em>. 2018;8(3):1-7. doi:10.1212/CPJ.0000000000000449</li>\r\n<li>Rieckmann P, Centonze D, Elovaara I, et al. Unmet needs, burden of treatment, and patient engagement in multiple sclerosis: a combined perspective from the MS in the 21st Century Steering Group. <em>Mult Scler Relat Disord</em>. 2018;19:153-160.</li>\r\n</ol>', '<p>Identifying liver disease patients who have primary biliary cholangitis, then providing those patients with vital education resources about the disease, may proactively reduce the risk of complications and downstream medical costs.</p>\r\n<p>Incorporating guideline recommandations for screening, diagnosis and clinical management from the AASLD PBC Guidance into clinical workflows is an important way to support standardized care and help optimize the care of your patients.</p>', '<p>Help optimize the care of your patients with primary biliary cholangitis (PBC) with resources that support patient engagement.</p>', '2020-11-25 01:25:00', '2021-06-29 03:16:52');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-05-08 03:24:32', '2019-05-08 03:24:32');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, 5, 1, '2019-05-08 03:24:32', '2020-12-15 18:51:52', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, 5, 8, '2019-05-08 03:24:32', '2020-12-15 18:51:52', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, 21, 1, '2019-05-08 03:24:32', '2020-12-15 18:48:21', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, 5, 6, '2019-05-08 03:24:32', '2020-12-15 18:51:52', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 6, '2019-05-08 03:24:32', '2021-04-07 06:55:54', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 2, '2019-05-08 03:24:32', '2020-12-15 18:51:52', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 3, '2019-05-08 03:24:32', '2020-12-15 18:51:52', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 4, '2019-05-08 03:24:32', '2020-12-15 18:51:52', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 5, '2019-05-08 03:24:32', '2020-12-15 18:51:52', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, 5, 9, '2019-05-08 03:24:33', '2020-12-15 18:51:52', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 7, '2019-05-08 03:24:36', '2020-12-15 18:51:52', 'voyager.hooks', NULL),
(12, 1, 'Organization Types', '', '_self', 'voyager-dot', '#000000', 21, 2, '2019-05-08 04:57:32', '2021-04-07 06:52:10', 'voyager.organizationtypes.index', 'null'),
(13, 1, 'Sources', '', '_self', 'voyager-dot', '#000000', 21, 3, '2019-05-08 05:22:00', '2021-04-07 06:52:19', 'voyager.sources.index', 'null'),
(16, 1, 'Target Audiences', '', '_self', 'voyager-dot', '#000000', 20, 1, '2019-05-13 03:47:23', '2021-04-07 06:53:58', 'voyager.targetaudiences.index', 'null'),
(17, 1, 'Patient Types', '', '_self', 'voyager-dot', '#000000', 20, 2, '2019-05-13 03:50:11', '2021-04-23 04:02:42', 'voyager.patienttypes.index', 'null'),
(18, 1, 'Health Tools', '', '_self', 'voyager-dot', '#000000', 20, 3, '2019-05-13 04:05:08', '2021-04-07 06:54:15', 'voyager.healthtools.index', 'null'),
(19, 1, 'Health Tool Static Contents', '', '_self', 'voyager-dot', '#000000', 20, 4, '2019-05-13 05:52:50', '2021-04-07 06:54:30', 'voyager.healthtoolstaticcontents.index', 'null'),
(20, 1, 'Health Tool Management', '', '_self', 'voyager-treasure-open', '#000000', NULL, 4, '2019-05-13 06:17:35', '2021-04-07 05:11:24', NULL, ''),
(21, 1, 'Registration Management', '', '_self', 'voyager-edit', '#000000', NULL, 2, '2019-05-13 06:30:27', '2020-12-15 18:52:14', NULL, ''),
(34, 1, 'Home Banner Contents', '', '_self', 'voyager-dot', '#000000', 39, 1, '2019-05-13 12:24:40', '2021-04-07 06:51:39', 'voyager.homebannercontents.index', 'null'),
(35, 1, 'Services', '', '_self', 'voyager-dot', '#000000', 20, 5, '2019-05-13 12:45:00', '2021-04-07 06:54:54', 'voyager.services.index', 'null'),
(38, 1, 'Footers', '', '_self', 'voyager-hook', NULL, NULL, 5, '2019-05-13 13:18:56', '2021-04-07 05:11:24', 'voyager.footers.index', NULL),
(39, 1, 'Home Page', '', '_self', 'voyager-home', '#000000', NULL, 1, '2019-05-14 03:52:14', '2019-05-14 03:53:17', NULL, ''),
(40, 1, 'Home Page Contents', '', '_self', 'voyager-dot', '#000000', 39, 2, '2020-02-29 10:55:37', '2021-04-07 06:51:16', 'voyager.home-page-contents.index', 'null'),
(41, 1, 'Service Sections', '', '_self', 'voyager-dot', '#000000', 20, 6, '2020-02-29 13:42:53', '2021-04-07 06:55:00', 'voyager.service-sections.index', 'null'),
(42, 1, 'Quality Cares', '', '_self', 'voyager-dot', '#000000', 20, 7, '2020-04-28 16:02:18', '2021-04-07 06:55:11', 'voyager.quality-cares.index', 'null'),
(44, 1, 'Homesliders', '', '_self', 'voyager-dot', '#000000', 39, 3, '2020-11-25 04:39:29', '2021-04-07 06:51:43', 'voyager.homesliders.index', 'null'),
(51, 1, 'Patient Journeys', '', '_self', 'voyager-dot', '#000000', 53, 3, '2020-12-02 01:41:33', '2021-04-07 06:56:17', 'voyager.patient-journeys.index', 'null'),
(52, 1, 'Shared Priorities', '', '_self', 'voyager-dot', '#000000', 53, 4, '2020-12-02 01:52:51', '2021-04-27 09:56:50', 'voyager.shared-priorities.index', 'null'),
(53, 1, 'customer needs assessment tool', '', '_self', 'voyager-edit', '#000000', NULL, 3, '2020-12-15 18:45:23', '2021-04-07 06:53:15', NULL, ''),
(54, 1, 'Novartis Account Managers', '', '_self', 'voyager-dot', '#000000', 21, 4, '2021-01-06 14:02:10', '2021-04-28 16:15:23', 'voyager.novartis-managers.index', 'null'),
(55, 1, 'User Assessments', '', '_self', 'voyager-dot', '#000000', 5, 10, '2021-01-11 06:01:02', '2021-04-27 09:56:54', 'voyager.user-assessments.index', 'null'),
(56, 1, 'States', '', '_self', 'voyager-dot', '#000000', 53, 1, '2021-01-11 15:48:50', '2021-04-07 06:55:59', 'voyager.states.index', 'null'),
(57, 1, 'Type of support', '', '_self', 'voyager-dot', '#000000', 53, 2, '2021-04-07 06:43:32', '2021-04-07 06:56:28', 'voyager.supports.index', 'null'),
(58, 1, 'EHR Plugin', '', '_self', 'voyager-dot', '#000000', 61, 1, '2021-04-07 07:00:43', '2021-04-09 13:23:29', 'voyager.ehr-homes.index', 'null'),
(59, 1, 'Support Requests', '', '_self', 'voyager-dot', '#000000', 61, 2, '2021-04-07 10:21:09', '2021-04-09 13:23:42', 'voyager.support-requests.index', 'null'),
(60, 1, 'Order Requests', '', '_self', 'voyager-dot', '#000000', 61, 3, '2021-04-07 10:22:33', '2021-04-09 13:23:49', 'voyager.order-requests.index', 'null'),
(61, 1, 'EHR Plugin Management', '', '_self', 'voyager-edit', '#000000', NULL, 7, '2021-04-09 12:37:24', '2021-04-27 09:56:52', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1);

-- --------------------------------------------------------

--
-- Table structure for table `order_requests`
--

CREATE TABLE `order_requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `manager_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_lname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_requests`
--

INSERT INTO `order_requests` (`id`, `customer_name`, `organization`, `organization_type`, `email`, `manager_name`, `customer_lname`, `created_at`, `updated_at`) VALUES
(79, 'Novartis Admin', 'aventia', NULL, 'admin@admin.com', 'Alli Brinker', 'Admin', '2021-06-09 13:57:34', '2021-06-09 13:57:34'),
(80, 'Eileen', 'Novartis', NULL, 'eileen.urban@novartis.com', NULL, 'Urban', '2021-06-09 17:03:54', '2021-06-09 17:03:54'),
(81, 'Prashanth', 'Advaana Testing', 'Other', 'clients@advaana.com', 'I don\'t know', 'raj', '2021-06-11 04:07:33', '2021-06-11 04:07:33'),
(82, 'Eric', 'Aventria Health Group', NULL, 'eric.hoch@aventriahealth.com', 'I don\'t know', 'Hoch', '2021-06-15 07:19:02', '2021-06-15 07:19:02'),
(83, 'Eric', 'Aventria Health Group', NULL, 'eric.hoch@aventriahealth.com', 'I don\'t know', 'Hoch', '2021-06-15 20:05:09', '2021-06-15 20:05:09'),
(84, 'Eric', 'Aventria Health Group', NULL, 'eric.hoch@aventriahealth.com', 'I don\'t know', 'Hoch', '2021-06-16 12:40:41', '2021-06-16 12:40:41'),
(85, 'Eric', 'Aventria Health Group', NULL, 'eric.hoch@aventriahealth.com', 'I don\'t know', 'Hoch', '2021-06-18 06:19:26', '2021-06-18 06:19:26'),
(86, 'Eric', 'Aventria Health Group', NULL, 'eric.hoch@aventriahealth.com', 'I don\'t know', 'Hoch', '2021-07-05 02:04:32', '2021-07-05 02:04:32'),
(87, 'Eric', 'Aventria Health Group', NULL, 'eric.hoch@aventriahealth.com', 'I don\'t know', 'Hoch', '2021-07-05 02:09:07', '2021-07-05 02:09:07');

-- --------------------------------------------------------

--
-- Table structure for table `organizationtypes`
--

CREATE TABLE `organizationtypes` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `organizationtypes`
--

INSERT INTO `organizationtypes` (`id`, `type`, `status`, `created_at`, `updated_at`) VALUES
(11, 'Health System', '1', '2021-04-12 16:22:00', '2021-07-01 06:18:38'),
(12, 'Large Group Practice', '1', '2021-04-12 16:22:00', '2021-07-01 06:20:58'),
(13, 'Primary Care Organization', '1', '2021-04-12 16:22:00', '2021-07-01 06:21:46'),
(14, 'Other', '1', '2021-04-21 15:10:40', '2021-04-21 15:10:40');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('sebaker6@verizon.net', '$2y$10$ngPs7FNOozAMBFcrFW0KtuZOuZ/ifxGx/zjeEUWZSmjikyYCANmvu', '2020-05-07 17:05:48'),
('bill.gagliardi@salix.com', '$2y$10$J/kaTKbzvlv3XnxKOw/.O.rNKUytM3YXw.yssgVPDWoBZoHlVGyO.', '2020-05-08 14:02:36'),
('deborah.benward@salix.com', '$2y$10$MgPf6lIujrPMKSKFc4pQBuOh83WTeTdPL8ERHIfWNog.F4.2jb6hm', '2020-05-14 09:48:50'),
('david.destefano@salix.com', '$2y$10$Z13E7m1vM8TbkOSx.m7JqO1M9l.s4UNhpLp6KsCw5owTkQZRjbkn6', '2020-05-19 14:35:39'),
('melissa.sattem@salix.com', '$2y$10$CprZofISQDX4i8ksIMBCh.xionlSiLHg9gAKSK0h/plsh28PkcFzK', '2020-05-26 01:22:07'),
('tgriffin@mhainc.com', '$2y$10$4SKHdkjQ4itEBclYA7EB5u4c6Gs7Bu7aBiQ5VTDsBrbReJ.Xw6CSu', '2020-05-26 18:16:17'),
('jennifer.czarnecki@healthonecares.com', '$2y$10$sf6kJCf8M61gGwODahrouuc7TDMgl1fig.88lLQBGzWyqp96rn.xS', '2020-05-30 05:59:39'),
('Jmartel@gastrohealth.com', '$2y$10$sIhVhlMOn7BEYpPTXYXjdOpcKmDgsvXB89kyw7aIP38Yz9S8XhskW', '2020-06-16 05:32:41'),
('catherine.mccarthy@salix.com', '$2y$10$tNry7MdNZhOiwXxCptuCbebBvuJos3S7R/ANvSw0yJJQ79fK8zjfG', '2020-07-08 15:10:55'),
('christy.eichelberger@unitypoint.org', '$2y$10$7SiTLi6ap/amSqOuhV3whuSoC6wi5ymS98iiHycDSICz0Zu5hgR4m', '2020-07-24 13:47:54'),
('sam.baldigowski@salix.com', '$2y$10$no.fdw0vimJdAhmrrQW/zuf3lw1pUzZvvMr5Cu62eF2lpJyDsBVOy', '2020-08-05 16:53:56'),
('dereck.lewis@salix.com', '$2y$10$7ixkz/AAus44e.UkB8ltYeZLBG8BXzgrE8l/JXDPBVxNDWWIRfPlu', '2020-08-13 12:19:39'),
('cory.elwell@salix.com', '$2y$10$GMIaOD5h1rHO7dwsJrSHau6PiKEAadlUd60wdvZWdtuDYQ69sgS0O', '2020-08-19 17:12:10'),
('matthew.keyhoe@salix.com', '$2y$10$BHcSmDZHIOHGBc8v2SN1Cez0OV/Dar/jZqbF3XKeIUvUOJsiRAPWW', '2020-08-20 11:47:33'),
('lysa.blau@salix.com', '$2y$10$b0HWS8Q.U9YRyXREdiEZ5eRnvLTU9E4DcCkQLYM9TJHs2Bq8UDTn2', '2020-08-21 18:02:17'),
('valerie.vella-wood@salix.com', '$2y$10$6CUbiUmCsVbadnHjb60epO9BIn18euzhEjEst5IklXmOe2mfeOhkK', '2020-08-21 21:28:36'),
('julie.wilson@salix.com', '$2y$10$hamh74seklk.a1IWdBnkXOf31tVgQAMNGAKnIyRTwgCMliFQYxibC', '2020-09-01 16:39:50'),
('william.scotton@salix.com', '$2y$10$4DZutfV/sjyaJd0L15Sb6Ov/neoxoRc4rc0qPwgCZnbQ4hzBIbqjm', '2020-09-02 17:06:33'),
('casey.calabro@salix.com', '$2y$10$ZaIlX17.ZnFARE4onDUi8.wnjZdBy0yIPtJ26zb7bOMMl0EklzZXG', '2020-09-09 20:32:18'),
('susan.baker@salix.com', '$2y$10$wbBr3vxnyVdE9Am37/SRWObvE6mCMu6PEhYCoJCMS./i7dxnbbgCe', '2020-09-15 19:42:22'),
('Chad.rupp@salix.com', '$2y$10$cByLohboug1z8DYZanurk.dwZNkYMx6n5Bq6rc6N1yuAIj1pmvZMa', '2020-09-16 14:49:09'),
('catherine.leach@salix.com', '$2y$10$wPK1Xfv/I.56J9VBxMmD5ODc.U2SgI3yd16kjkKdNUkGuPwvW3Xga', '2020-09-18 18:39:12'),
('ann.herrin@va.gov', '$2y$10$/c8KGF4P/Pi8XRKPxnKDwObnVTaK28M6aGrcTLPFnnfQGJwsQEYO6', '2020-09-24 23:35:07'),
('robert.masse@salix.com', '$2y$10$Idu2VxwNB5fPk4yIwDtiSurFaXhffOWkA5k0TArdNmR4Btc2wNk/2', '2020-10-06 15:37:57'),
('chrissa.pearson@salix.com', '$2y$10$G3w3xqYgR6kNZG2h627h8eRjjIEgcV608/LEn2YSO4h80PnVilWli', '2020-10-14 15:13:06'),
('marlo.barton@salix.com', '$2y$10$iz9bTso550PSDJ.A52DjcO4n/tmsO9pgRJPF2kIzQzfXB4aJAazsi', '2020-10-14 17:31:27'),
('philip.lucina@salix.com', '$2y$10$9kdwcKWY57hjZPnFcVmMp.wbaHobozFXmDD2EkCxnuORb/FXGll9a', '2020-10-21 19:10:26'),
('jennifer.culkin@providence.org', '$2y$10$0mfblnNFzgJ5AJEaYxHG3.1gxeFjk9gxttJhInVEWmsCUv00Ila86', '2020-10-28 22:12:57'),
('ameyaleo9@gmai', '$2y$10$0..w2cpouTjRCDyQKYV4IeexiZ.wGIk2bkZyBu7i89TFTFF/l5xb2', '2020-12-10 06:26:59'),
('piyushpanchalleo9@gmail.com', '$2y$10$1baX6p9cTC9ZNZ8hQ6eWguPdYocwnZViadAhqV9unbfp5uzxugN4S', '2021-01-12 06:48:44'),
('carolyn.odenwald@aventriahealth.com', '$2y$10$JJAdDRVsyNaZxUOUemjra.daJLfwyzHmrw90UFCHBgVNNsPVS/W8y', '2021-04-26 13:42:45'),
('briana.brogan@aventriahealth.com', '$2y$10$qWAE.gpBp6Gn9ofsOFYPteyjeqsL5VBz1uKak5nS1SZjOIx5FEOpy', '2021-05-03 14:28:10'),
('sandra.arts@aventriahealth.com', '$2y$10$nI52jI1rS.WV.lH4laZuPufOHF9IwhiTuDgKsKzYtLirrkLxuWQ8O', '2021-05-05 18:23:34'),
('heather.collins@franklynhc.com', '$2y$10$FQvLSCTo.BWDKAB5j3iM..0D2cyi8TGwPXn3kcVPwzQNblTnKq.DC', '2021-05-25 13:13:40'),
('carolyn@theodenwalds.com', '$2y$10$htczhd6IeF46Ejq88qFYQ.Y3wYq67.26vwNNNpSzNC7qKo5Y2jct.', '2021-06-04 13:56:29'),
('carolyn@odenwalds.com', '$2y$10$g2bpTjAdFcFr5kvVBVUNfuPGc1rDHna7nQPDd7YrmYEHwrqvedRBG', '2021-06-07 07:52:27'),
('ejh75@optonline.net', '$2y$10$XnArQU5yt3OgQd9tltRlo.QyXsyEj81.xUSxgj4HefjoL3apneckq', '2021-06-07 13:56:17'),
('eric.hoch@aventriahealth.com', '$2y$10$Ljsbp3yWQXyRvVYSPaQZveb1rqmhK.5sW63aHXII5nOAU4ieAq/vK', '2021-06-07 14:13:50'),
('gawade.sanat@gmail.com', '$2y$10$NGY1ElEzXEzx8jy5Z5Z.dul13NxOnnquHGtAsiKKCHp3s2OGaIVdm', '2021-06-16 09:38:53'),
('sanat@leo9studio.com', '$2y$10$TNN95kDDzyqWa939N0bGS.h4sJ.J6jYDTJf183UlVXqKQLBP1DtAS', '2021-06-16 09:42:10'),
('sanat.leo9@gmail.com', '$2y$10$LAKG10.PhAxxx5HJXaONLOtbKHIx/dnfVPXUmxDQT3KGerKBYwpBC', '2021-06-16 09:47:27'),
('hannahcassel7@gmail.com', '$2y$10$e77sJfDjLY3oesuJoGoFRuoQwHek2WNxjgZEzJXHT0VHTBlqyjtFO', '2021-06-16 14:25:23'),
('thirumurugan6497@gmail.com', '$2y$10$1YAGd2ktbOhQieHgfPYkbOi7Ct5Tr3m8CHY4HdF5HV0DJfKqZ/d2S', '2021-07-02 02:26:04');

-- --------------------------------------------------------

--
-- Table structure for table `patienttypes`
--

CREATE TABLE `patienttypes` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patienttypes`
--

INSERT INTO `patienttypes` (`id`, `type`, `status`, `created_at`, `updated_at`) VALUES
(4, 'Patient', '1', '2021-04-07 06:12:31', '2021-04-07 06:12:31'),
(6, 'Health Care Provider', '0', '2021-04-23 03:59:00', '2021-04-27 10:01:38');

-- --------------------------------------------------------

--
-- Table structure for table `patient_journeys`
--

CREATE TABLE `patient_journeys` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `shared_prioritie` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patient_journeys`
--

INSERT INTO `patient_journeys` (`id`, `title`, `created_at`, `updated_at`, `shared_prioritie`) VALUES
(1, 'Care planning', '2020-12-02 01:42:00', '2020-12-18 12:54:57', '[]'),
(2, 'Patient counseling/ education', '2020-12-02 01:42:00', '2021-05-06 16:38:24', '[]'),
(3, 'Disease progression monitoring', '2020-12-02 01:42:00', '2020-12-18 12:54:42', '[]'),
(4, 'Virtual care/telehealth', '2020-12-02 01:42:00', '2020-12-18 12:54:29', '[]'),
(5, 'Patient identification', '2020-12-02 01:42:00', '2020-12-18 12:54:16', '[]'),
(6, 'Transitions of care', '2020-12-02 01:42:00', '2021-04-02 10:29:57', '[]');

-- --------------------------------------------------------

--
-- Table structure for table `patient_prioritey`
--

CREATE TABLE `patient_prioritey` (
  `id` int(10) UNSIGNED NOT NULL,
  `patient_journey_id` int(11) DEFAULT NULL,
  `shared_priority_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patient_prioritey`
--

INSERT INTO `patient_prioritey` (`id`, `patient_journey_id`, `shared_priority_id`, `created_at`, `updated_at`) VALUES
(1, 7, 1, NULL, NULL),
(3, 7, 3, NULL, NULL),
(4, 7, 4, NULL, NULL),
(5, 7, 5, NULL, NULL),
(6, 7, 6, NULL, NULL),
(7, 1, 1, NULL, NULL),
(8, 1, 2, NULL, NULL),
(9, 1, 3, NULL, NULL),
(10, 1, 4, NULL, NULL),
(11, 1, 5, NULL, NULL),
(12, 1, 6, NULL, NULL),
(13, 2, 1, NULL, NULL),
(14, 2, 2, NULL, NULL),
(15, 2, 3, NULL, NULL),
(16, 2, 4, NULL, NULL),
(17, 2, 5, NULL, NULL),
(18, 2, 6, NULL, NULL),
(19, 3, 1, NULL, NULL),
(20, 3, 2, NULL, NULL),
(21, 3, 3, NULL, NULL),
(22, 3, 4, NULL, NULL),
(23, 3, 5, NULL, NULL),
(24, 3, 6, NULL, NULL),
(25, 4, 1, NULL, NULL),
(26, 4, 2, NULL, NULL),
(27, 4, 3, NULL, NULL),
(28, 4, 4, NULL, NULL),
(29, 4, 5, NULL, NULL),
(30, 4, 6, NULL, NULL),
(31, 5, 1, NULL, NULL),
(32, 5, 2, NULL, NULL),
(33, 5, 3, NULL, NULL),
(34, 5, 4, NULL, NULL),
(35, 5, 5, NULL, NULL),
(36, 5, 6, NULL, NULL),
(37, 6, 1, NULL, NULL),
(38, 6, 2, NULL, NULL),
(39, 6, 3, NULL, NULL),
(40, 6, 4, NULL, NULL),
(41, 6, 5, NULL, NULL),
(42, 6, 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2019-05-08 03:24:33', '2019-05-08 03:24:33'),
(2, 'browse_bread', NULL, '2019-05-08 03:24:33', '2019-05-08 03:24:33'),
(3, 'browse_database', NULL, '2019-05-08 03:24:33', '2019-05-08 03:24:33'),
(4, 'browse_media', NULL, '2019-05-08 03:24:33', '2019-05-08 03:24:33'),
(5, 'browse_compass', NULL, '2019-05-08 03:24:33', '2019-05-08 03:24:33'),
(6, 'browse_menus', 'menus', '2019-05-08 03:24:33', '2019-05-08 03:24:33'),
(7, 'read_menus', 'menus', '2019-05-08 03:24:33', '2019-05-08 03:24:33'),
(8, 'edit_menus', 'menus', '2019-05-08 03:24:33', '2019-05-08 03:24:33'),
(9, 'add_menus', 'menus', '2019-05-08 03:24:33', '2019-05-08 03:24:33'),
(10, 'delete_menus', 'menus', '2019-05-08 03:24:33', '2019-05-08 03:24:33'),
(11, 'browse_roles', 'roles', '2019-05-08 03:24:33', '2019-05-08 03:24:33'),
(12, 'read_roles', 'roles', '2019-05-08 03:24:33', '2019-05-08 03:24:33'),
(13, 'edit_roles', 'roles', '2019-05-08 03:24:33', '2019-05-08 03:24:33'),
(14, 'add_roles', 'roles', '2019-05-08 03:24:33', '2019-05-08 03:24:33'),
(15, 'delete_roles', 'roles', '2019-05-08 03:24:34', '2019-05-08 03:24:34'),
(16, 'browse_users', 'users', '2019-05-08 03:24:34', '2019-05-08 03:24:34'),
(17, 'read_users', 'users', '2019-05-08 03:24:34', '2019-05-08 03:24:34'),
(18, 'edit_users', 'users', '2019-05-08 03:24:34', '2019-05-08 03:24:34'),
(19, 'add_users', 'users', '2019-05-08 03:24:34', '2019-05-08 03:24:34'),
(20, 'delete_users', 'users', '2019-05-08 03:24:34', '2019-05-08 03:24:34'),
(21, 'browse_settings', 'settings', '2019-05-08 03:24:34', '2019-05-08 03:24:34'),
(22, 'read_settings', 'settings', '2019-05-08 03:24:34', '2019-05-08 03:24:34'),
(23, 'edit_settings', 'settings', '2019-05-08 03:24:34', '2019-05-08 03:24:34'),
(24, 'add_settings', 'settings', '2019-05-08 03:24:34', '2019-05-08 03:24:34'),
(25, 'delete_settings', 'settings', '2019-05-08 03:24:34', '2019-05-08 03:24:34'),
(26, 'browse_hooks', NULL, '2019-05-08 03:24:36', '2019-05-08 03:24:36'),
(27, 'browse_organizationtypes', 'organizationtypes', '2019-05-08 04:57:32', '2019-05-08 04:57:32'),
(28, 'read_organizationtypes', 'organizationtypes', '2019-05-08 04:57:32', '2019-05-08 04:57:32'),
(29, 'edit_organizationtypes', 'organizationtypes', '2019-05-08 04:57:32', '2019-05-08 04:57:32'),
(30, 'add_organizationtypes', 'organizationtypes', '2019-05-08 04:57:32', '2019-05-08 04:57:32'),
(31, 'delete_organizationtypes', 'organizationtypes', '2019-05-08 04:57:32', '2019-05-08 04:57:32'),
(32, 'browse_sources', 'sources', '2019-05-08 05:22:00', '2019-05-08 05:22:00'),
(33, 'read_sources', 'sources', '2019-05-08 05:22:00', '2019-05-08 05:22:00'),
(34, 'edit_sources', 'sources', '2019-05-08 05:22:00', '2019-05-08 05:22:00'),
(35, 'add_sources', 'sources', '2019-05-08 05:22:00', '2019-05-08 05:22:00'),
(36, 'delete_sources', 'sources', '2019-05-08 05:22:00', '2019-05-08 05:22:00'),
(47, 'browse_targetaudiences', 'targetaudiences', '2019-05-13 03:47:23', '2019-05-13 03:47:23'),
(48, 'read_targetaudiences', 'targetaudiences', '2019-05-13 03:47:23', '2019-05-13 03:47:23'),
(49, 'edit_targetaudiences', 'targetaudiences', '2019-05-13 03:47:23', '2019-05-13 03:47:23'),
(50, 'add_targetaudiences', 'targetaudiences', '2019-05-13 03:47:23', '2019-05-13 03:47:23'),
(51, 'delete_targetaudiences', 'targetaudiences', '2019-05-13 03:47:23', '2019-05-13 03:47:23'),
(52, 'browse_patienttypes', 'patienttypes', '2019-05-13 03:50:11', '2019-05-13 03:50:11'),
(53, 'read_patienttypes', 'patienttypes', '2019-05-13 03:50:11', '2019-05-13 03:50:11'),
(54, 'edit_patienttypes', 'patienttypes', '2019-05-13 03:50:11', '2019-05-13 03:50:11'),
(55, 'add_patienttypes', 'patienttypes', '2019-05-13 03:50:11', '2019-05-13 03:50:11'),
(56, 'delete_patienttypes', 'patienttypes', '2019-05-13 03:50:11', '2019-05-13 03:50:11'),
(57, 'browse_healthtools', 'healthtools', '2019-05-13 04:05:08', '2019-05-13 04:05:08'),
(58, 'read_healthtools', 'healthtools', '2019-05-13 04:05:08', '2019-05-13 04:05:08'),
(59, 'edit_healthtools', 'healthtools', '2019-05-13 04:05:08', '2019-05-13 04:05:08'),
(60, 'add_healthtools', 'healthtools', '2019-05-13 04:05:08', '2019-05-13 04:05:08'),
(61, 'delete_healthtools', 'healthtools', '2019-05-13 04:05:08', '2019-05-13 04:05:08'),
(62, 'browse_healthtoolstaticcontents', 'healthtoolstaticcontents', '2019-05-13 05:52:50', '2019-05-13 05:52:50'),
(63, 'read_healthtoolstaticcontents', 'healthtoolstaticcontents', '2019-05-13 05:52:50', '2019-05-13 05:52:50'),
(64, 'edit_healthtoolstaticcontents', 'healthtoolstaticcontents', '2019-05-13 05:52:50', '2019-05-13 05:52:50'),
(65, 'add_healthtoolstaticcontents', 'healthtoolstaticcontents', '2019-05-13 05:52:50', '2019-05-13 05:52:50'),
(66, 'delete_healthtoolstaticcontents', 'healthtoolstaticcontents', '2019-05-13 05:52:50', '2019-05-13 05:52:50'),
(67, 'browse_evidencebasedworkflowscontent', 'evidencebasedworkflowscontent', '2019-05-13 06:38:08', '2019-05-13 06:38:08'),
(68, 'read_evidencebasedworkflowscontent', 'evidencebasedworkflowscontent', '2019-05-13 06:38:08', '2019-05-13 06:38:08'),
(69, 'edit_evidencebasedworkflowscontent', 'evidencebasedworkflowscontent', '2019-05-13 06:38:08', '2019-05-13 06:38:08'),
(70, 'add_evidencebasedworkflowscontent', 'evidencebasedworkflowscontent', '2019-05-13 06:38:08', '2019-05-13 06:38:08'),
(71, 'delete_evidencebasedworkflowscontent', 'evidencebasedworkflowscontent', '2019-05-13 06:38:08', '2019-05-13 06:38:08'),
(72, 'browse_evidencebasedworkflowscontents', 'evidencebasedworkflowscontents', '2019-05-13 06:40:27', '2019-05-13 06:40:27'),
(73, 'read_evidencebasedworkflowscontents', 'evidencebasedworkflowscontents', '2019-05-13 06:40:27', '2019-05-13 06:40:27'),
(74, 'edit_evidencebasedworkflowscontents', 'evidencebasedworkflowscontents', '2019-05-13 06:40:27', '2019-05-13 06:40:27'),
(75, 'add_evidencebasedworkflowscontents', 'evidencebasedworkflowscontents', '2019-05-13 06:40:27', '2019-05-13 06:40:27'),
(76, 'delete_evidencebasedworkflowscontents', 'evidencebasedworkflowscontents', '2019-05-13 06:40:27', '2019-05-13 06:40:27'),
(92, 'browse_guidelineandlinkscontent', 'guidelineandlinkscontent', '2019-05-13 09:46:05', '2019-05-13 09:46:05'),
(93, 'read_guidelineandlinkscontent', 'guidelineandlinkscontent', '2019-05-13 09:46:05', '2019-05-13 09:46:05'),
(94, 'edit_guidelineandlinkscontent', 'guidelineandlinkscontent', '2019-05-13 09:46:05', '2019-05-13 09:46:05'),
(95, 'add_guidelineandlinkscontent', 'guidelineandlinkscontent', '2019-05-13 09:46:05', '2019-05-13 09:46:05'),
(96, 'delete_guidelineandlinkscontent', 'guidelineandlinkscontent', '2019-05-13 09:46:05', '2019-05-13 09:46:05'),
(107, 'browse_homebannercontents', 'homebannercontents', '2019-05-13 12:24:40', '2019-05-13 12:24:40'),
(108, 'read_homebannercontents', 'homebannercontents', '2019-05-13 12:24:40', '2019-05-13 12:24:40'),
(109, 'edit_homebannercontents', 'homebannercontents', '2019-05-13 12:24:40', '2019-05-13 12:24:40'),
(110, 'add_homebannercontents', 'homebannercontents', '2019-05-13 12:24:40', '2019-05-13 12:24:40'),
(111, 'delete_homebannercontents', 'homebannercontents', '2019-05-13 12:24:40', '2019-05-13 12:24:40'),
(112, 'browse_services', 'services', '2019-05-13 12:45:00', '2019-05-13 12:45:00'),
(113, 'read_services', 'services', '2019-05-13 12:45:00', '2019-05-13 12:45:00'),
(114, 'edit_services', 'services', '2019-05-13 12:45:00', '2019-05-13 12:45:00'),
(115, 'add_services', 'services', '2019-05-13 12:45:00', '2019-05-13 12:45:00'),
(116, 'delete_services', 'services', '2019-05-13 12:45:00', '2019-05-13 12:45:00'),
(127, 'browse_footers', 'footers', '2019-05-13 13:18:56', '2019-05-13 13:18:56'),
(128, 'read_footers', 'footers', '2019-05-13 13:18:56', '2019-05-13 13:18:56'),
(129, 'edit_footers', 'footers', '2019-05-13 13:18:56', '2019-05-13 13:18:56'),
(130, 'add_footers', 'footers', '2019-05-13 13:18:56', '2019-05-13 13:18:56'),
(131, 'delete_footers', 'footers', '2019-05-13 13:18:56', '2019-05-13 13:18:56'),
(132, 'browse_home_page_contents', 'home_page_contents', '2020-02-29 10:55:37', '2020-02-29 10:55:37'),
(133, 'read_home_page_contents', 'home_page_contents', '2020-02-29 10:55:37', '2020-02-29 10:55:37'),
(134, 'edit_home_page_contents', 'home_page_contents', '2020-02-29 10:55:37', '2020-02-29 10:55:37'),
(135, 'add_home_page_contents', 'home_page_contents', '2020-02-29 10:55:37', '2020-02-29 10:55:37'),
(136, 'delete_home_page_contents', 'home_page_contents', '2020-02-29 10:55:37', '2020-02-29 10:55:37'),
(137, 'browse_service_sections', 'service_sections', '2020-02-29 13:42:53', '2020-02-29 13:42:53'),
(138, 'read_service_sections', 'service_sections', '2020-02-29 13:42:53', '2020-02-29 13:42:53'),
(139, 'edit_service_sections', 'service_sections', '2020-02-29 13:42:53', '2020-02-29 13:42:53'),
(140, 'add_service_sections', 'service_sections', '2020-02-29 13:42:53', '2020-02-29 13:42:53'),
(141, 'delete_service_sections', 'service_sections', '2020-02-29 13:42:53', '2020-02-29 13:42:53'),
(142, 'browse_quality_cares', 'quality_cares', '2020-04-28 16:02:17', '2020-04-28 16:02:17'),
(143, 'read_quality_cares', 'quality_cares', '2020-04-28 16:02:17', '2020-04-28 16:02:17'),
(144, 'edit_quality_cares', 'quality_cares', '2020-04-28 16:02:17', '2020-04-28 16:02:17'),
(145, 'add_quality_cares', 'quality_cares', '2020-04-28 16:02:17', '2020-04-28 16:02:17'),
(146, 'delete_quality_cares', 'quality_cares', '2020-04-28 16:02:17', '2020-04-28 16:02:17'),
(147, 'browse_departments', 'departments', '2020-11-10 06:38:03', '2020-11-10 06:38:03'),
(148, 'read_departments', 'departments', '2020-11-10 06:38:03', '2020-11-10 06:38:03'),
(149, 'edit_departments', 'departments', '2020-11-10 06:38:03', '2020-11-10 06:38:03'),
(150, 'add_departments', 'departments', '2020-11-10 06:38:03', '2020-11-10 06:38:03'),
(151, 'delete_departments', 'departments', '2020-11-10 06:38:03', '2020-11-10 06:38:03'),
(152, 'browse_homesliders', 'homesliders', '2020-11-25 04:39:29', '2020-11-25 04:39:29'),
(153, 'read_homesliders', 'homesliders', '2020-11-25 04:39:29', '2020-11-25 04:39:29'),
(154, 'edit_homesliders', 'homesliders', '2020-11-25 04:39:29', '2020-11-25 04:39:29'),
(155, 'add_homesliders', 'homesliders', '2020-11-25 04:39:29', '2020-11-25 04:39:29'),
(156, 'delete_homesliders', 'homesliders', '2020-11-25 04:39:29', '2020-11-25 04:39:29'),
(182, 'browse_patient_journey', 'patient_journey', '2020-12-02 01:35:52', '2020-12-02 01:35:52'),
(183, 'read_patient_journey', 'patient_journey', '2020-12-02 01:35:52', '2020-12-02 01:35:52'),
(184, 'edit_patient_journey', 'patient_journey', '2020-12-02 01:35:52', '2020-12-02 01:35:52'),
(185, 'add_patient_journey', 'patient_journey', '2020-12-02 01:35:52', '2020-12-02 01:35:52'),
(186, 'delete_patient_journey', 'patient_journey', '2020-12-02 01:35:52', '2020-12-02 01:35:52'),
(187, 'browse_patient_journeys', 'patient_journeys', '2020-12-02 01:41:33', '2020-12-02 01:41:33'),
(188, 'read_patient_journeys', 'patient_journeys', '2020-12-02 01:41:33', '2020-12-02 01:41:33'),
(189, 'edit_patient_journeys', 'patient_journeys', '2020-12-02 01:41:33', '2020-12-02 01:41:33'),
(190, 'add_patient_journeys', 'patient_journeys', '2020-12-02 01:41:33', '2020-12-02 01:41:33'),
(191, 'delete_patient_journeys', 'patient_journeys', '2020-12-02 01:41:33', '2020-12-02 01:41:33'),
(192, 'browse_shared_priorities', 'shared_priorities', '2020-12-02 01:52:50', '2020-12-02 01:52:50'),
(193, 'read_shared_priorities', 'shared_priorities', '2020-12-02 01:52:50', '2020-12-02 01:52:50'),
(194, 'edit_shared_priorities', 'shared_priorities', '2020-12-02 01:52:50', '2020-12-02 01:52:50'),
(195, 'add_shared_priorities', 'shared_priorities', '2020-12-02 01:52:50', '2020-12-02 01:52:50'),
(196, 'delete_shared_priorities', 'shared_priorities', '2020-12-02 01:52:50', '2020-12-02 01:52:50'),
(197, 'browse_salix_managers', 'salix_managers', '2021-01-06 14:02:10', '2021-01-06 14:02:10'),
(198, 'read_salix_managers', 'salix_managers', '2021-01-06 14:02:10', '2021-01-06 14:02:10'),
(199, 'edit_salix_managers', 'salix_managers', '2021-01-06 14:02:10', '2021-01-06 14:02:10'),
(200, 'add_salix_managers', 'salix_managers', '2021-01-06 14:02:10', '2021-01-06 14:02:10'),
(201, 'delete_salix_managers', 'salix_managers', '2021-01-06 14:02:10', '2021-01-06 14:02:10'),
(202, 'browse_user_assessments', 'user_assessments', '2021-01-11 06:01:02', '2021-01-11 06:01:02'),
(203, 'read_user_assessments', 'user_assessments', '2021-01-11 06:01:02', '2021-01-11 06:01:02'),
(204, 'edit_user_assessments', 'user_assessments', '2021-01-11 06:01:02', '2021-01-11 06:01:02'),
(205, 'add_user_assessments', 'user_assessments', '2021-01-11 06:01:02', '2021-01-11 06:01:02'),
(206, 'delete_user_assessments', 'user_assessments', '2021-01-11 06:01:02', '2021-01-11 06:01:02'),
(207, 'browse_states', 'states', '2021-01-11 15:48:50', '2021-01-11 15:48:50'),
(208, 'read_states', 'states', '2021-01-11 15:48:50', '2021-01-11 15:48:50'),
(209, 'edit_states', 'states', '2021-01-11 15:48:50', '2021-01-11 15:48:50'),
(210, 'add_states', 'states', '2021-01-11 15:48:50', '2021-01-11 15:48:50'),
(211, 'delete_states', 'states', '2021-01-11 15:48:50', '2021-01-11 15:48:50'),
(212, 'browse_supports', 'supports', '2021-04-07 06:43:32', '2021-04-07 06:43:32'),
(213, 'read_supports', 'supports', '2021-04-07 06:43:32', '2021-04-07 06:43:32'),
(214, 'edit_supports', 'supports', '2021-04-07 06:43:32', '2021-04-07 06:43:32'),
(215, 'add_supports', 'supports', '2021-04-07 06:43:32', '2021-04-07 06:43:32'),
(216, 'delete_supports', 'supports', '2021-04-07 06:43:32', '2021-04-07 06:43:32'),
(217, 'browse_ehr_homes', 'ehr_homes', '2021-04-07 07:00:43', '2021-04-07 07:00:43'),
(218, 'read_ehr_homes', 'ehr_homes', '2021-04-07 07:00:43', '2021-04-07 07:00:43'),
(219, 'edit_ehr_homes', 'ehr_homes', '2021-04-07 07:00:43', '2021-04-07 07:00:43'),
(220, 'add_ehr_homes', 'ehr_homes', '2021-04-07 07:00:43', '2021-04-07 07:00:43'),
(221, 'delete_ehr_homes', 'ehr_homes', '2021-04-07 07:00:43', '2021-04-07 07:00:43'),
(222, 'browse_support_requests', 'support_requests', '2021-04-07 10:21:09', '2021-04-07 10:21:09'),
(223, 'read_support_requests', 'support_requests', '2021-04-07 10:21:09', '2021-04-07 10:21:09'),
(224, 'edit_support_requests', 'support_requests', '2021-04-07 10:21:09', '2021-04-07 10:21:09'),
(225, 'add_support_requests', 'support_requests', '2021-04-07 10:21:09', '2021-04-07 10:21:09'),
(226, 'delete_support_requests', 'support_requests', '2021-04-07 10:21:09', '2021-04-07 10:21:09'),
(227, 'browse_order_requests', 'order_requests', '2021-04-07 10:22:33', '2021-04-07 10:22:33'),
(228, 'read_order_requests', 'order_requests', '2021-04-07 10:22:33', '2021-04-07 10:22:33'),
(229, 'edit_order_requests', 'order_requests', '2021-04-07 10:22:33', '2021-04-07 10:22:33'),
(230, 'add_order_requests', 'order_requests', '2021-04-07 10:22:33', '2021-04-07 10:22:33'),
(231, 'delete_order_requests', 'order_requests', '2021-04-07 10:22:33', '2021-04-07 10:22:33');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(135, 1),
(136, 1),
(137, 1),
(138, 1),
(139, 1),
(140, 1),
(141, 1),
(142, 1),
(143, 1),
(144, 1),
(145, 1),
(146, 1),
(147, 1),
(148, 1),
(149, 1),
(150, 1),
(151, 1),
(152, 1),
(153, 1),
(154, 1),
(155, 1),
(156, 1),
(182, 1),
(183, 1),
(184, 1),
(185, 1),
(186, 1),
(187, 1),
(188, 1),
(189, 1),
(190, 1),
(191, 1),
(192, 1),
(193, 1),
(194, 1),
(195, 1),
(196, 1),
(197, 1),
(198, 1),
(199, 1),
(200, 1),
(201, 1),
(202, 1),
(203, 1),
(204, 1),
(205, 1),
(206, 1),
(207, 1),
(208, 1),
(209, 1),
(210, 1),
(211, 1),
(212, 1),
(213, 1),
(214, 1),
(215, 1),
(216, 1),
(217, 1),
(218, 1),
(219, 1),
(220, 1),
(221, 1),
(222, 1),
(223, 1),
(224, 1),
(225, 1),
(226, 1),
(227, 1),
(228, 1),
(229, 1),
(230, 1),
(231, 1);

-- --------------------------------------------------------

--
-- Table structure for table `quality_cares`
--

CREATE TABLE `quality_cares` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_three` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quality_cares`
--

INSERT INTO `quality_cares` (`id`, `title`, `content_one`, `content_two`, `content_three`, `created_at`, `updated_at`) VALUES
(1, 'Quality Measures and Evidence-based Guidelines', '<p>Quality measures help quantify processes, outcomes, patient perceptions, and/or risk-share, with the goal of providing high-quality health care. Measures drive the standardization of care by identifying differences in care delivery and outcomes.</p>\r\n<p>Quality measures are becoming increasingly important as our health care system shifts from traditional fee-for-service toward value-based care and Centers for Medicare &amp; Medicaid Services (CMS) quality-based reimbursement models.</p>\r\n<p>The links below include the latest quality measures for cirrhosis care as well as a link to the AASLD.org for more information around managing chronic liver disease.</p>\r\n<p>All of the resources on this site are written to CMS guidelines for font and readability and support the American Association for the Study of Liver Diseases (AASLD) guidelines below. LIVERHEALTHNOW is committed to providing members with the latest CMS and AASLD updates as they are released to help members stay aligned with the latest quality care.</p>', '<ul>\r\n<li><a class=\"click_check redirectUrl \" href=\"#\" data-toggle=\"modal\" data-target=\"#guideModal_1\" data-url=\"https://aasldpubs.onlinelibrary.wiley.com/doi/10.1002/hep.30489\">Development of Quality Measures in Cirrhosis by the Practice Metrics Committee of the American Association for the Study of Liver Diseases&mdash;2019</a></li>\r\n<li>For more information on managing patients with liver disease visit <a class=\"click_check redirectUrl\" href=\"#\" data-target=\"#guideModal_2\" data-toggle=\"modal\" data-url=\"https://www.aasld.org/\">AASLD.org</a></li>\r\n</ul>', '<p>to get resources aligned to quality care. Integrate these resources into your workflow to help standardize care and improve quality.</p>', '2020-04-28 05:29:00', '2020-05-01 10:27:09');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2019-05-08 03:24:33', '2019-05-08 03:24:33'),
(2, 'user', 'Normal User', '2019-05-08 03:24:33', '2019-05-08 03:24:33'),
(3, 'AHG', 'AHG', '2020-11-06 15:19:34', '2020-11-06 15:19:34');

-- --------------------------------------------------------

--
-- Table structure for table `salix_managers`
--

CREATE TABLE `salix_managers` (
  `id` int(10) UNSIGNED NOT NULL,
  `salix_manager_name` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `salix_managers`
--

INSERT INTO `salix_managers` (`id`, `salix_manager_name`, `created_at`, `updated_at`, `email`) VALUES
(1, 'Alli Brinker', '2021-01-06 14:03:10', '2021-04-26 13:46:23', 'alli.brinker@novartis.com'),
(2, 'Jacqueline DeLaRosa', '2021-01-06 14:03:17', '2021-04-26 13:46:36', 'jacqueline.delarosa@novartis.com'),
(3, 'Marci Grebing', '2021-01-06 14:03:25', '2021-04-26 13:46:47', 'marci.grebing@novartis.com'),
(4, 'Ashley Ingram', '2021-01-06 14:03:33', '2021-04-26 13:47:01', 'ashley.ingram@novartis.com'),
(5, 'Jodi Johnson', '2021-01-06 14:03:42', '2021-04-26 13:47:11', 'jodi.johnson@novartis.com'),
(6, 'Lori Martin', '2021-01-06 14:03:50', '2021-04-26 13:47:22', 'lori-2.martin@novartis.com'),
(7, 'Lauren A. McKenna', '2021-01-06 14:03:58', '2021-05-21 19:43:35', 'robert.creech@novartis.com'),
(8, 'I don\'t know', '2021-04-12 15:24:06', '2021-05-06 20:27:22', 'robert.creech@novartis.com');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_tools` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_tools` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `created_at`, `updated_at`, `name`, `banner_content`, `provider_tools`, `patient_tools`, `slug`, `ref`) VALUES
(4, 'The Importance of Screening Patients With Chronic Liver Disease for Hepatic Encephalopathy', '2020-02-29 13:57:00', '2020-04-28 23:29:05', 'Screen Patients', '<h4>Chronic liver disease (CLD)</h4>\r\n<ul>\r\n<li>Affects an<strong> estimated 4.5 million people</strong> in the United States<sup>1</sup></li>\r\n<li>6th leading cause of death in persons aged 25-44<sup>2</sup></li>\r\n<li>5th leading cause of death in persons aged 45-64<sup>2</sup></li>\r\n</ul>', '<ul class=\"\">\r\n<li>Assess HE With the Stroop Test</li>\r\n<li>West Haven Criteria</li>\r\n<li>Diagnosing Hepatic Encephalopathy in Patients With Liver Disease</li>\r\n<li>Cirrhosis and Its Complications</li>\r\n</ul>', '<ul class=\"\">\r\n<li>What Is Hepatic Encephalopathy?</li>\r\n<li>Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers</li>\r\n<li>Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy</li>\r\n<li>Symptoms, Complications, and Management of Cirrhosis</li>\r\n<li>Stages and Types of Liver Disease</li>\r\n</ul>', 'screen-patients', '<ol>\r\n<li>Chronic Liver Disease Foundation. About CLDF/Mission Statement. <span class=\"break\">https://www.chronicliverdisease.org/about/mission.cfm.</span> Accessed December 12, 2019.</li>\r\n<li>National Center for Health Statistics. Health, United States, 2017. Hyattsville, MD. 2018.</li>\r\n<li>Estes C, Razavi H, Loomba R, et al. Modeling the epidemic of nonalcoholic fatty liver disease demonstrates an exponential increase in burden of disease. <em>Hepatology.</em> 2018;67(1):123-133.</li>\r\n<li>Vilstrup H, Amodio P, Bajaj J, et al. Hepatic encephalopathy in chronic liver disease: 2014 practice guideline by the American Association for the Study of Liver Diseases and the European Association for the Study of the Liver. <em>Hepatology.</em> 2014;60(2):715-735.</li>\r\n<li>Patidar KR, Bajaj JS. Covert and overt hepatic encephalopathy: diagnosis and management. <em>Clin Gastroenterol Hepatol.</em> 2015;13(12):2048-2061.</li>\r\n</ol>'),
(5, 'Define an Episode of Hepatic Encephalopathy', '2020-02-29 14:49:00', '2020-04-29 02:22:58', 'Define an Episode', '<h6>Patients with a previous occurrence of OHE have a <br /><strong>40% risk of recurring OHE</strong> at 1 year.<sup>1</sup></h6>\r\n<h6>Patients with recurrent HE have a <br /><strong>40% risk of another recurring</strong> within 6 months, despite lactulose treatment.<sup>1</sup></h6>', '<ul class=\"\">\r\n<li>Monitoring and Documenting Overt Hepatic Encephalopathy Episodes</li>\r\n<li>Help Patients Set Goals Using Shared Decision Making</li>\r\n<li>Counseling Your Hepatic Encephalopathy Patients About Driving</li>\r\n</ul>', '<ul class=\"\">\r\n<li>Important Signs and Symptoms for Patients With Chronic Liver Disease</li>\r\n<li>Prevent Another Attack for Overt Hepatic Encephalopathy</li>\r\n<li>Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy</li>\r\n<li>Goal Setting When You Have Chronic Liver Disease</li>\r\n<li>Living With Diabetes and Liver Disease</li>\r\n<li>Setting Up Medical Alerts on Digital Devices</li>\r\n</ul>', 'define-an-episode', '<ol>\r\n<li>Vilstrup H, Amodio P, Bajaj J, et al. Hepatic encephalopathy in chronic liver disease: 2014 practice guideline by the American Association for the Study of Liver Diseases and the European Association for the Study of the Liver. <em>Hepatology.</em> 2014;60(2):715-735.</li>\r\n</ol>'),
(6, 'Coordinate Care', '2020-03-02 08:23:00', '2020-05-21 15:24:56', 'Coordinate Care', '<h6><strong>22% to 37% of 30-day readmissions</strong></h6>\r\n<h6>among patients with cirrhosis are potentially preventable with improved disease management.<sup>1,2</sup></h6>', '<ul class=\"\">\r\n<li>Nurse Checklist for Patients With Hepatic Encephalopathy Who Are Transitioning to Another Care Setting</li>\r\n<li>Discuss Patient Journaling With Your Patients</li>\r\n<li>Nurse Checklist for Liver Disease and Hepatic Encephalopathy</li>\r\n</ul>', '<ul class=\"\">\r\n<li>I Have HE</li>\r\n<li>Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers</li>\r\n<li>Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy</li>\r\n<li>The Importance of Patient Journaling</li>\r\n</ul>', 'coordinate-care', '<ol>\r\n<li>Volk ML, Tocco RS, Bazick J, et al. Hospital re-admissions among patients with decompensated cirrhosis. <em>Am J Gastroenterol.</em> 2012;107(2):247-252.</li>\r\n<li>Agrawal K, Kumar P, Markert R, Agrawal S. Risk factors for 30-day readmissions of individuals with decompensated cirrhosis. <em>South Med J.</em> 2015;108(11):682-687.</li>\r\n<li>Tapper EB, Halbert B, Mellinger J. Rates of and reasons for hospital readmissions in patients with cirrhosis: a multistate population-based cohort study. <em>Clin Gastroenterol Hepatol.</em> 2016;14:1181-1188.</li>\r\n<li>Hayward KL, Valery PC, Martin JH, et al. Medication beliefs predict medication adherence in ambulatory patients with decompensated cirrhosis. <em>World J Gastroenterol.</em> 2017;23(40):7321-7331.</li>\r\n</ol>');

-- --------------------------------------------------------

--
-- Table structure for table `service_sections`
--

CREATE TABLE `service_sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seq_no` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_sections`
--

INSERT INTO `service_sections` (`id`, `service_id`, `content`, `seq_no`, `created_at`, `updated_at`) VALUES
(1, 4, '<div class=\"title\">\r\n<p>Common types of CLD in the United States are increasing:</p>\r\n</div>\r\n<p>Non-alcoholic fatty liver disease (NAFLD) and resulting non-alcoholic steatohepatitis (NASH) are forecasted to increase 21% and 63%, respectively, between 2015 and 2030 due to high rates of diabetes and obesity.<sup>*3</sup></p>\r\n<p>Decompensated cirrhosis may increase 168% by 2030, while hepatocellular carcinoma will increase by 137%.<sup>*3</sup></p>\r\n<div class=\"title margin-top1\">\r\n<p>As the prevalence of cirrhosis rises, it is expected that cirrhosis complications, including hepatic encephalopathy (HE), will also rise.</p>\r\n</div>\r\n<p>The prevalence of minimal or covert HE (CHE) occurs in as many as 50% of patients with CLD, so guidelines recommend every CLD patient be screened.<sup>4</sup></p>\r\n<p>Overt HE (OHE) is estimated to occur in up to 30% to 40% of patients with cirrhosis.<sup>4</sup></p>\r\n<p>Screening for CHE is important because it can prognosticate OHE development, indicate poor quality of life, and help identify patients and caregivers who may need to be counseled about the disease.<sup>4</sup></p>\r\n<p>HE affects the patient, the patient&rsquo;s family, and everyone the patient comes in contact with. HE affects the patient&rsquo;s ability to work and drive, and increases health care costs. It is important to recognize and treat HE to improve these conditions.<sup>5</sup></p>\r\n<div class=\"small-note\">*These statistics are based on projections and actual prevalence may differ.</div>', 1, '2020-02-29 14:06:00', '2020-04-29 15:09:39'),
(2, 4, '<p><strong>LIVERHEALTHNOW</strong> has resources to support you as you screen CLD patients for HE, with tools for providers and companion patient and caregiver tools to educate patients through screening and diagnosis of HE.</p>', 2, '2020-02-29 14:07:33', '2020-02-29 14:07:33'),
(3, 5, '<p>Once you have identified patients with hepatic encephalopathy (HE), work with them and/or their caregivers to come up with a management plan to help reduce the risk of recurrence by monitoring for symptoms and identifying an episode of overt HE (OHE). If patients are monitored regularly, symptoms of an OHE episode can be addressed before a transition of care is needed.</p>\r\n<div class=\"title\">\r\n<p>An episode or occurrence in patients with OHE can be identified by testing/monitoring for the following symptoms:</p>\r\n</div>\r\n<div class=\"list-one \">\r\n<ul class=\"\">\r\n<li>Disorientation for time. Getting any 3 of the following wrong: day of the month, day of the week, month, season, or year</li>\r\n<li>Lethargy or apathy</li>\r\n<li>Obvious personality change</li>\r\n<li>Inappropriate behavior</li>\r\n<li>Dyspraxia</li>\r\n<li>Asterixis</li>\r\n<li>Somnolence to semistupor</li>\r\n<li>Responsive to stimuli</li>\r\n<li>Confusion</li>\r\n<li>Gross disorientation</li>\r\n<li>Bizarre behavior</li>\r\n<li>Coma</li>\r\n</ul>\r\n</div>', 1, '2020-02-29 14:51:00', '2020-04-28 23:42:46'),
(4, 5, '<p>The <strong>LIVERHEALTHNOW</strong> tools in this section have been developed to help you recognize evidence of HE episodes so you can help reduce the risk of recurrence and even hospitalization. There are also tools to help patients and/or their caregivers monitor symptoms between medical visits.</p>', 2, '2020-02-29 14:52:00', '2020-04-28 23:35:03'),
(5, 6, '<p>Patients with cirrhosis can present with complex, often conflicting medical needs (eg, diuretics for ascites with renal insufficiency or hyponatremia; anxiety treated with sedatives that may precipitate encephalopathy).<sup>3</sup></p>\r\n<p>Continuous coordination of care is needed in the areas of medication reconciliation and patient/ family education interventions are needed to maintain vigilance.<sup>3</sup></p>\r\n<p>Open discussions with patients about medication and liver disease may help improve adherence.<sup>4</sup></p>\r\n<p>Collaboration with the patient&rsquo;s family, the general practitioner, and caregivers helps everyone involved understand the management plan for the patient, including new medications, comorbidities, and follow-up visits.</p>', 1, '2020-03-02 08:24:00', '2020-04-28 23:50:19'),
(6, 6, '<p><strong>LIVERHEALTHNOW</strong> has resources to support you as you coordinate care for your patients with chronic liver disease with tools for providers and companion patient and caregiver tools to educate patients.</p>', 2, '2020-03-02 08:25:22', '2020-03-02 08:25:22');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings\\June2021\\iYxoEMHcjzfgxMn4LxSt.jpg', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings/April2021/hRfE8Vzmi6qoM3UyPCA7.png', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Novartis', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to NovartisAdmin', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings\\June2021\\r9tb67EJxAi1wCSKWV88.jpg', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `shared_priorities`
--

CREATE TABLE `shared_priorities` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shared_priorities`
--

INSERT INTO `shared_priorities` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Deeper patient empowerment and engagement', '2020-12-18 12:52:38', '2020-12-18 12:52:38'),
(2, 'Better adherence', '2020-12-18 12:52:00', '2021-04-02 10:33:22'),
(3, 'Earlier diagnosis and treatment', '2020-12-18 12:52:00', '2020-12-31 10:52:42'),
(4, 'Improved MS patient outcomes', '2020-12-18 12:53:00', '2020-12-31 10:53:15'),
(5, 'Improved standards of care', '2020-12-18 12:53:07', '2020-12-18 12:53:07'),
(6, 'Improved patient access to care', '2020-12-18 12:53:00', '2020-12-31 10:53:47');

-- --------------------------------------------------------

--
-- Table structure for table `sources`
--

CREATE TABLE `sources` (
  `id` int(10) UNSIGNED NOT NULL,
  `sources` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sr_no` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sources`
--

INSERT INTO `sources` (`id`, `sources`, `status`, `sr_no`, `created_at`, `updated_at`) VALUES
(1, 'Through my Intercept Contact', '1', 1, '2019-05-08 05:22:00', '2021-07-01 06:23:34'),
(2, 'Through a colleague', '1', 2, '2019-05-08 05:23:05', '2019-05-08 05:23:05'),
(3, 'Through a Web search', '1', 3, '2019-05-08 05:23:00', '2021-04-21 15:06:56'),
(4, 'Via email', '1', 4, '2019-05-08 05:23:00', '2021-04-21 15:06:13'),
(5, 'Other', '1', 5, '2019-05-08 05:23:41', '2019-05-08 05:23:41');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `state` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `state`, `created_at`, `updated_at`) VALUES
(3, 'AK', '2021-04-19 18:06:25', '2021-04-19 18:06:25'),
(4, 'AL', '2021-04-19 18:06:50', '2021-04-19 18:06:50'),
(5, 'AZ', '2021-04-19 18:07:06', '2021-04-19 18:07:06'),
(6, 'CA', '2021-04-19 18:07:13', '2021-04-19 18:07:13'),
(7, 'CO', '2021-04-19 18:07:20', '2021-04-19 18:07:20'),
(8, 'CT', '2021-04-19 18:07:26', '2021-04-19 18:07:26'),
(9, 'DE', '2021-04-19 18:07:32', '2021-04-19 18:07:32'),
(10, 'FL', '2021-04-19 18:07:39', '2021-04-19 18:07:39'),
(11, 'GA', '2021-04-19 18:07:45', '2021-04-19 18:07:45'),
(12, 'HI', '2021-04-19 18:07:52', '2021-04-19 18:07:52'),
(13, 'IA', '2021-04-19 18:07:58', '2021-04-19 18:07:58'),
(14, 'ID', '2021-04-19 18:08:09', '2021-04-19 18:08:09'),
(15, 'IL', '2021-04-19 18:08:23', '2021-04-19 18:08:23'),
(16, 'IN', '2021-04-19 18:08:30', '2021-04-19 18:08:30'),
(17, 'KS', '2021-04-19 18:08:38', '2021-04-19 18:08:38'),
(18, 'KY', '2021-04-19 18:08:44', '2021-04-19 18:08:44'),
(19, 'LA', '2021-04-19 18:08:51', '2021-04-19 18:08:51'),
(20, 'MA', '2021-04-19 18:08:58', '2021-04-19 18:08:58'),
(21, 'MD', '2021-04-19 18:09:03', '2021-04-19 18:09:03'),
(22, 'ME', '2021-04-19 18:09:10', '2021-04-19 18:09:10'),
(23, 'MI', '2021-04-19 18:09:16', '2021-04-19 18:09:16'),
(24, 'MN', '2021-04-19 18:09:22', '2021-04-19 18:09:22'),
(25, 'MO', '2021-04-19 18:09:29', '2021-04-19 18:09:29'),
(26, 'MS', '2021-04-19 18:09:36', '2021-04-19 18:09:36'),
(27, 'MT', '2021-04-19 18:09:56', '2021-04-19 18:09:56'),
(28, 'NC', '2021-04-19 18:10:05', '2021-04-19 18:10:05'),
(29, 'ND', '2021-04-19 18:10:12', '2021-04-19 18:10:12'),
(30, 'NE', '2021-04-19 18:10:18', '2021-04-19 18:10:18'),
(31, 'NH', '2021-04-19 18:10:24', '2021-04-19 18:10:24'),
(32, 'NJ', '2021-04-19 18:10:31', '2021-04-19 18:10:31'),
(33, 'NM', '2021-04-19 18:10:40', '2021-04-19 18:10:40'),
(34, 'NV', '2021-04-19 18:10:45', '2021-04-19 18:10:45'),
(35, 'NY', '2021-04-19 18:10:51', '2021-04-19 18:10:51'),
(36, 'OH', '2021-04-19 18:10:57', '2021-04-19 18:10:57'),
(37, 'OK', '2021-04-19 18:11:02', '2021-04-19 18:11:02'),
(38, 'OR', '2021-04-19 18:11:10', '2021-04-19 18:11:10'),
(39, 'PA', '2021-04-19 18:11:16', '2021-04-19 18:11:16'),
(40, 'RI', '2021-04-19 18:11:22', '2021-04-19 18:11:22'),
(41, 'SC', '2021-04-19 18:11:29', '2021-04-19 18:11:29'),
(42, 'SD', '2021-04-19 18:11:35', '2021-04-19 18:11:35'),
(43, 'TN', '2021-04-19 18:11:41', '2021-04-19 18:11:41'),
(44, 'TX', '2021-04-19 18:11:49', '2021-04-19 18:11:49'),
(45, 'UT', '2021-04-19 18:11:55', '2021-04-19 18:11:55'),
(46, 'VA', '2021-04-19 18:12:01', '2021-04-19 18:12:01'),
(47, 'VT', '2021-04-19 18:12:07', '2021-04-19 18:12:07'),
(48, 'WA', '2021-04-19 18:12:13', '2021-04-19 18:12:13'),
(49, 'WI', '2021-04-19 18:12:20', '2021-04-19 18:12:20'),
(50, 'WV', '2021-04-19 18:12:26', '2021-04-19 18:12:26'),
(51, 'WY', '2021-04-19 18:12:33', '2021-04-19 18:12:33');

-- --------------------------------------------------------

--
-- Table structure for table `supports`
--

CREATE TABLE `supports` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supports`
--

INSERT INTO `supports` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Support 1', '2021-04-07 10:19:42', '2021-04-07 10:19:42'),
(2, 'Support 2', '2021-04-07 10:20:01', '2021-04-07 10:20:01');

-- --------------------------------------------------------

--
-- Table structure for table `support_requests`
--

CREATE TABLE `support_requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `manager_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `support_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `support_requests`
--

INSERT INTO `support_requests` (`id`, `manager_name`, `support_type`, `first_name`, `last_name`, `organization`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Alli Brinker', 'Support 1', 'Novartis Admin', 'Admin', 'aventia', 'admin@admin.com', '2021-04-07 10:20:15', '2021-04-07 10:20:15'),
(2, 'Alli Brinker', NULL, 'sanat', 'Admin', 'Leo9 Studio', 'sanat.leo9@gmail.com', '2021-04-27 09:58:52', '2021-04-27 09:58:52'),
(3, 'Ashley Ingram', NULL, 'Carolyn', 'Odenwald', 'Aventria Health', 'carolyn.odenwald@aventriahealth.com', '2021-04-27 13:50:14', '2021-04-27 13:50:14'),
(4, 'Ashley Ingram', NULL, 'Carolyn', 'Odenwald', 'Aventria Health', 'carolyn.odenwald@aventriahealth.com', '2021-04-27 13:50:55', '2021-04-27 13:50:55'),
(5, 'Ashley Ingram', NULL, 'Carolyn', 'Odenwald', 'Aventria Health', 'carolyn.odenwald@aventriahealth.com', '2021-04-27 21:14:14', '2021-04-27 21:14:14'),
(6, 'Alli Brinker', NULL, 'Novartis Admin', 'Admin', 'aventia', 'admin@admin.com', '2021-04-28 07:03:21', '2021-04-28 07:03:21'),
(7, 'Ashley Ingram', NULL, 'Carolyn', 'Odenwald', 'Aventria Health', 'carolyn.odenwald@aventriahealth.com', '2021-04-28 20:59:09', '2021-04-28 20:59:09'),
(8, 'Alli Brinker', NULL, 'sanat', 'gawade', 'leo9', 'gawade.sanat@gmail.com', '2021-04-29 02:41:48', '2021-04-29 02:41:48'),
(9, 'Alli Brinker', NULL, 'sanat', 'gawade', 'leo9', 'gawade.sanat@gmail.com', '2021-04-29 06:37:18', '2021-04-29 06:37:18'),
(10, 'Alli Brinker', NULL, 'sanat', 'gawade', 'leo9', 'gawade.sanat@gmail.com', '2021-04-29 10:32:15', '2021-04-29 10:32:15'),
(11, 'Alli Brinker', NULL, 'sanat', 'gawade', 'leo9', 'gawade.sanat@gmail.com', '2021-04-29 10:37:20', '2021-04-29 10:37:20'),
(12, 'Ashley Ingram', NULL, 'Carolyn', 'Odenwald', 'Aventria Health', 'carolyn.odenwald@aventriahealth.com', '2021-04-29 10:58:19', '2021-04-29 10:58:19'),
(13, 'Alli Brinker', NULL, 'sanat', 'gawade', 'leo9', 'gawade.sanat@gmail.com', '2021-04-29 11:03:02', '2021-04-29 11:03:02'),
(14, 'I Don\'t Know', NULL, 'Kathleen', 'Kurtz', 'Aventria', 'kathleen.kurtz@franklynhc.com', '2021-04-29 13:29:05', '2021-04-29 13:29:05'),
(15, 'I Don\'t Know', NULL, 'Healthcare', 'Provider', 'MS Center', 'hcp@mscenter.com', '2021-04-29 16:21:01', '2021-04-29 16:21:01'),
(16, 'I Don\'t Know', NULL, 'Healthcare', 'Provider', 'MS Center', 'hcp@mscenter.com', '2021-04-29 16:26:19', '2021-04-29 16:26:19'),
(17, 'I Don\'t Know', NULL, 'Healthcare', 'Provider', 'MS Center', 'hcp@mscenter.com', '2021-04-30 13:39:05', '2021-04-30 13:39:05'),
(18, 'Ashley Ingram', NULL, 'Briana', 'Brogan', 'Aventria Health Group', 'briana.brogan@aventriahealth.com', '2021-05-03 18:22:08', '2021-05-03 18:22:08'),
(19, 'I Don\'t Know', NULL, 'Healthcare', 'Provider', 'MS Center', 'hcp@mscenter.com', '2021-05-03 18:30:09', '2021-05-03 18:30:09'),
(20, 'I Don\'t Know', NULL, 'Healthcare', 'Provider', 'MS Center', 'hcp@mscenter.com', '2021-05-03 19:55:06', '2021-05-03 19:55:06'),
(21, 'I Don\'t Know', NULL, 'Healthcare', 'Provider', 'MS Center', 'hcp@mscenter.com', '2021-05-03 20:24:13', '2021-05-03 20:24:13'),
(22, 'I Don\'t Know', NULL, 'Healthcare', 'Provider', 'MS Center', 'hcp@mscenter.com', '2021-05-05 13:37:06', '2021-05-05 13:37:06'),
(23, 'I Don\'t Know', NULL, 'Healthcare', 'Provider', 'MS Center', 'hcp@mscenter.com', '2021-05-05 14:52:16', '2021-05-05 14:52:16'),
(24, 'I Don\'t Know', NULL, 'Sandra', 'Arts', 'Aventria Health Group', 'sandra.arts@aventriahealth.com', '2021-05-05 18:35:58', '2021-05-05 18:35:58'),
(25, 'I Don\'t Know', NULL, 'Deb', 'Buhosky', 'Aventria', 'deb.buhosky@avantriahealth.com', '2021-05-06 12:41:08', '2021-05-06 12:41:08'),
(26, 'I don\'t know', NULL, 'Healthcare', 'Provider', 'MS Center', 'hcp@mscenter.com', '2021-05-14 18:45:03', '2021-05-14 18:45:03'),
(27, 'I don\'t know', NULL, 'Healthcare', 'Provider', 'MS Center', 'hcp@mscenter.com', '2021-05-21 14:31:04', '2021-05-21 14:31:04'),
(28, 'I don\'t know', NULL, 'Healthcare', 'Provider', 'MS Center', 'hcp@mscenter.com', '2021-05-21 20:51:49', '2021-05-21 20:51:49'),
(29, 'Alli Brinker', NULL, 'sanat', 'Admin', 'Leo9 Studio', 'sanat@leo9studio.com', '2021-06-09 14:01:51', '2021-06-09 14:01:51'),
(30, 'Alli Brinker', NULL, 'sanat', 'Admin', 'Leo9 Studio', 'sanat@leo9studio.com', '2021-06-09 14:01:51', '2021-06-09 14:01:51'),
(31, 'I don\'t know', NULL, 'Eric', 'Hoch', 'Aventria Health Group', 'eric.hoch@aventriahealth.com', '2021-06-17 02:14:46', '2021-06-17 02:14:46');

-- --------------------------------------------------------

--
-- Table structure for table `targetaudiences`
--

CREATE TABLE `targetaudiences` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `targetaudiences`
--

INSERT INTO `targetaudiences` (`id`, `title`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Patient', '1', '2019-05-13 03:47:00', '2019-05-13 03:48:02'),
(3, 'Health Care Provider', '1', '2019-05-13 03:48:00', '2020-03-11 19:52:16');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at_time_zone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at_timezone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salix_manager_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `update` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agreement_check` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timezone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login_timezone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salix_manager_id` int(11) DEFAULT NULL,
  `need_assessment` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `created_at_time_zone`, `updated_at_timezone`, `lname`, `organization`, `organization_id`, `job_title`, `contact`, `address`, `city`, `state`, `salix_manager_name`, `source_id`, `update`, `agreement_check`, `timezone`, `last_login`, `last_login_timezone`, `code`, `status`, `department_id`, `salix_manager_id`, `need_assessment`) VALUES
(1, 1, 'Novartis Admin', 'admin@admin.com', 'users/312.jpg', '2020-05-06 15:40:00', '$2y$10$no7rZAE/hfybvxLNYZblWeAxDlzVJiZkmuaELSRdtB2JHwlHqe0ta', 'CiPbrvnZjBYcG4zaSurfwSgv3vC764HGafJeH4LVaMhuYbY5dRbtpG3GPeI4', NULL, NULL, '2021-06-16 14:09:07', NULL, NULL, 'Admin', 'aventia', NULL, 'Admin', '2321321312', NULL, '', '', 'o', NULL, '1', '1', 'America/New_York', '2021-06-16 10:04:08', 'America/New_York', NULL, '1', NULL, 1, 1),
(567, 2, 'Kathleen', 'kathleen.kurtz@franklynhc.com', 'users/RHJhZ29uZmx5LmpwZw==20210426124913.jpg', NULL, '$2y$10$k6TWVXBYRElI/CrbeDrZ5en6JS7zy0hxQ/qNR.s8bufL9XCTIbT1S', 'qDfiPzOVKvmotE4hz1oNqijXGLjDOqZrb4REasrcX4IYy0ipNRFvTbStC2Fj', NULL, '2021-04-12 15:46:02', '2021-06-07 18:15:57', NULL, 'admin@admin.com', 'Kurtz', 'Aventria', NULL, 'Editor', NULL, '6 Century Drive, Suite 170, Parsippany, NJ, 07054', '', '', NULL, '5', '1', '1', 'America/New_York', '2021-06-07 18:15:57', 'America/New_York', NULL, '1', NULL, 8, 1),
(568, 2, 'Deb', 'deb.buhosky@avantriahealth.com', 'users/Y2FtZXJhLnBuZw==20210607221936.png', NULL, '$2y$10$cXjf5naU00/pD5i7ALGbKe2JmUmYtaBIo0akTMmz7rCfAUc/Xw/uW', NULL, NULL, '2021-04-12 15:50:05', '2021-06-16 13:22:08', NULL, 'deb.buhosky@avantriahealth.com', 'Buhosky', 'Aventria', '10', 'Designer', NULL, 'Doylestown, PA', '', '', NULL, '5', '1', '1', 'America/New_York', '2021-06-16 13:22:08', 'America/New_York', NULL, '1', NULL, 8, NULL),
(569, 2, 'Eric', 'eric.hoch@aventriahealth.com', 'users/QXZlbnRyaWFfTE9HT19mdWxsIGNvbG9yIEhvcml6b250YWwucG5n20210607215855.png', NULL, '$2y$10$Pn7UAn5xXWT4uFoHFif9k.ENxp4rhXDIcrZjrWlKhsXkIl6SLOdxO', 'zGyLFvpoyl8wmsAdu9lmCkpQdPp0ziqfe6th7gMEe5QbOqhH4aj4Z98hB7SV', NULL, '2021-04-19 16:44:14', '2021-06-17 07:07:57', NULL, 'admin@admin.com', 'Hoch', 'Aventria Health Group', NULL, 'VP', NULL, '6 Century Drive Ste 170', '', '', NULL, '5', '1', '1', 'America/New_York', NULL, NULL, NULL, '1', NULL, 8, 0),
(570, 2, 'sanat', 'sanat.leo9@gmail.com', 'users/default.png', NULL, '$2y$10$mvtRqjoU.BZpYC.jUAnKDe4Nk267EypWfKeDokQ4nNej0n8cKyDSC', 'rJToWxJ0vhjY0sJsH3PQ8jEONh3TlIFTGxd1FP3Fx0eXkWKjguhMFWwoO6Jh', NULL, '2021-04-20 10:13:42', '2021-06-09 19:31:29', NULL, NULL, 'Admin', 'Leo9 Studio', '11', 'developer', '9004318850', 'Umberto Ceramics International Pvt. Ltd. Sadoliya - Galteshwar Road, Off. N.H.-8, Tal. Prantij, Dist. Sabarkantha, Gujarat -383205, India', '', '', NULL, '1', '1', '1', 'Asia/Kolkata', '2021-06-09 19:31:29', 'Asia/Kolkata', NULL, '1', NULL, 1, 0),
(571, 2, 'sanat', 'harsh@leo9studio.com', 'users/default.png', NULL, '$2y$10$Vvd2bX5Bl3C0yvPuKoLm5OdrHYbkrNjWcYXKqDumqrtp8MZO4OsOq', NULL, NULL, '2021-04-20 10:22:06', '2021-04-20 10:22:06', NULL, NULL, 'Admin', 'Leo9 Studio', '11', 'developer', '9004318850', 'Umberto Ceramics International Pvt. Ltd. Sadoliya - Galteshwar Road, Off. N.H.-8, Tal. Prantij, Dist. Sabarkantha, Gujarat -383205, India', '', '', NULL, '2', '1', '1', NULL, NULL, NULL, NULL, '0', NULL, 2, NULL),
(573, 2, 'Briana', 'briana.brogan@aventriahealth.com', 'users/default.png', NULL, '$2y$10$5mkGoIc1LcOg.3901YPQTe8vymwc/7cB5TB/1GOe/6R/lBL7M5HOS', NULL, NULL, '2021-04-21 19:23:05', '2021-05-25 09:17:27', NULL, NULL, 'Brogan', 'Aventria Health Group', '14', 'Account Executive', NULL, '6 Century Drive, Suite 170, Parsippany, NJ, 07054', '', '', NULL, '5', '1', '1', 'America/New_York', '2021-05-25 09:17:27', 'America/New_York', NULL, '1', NULL, 4, NULL),
(574, 2, 'Carolyn', 'carolyn.odenwald@aventriahealth.com', 'users/Y2xldmVsYW5kIGNsaW5pYy5wbmc=20210503175628.png', NULL, '$2y$10$6vQUizXViFNvWdGMoC6vCOofp/gJm3fPNhqY3EpSm7hRY0ES0WwJa', 'dvre7khjjbe08NSgaXF2XyohDqRDKgCwkKqGofBvp4c1usJFNsIygLDFm1FY', NULL, '2021-04-21 21:24:05', '2021-06-16 11:49:52', NULL, NULL, 'Odenwald', 'Aventria Health', '14', 'Account Supervisor', '2672219347', '5661 Timberly Lane', '', '', NULL, '1', '1', '1', 'America/New_York', '2021-06-16 11:49:52', 'America/New_York', NULL, '1', NULL, 4, 1),
(577, 2, 'sanat', 'gawade.sanat@gmail.com', 'users/default.png', NULL, '$2y$10$eldvTXnrzmMxDIZ5xVA7T.7QqtwgH0AEIWySvkg/RTZ2hutQe/NRW', 'BIy6Li65bzRbzCvwRVc5ZFsKe3kXBmYh7sgLnYpS8TNHfuz74CRm1RYbGaT2', NULL, '2021-04-22 13:45:22', '2021-05-06 14:34:41', NULL, NULL, 'gawade', 'leo9', '11', 'Php Developer', '9930526914', 'A/102, Haritara Mhatre wadi Dahisar (w)', '', '', NULL, '1', '1', '1', 'Asia/Kolkata', '2021-05-06 11:54:13', 'Asia/Kolkata', NULL, '0', NULL, 1, 1),
(578, 2, 'harsh', 'harsh@lstudio.com', 'users/default.png', NULL, '$2y$10$xy52U2erCaz7R/kJJ6qJL.sqkcERCLoPZ4aFbRGRJjVLec9iaT83i', NULL, NULL, '2021-04-23 05:54:40', '2021-04-23 05:54:42', NULL, NULL, 'Admin', 'harsh', '11', 'developer', '9004318850', 'Umberto Ceramics International Pvt. Ltd. Sadoliya - Galteshwar Road, Off. N.H.-8, Tal. Prantij, Dist. Sabarkantha, Gujarat -383205, India', '', '', NULL, '1', '1', '1', 'Asia/Kolkata', NULL, NULL, NULL, '0', NULL, 1, NULL),
(580, 2, 'Office', 'office@aventriahealth.com', 'users/default.png', NULL, '$2y$10$jlMJhOMYnJ/rKMYV2rgFb.N7FZTTKJKfNTYP8OK4/U93CnUm.fzxS', NULL, NULL, '2021-04-25 23:47:39', '2021-04-26 06:22:30', NULL, NULL, 'Aventria', 'Aventria Health Group', '14', 'VP', NULL, '6 Century Drive', '', '', NULL, '5', '1', '1', 'America/New_York', NULL, NULL, NULL, '1', NULL, 8, 0),
(581, 2, 'Alli', 'alli.brinker@novartis.com', 'users/default.png', '2021-04-26 09:51:00', '$2y$10$AkoYuNkrop4Z2Sw9Suug6.1ODBuFn1EK8ZO.SW10MMMIseIqN1hcy', NULL, NULL, '2021-04-26 13:51:46', '2021-05-26 14:47:30', NULL, NULL, 'Brinker', 'Novartis', NULL, 'Account Manager', NULL, NULL, '', '', NULL, NULL, '1', '1', 'America/New_York', '2021-05-26 14:47:30', 'America/New_York', NULL, '1', NULL, 1, 1),
(582, 2, 'Hannah', 'hannahcassel7@gmail.com', 'users/default.png', NULL, '$2y$10$TbdZq0RWJx2u7dF1/tq23OcJHdwWV3/Lk8qxlIyxs02/t9X8sGHjO', NULL, NULL, '2021-04-26 14:04:20', '2021-04-26 16:23:46', NULL, NULL, 'Cassel', 'Aventria Health Group', '14', 'a', '4848661052', '131 N 4th St', '', '', NULL, '5', '1', '1', 'America/New_York', NULL, NULL, NULL, '1', NULL, 8, 0),
(583, 2, 'Jacqueline', 'jacqueline.delarosa@novartis.com', 'users/default.png', NULL, '$2y$10$NF/L9XMfRhy54ZehDkLAmeADNlmL8avBzXO3jkZU8DknircDdPCw2', NULL, NULL, '2021-04-26 14:32:23', '2021-05-25 19:54:39', NULL, NULL, 'DeLaRosa', 'Novartis', NULL, 'Account Manager', '2032321380', NULL, '', '', NULL, NULL, '1', '1', 'America/New_York', '2021-05-25 15:53:41', 'America/New_York', NULL, '1', NULL, NULL, 1),
(584, 2, 'Marci', 'marci.grebing@novartis.com', 'users/default.png', NULL, '$2y$10$An/IaUnSm8DYzUYUuvjXguNdc2rLUKw.D0Xgc.z67giVFGKuXL5Pq', NULL, NULL, '2021-04-26 14:37:23', '2021-04-29 11:46:19', NULL, NULL, 'Grebing', 'Novartis', NULL, 'Account Manager', NULL, NULL, '', '', NULL, NULL, '1', '1', 'America/New_York', '2021-04-29 11:46:19', 'America/New_York', NULL, '1', NULL, NULL, 1),
(585, 2, 'Ashley', 'ashley.ingram@novartis.com', 'users/MjAxOS0wMy0wN18xMC0wMS0yOC5wbmc=20210525160548.png', NULL, '$2y$10$f.Ng.h5CeVjMHTLDFbpbK.L2dkASZGdDr1fWM/xkUvf7.iW/.Gc9C', NULL, NULL, '2021-04-26 14:44:59', '2021-06-16 11:15:25', NULL, NULL, 'Ingram', 'Novartis', NULL, 'Account Manager', NULL, NULL, '', '', NULL, NULL, '1', '1', 'America/New_York', '2021-06-16 11:15:25', 'America/New_York', NULL, '1', NULL, NULL, 1),
(586, 2, 'Jodi', 'jodi.johnson@novartis.com', 'users/default.png', NULL, '$2y$10$1K/u.7yLXNUB4cEZiu8q1Oa0cevf5TEN6PLgcIS8SErMwDjZjTuea', 'Om5f5r6oHassYS9GEA1cGHt9rk5vDNLp6fDwyRxZgI9LWAJsTHJZsBNFUl9u', NULL, '2021-04-26 14:46:00', '2021-06-14 17:34:35', NULL, NULL, 'Johnson', 'Novartis', NULL, 'Account Manager', NULL, NULL, '', '', NULL, NULL, '1', '1', 'Europe/Zurich', '2021-05-25 14:55:14', 'America/Chicago', NULL, '1', NULL, NULL, 1),
(587, 2, 'Lori', 'lori-2.martin@novartis.com', 'users/default.png', NULL, '$2y$10$fH2.B/bjaj7OI2pAGLmaFOyfeGXOjcepSOcwWGq1hm7e9B.bVuqsK', NULL, NULL, '2021-04-26 14:55:44', '2021-05-26 12:09:21', NULL, NULL, 'Martin', 'Novartis', NULL, 'Account Manager', NULL, NULL, '', '', NULL, NULL, '1', '1', 'America/Chicago', '2021-05-26 12:09:21', 'America/Chicago', NULL, '1', NULL, NULL, 1),
(588, 2, 'Lauren', 'lauren.mckenna@novartis.com', 'users/default.png', NULL, '$2y$10$orTeig9HVPibrTXZI/mrEulauRRanCqguWEiQ0NSuVs9206Z548HW', NULL, NULL, '2021-04-26 15:00:58', '2021-04-29 11:47:40', NULL, NULL, 'McKenna', 'Novartis', NULL, 'Account Manager', NULL, NULL, '', '', NULL, NULL, '1', '1', 'America/New_York', '2021-04-29 11:47:40', 'America/New_York', NULL, '1', NULL, NULL, 1),
(589, 2, 'Eileen', 'eileen.urban@novartis.com', 'users/SGFwcHkgQmlydGhkYXkgQ2FuZGxlcy5qcGc=20210525200855.jpg', NULL, '$2y$10$kWZ/g39JrPYvabYxGtORCesSWrHsa.Q8ayDlDLZM0TIrmERsSsdEy', 'scvtiODP1t7IRdIoctMXRwOg30ABxIhVJdSFZLdxSEmKQXdlGfNzsnWUkvkh', NULL, '2021-04-26 15:05:57', '2021-06-09 13:03:45', NULL, NULL, 'Urban', 'Novartis', NULL, 'Account Manager', '9737786293', NULL, '', '', NULL, NULL, '1', '1', 'America/New_York', '2021-06-09 13:03:45', 'America/New_York', NULL, '1', NULL, NULL, 1),
(590, 2, 'Robert', 'robert.creech@novartis.com', 'users/default.png', NULL, '$2y$10$JSWDw8dnkU9zjzQuAubhDumV8OV/NKSlSJuBeiGHS/ZAmIn1Hc4aW', NULL, NULL, '2021-04-26 15:07:40', '2021-05-26 14:38:20', NULL, NULL, 'Creech', 'Novartis', NULL, 'Account Manager', NULL, NULL, '', '', NULL, NULL, '1', '1', 'America/New_York', '2021-05-26 14:38:20', 'America/New_York', NULL, '1', NULL, NULL, 1),
(591, 2, 'Name', 'sandra.arts@aventriahealth.com', 'users/U2NyZWVuIFNob3QgMjAyMS0wNS0xOSBhdCA5LjQ4LjE0IEFNLnBuZw==20210519135919.png', NULL, '$2y$10$2m5lbwC6.5iRn6jc9Ic4EO0/iER7i/l1QcMRFq5RRAlFfmCNwhLoq', 'XY7C4jWjc9Yxh2BYAUAj8N1bU2GYfwnrwxsRPKjhmp9Sx4mtQmJIAefbnQdu', NULL, '2021-04-27 15:41:36', '2021-06-02 15:28:49', NULL, NULL, 'Arts', 'Aventria Health Group', '14', 'Project Manager', '2154899000', '259 Veterans Lane', '', '', NULL, '5', '1', '1', 'America/New_York', '2021-06-02 15:28:49', 'America/New_York', NULL, '1', NULL, 8, 0),
(592, 2, 'Kathy', 'kkwheel@gmail.com', 'users/default.png', NULL, '$2y$10$VG6deS6.9nKav3ivg0tUM.GzKH/jGurWGvpJR9zllfzeDpPCxWBwy', NULL, NULL, '2021-04-27 18:40:31', '2021-04-27 20:23:20', NULL, NULL, 'Kurtz', 'Health care', '14', 'TEST', NULL, 'Parsippany', '', '', NULL, '4', '1', '1', 'America/New_York', NULL, NULL, NULL, '0', NULL, 8, 0),
(593, 2, 'Account', 'accountmanager@novartis.com', 'users/default.png', NULL, '$2y$10$JbR5bZeRbygt2t0zS4OslO5mwV39fHHDMKekC9UfSzeDCRTmOGlXK', 'lUPbn874bzxxpbWXCWtntFdlOkszqfWkx1WSJFVeyRPyBw4PlExrfjA0LcZB', NULL, '2021-04-28 21:14:16', '2021-06-16 10:23:00', NULL, 'admin@admin.com', 'Manager', 'Novartis', '14', 'Account Manager', NULL, '123 Main Street', '', '', NULL, '2', '1', '1', 'America/New_York', '2021-06-16 10:22:59', 'America/New_York', NULL, '1', NULL, 8, 1),
(594, 2, 'Healthcare', 'hcp@mscenter.com', 'users/default.png', NULL, '$2y$10$gLi9QdoKfFzaLlBWQG/La.bWY8amkwWkr1d/tsZMRGoH/mkNnqrlq', 'Dtl0FiNpPKXA18hTpMx4FXKHhXMGA3gCRbYNIft8K1w0kEHHFXEbV5IIOGfm', NULL, '2021-04-28 21:19:09', '2021-05-26 10:50:49', NULL, 'admin@admin.com', 'Provider', 'MS Center', '11', 'HCP', NULL, '123 Main Street', '', '', NULL, '2', '1', '1', 'America/New_York', '2021-05-26 10:50:49', 'America/New_York', NULL, '1', NULL, 8, 0),
(595, 2, 'testing', 'testingemail@gmail.com', 'users/default.png', NULL, '$2y$10$BQFGHnQZ/LdkOHDNIUY2U.OVAzIsmo1I8M25mrcS.oEnPNjULY6dG', NULL, NULL, '2021-04-29 07:31:56', '2021-04-29 16:07:31', NULL, NULL, 'email', 'leo9 studio', '11', 'Senior Developer', '9930526914', 'Mumbai Dahiasr', '', '', NULL, '2', '1', '1', 'Asia/Kolkata', NULL, NULL, NULL, '0', NULL, 1, 0),
(597, 2, 'Hannah', 'hannah.cassel@aventriahealth.com', 'users/default.png', NULL, '$2y$10$/vbHgBXV4OojXbRfoWY2j.iG3dyNFbjOQnpUJfGpMZmjMet.9A.B.', 'lgOdErYqDdQpv8pOUraFcItbCFu1jPB3OHxgPqVld3WbN1oTA98DgX2GNyuj', NULL, '2021-05-05 18:15:19', '2021-05-05 18:32:02', NULL, NULL, 'Cassel', 'Aventria Health Group', '14', 'z', '4848661052', '1766 Church View Road', '', '', NULL, '5', '1', '1', 'America/New_York', NULL, NULL, NULL, '0', NULL, 8, 0),
(598, 2, 'Hannah', 'hannahcassel113@gmail.com', 'users/default.png', NULL, '$2y$10$LIUfHPDCvsVGObBWDCPkm.fQp4YsrYHw39oXyxl945tk/r9J5ESQG', 'pqJxGS8v1BLv2GVWp6hbjhKtQUroBpW5e0lQvFJx4WBpPB8fS2cJFaWXwQAR', NULL, '2021-05-06 01:31:06', '2021-05-06 13:52:07', NULL, 'deb.buhosky@avantriahealth.com', 'Cassel', 'Test', '14', 'A', NULL, 'Test', '', '', NULL, '5', '1', '1', 'America/New_York', NULL, NULL, NULL, '1', NULL, 8, 0),
(599, 2, 'mary', 'marymoon@moon.com', 'users/default.png', NULL, '$2y$10$ukNgeQhkOO/HuBJ57BkrFuPawNcVmqp8ex9M.C7Hf9hmVMNxtA5TS', NULL, NULL, '2021-05-06 12:33:54', '2021-05-06 12:49:24', NULL, 'deb.buhosky@avantriahealth.com', 'moon', 'Aventria', '14', 'Account Manager', NULL, '123 ABC Street', '', '', NULL, '5', '1', '1', 'America/New_York', NULL, NULL, NULL, '1', NULL, 8, 0),
(600, 2, 'Hannah', 'hannahcassel114@gmail.com', 'users/default.png', NULL, '$2y$10$FqylpimrwfdVWRTxe5xsguBWnpivqjgBTT9AeMTTEnVHvV2NYYEue', NULL, NULL, '2021-05-07 13:54:31', '2021-05-19 12:30:33', NULL, NULL, 'Cassel', 'Test', '14', 'Test', '4848661052', '1766 test', '', '', NULL, '5', '1', '1', 'America/New_York', NULL, NULL, NULL, '0', NULL, 8, NULL),
(601, 2, 'Meghan', 'meghan.heelan@novartis.com', 'users/default.png', NULL, '$2y$10$xp8bGlHzq3SqoNkxFwyrhONAs6ULMSuiDG.dbTUHS/j12q4mtPRyu', NULL, NULL, '2021-05-14 18:16:42', '2021-05-14 18:16:43', NULL, NULL, 'Heelan', 'Novartis', '14', 'Editor', NULL, 'One Health Plaza', '', '', NULL, '5', '0', '1', 'Australia/Sydney', NULL, NULL, NULL, '0', NULL, 8, NULL),
(602, 2, 'Office', 'tech@aventriahealth.com', 'users/default.png', NULL, '$2y$10$gE8GpGH/cLjTGwaZ6PSixe/3nOgHgSJc0a0s36E6KNiyXZoXfWsd2', NULL, NULL, '2021-05-18 13:07:55', '2021-05-18 13:07:56', NULL, NULL, 'AHG', 'Aventria Health Group', '14', 'vp', NULL, '6 Century Drive Ste 170', '', '', NULL, '5', '0', '1', 'America/New_York', NULL, NULL, NULL, '0', NULL, 8, NULL),
(603, 2, 'Deb', 'test@aventriahealth.com', 'users/default.png', NULL, '$2y$10$oZQnYQd8NTwOubYFu9vuTeSgK/.dcJaSjXPjSGF564lr9a8iNOZLy', NULL, NULL, '2021-05-19 13:26:05', '2021-05-19 13:26:06', NULL, NULL, 'Test', 'Aventria', '14', 'digital', NULL, '123 abc street', '', '', NULL, '5', '0', '1', 'America/New_York', NULL, NULL, NULL, '0', NULL, 8, NULL),
(604, 2, 'Hannah', 'hannahcassel@aol.com', 'users/default.png', NULL, '$2y$10$XmwHz9YhvZQqteZuCQtMy.kafxkTX6BTx1GNnZkhuRmS0zzWDPcmm', NULL, NULL, '2021-05-19 13:40:00', '2021-05-19 13:40:01', NULL, NULL, 'Cassel', 'AHG', '14', 'a', '4848661052', '1744', '', '', NULL, '5', '0', '1', 'America/New_York', NULL, NULL, NULL, '0', NULL, 8, NULL),
(605, 2, 'Meghan', 'meghan.heelan@gmail.com', 'users/default.png', NULL, '$2y$10$rTq/X6pTBDaKtFVeIHvPzuDOtEtxt5L.cBf2x/6/aPgSwv/7TYuPm', NULL, NULL, '2021-05-21 14:39:46', '2021-05-21 14:39:46', NULL, NULL, 'Heelan', 'Novartis', '11', 'Test', NULL, '1 Maple St', '', '', NULL, '5', '0', '1', 'America/New_York', NULL, NULL, NULL, '0', NULL, 8, NULL),
(606, 2, 'Heather', 'heather.collins@franklynhc.com', 'users/aGF3YWlpLWxvZ28ucG5n20210525132210.png', NULL, '$2y$10$DdSAUN7R8cMplRoax6bW1.HrHxdVNUWzp52jGzQrWDh6JCn3y9zma', 'FTBNFS1yNamYJqB3FCBo1RpMAGmIwtsLAFKLe2FNc38Xp5Fy2XAyEqfkXOCm', NULL, '2021-05-24 15:19:38', '2021-05-26 15:24:57', NULL, 'admin@admin.com', 'Collins', 'Aventria', '14', 'VP', NULL, '123 main st', '', '', NULL, '5', '1', '1', 'America/New_York', '2021-05-26 15:24:57', 'America/New_York', NULL, '1', NULL, 8, 1),
(607, 2, 'Carolyn', 'carolyn@odenwalds.com', 'users/default.png', NULL, '$2y$10$Av9wzU2SceBp6Q.QajbsR.AotPFHtVf1rskeUDV9.ewah2V8Uf3RC', NULL, NULL, '2021-06-04 15:30:39', '2021-06-04 15:30:39', NULL, NULL, 'Odenwald', 'AHG', '14', 'Account Supervisor', '2672219347', '259 Veterans Lane, Ste 301', '', '', NULL, '2', '0', '1', NULL, NULL, NULL, NULL, '0', NULL, 8, NULL),
(608, 2, 'joe', 'joe.maynard@aventriahealth.com', 'users/default.png', NULL, '$2y$10$ugauxnvbaS1C.hP6GTGIoee7pRcpXwcb7Mq99ADQKogIEjUZ2Ncb6', NULL, NULL, '2021-06-07 12:56:12', '2021-06-07 12:56:12', NULL, NULL, 'Maynard', 'AHG', '14', 'Managing Director', '2017876926', 'Parsippany, NJ', '', '', NULL, '5', '1', '1', 'America/New_York', NULL, NULL, NULL, '0', NULL, 8, NULL),
(609, 2, 'Eric J Hoch', 'ejh75@optonline.net', 'users/QXZlbnRyaWFfRklOQUxMT0dPX1JHQiBzbSBjb3B5LmpwZw==20210607221433.jpg', NULL, '$2y$10$wR1tXYCYTSKcH1qRYcj0h.Hglewmdcla4EejOmKusQBKjYf6lWqq.', NULL, NULL, '2021-06-07 13:45:29', '2021-06-08 08:42:11', NULL, NULL, 'Hoch', 'Aventria Health Group', '14', 'VP', NULL, '75 Main Street', '', '', NULL, '5', '1', '1', 'America/New_York', '2021-06-08 08:42:11', 'America/New_York', NULL, '1', NULL, 8, 1),
(610, 2, 'Sean', 'sean.guzman@novartis.com', 'users/YW5pbW9qaS1tZS5qcGc=20210607202142.jpg', NULL, '$2y$10$8F.F7sAMasQfLLOzc2Z6L.I3a6vc3v1Sy4XG/Hp.XSLCMvmRjoHZ6', 'Hrxf9FHRKaIcUUZP7MXhiWQiJ0YjnVX2t1sZz16AJn4EHbjPDk4oM6QIgeDx', NULL, '2021-06-07 18:42:12', '2021-06-08 13:03:29', NULL, NULL, 'Guzman', 'Novartis', '14', NULL, NULL, NULL, '', '', NULL, '5', '1', '1', 'America/New_York', '2021-06-08 13:03:28', 'America/New_York', NULL, '1', NULL, 8, 1),
(611, 2, 'Prashanth', 'clients@advaana.com', 'users/default.png', NULL, '$2y$10$BoUBeDmd.Sitfp9h/9KKUeqH7pJow8ZDaUuA6nCFLlsR24yUnLaZ6', NULL, NULL, '2021-06-10 20:45:02', '2021-06-15 00:11:53', NULL, NULL, 'raj', 'Advaana Testing', '14', 'DATA ANALYST', NULL, '121 1/2 Cottage St, 2', '', '', NULL, '2', '1', '1', 'America/New_York', '2021-06-15 00:11:53', 'America/New_York', NULL, '1', NULL, 8, 1),
(612, 2, 'David', 'dave.dierk@aventriahealth.com', 'users/default.png', NULL, '$2y$10$FOv05nw/1pUx32wt898X8uyeA1dhjB6hxiBpRKOEm.XQSEGHOjit2', 'iGiFAbVMkG06GS4qqHW0YuoXNy669aR4uQbvwSfdJvi8Ek99hLyqdqSm2HWF', NULL, '2021-06-16 14:10:24', '2021-06-16 14:52:13', NULL, NULL, 'Dierk', 'Aventria Health Group', '14', 'CEO', NULL, NULL, '', '', NULL, NULL, '1', '1', 'America/New_York', '2021-06-16 14:52:13', 'America/New_York', NULL, '1', NULL, NULL, 1),
(613, 2, 'Thirumurugan', 'thirumurugans@hasotech.com', 'users/default.png', NULL, '$2y$10$no7rZAE/hfybvxLNYZblWeAxDlzVJiZkmuaELSRdtB2JHwlHqe0ta', NULL, NULL, '2021-06-17 00:52:58', '2021-06-17 00:53:11', NULL, NULL, 'Senthilvel', 'hasotech', '14', 'PHP developer', '08072032281', '12', '', '', NULL, '3', '1', '1', 'America/New_York', NULL, NULL, NULL, '0', NULL, 6, NULL),
(616, 2, 'Thirumurugan', 'thirumurugan6497@gmail.com', 'users/default.png', NULL, '$2y$10$f.v2m7eZZkQ2CiRozUbj2eKFiyLGvvxWpVZ.3SQBZx8M6lSll1fpO', NULL, NULL, '2021-07-01 06:04:32', '2021-07-01 06:04:32', NULL, NULL, 'Senthilvel', 'Hasotech', '14', 'PHP developer', '8428132443', NULL, 'Madurai', 'Tamilnadu', NULL, '5', '0', '1', 'America/New_York', NULL, NULL, NULL, '0', NULL, 8, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_assessments`
--

CREATE TABLE `user_assessments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_assessments`
--

INSERT INTO `user_assessments` (`id`, `user_id`, `created_at`, `updated_at`, `title`, `city`, `state`, `customer_name`, `date`, `organization`) VALUES
(31, 1, '2021-06-09 14:13:51', '2021-06-09 14:13:51', 'Php Developer', 'Mumbai', '22', 'Sanat Gawade', '06-09-2021', 'Leo9'),
(32, 1, '2021-06-09 14:24:46', '2021-06-09 14:24:46', 'VP', 'Parsippany', '32', 'Eric Hoch', '06-09-2021', 'Aventria Health Group'),
(33, 1, '2021-06-09 14:28:46', '2021-06-09 14:28:46', 'VP', 'Parsippany', '32', 'Eric Hoch', '06-09-2021', 'Aventria Health Group'),
(34, 1, '2021-06-09 14:35:40', '2021-06-09 14:35:40', 'Php Developer', 'Mumbai', '3', 'Sanat Gawade', '06-09-2021', 'Leo9'),
(35, 1, '2021-06-09 14:53:49', '2021-06-09 14:53:49', 'Php Developer', 'Mumbai', '18', 'Sanat Gawade', '06-09-2021', 'Leo9');

-- --------------------------------------------------------

--
-- Table structure for table `user_customer_assessments`
--

CREATE TABLE `user_customer_assessments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `patient_journey` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shared_priorities` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_customer_assessments`
--

INSERT INTO `user_customer_assessments` (`id`, `user_id`, `patient_journey`, `shared_priorities`, `other`, `created_at`, `updated_at`, `ref_id`) VALUES
(28, 1, '1', '[\"1\"]', NULL, '2021-06-09 14:13:51', '2021-06-09 14:13:51', 31),
(29, 1, '2', '[\"1\"]', NULL, '2021-06-09 14:13:51', '2021-06-09 14:13:51', 31),
(30, 1, '3', '[\"1\"]', NULL, '2021-06-09 14:13:51', '2021-06-09 14:13:51', 31),
(31, 1, '4', '[\"1\"]', NULL, '2021-06-09 14:13:51', '2021-06-09 14:13:51', 31),
(32, 1, '5', '[\"1\"]', NULL, '2021-06-09 14:13:51', '2021-06-09 14:13:51', 31),
(33, 1, '6', '[\"1\"]', NULL, '2021-06-09 14:13:51', '2021-06-09 14:13:51', 31),
(34, 1, '1', '[\"1\"]', NULL, '2021-06-09 14:24:46', '2021-06-09 14:24:46', 32),
(35, 1, '2', '[\"2\"]', NULL, '2021-06-09 14:24:46', '2021-06-09 14:24:46', 32),
(36, 1, '3', '[\"3\"]', NULL, '2021-06-09 14:24:46', '2021-06-09 14:24:46', 32),
(37, 1, '4', '[\"4\"]', NULL, '2021-06-09 14:24:46', '2021-06-09 14:24:46', 32),
(38, 1, '5', '[\"5\"]', NULL, '2021-06-09 14:24:46', '2021-06-09 14:24:46', 32),
(39, 1, '6', '[\"6\"]', NULL, '2021-06-09 14:24:46', '2021-06-09 14:24:46', 32),
(40, 1, '1', NULL, 'Add some misc points for testing of the system', '2021-06-09 14:28:46', '2021-06-09 14:28:46', 33),
(41, 1, '2', '[\"4\"]', NULL, '2021-06-09 14:28:46', '2021-06-09 14:28:46', 33),
(42, 1, '3', NULL, 'Add some misc points for testing of the system', '2021-06-09 14:28:46', '2021-06-09 14:28:46', 33),
(43, 1, '4', '[\"4\"]', NULL, '2021-06-09 14:28:46', '2021-06-09 14:28:46', 33),
(44, 1, '6', NULL, 'Add some misc points for testing of the system', '2021-06-09 14:28:46', '2021-06-09 14:28:46', 33),
(45, 1, '1', '[\"1\"]', 'This is a test', '2021-06-09 14:35:40', '2021-06-09 14:35:40', 34),
(46, 1, '2', '[\"1\"]', 'Again test', '2021-06-09 14:35:40', '2021-06-09 14:35:40', 34),
(47, 1, '3', '[\"1\"]', NULL, '2021-06-09 14:35:40', '2021-06-09 14:35:40', 34),
(48, 1, '4', '[\"1\"]', NULL, '2021-06-09 14:35:40', '2021-06-09 14:35:40', 34),
(49, 1, '5', '[\"1\"]', NULL, '2021-06-09 14:35:40', '2021-06-09 14:35:40', 34),
(50, 1, '1', '[\"1\"]', NULL, '2021-06-09 14:53:49', '2021-06-09 14:53:49', 35),
(51, 1, '2', '[\"1\"]', NULL, '2021-06-09 14:53:49', '2021-06-09 14:53:49', 35),
(52, 1, '3', '[\"1\"]', NULL, '2021-06-09 14:53:49', '2021-06-09 14:53:49', 35),
(53, 1, '4', '[\"1\"]', NULL, '2021-06-09 14:53:49', '2021-06-09 14:53:49', 35),
(54, 1, '5', '[\"1\"]', NULL, '2021-06-09 14:53:49', '2021-06-09 14:53:49', 35),
(55, 1, '6', '[\"1\"]', 'adsdasdsdfsdf', '2021-06-09 14:53:49', '2021-06-09 14:53:49', 35);

-- --------------------------------------------------------

--
-- Table structure for table `user_health_tools`
--

CREATE TABLE `user_health_tools` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `custom_logo` varchar(191) DEFAULT NULL,
  `company_logo` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_health_tools`
--

INSERT INTO `user_health_tools` (`id`, `name`, `slug`, `url`, `custom_logo`, `company_logo`) VALUES
(376, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/leo9-studio/21', NULL, NULL),
(377, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/leo9-studio/21/?image=316', 'https://liverhealthnow.com/storage/userstemplogos/20200618094357.png', NULL),
(378, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/leo9-studio/21/?image=317', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(379, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/bausch-health/30/?image=318', 'https://liverhealthnow.com/storage/userstemplogos/20200618150951.png', NULL),
(380, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/salix/20', NULL, NULL),
(381, 'Prevent Another Attack From Overt Hepatic Encephalopathy', 'prevent-another-attack-from-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/prevent-another-attack-from-overt-hepatic-encephalopathy/bausch-health/43/?image=319', 'https://liverhealthnow.com/storage/userstemplogos/20200619180918.png', NULL),
(382, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://www.liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41', NULL, NULL),
(383, 'I Have HE', 'i-have-he', 'https://www.liverhealthnow.com/health-tools-detail/i-have-he/salix/25', NULL, NULL),
(384, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/salix/32', NULL, NULL),
(385, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/bausch-health/25/?image=320', 'https://liverhealthnow.com/storage/userstemplogos/20200622131445.png', NULL),
(386, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/bausch-health/32/?image=168', 'https://liverhealthnow.com/storage/userstemplogos/20200514182753.jpeg', NULL),
(387, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/bausch-health/21/?image=149', 'https://liverhealthnow.com/storage/userstemplogos/20200508191718.jpeg', NULL),
(388, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/aventira/21/?image=321', 'https://liverhealthnow.com/storage/users/bmFtZS5wbmc=20200604044404.png', NULL),
(389, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/aventira/21/?image=322', 'https://liverhealthnow.com/storage/users/bmFtZS5wbmc=20200604044404.png', NULL),
(390, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/aventira/41/?image=324', 'https://liverhealthnow.com/storage/users/bmFtZS5wbmc=20200604044404.png', NULL),
(391, 'Prevent Another Attack From Overt Hepatic Encephalopathy', 'prevent-another-attack-from-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/prevent-another-attack-from-overt-hepatic-encephalopathy/salix/43', NULL, NULL),
(392, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/bausch-health/25/?image=151', 'https://liverhealthnow.com/storage/userstemplogos/20200508193608.jpeg', NULL),
(393, 'Help Patients Set Goals Using Shared Decision Making', 'help-patients-set-goals-using-shared-decision-making', 'https://liverhealthnow.com/health-tools-detail/help-patients-set-goals-using-shared-decision-making/bausch-health/24/?image=150', 'https://liverhealthnow.com/storage/userstemplogos/20200508192553.jpeg', NULL),
(394, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/bausch-health/41/?image=201', 'https://liverhealthnow.com/storage/userstemplogos/20200522165922.jpeg', NULL),
(395, 'Symptoms, Complications, and Management of Cirrhosis', 'symptoms-complications-and-management-of-cirrhosis', 'https://liverhealthnow.com/health-tools-detail/symptoms-complications-and-management-of-cirrhosis/bausch-health/37/?image=328', 'https://liverhealthnow.com/storage/userstemplogos/20200622182235.png', NULL),
(396, 'Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy/bausch-health/26/?image=152', 'https://liverhealthnow.com/storage/userstemplogos/20200508193717.jpeg', NULL),
(397, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/salix/32', NULL, NULL),
(398, 'Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy/salix/26', NULL, NULL),
(399, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://www.liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/salix/35', NULL, NULL),
(400, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/salix/21', NULL, NULL),
(401, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://www.liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/bausch-health/21', NULL, NULL),
(402, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://www.liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/bausch-health/41', NULL, NULL),
(403, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://www.liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/bausch-health/27', NULL, NULL),
(404, 'Help Patients Set Goals Using Shared Decision Making', 'help-patients-set-goals-using-shared-decision-making', 'https://www.liverhealthnow.com/health-tools-detail/help-patients-set-goals-using-shared-decision-making/bausch-health/24', NULL, NULL),
(405, 'Living With Diabetes and Liver Disease', 'living-with-diabetes-and-liver-disease', 'https://www.liverhealthnow.com/health-tools-detail/living-with-diabetes-and-liver-disease/salix/29', NULL, NULL),
(406, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/aventria/41/?image=330', 'https://liverhealthnow.com/storage/users/983.jpg', NULL),
(407, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/aventria/41/?image=178', 'https://liverhealthnow.com/storage', NULL),
(408, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/aventria/41/?image=331', 'https://liverhealthnow.com/storage/users/983.jpg', NULL),
(409, 'Goal Setting When You Have Chronic Liver Disease', 'goal-setting-when-you-have-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/goal-setting-when-you-have-chronic-liver-disease/bausch-health/23', NULL, NULL),
(410, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/bausch-health/27', NULL, NULL),
(411, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/aventria/41/?image=334', 'https://liverhealthnow.com/storage/users/bmFtZS5wbmc=20200604044404.png', NULL),
(412, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/james/25/?image=151', 'https://liverhealthnow.com/storage/userstemplogos/20200508193608.jpeg', NULL),
(413, 'Symptoms, Complications, and Management of Cirrhosis', 'symptoms-complications-and-management-of-cirrhosis', 'https://www.liverhealthnow.com/health-tools-detail/symptoms-complications-and-management-of-cirrhosis/salix/37', NULL, NULL),
(414, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/gastrohealth/41', NULL, NULL),
(415, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/gastrohealth/19', NULL, NULL),
(416, 'West Haven Criteria', 'west-haven-criteria', 'https://liverhealthnow.com/health-tools-detail/west-haven-criteria/gastrohealth/39', NULL, NULL),
(417, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/aventria/41/?image=335', 'https://liverhealthnow.com/storage/users/bmFtZS5wbmc=20200604044404.png', NULL),
(418, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/aventria/19/?image=336', 'https://liverhealthnow.com/storage/users/bmFtZS5wbmc=20200604044404.png', NULL),
(419, 'Monitoring and Documenting Overt Hepatic Encephalopathy Episodes', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'https://liverhealthnow.com/health-tools-detail/monitoring-and-documenting-overt-hepatic-encephalopathy-episodes/salix/42/?image=337', 'https://liverhealthnow.com/storage/userstemplogos/20200629182358.gif', NULL),
(420, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=189', 'https://liverhealthnow.com/storage', NULL),
(421, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/salix/32/?image=338', 'https://liverhealthnow.com/storage/userstemplogos/20200629182611.gif', NULL),
(422, 'Living With Diabetes and Liver Disease', 'living-with-diabetes-and-liver-disease', 'https://liverhealthnow.com/health-tools-detail/living-with-diabetes-and-liver-disease/aventria/29', NULL, NULL),
(423, 'Prevent Another Attack From Overt Hepatic Encephalopathy', 'prevent-another-attack-from-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/prevent-another-attack-from-overt-hepatic-encephalopathy/bausch-health/43/?image=339', 'https://liverhealthnow.com/storage/userstemplogos/20200630144116.png', NULL),
(424, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=342', 'https://liverhealthnow.com/storage/userstemplogos/20200630154952.png', NULL),
(425, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=343', 'https://liverhealthnow.com/storage/userstemplogos/20200630155229.png', NULL),
(426, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/salix/21/?image=344', 'https://liverhealthnow.com/storage/userstemplogos/20200630160719.png', NULL),
(427, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41', NULL, NULL),
(428, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=346', 'https://liverhealthnow.com/storage/userstemplogos/20200630162436.png', NULL),
(429, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=345', 'https://liverhealthnow.com/storage/userstemplogos/20200630162337.png', NULL),
(430, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=349', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(431, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://www.liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/salix-pharmacuticals/21', NULL, NULL),
(432, 'West Haven Criteria', 'west-haven-criteria', 'https://www.liverhealthnow.com/health-tools-detail/west-haven-criteria/salix-pharmacuticals/39', NULL, NULL),
(433, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://www.liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/salix-pharmacuticals/35', NULL, NULL),
(434, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/salix-pharmacuticals/32', NULL, NULL),
(435, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=353', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(436, 'Stages and Types of Liver Disease', 'stages-and-types-of-liver-disease', 'https://liverhealthnow.com/health-tools-detail/stages-and-types-of-liver-disease/bausch-health/36/?image=354', 'https://liverhealthnow.com/storage/userstemplogos/20200702201127.png', NULL),
(437, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/aventria/21', NULL, NULL),
(438, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=355', 'https://liverhealthnow.com/storage/userstemplogos/20200708130529.jpeg', NULL),
(439, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=356', 'https://liverhealthnow.com/storage/userstemplogos/20200708130642.jpeg', NULL),
(440, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/marc/32/?image=164', 'https://liverhealthnow.com/storage', NULL),
(441, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=357', 'https://liverhealthnow.com/storage/userstemplogos/20200708134235.jpeg', NULL),
(442, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/bausch-health/41/?image=358', 'https://liverhealthnow.com/storage', NULL),
(443, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/bausch-health/32', NULL, NULL),
(444, 'The Importance of Patient Journaling', 'the-importance-of-patient-journaling', 'https://liverhealthnow.com/health-tools-detail/the-importance-of-patient-journaling/bausch-health/38', NULL, NULL),
(445, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/marc/27/?image=163', 'https://liverhealthnow.com/storage', NULL),
(446, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://www.liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/salix/21/?image=363', 'https://www.liverhealthnow.com/storage/userstemplogos/20200709125521.jpeg', NULL),
(447, 'West Haven Criteria', 'west-haven-criteria', 'https://www.liverhealthnow.com/health-tools-detail/west-haven-criteria/salix/39/?image=365', 'https://www.liverhealthnow.com/storage/userstemplogos/20200709175000.jpeg', NULL),
(448, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://www.liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/salix/27/?image=170', 'https://www.liverhealthnow.com/storage', NULL),
(449, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/aventria/20/?image=366', 'https://liverhealthnow.com/storage/users/983.jpg', NULL),
(450, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/aventria/20/?image=367', 'https://liverhealthnow.com/storage/userstemplogos/20200710131347.png', NULL),
(451, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/aventria/20/?image=369', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(452, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/aventria/20/?image=370', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(453, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/aventria/20/?image=371', 'https://liverhealthnow.com/storage/userstemplogos/20200710131635.png', NULL),
(454, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/salix/32/?image=372', 'https://liverhealthnow.com/storage/userstemplogos/20200710144430.png', NULL),
(455, 'West Haven Criteria', 'west-haven-criteria', 'https://liverhealthnow.com/health-tools-detail/west-haven-criteria/salix/39/?image=373', 'https://liverhealthnow.com/storage/userstemplogos/20200710144543.png', NULL),
(456, 'Monitoring and Documenting Overt Hepatic Encephalopathy Episodes', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'https://liverhealthnow.com/health-tools-detail/monitoring-and-documenting-overt-hepatic-encephalopathy-episodes/salix/42/?image=374', 'https://liverhealthnow.com/storage/userstemplogos/20200710144632.png', NULL),
(457, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/salix/27/?image=375', 'https://liverhealthnow.com/storage/userstemplogos/20200710144735.png', NULL),
(458, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/aventria/20/?image=376', 'https://liverhealthnow.com/storage/userstemplogos/20200710145000.png', NULL),
(459, 'Symptoms, Complications, and Management of Cirrhosis', 'symptoms-complications-and-management-of-cirrhosis', 'https://liverhealthnow.com/health-tools-detail/symptoms-complications-and-management-of-cirrhosis/vito/37/?image=259', 'https://liverhealthnow.com/storage/userstemplogos/20200601033935.jpeg', NULL),
(460, 'Symptoms, Complications, and Management of Cirrhosis', 'symptoms-complications-and-management-of-cirrhosis', 'https://liverhealthnow.com/health-tools-detail/symptoms-complications-and-management-of-cirrhosis/vito/37', NULL, NULL),
(461, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/vito/25/?image=261', 'https://liverhealthnow.com/storage/userstemplogos/20200601034619.jpeg', NULL),
(462, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/vito/35/?image=264', 'https://liverhealthnow.com/storage/userstemplogos/20200601042453.jpeg', NULL),
(463, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/vito/25', NULL, NULL),
(464, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/vito/35', NULL, NULL),
(465, 'West Haven Criteria', 'west-haven-criteria', 'https://liverhealthnow.com/health-tools-detail/west-haven-criteria/salix/39', NULL, NULL),
(466, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/bausch-health/41/?image=380', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(467, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/bausch-health/41/?image=381', 'https://liverhealthnow.com/storage/userstemplogos/20200713214729.png', NULL),
(468, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/bausch-health/27/?image=383', 'https://liverhealthnow.com/storage/userstemplogos/20200713215200.png', NULL),
(469, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/salix/21/?image=385', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(470, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/salix/21/?image=386', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(471, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/salix/21/?image=387', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(472, 'Symptoms, Complications, and Management of Cirrhosis', 'symptoms-complications-and-management-of-cirrhosis', 'https://liverhealthnow.com/health-tools-detail/symptoms-complications-and-management-of-cirrhosis/centura-transplant/37', NULL, NULL),
(473, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/aventria/41', NULL, NULL),
(474, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/aventria/25', NULL, NULL),
(475, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/salix/21/?image=388', 'https://liverhealthnow.com/storage/userstemplogos/20200715173733.jpeg', NULL),
(476, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/salix/21/?image=389', 'https://liverhealthnow.com/storage/users/2.gif', NULL),
(477, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=390', 'https://liverhealthnow.com/storage/userstemplogos/20200715173947.jpeg', NULL),
(478, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/bausch-health/41/?image=391', 'https://liverhealthnow.com/storage/userstemplogos/20200715232049.png', NULL),
(479, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/bausch-health/27/?image=392', 'https://liverhealthnow.com/storage/userstemplogos/20200715232221.png', NULL),
(480, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/salix/27/?image=395', 'https://liverhealthnow.com/storage', NULL),
(481, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/salix/35/?image=401', 'https://liverhealthnow.com/storage', NULL),
(482, 'Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy/bausch-health/28', NULL, NULL),
(483, 'Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy/bausch-health/28/?image=402', 'https://liverhealthnow.com/storage/userstemplogos/20200716124745.png', NULL),
(484, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/bausch-health/32/?image=404', 'https://liverhealthnow.com/storage/userstemplogos/20200716133530.png', NULL),
(485, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/salix/27', NULL, NULL),
(486, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/pharmerica/21', NULL, NULL),
(487, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=405', 'https://liverhealthnow.com/storage/userstemplogos/20200716180759.jpeg', NULL),
(488, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/salix/30/?image=406', 'https://liverhealthnow.com/storage/userstemplogos/20200716185843.png', NULL),
(489, 'Help Patients Set Goals Using Shared Decision Making', 'help-patients-set-goals-using-shared-decision-making', 'https://liverhealthnow.com/health-tools-detail/help-patients-set-goals-using-shared-decision-making/bausch-health/24', NULL, NULL),
(490, 'The Importance of Patient Journaling', 'the-importance-of-patient-journaling', 'https://liverhealthnow.com/health-tools-detail/the-importance-of-patient-journaling/bausch-health/38/?image=362', 'https://liverhealthnow.com/storage', NULL),
(491, 'Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy/salix/26/?image=254', 'https://liverhealthnow.com/storage/users/QlNXIGxvZ28ucG5n20200526145251.png', NULL),
(492, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/salix/30', NULL, NULL),
(493, 'Nurse Checklist for Patients With Hepatic Encephalopathy Who Are Transitioning to Another Care Setting', 'nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting/salix/44', NULL, NULL),
(494, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/aventria/27', NULL, NULL),
(495, 'West Haven Criteria', 'west-haven-criteria', 'https://liverhealthnow.com/health-tools-detail/west-haven-criteria/american-gastroenterological-association/39', NULL, NULL),
(496, 'The Importance of Patient Journaling', 'the-importance-of-patient-journaling', 'https://liverhealthnow.com/health-tools-detail/the-importance-of-patient-journaling/american-gastroenterological-association/38', NULL, NULL),
(497, 'Symptoms, Complications, and Management of Cirrhosis', 'symptoms-complications-and-management-of-cirrhosis', 'https://liverhealthnow.com/health-tools-detail/symptoms-complications-and-management-of-cirrhosis/american-gastroenterological-association/37', NULL, NULL),
(498, 'What Is Hepatic Encephalopathy?', 'what-is-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/what-is-hepatic-encephalopathy/american-gastroenterological-association/40', NULL, NULL),
(499, 'Stages and Types of Liver Disease', 'stages-and-types-of-liver-disease', 'https://liverhealthnow.com/health-tools-detail/stages-and-types-of-liver-disease/american-gastroenterological-association/36', NULL, NULL),
(500, 'Prevent Another Attack From Overt Hepatic Encephalopathy', 'prevent-another-attack-from-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/prevent-another-attack-from-overt-hepatic-encephalopathy/american-gastroenterological-association/43', NULL, NULL),
(501, 'Nurse Checklist for Patients With Hepatic Encephalopathy Who Are Transitioning to Another Care Setting', 'nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting/american-gastroenterological-association/44', NULL, NULL),
(502, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/american-gastroenterological-association/32', NULL, NULL),
(503, 'Monitoring and Documenting Overt Hepatic Encephalopathy Episodes', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'https://liverhealthnow.com/health-tools-detail/monitoring-and-documenting-overt-hepatic-encephalopathy-episodes/american-gastroenterological-association/42', NULL, NULL),
(504, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/american-gastroenterological-association/30', NULL, NULL),
(505, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/american-gastroenterological-association/20', NULL, NULL),
(506, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://www.liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/salix/27/?image=408', 'https://www.liverhealthnow.com/storage/userstemplogos/20200728161937.jpeg', NULL),
(507, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/bausch-health/19/?image=157', 'https://liverhealthnow.com/storage/userstemplogos/20200511143457.jpeg', NULL),
(508, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/bausch-health/20/?image=162', 'https://liverhealthnow.com/storage/userstemplogos/20200512150603.jpeg', NULL),
(509, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/bausch-health/41/?image=409', 'https://liverhealthnow.com/storage/userstemplogos/20200729132400.png', NULL),
(510, 'Monitoring and Documenting Overt Hepatic Encephalopathy Episodes', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'https://liverhealthnow.com/health-tools-detail/monitoring-and-documenting-overt-hepatic-encephalopathy-episodes/aventria/42', NULL, NULL),
(511, 'Goal Setting When You Have Chronic Liver Disease', 'goal-setting-when-you-have-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/goal-setting-when-you-have-chronic-liver-disease/aventria/23', NULL, NULL),
(512, 'Monitoring and Documenting Overt Hepatic Encephalopathy Episodes', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'https://liverhealthnow.com/health-tools-detail/monitoring-and-documenting-overt-hepatic-encephalopathy-episodes/aventria/42/?image=410', 'https://liverhealthnow.com/storage/users/bmFtZS5wbmc=20200604044404.png', NULL),
(513, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/aventria/20/?image=411', 'https://liverhealthnow.com/storage/users/bmFtZS5wbmc=20200604044404.png', NULL),
(514, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/aventria/21/?image=412', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(515, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/aventria/41/?image=413', 'https://liverhealthnow.com/storage/userstemplogos/20200730202122.jpeg', NULL),
(516, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/aventria/21/?image=414', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(517, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/aventria/19/?image=416', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(518, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/aventria/19/?image=418', 'https://liverhealthnow.com/storage/userstemplogos/20200730203435.jpeg', NULL),
(519, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/aventria/20/?image=419', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(520, 'Discuss Patient Journaling With Your Patients', 'discuss-patient-journaling-with-your-patients', 'https://liverhealthnow.com/health-tools-detail/discuss-patient-journaling-with-your-patients/aventria/22/?image=420', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(521, 'Goal Setting When You Have Chronic Liver Disease', 'goal-setting-when-you-have-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/goal-setting-when-you-have-chronic-liver-disease/aventria/23/?image=421', 'https://liverhealthnow.com/storage/userstemplogos/20200730211206.jpeg', NULL),
(522, 'Help Patients Set Goals Using Shared Decision Making', 'help-patients-set-goals-using-shared-decision-making', 'https://liverhealthnow.com/health-tools-detail/help-patients-set-goals-using-shared-decision-making/aventria/24/?image=422', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(523, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/aventria/25/?image=423', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(524, 'Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy/aventria/26/?image=424', 'https://liverhealthnow.com/storage/userstemplogos/20200730211750.jpeg', NULL),
(525, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/aventria/27/?image=425', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(526, 'Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy/aventria/28/?image=426', 'https://liverhealthnow.com/storage/userstemplogos/20200730211938.jpeg', NULL),
(527, 'Living With Diabetes and Liver Disease', 'living-with-diabetes-and-liver-disease', 'https://liverhealthnow.com/health-tools-detail/living-with-diabetes-and-liver-disease/aventria/29/?image=427', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(528, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/aventria/30/?image=428', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(529, 'Monitoring and Documenting Overt Hepatic Encephalopathy Episodes', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'https://liverhealthnow.com/health-tools-detail/monitoring-and-documenting-overt-hepatic-encephalopathy-episodes/aventria/42/?image=429', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(530, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/aventria/32/?image=430', 'https://liverhealthnow.com/storage/userstemplogos/20200730212235.jpeg', NULL),
(531, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/aventria/32/?image=431', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(532, 'Nurse Checklist for Patients With Hepatic Encephalopathy Who Are Transitioning to Another Care Setting', 'nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting/aventria/44/?image=432', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(533, 'Prevent Another Attack From Overt Hepatic Encephalopathy', 'prevent-another-attack-from-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/prevent-another-attack-from-overt-hepatic-encephalopathy/aventria/43/?image=433', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(534, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/aventria/35/?image=434', 'https://liverhealthnow.com/storage/userstemplogos/20200730212507.jpeg', NULL),
(535, 'Stages and Types of Liver Disease', 'stages-and-types-of-liver-disease', 'https://liverhealthnow.com/health-tools-detail/stages-and-types-of-liver-disease/aventria/36/?image=435', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(536, 'Symptoms, Complications, and Management of Cirrhosis', 'symptoms-complications-and-management-of-cirrhosis', 'https://liverhealthnow.com/health-tools-detail/symptoms-complications-and-management-of-cirrhosis/aventria/37/?image=436', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(537, 'The Importance of Patient Journaling', 'the-importance-of-patient-journaling', 'https://liverhealthnow.com/health-tools-detail/the-importance-of-patient-journaling/aventria/38/?image=437', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(538, 'West Haven Criteria', 'west-haven-criteria', 'https://liverhealthnow.com/health-tools-detail/west-haven-criteria/aventria/39/?image=438', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(539, 'What Is Hepatic Encephalopathy?', 'what-is-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/what-is-hepatic-encephalopathy/aventria/40/?image=440', 'https://liverhealthnow.com/storage/userstemplogos/20200730212944.jpeg', NULL),
(540, 'Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy/aventria/28/?image=441', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(541, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/aventria/21/?image=442', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(542, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/aventria/20/?image=443', 'https://liverhealthnow.com/storage/userstemplogos/20200731133433.jpeg', NULL),
(543, 'Discuss Patient Journaling With Your Patients', 'discuss-patient-journaling-with-your-patients', 'https://liverhealthnow.com/health-tools-detail/discuss-patient-journaling-with-your-patients/aventria/22/?image=444', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(544, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/aventria/19/?image=445', 'https://liverhealthnow.com/storage/userstemplogos/20200731133904.jpeg', NULL),
(545, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/salix/27/?image=153', 'https://liverhealthnow.com/storage/userstemplogos/20200508200114.png', NULL),
(546, 'West Haven Criteria', 'west-haven-criteria', 'https://liverhealthnow.com/health-tools-detail/west-haven-criteria/u-of-l-physicians/39/?image=447', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(547, 'What Is Hepatic Encephalopathy?', 'what-is-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/what-is-hepatic-encephalopathy/salix/40/?image=180', 'https://liverhealthnow.com/storage', NULL),
(548, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/salix/25/?image=393', 'https://liverhealthnow.com/storage', NULL),
(549, 'Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy/salix/26/?image=394', 'https://liverhealthnow.com/storage', NULL);
INSERT INTO `user_health_tools` (`id`, `name`, `slug`, `url`, `custom_logo`, `company_logo`) VALUES
(550, 'Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy/salix/28/?image=396', 'https://liverhealthnow.com/storage', NULL),
(551, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/salix/30/?image=397', 'https://liverhealthnow.com/storage', NULL),
(552, 'Monitoring and Documenting Overt Hepatic Encephalopathy Episodes', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'https://liverhealthnow.com/health-tools-detail/monitoring-and-documenting-overt-hepatic-encephalopathy-episodes/salix/42/?image=398', 'https://liverhealthnow.com/storage', NULL),
(553, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/salix/32/?image=179', 'https://liverhealthnow.com/storage', NULL),
(554, 'Prevent Another Attack From Overt Hepatic Encephalopathy', 'prevent-another-attack-from-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/prevent-another-attack-from-overt-hepatic-encephalopathy/salix/43/?image=400', 'https://liverhealthnow.com/storage', NULL),
(555, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=448', 'https://liverhealthnow.com/storage', NULL),
(556, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/salix/20/?image=449', 'https://liverhealthnow.com/storage', NULL),
(557, 'Goal Setting When You Have Chronic Liver Disease', 'goal-setting-when-you-have-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/goal-setting-when-you-have-chronic-liver-disease/salix/23/?image=450', 'https://liverhealthnow.com/storage', NULL),
(558, 'Stages and Types of Liver Disease', 'stages-and-types-of-liver-disease', 'https://liverhealthnow.com/health-tools-detail/stages-and-types-of-liver-disease/salix/36/?image=451', 'https://liverhealthnow.com/storage', NULL),
(559, 'Symptoms, Complications, and Management of Cirrhosis', 'symptoms-complications-and-management-of-cirrhosis', 'https://liverhealthnow.com/health-tools-detail/symptoms-complications-and-management-of-cirrhosis/salix/37/?image=452', 'https://liverhealthnow.com/storage', NULL),
(560, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=453', 'https://liverhealthnow.com/storage/userstemplogos/20200804194339.jpeg', NULL),
(561, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/salix/20/?image=457', 'https://liverhealthnow.com/storage/users/571.jpg', NULL),
(562, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/salix/25/?image=458', 'https://liverhealthnow.com/storage/users/571.jpg', NULL),
(563, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/salix/25/?image=459', 'https://liverhealthnow.com/storage/users/571.jpg', NULL),
(564, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/salix/25', NULL, NULL),
(565, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/salix/25/?image=460', 'https://liverhealthnow.com/storage/users/571.jpg', NULL),
(566, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/tower-health/20', NULL, NULL),
(567, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/texas-health-physicians-group/41/?image=461', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(568, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/texas-health-physicians-group/41/?image=466', 'https://liverhealthnow.com/storage/userstemplogos/20200806163134.jpeg', NULL),
(569, 'Goal Setting When You Have Chronic Liver Disease', 'goal-setting-when-you-have-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/goal-setting-when-you-have-chronic-liver-disease/bausch-health/23/?image=467', 'https://liverhealthnow.com/storage/userstemplogos/20200806185824.png', NULL),
(570, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/bausch-health/35/?image=469', 'https://liverhealthnow.com/storage/userstemplogos/20200806190154.png', NULL),
(571, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/bausch-health/27/?image=470', 'https://liverhealthnow.com/storage/userstemplogos/20200806190648.png', NULL),
(572, 'Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy/bausch-health/26/?image=471', 'https://liverhealthnow.com/storage/userstemplogos/20200806190751.png', NULL),
(573, 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy/aventria/45/?image=473', 'https://liverhealthnow.com/storage/users/bmFtZS5wbmc=20200604044404.png', NULL),
(574, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/bausch-health/20/?image=474', 'https://liverhealthnow.com/storage/userstemplogos/20200806201639.png', NULL),
(575, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/bausch-health/19/?image=475', 'https://liverhealthnow.com/storage/userstemplogos/20200806202304.png', NULL),
(576, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/bausch-health/35/?image=476', 'https://liverhealthnow.com/storage/userstemplogos/20200806202505.png', NULL),
(577, 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy/aventria/45/?image=477', 'https://liverhealthnow.com/storage/users/bmFtZS5wbmc=20200604044404.png', NULL),
(578, 'Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy/salix/26', NULL, NULL),
(579, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/texas-health-physicians-group/27', NULL, NULL),
(580, 'Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy/texas-health-physicians-group/26', NULL, NULL),
(581, 'Living With Diabetes and Liver Disease', 'living-with-diabetes-and-liver-disease', 'https://liverhealthnow.com/health-tools-detail/living-with-diabetes-and-liver-disease/texas-health-physicians-group/29', NULL, NULL),
(582, 'Stages and Types of Liver Disease', 'stages-and-types-of-liver-disease', 'https://liverhealthnow.com/health-tools-detail/stages-and-types-of-liver-disease/texas-health-physicians-group/36', NULL, NULL),
(583, 'Symptoms, Complications, and Management of Cirrhosis', 'symptoms-complications-and-management-of-cirrhosis', 'https://liverhealthnow.com/health-tools-detail/symptoms-complications-and-management-of-cirrhosis/texas-health-physicians-group/37', NULL, NULL),
(584, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/texas-health-physicians-group/30', NULL, NULL),
(585, 'What Is Hepatic Encephalopathy?', 'what-is-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/what-is-hepatic-encephalopathy/texas-health-physicians-group/40', NULL, NULL),
(586, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/aventria/21/?image=479', 'https://liverhealthnow.com/storage/users/bmFtZS5wbmc=20200604044404.png', NULL),
(587, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/aventria/41/?image=480', 'https://liverhealthnow.com/storage/users/VGV4YXMgSGVhbHRoLmpwZw==20200810192648.jpeg', NULL),
(588, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/salix/21/?image=481', 'https://liverhealthnow.com/storage/userstemplogos/20200811131919.jpeg', NULL),
(589, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=482', 'https://liverhealthnow.com/storage/userstemplogos/20200811132056.jpeg', NULL),
(590, 'Goal Setting When You Have Chronic Liver Disease', 'goal-setting-when-you-have-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/goal-setting-when-you-have-chronic-liver-disease/salix/23/?image=483', 'https://liverhealthnow.com/storage/userstemplogos/20200811132154.jpeg', NULL),
(591, 'Discuss Patient Journaling With Your Patients', 'discuss-patient-journaling-with-your-patients', 'https://liverhealthnow.com/health-tools-detail/discuss-patient-journaling-with-your-patients/salix/22/?image=484', 'https://liverhealthnow.com/storage/userstemplogos/20200811132259.jpeg', NULL),
(592, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/salix/25/?image=485', 'https://liverhealthnow.com/storage/userstemplogos/20200811132403.jpeg', NULL),
(593, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/salix/35/?image=486', 'https://liverhealthnow.com/storage/userstemplogos/20200811132547.jpeg', NULL),
(594, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/salix/35/?image=487', 'https://liverhealthnow.com/storage/userstemplogos/20200811134151.png', NULL),
(595, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/salix/35/?image=489', 'https://liverhealthnow.com/storage/userstemplogos/20200811134431.png', NULL),
(596, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/salix/35/?image=490', 'https://liverhealthnow.com/storage/userstemplogos/20200811134616.png', NULL),
(597, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/salix/35/?image=491', 'https://liverhealthnow.com/storage/userstemplogos/20200811134912.jpeg', NULL),
(598, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/salix/30/?image=492', 'https://liverhealthnow.com/storage/userstemplogos/20200811135004.jpeg', NULL),
(599, 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy/aventria/45', NULL, NULL),
(600, 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy/aventria/45/?image=493', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(601, 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy/aventria/45/?image=494', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(602, 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy/aventria/45/?image=496', 'https://liverhealthnow.com/storage/userstemplogos/20200811185633.jpeg', NULL),
(603, 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy/aventria/45/?image=497', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(604, 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy/aventria/45/?image=498', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(605, 'Goal Setting When You Have Chronic Liver Disease', 'goal-setting-when-you-have-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/goal-setting-when-you-have-chronic-liver-disease/aventria/23/?image=499', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(606, 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy/aventria/45/?image=500', 'https://liverhealthnow.com/storage/users/UGl5dXNoLmpwZw==20200810193049.jpeg', NULL),
(607, 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy/aventria/45/?image=501', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(608, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/salix/19', NULL, NULL),
(609, 'Help Patients Set Goals Using Shared Decision Making', 'help-patients-set-goals-using-shared-decision-making', 'https://liverhealthnow.com/public/health-tools-detail/help-patients-set-goals-using-shared-decision-making/bausch-health/24/?image=150', NULL, NULL),
(610, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/public/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=482', 'https://liverhealthnow.com/public/storage/userstemplogos/20200811132056.jpeg', NULL),
(611, 'Monitoring and Documenting Overt Hepatic Encephalopathy Episodes', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'https://liverhealthnow.com/health-tools-detail/monitoring-and-documenting-overt-hepatic-encephalopathy-episodes/salix/42', NULL, NULL),
(612, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/aventria/41/?image=504', 'https://liverhealthnow.com/storage/users/UGl5dXNoLmpwZw==20200810193049.jpeg', NULL),
(613, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://www.liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/salix-pharmaceuticals/21', NULL, NULL),
(614, 'Goal Setting When You Have Chronic Liver Disease', 'goal-setting-when-you-have-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/goal-setting-when-you-have-chronic-liver-disease/virtua-our-lady-of-lourdes-hospital/23', NULL, NULL),
(615, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/virtua-our-lady-of-lourdes-hospital/27', NULL, NULL),
(616, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://www.liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/salix/21', NULL, NULL),
(617, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://www.liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=505', 'https://www.liverhealthnow.com/storage/userstemplogos/20200814191114.jpeg', NULL),
(618, 'The Importance of Patient Journaling', 'the-importance-of-patient-journaling', 'https://www.liverhealthnow.com/health-tools-detail/the-importance-of-patient-journaling/salix/38', NULL, NULL),
(619, 'The Importance of Patient Journaling', 'the-importance-of-patient-journaling', 'https://www.liverhealthnow.com/health-tools-detail/the-importance-of-patient-journaling/salix/38/?image=506', 'https://www.liverhealthnow.com/storage/userstemplogos/20200814201749.jpeg', NULL),
(620, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/bausch-health-care-gi/21', NULL, NULL),
(621, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/salix/25/?image=507', 'https://liverhealthnow.com/storage/userstemplogos/20200818175805.jpeg', NULL),
(622, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/aventria/21/?image=509', 'https://liverhealthnow.com/storage/users/UGl5dXNoLmpwZw==20200810193049.jpeg', NULL),
(623, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/salix-pharmaceuticals/27', NULL, NULL),
(624, 'Goal Setting When You Have Chronic Liver Disease', 'goal-setting-when-you-have-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/goal-setting-when-you-have-chronic-liver-disease/salix/23', NULL, NULL),
(625, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://www.liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/integra/19', NULL, NULL),
(626, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://www.liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/integra/21', NULL, NULL),
(627, 'Discuss Patient Journaling With Your Patients', 'discuss-patient-journaling-with-your-patients', 'https://www.liverhealthnow.com/health-tools-detail/discuss-patient-journaling-with-your-patients/integra/22', NULL, NULL),
(628, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://www.liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/integra/27', NULL, NULL),
(629, 'Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy/integra/28', NULL, NULL),
(630, 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy/integra/45', NULL, NULL),
(631, 'What Is Hepatic Encephalopathy?', 'what-is-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/what-is-hepatic-encephalopathy/integra/40', NULL, NULL),
(632, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/indiana-university/30', NULL, NULL),
(633, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://www.liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix-gi/41', NULL, NULL),
(634, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://www.liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/salix-gi/20', NULL, NULL),
(635, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://www.liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/salix-gi/27', NULL, NULL),
(636, 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy/salix-gi/45', NULL, NULL),
(637, 'Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy/salix-gi/28', NULL, NULL),
(638, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://www.liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/salix-gi/19', NULL, NULL),
(639, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/salix-gi/32', NULL, NULL),
(640, 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy/salix/45', NULL, NULL),
(641, 'Help Patients Set Goals Using Shared Decision Making', 'help-patients-set-goals-using-shared-decision-making', 'https://liverhealthnow.com/health-tools-detail/help-patients-set-goals-using-shared-decision-making/salix/24', NULL, NULL),
(642, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://www.liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/salix-gi/21', NULL, NULL),
(643, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/salix/30/?image=522', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(644, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/salix/30/?image=523', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(645, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/salix/19/?image=524', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(646, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/salix/20/?image=525', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(647, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/organization/21/?image=526', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(648, 'Discuss Patient Journaling With Your Patients', 'discuss-patient-journaling-with-your-patients', 'https://liverhealthnow.com/health-tools-detail/discuss-patient-journaling-with-your-patients/organization/22', NULL, NULL),
(649, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=527', 'https://liverhealthnow.com/storage/userstemplogos/20200821163811.jpeg', NULL),
(650, 'I Have HE', 'i-have-he', 'https://www.liverhealthnow.com/health-tools-detail/i-have-he/salix-gi/25', NULL, NULL),
(651, 'Discuss Patient Journaling With Your Patients', 'discuss-patient-journaling-with-your-patients', 'https://www.liverhealthnow.com/health-tools-detail/discuss-patient-journaling-with-your-patients/salix-gi/22', NULL, NULL),
(652, 'Nurse Checklist for Patients With Hepatic Encephalopathy Who Are Transitioning to Another Care Setting', 'nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting', 'https://www.liverhealthnow.com/health-tools-detail/nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting/salix-gi/44', NULL, NULL),
(653, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://www.liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/salix/19/?image=528', 'https://www.liverhealthnow.com/storage', NULL),
(654, 'West Haven Criteria', 'west-haven-criteria', 'https://www.liverhealthnow.com/health-tools-detail/west-haven-criteria/salix-pharmaceuticals/39/?image=530', 'https://www.liverhealthnow.com/storage/users/default.png', NULL),
(655, 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy/aventria/45/?image=531', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(656, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/bidmc-specialty-pharmacy/41', NULL, NULL),
(657, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/bidmc-specialty-pharmacy/27', NULL, NULL),
(658, 'What Is Hepatic Encephalopathy?', 'what-is-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/what-is-hepatic-encephalopathy/bidmc-specialty-pharmacy/40', NULL, NULL),
(659, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/bidmc-specialty-pharmacy/21', NULL, NULL),
(660, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://www.liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/bausch-health/20', NULL, NULL),
(661, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/bausch-health/20', NULL, NULL),
(662, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/bausch-health/41', NULL, NULL),
(663, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://www.liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/salix/30/?image=533', 'https://www.liverhealthnow.com/storage/users/default.png', NULL),
(664, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://www.liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/salix/30/?image=537', 'https://www.liverhealthnow.com/storage/users/default.png', NULL),
(665, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/urmc/19', NULL, NULL),
(666, 'Goal Setting When You Have Chronic Liver Disease', 'goal-setting-when-you-have-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/goal-setting-when-you-have-chronic-liver-disease/urmc/23', NULL, NULL),
(667, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/urmc/27', NULL, NULL),
(668, 'Living With Diabetes and Liver Disease', 'living-with-diabetes-and-liver-disease', 'https://liverhealthnow.com/health-tools-detail/living-with-diabetes-and-liver-disease/urmc/29', NULL, NULL),
(669, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/urmc/30', NULL, NULL),
(670, 'Discuss Patient Journaling With Your Patients', 'discuss-patient-journaling-with-your-patients', 'https://liverhealthnow.com/health-tools-detail/discuss-patient-journaling-with-your-patients/salix/22', NULL, NULL),
(671, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/salix/35', NULL, NULL),
(672, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/aventria/20/?image=543', 'https://liverhealthnow.com/storage/users/UGl5dXNoLmpwZw==20200810193049.jpeg', NULL),
(673, 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy/aventria/45/?image=544', 'https://liverhealthnow.com/storage/users/UGl5dXNoLmpwZw==20200810193049.jpeg', NULL),
(674, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/aventria/20/?image=550', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(675, 'Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy/salix/28', NULL, NULL),
(676, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/aventria/20', NULL, NULL),
(677, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://www.liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/salix/20', NULL, NULL),
(678, 'Goal Setting When You Have Chronic Liver Disease', 'goal-setting-when-you-have-chronic-liver-disease', 'https://www.liverhealthnow.com/health-tools-detail/goal-setting-when-you-have-chronic-liver-disease/salix/23', NULL, NULL),
(679, 'Discuss Patient Journaling With Your Patients', 'discuss-patient-journaling-with-your-patients', 'https://liverhealthnow.com/health-tools-detail/discuss-patient-journaling-with-your-patients/aventria/22', NULL, NULL),
(680, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/bausch-health-care-gi/19', NULL, NULL),
(681, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/salix/32/?image=554', 'https://liverhealthnow.com/storage/userstemplogos/20200903162949.png', NULL),
(682, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/aventria/21/?image=555', 'https://liverhealthnow.com/storage/users/TmVlcmFsIFNoYWguanBn20200903165547.jpeg', NULL),
(683, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/salix/32/?image=556', 'https://liverhealthnow.com/storage/userstemplogos/20200903165732.jpeg', NULL),
(684, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/aventria/21/?image=557', 'https://liverhealthnow.com/storage/users/TmVlcmFsIFNoYWgxLmpwZw==20200903165818.jpeg', NULL),
(685, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/aventria/21/?image=558', 'https://liverhealthnow.com/storage/users/TmVlcmFsIFNoYWguanBn20200903202121.jpeg', NULL),
(686, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/neeralshah/21/?image=557', 'https://liverhealthnow.com/storage/users/TmVlcmFsIFNoYWgxLmpwZw==20200903165818.jpeg', NULL),
(687, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/stuti/21/?image=557', 'https://liverhealthnow.com/storage/users/TmVlcmFsIFNoYWgxLmpwZw==20200903165818.jpeg', NULL),
(688, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/universityofvirginia/21/?image=557', 'https://liverhealthnow.com/storage/users/TmVlcmFsIFNoYWgxLmpwZw==20200903165818.jpeg', NULL),
(689, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=559', 'https://liverhealthnow.com/storage/userstemplogos/20200904143848.jpeg', NULL),
(690, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=560', 'https://liverhealthnow.com/storage/userstemplogos/20200904144033.png', NULL),
(691, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/salix/25/?image=561', 'https://liverhealthnow.com/storage/userstemplogos/20200904144158.png', NULL),
(692, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix-pharmacuticals/41', NULL, NULL),
(693, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/salix-pharmacuticals/32', NULL, NULL),
(694, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/salix-pharmacuticals/21', NULL, NULL),
(695, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/virtua-our-lady-of-lourdes-hospital/25', NULL, NULL),
(696, 'Living With Diabetes and Liver Disease', 'living-with-diabetes-and-liver-disease', 'https://liverhealthnow.com/health-tools-detail/living-with-diabetes-and-liver-disease/bausch-health/29', NULL, NULL),
(697, 'The Importance of Patient Journaling', 'the-importance-of-patient-journaling', 'https://liverhealthnow.com/health-tools-detail/the-importance-of-patient-journaling/salix/38', NULL, NULL),
(698, 'What Is Hepatic Encephalopathy?', 'what-is-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/what-is-hepatic-encephalopathy/salix/40', NULL, NULL),
(699, 'Stages and Types of Liver Disease', 'stages-and-types-of-liver-disease', 'https://liverhealthnow.com/health-tools-detail/stages-and-types-of-liver-disease/salix/36', NULL, NULL),
(700, 'Symptoms, Complications, and Management of Cirrhosis', 'symptoms-complications-and-management-of-cirrhosis', 'https://liverhealthnow.com/health-tools-detail/symptoms-complications-and-management-of-cirrhosis/salix/37', NULL, NULL),
(701, 'Living With Diabetes and Liver Disease', 'living-with-diabetes-and-liver-disease', 'https://liverhealthnow.com/health-tools-detail/living-with-diabetes-and-liver-disease/salix/29', NULL, NULL),
(702, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/virtua-our-lady-of-lourdes-hospital/41', NULL, NULL),
(703, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix-pharmaceuticals/41/?image=144', 'https://liverhealthnow.com/storage', NULL),
(704, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/salix-pharmaceuticals/21', NULL, NULL),
(705, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix-pharmaceuticals/41', NULL, NULL),
(706, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/public/health-tools-detail/assess-he-with-the-stroop-test/leo9-studio/21/?image=316', 'https://liverhealthnow.com/public/storage/userstemplogos/20200618094357.png', NULL),
(707, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/public/health-tools-detail/i-have-he/bausch-health/25/?image=320', 'https://liverhealthnow.com/public/storage/userstemplogos/20200622131445.png', NULL),
(708, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/digestive-disease-associates-of-rockland/21', NULL, NULL),
(709, 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy/digestive-disease-associates-of-rockland/45', NULL, NULL),
(710, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/thomas-jefferson-university-hospital/41', NULL, NULL),
(711, 'Symptoms, Complications, and Management of Cirrhosis', 'symptoms-complications-and-management-of-cirrhosis', 'https://liverhealthnow.com/health-tools-detail/symptoms-complications-and-management-of-cirrhosis/salix/37/?image=271', 'https://liverhealthnow.com/storage', NULL),
(712, 'Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy/salix/26/?image=154', 'https://liverhealthnow.com/storage/userstemplogos/20200508200636.jpeg', NULL),
(713, 'Discuss Patient Journaling With Your Patients', 'discuss-patient-journaling-with-your-patients', 'https://liverhealthnow.com/health-tools-detail/discuss-patient-journaling-with-your-patients/salix/22/?image=572', 'https://liverhealthnow.com/storage/userstemplogos/20200915182100.jpeg', NULL),
(714, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/salix/21/?image=573', NULL, NULL),
(715, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/thomas-jefferson-university-hospital/35', NULL, NULL),
(716, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://www.liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/salix-pharmaceuticals/19', NULL, NULL),
(717, 'Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy/salix-pharmaceuticals/28', NULL, NULL),
(718, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/salix-pharmaceuticals/32', NULL, NULL),
(719, 'Stages and Types of Liver Disease', 'stages-and-types-of-liver-disease', 'https://www.liverhealthnow.com/health-tools-detail/stages-and-types-of-liver-disease/salix-pharmaceuticals/36', NULL, NULL),
(720, 'West Haven Criteria', 'west-haven-criteria', 'https://www.liverhealthnow.com/health-tools-detail/west-haven-criteria/salix-pharmaceuticals/39', NULL, NULL),
(721, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://www.liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/salix-pharmaceuticals/20', NULL, NULL),
(722, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/salix/19/?image=600', 'https://liverhealthnow.com/storage/users/744.png', NULL),
(723, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=601', 'https://liverhealthnow.com/storage/userstemplogos/20200917154425.jpeg', NULL),
(724, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/salix/21/?image=574', 'https://liverhealthnow.com/storage/users/744.png', NULL),
(725, 'Goal Setting When You Have Chronic Liver Disease', 'goal-setting-when-you-have-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/goal-setting-when-you-have-chronic-liver-disease/salix/23/?image=603', 'https://liverhealthnow.com/storage/userstemplogos/20200917171334.jpeg', NULL),
(726, 'Stages and Types of Liver Disease', 'stages-and-types-of-liver-disease', 'https://liverhealthnow.com/health-tools-detail/stages-and-types-of-liver-disease/aventria/36/?image=604', 'https://liverhealthnow.com/storage', NULL),
(727, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=608', NULL, NULL),
(728, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/salix/21/?image=609', NULL, NULL),
(729, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/salix/19/?image=610', NULL, NULL);
INSERT INTO `user_health_tools` (`id`, `name`, `slug`, `url`, `custom_logo`, `company_logo`) VALUES
(730, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/aventria/19', NULL, NULL),
(731, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/aventria/21/?image=614', 'https://liverhealthnow.com/storage/userstemplogos/20200922070303.jpeg', NULL),
(732, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/prk/21/?image=616', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(733, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/sanat/41/?image=618', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(734, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/aventria/41/?image=620', 'https://liverhealthnow.com/storage/userstemplogos/20200922071816.png', NULL),
(735, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/aventria/21/?image=633', 'https://liverhealthnow.com/storage/userstemplogos/20200922130810.png', NULL),
(736, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/gastroenterology-specialists-inc/41', NULL, NULL),
(737, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/gastroenterology-specialists-inc/41/?image=635', 'https://liverhealthnow.com/storage/userstemplogos/20200922145246.png', NULL),
(738, 'Symptoms, Complications, and Management of Cirrhosis', 'symptoms-complications-and-management-of-cirrhosis', 'https://liverhealthnow.com/health-tools-detail/symptoms-complications-and-management-of-cirrhosis/gastroenterology-specialists-inc/37/?image=636', 'https://liverhealthnow.com/storage/userstemplogos/20200922150326.png', NULL),
(739, 'Symptoms, Complications, and Management of Cirrhosis', 'symptoms-complications-and-management-of-cirrhosis', 'https://liverhealthnow.com/health-tools-detail/symptoms-complications-and-management-of-cirrhosis/gastroenterology-specialists-inc/37/?image=637', 'https://liverhealthnow.com/storage/userstemplogos/20200922153602.png', NULL),
(740, 'Help Patients Set Goals Using Shared Decision Making', 'help-patients-set-goals-using-shared-decision-making', 'https://www.liverhealthnow.com/health-tools-detail/help-patients-set-goals-using-shared-decision-making/salix-pharmaceuticals/24', NULL, NULL),
(741, 'Prevent Another Attack From Overt Hepatic Encephalopathy', 'prevent-another-attack-from-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/prevent-another-attack-from-overt-hepatic-encephalopathy/salix/43/?image=640', 'https://liverhealthnow.com/storage', NULL),
(742, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/texas-digestive-disease-consultants/21', NULL, NULL),
(743, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/stacie/21/?image=642', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(744, 'West Haven Criteria', 'west-haven-criteria', 'https://liverhealthnow.com/health-tools-detail/west-haven-criteria/stacie/39/?image=644', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(745, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/stacie/21/?image=645', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(746, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/stacie/41/?image=646', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(747, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/salix-pharmaceuticals/19/?image=145', 'https://liverhealthnow.com/storage', NULL),
(748, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/tower-health/19', NULL, NULL),
(749, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/stacie/41/?image=647', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(750, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/aventria/21/?image=648', 'https://liverhealthnow.com/storage/userstemplogos/20200924174100.png', NULL),
(751, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/aventria/41/?image=650', 'https://liverhealthnow.com/storage/userstemplogos/20200924174255.png', NULL),
(752, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/stacie/41/?image=651', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(753, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/aventria/41/?image=649', 'https://liverhealthnow.com/storage/userstemplogos/20200924174241.png', NULL),
(754, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/kathleen/41/?image=652', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(755, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/aventria/41/?image=653', 'https://liverhealthnow.com/storage/userstemplogos/20200924194544.png', NULL),
(756, 'Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy/salix-pharmaceuticals/26', NULL, NULL),
(757, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/salix-pharmaceuticals/19', NULL, NULL),
(758, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/stacie/19/?image=654', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(759, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/stacie/20/?image=655', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(760, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/stacie/21/?image=656', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(761, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/stacie/41/?image=657', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(762, 'Discuss Patient Journaling With Your Patients', 'discuss-patient-journaling-with-your-patients', 'https://liverhealthnow.com/health-tools-detail/discuss-patient-journaling-with-your-patients/stacie/22/?image=658', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(763, 'Goal Setting When You Have Chronic Liver Disease', 'goal-setting-when-you-have-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/goal-setting-when-you-have-chronic-liver-disease/stacie/23/?image=659', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(764, 'Help Patients Set Goals Using Shared Decision Making', 'help-patients-set-goals-using-shared-decision-making', 'https://liverhealthnow.com/health-tools-detail/help-patients-set-goals-using-shared-decision-making/stacie/24/?image=662', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(765, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/stacie/25/?image=663', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(766, 'Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy/stacie/26/?image=664', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(767, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/stacie/27/?image=665', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(768, 'Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy/stacie/28/?image=666', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(769, 'Living With Diabetes and Liver Disease', 'living-with-diabetes-and-liver-disease', 'https://liverhealthnow.com/health-tools-detail/living-with-diabetes-and-liver-disease/stacie/29/?image=667', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(770, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/stacie/30/?image=668', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(771, 'Monitoring and Documenting Overt Hepatic Encephalopathy Episodes', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'https://liverhealthnow.com/health-tools-detail/monitoring-and-documenting-overt-hepatic-encephalopathy-episodes/stacie/42/?image=669', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(772, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/stacie/32/?image=670', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(773, 'Nurse Checklist for Patients With Hepatic Encephalopathy Who Are Transitioning to Another Care Setting', 'nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting/stacie/44/?image=671', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(774, 'Prevent Another Attack From Overt Hepatic Encephalopathy', 'prevent-another-attack-from-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/prevent-another-attack-from-overt-hepatic-encephalopathy/stacie/43/?image=672', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(775, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/stacie/35/?image=673', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(776, 'Stages and Types of Liver Disease', 'stages-and-types-of-liver-disease', 'https://liverhealthnow.com/health-tools-detail/stages-and-types-of-liver-disease/stacie/36/?image=674', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(777, 'Symptoms, Complications, and Management of Cirrhosis', 'symptoms-complications-and-management-of-cirrhosis', 'https://liverhealthnow.com/health-tools-detail/symptoms-complications-and-management-of-cirrhosis/stacie/37/?image=675', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(778, 'The Importance of Patient Journaling', 'the-importance-of-patient-journaling', 'https://liverhealthnow.com/health-tools-detail/the-importance-of-patient-journaling/stacie/38/?image=676', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(779, 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy/stacie/45/?image=677', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(780, 'West Haven Criteria', 'west-haven-criteria', 'https://liverhealthnow.com/health-tools-detail/west-haven-criteria/stacie/39/?image=678', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(781, 'What Is Hepatic Encephalopathy?', 'what-is-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/what-is-hepatic-encephalopathy/stacie/40/?image=679', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(782, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=681', 'https://liverhealthnow.com/storage/userstemplogos/20200928114124.png', NULL),
(783, 'Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy/salix/28/?image=682', 'https://liverhealthnow.com/storage/userstemplogos/20200928115934.png', NULL),
(784, 'Prevent Another Attack From Overt Hepatic Encephalopathy', 'prevent-another-attack-from-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/prevent-another-attack-from-overt-hepatic-encephalopathy/salix/43/?image=683', 'https://liverhealthnow.com/storage/userstemplogos/20200928120016.png', NULL),
(785, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/salix/35/?image=684', 'https://liverhealthnow.com/storage/userstemplogos/20200928120103.png', NULL),
(786, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/salix/19/?image=685', 'https://liverhealthnow.com/storage/userstemplogos/20200928120304.png', NULL),
(787, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/organization/20', NULL, NULL),
(788, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/salix/27/?image=687', 'https://liverhealthnow.com/storage/userstemplogos/20200928170034.png', NULL),
(789, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/salix/35/?image=688', 'https://liverhealthnow.com/storage/userstemplogos/20200928170201.png', NULL),
(790, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/salix/21/?image=689', 'https://liverhealthnow.com/storage/userstemplogos/20200928170253.png', NULL),
(791, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/salix/19/?image=690', 'https://liverhealthnow.com/storage/userstemplogos/20200928170441.png', NULL),
(792, 'Monitoring and Documenting Overt Hepatic Encephalopathy Episodes', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'https://liverhealthnow.com/public/health-tools-detail/monitoring-and-documenting-overt-hepatic-encephalopathy-episodes/salix/42/?image=374', 'https://liverhealthnow.com/public/storage/userstemplogos/20200710144632.png', NULL),
(793, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/heather/20/?image=691', 'https://liverhealthnow.com/storage/users/983.jpg', NULL),
(794, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/aventria/20/?image=691', 'https://liverhealthnow.com/storage/users/983.jpg', NULL),
(795, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/bausch-health-care-gi/35', NULL, NULL),
(796, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/salix/20/?image=693', 'https://liverhealthnow.com/storage/userstemplogos/20201001160207.jpeg', NULL),
(797, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/salix/27/?image=694', 'https://liverhealthnow.com/storage/userstemplogos/20201001160342.jpeg', NULL),
(798, 'Monitoring and Documenting Overt Hepatic Encephalopathy Episodes', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'https://liverhealthnow.com/health-tools-detail/monitoring-and-documenting-overt-hepatic-encephalopathy-episodes/salix/42/?image=696', 'https://liverhealthnow.com/storage/userstemplogos/20201001161943.jpeg', NULL),
(799, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://www.liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/indiana-university/30', NULL, NULL),
(800, 'Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy/indiana-university/26', NULL, NULL),
(801, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/salix/25/?image=697', 'https://liverhealthnow.com/storage/userstemplogos/20201001211147.png', NULL),
(802, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/salix/25/?image=698', 'https://liverhealthnow.com/storage/userstemplogos/20201001211340.jpeg', NULL),
(803, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/salix/27/?image=699', 'https://liverhealthnow.com/storage/userstemplogos/20201001211432.jpeg', NULL),
(804, 'Living With Diabetes and Liver Disease', 'living-with-diabetes-and-liver-disease', 'https://liverhealthnow.com/health-tools-detail/living-with-diabetes-and-liver-disease/salix/29/?image=700', 'https://liverhealthnow.com/storage/userstemplogos/20201001211522.jpeg', NULL),
(805, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/salix/30/?image=701', 'https://liverhealthnow.com/storage/userstemplogos/20201001211558.jpeg', NULL),
(806, 'Prevent Another Attack From Overt Hepatic Encephalopathy', 'prevent-another-attack-from-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/prevent-another-attack-from-overt-hepatic-encephalopathy/salix/43/?image=702', 'https://liverhealthnow.com/storage/userstemplogos/20201001211635.jpeg', NULL),
(807, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/salix/35/?image=703', 'https://liverhealthnow.com/storage/userstemplogos/20201001211653.jpeg', NULL),
(808, 'Stages and Types of Liver Disease', 'stages-and-types-of-liver-disease', 'https://liverhealthnow.com/health-tools-detail/stages-and-types-of-liver-disease/salix/36/?image=704', 'https://liverhealthnow.com/storage/userstemplogos/20201001211743.jpeg', NULL),
(809, 'Symptoms, Complications, and Management of Cirrhosis', 'symptoms-complications-and-management-of-cirrhosis', 'https://liverhealthnow.com/health-tools-detail/symptoms-complications-and-management-of-cirrhosis/salix/37/?image=705', 'https://liverhealthnow.com/storage/userstemplogos/20201001211813.jpeg', NULL),
(810, 'West Haven Criteria', 'west-haven-criteria', 'https://liverhealthnow.com/health-tools-detail/west-haven-criteria/salix/39/?image=706', 'https://liverhealthnow.com/storage/userstemplogos/20201001211846.jpeg', NULL),
(811, 'Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy/bausch-health/26', NULL, NULL),
(812, 'Nurse Checklist for Patients With Hepatic Encephalopathy Who Are Transitioning to Another Care Setting', 'nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting', 'https://www.liverhealthnow.com/health-tools-detail/nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting/bausch-health/44', NULL, NULL),
(813, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/bausch-health/32', NULL, NULL),
(814, 'West Haven Criteria', 'west-haven-criteria', 'https://www.liverhealthnow.com/health-tools-detail/west-haven-criteria/bausch-health/39', NULL, NULL),
(815, 'Symptoms, Complications, and Management of Cirrhosis', 'symptoms-complications-and-management-of-cirrhosis', 'https://www.liverhealthnow.com/health-tools-detail/symptoms-complications-and-management-of-cirrhosis/bausch-health/37', NULL, NULL),
(816, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://www.liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=345', 'https://www.liverhealthnow.com/storage/userstemplogos/20200630162337.png', NULL),
(817, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://www.liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/salix/19', NULL, NULL),
(818, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/bausch-health/41/?image=709', 'https://liverhealthnow.com/storage/userstemplogos/20201005205221.png', NULL),
(819, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/bausch-health/19', NULL, NULL),
(820, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/bausch-health/21', NULL, NULL),
(821, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/bausch-health/41/?image=711', 'https://liverhealthnow.com/storage/userstemplogos/20201005232249.png', NULL),
(822, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/bausch-health/41/?image=716', 'https://liverhealthnow.com/storage/userstemplogos/20201006002001.png', NULL),
(823, 'What Is Hepatic Encephalopathy?', 'what-is-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/what-is-hepatic-encephalopathy/salix/40', NULL, NULL),
(824, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/stacie/19/?image=718', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(825, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/stacie/27/?image=719', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(826, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/stacie/19/?image=720', 'https://liverhealthnow.com/storage/users/S2Fpc2VyIGxvZ28gMi5wbmc=20200630164318.png', NULL),
(827, 'Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy/salix/28', NULL, NULL),
(828, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://www.liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/salix/30', NULL, NULL),
(829, 'Prevent Another Attack From Overt Hepatic Encephalopathy', 'prevent-another-attack-from-overt-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/prevent-another-attack-from-overt-hepatic-encephalopathy/salix/43', NULL, NULL),
(830, 'Help Patients Set Goals Using Shared Decision Making', 'help-patients-set-goals-using-shared-decision-making', 'https://www.liverhealthnow.com/health-tools-detail/help-patients-set-goals-using-shared-decision-making/salix/24', NULL, NULL),
(831, 'Nurse Checklist for Patients With Hepatic Encephalopathy Who Are Transitioning to Another Care Setting', 'nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting', 'https://www.liverhealthnow.com/health-tools-detail/nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting/salix/44', NULL, NULL),
(832, 'West Haven Criteria', 'west-haven-criteria', 'https://www.liverhealthnow.com/health-tools-detail/west-haven-criteria/salix/39', NULL, NULL),
(833, 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy/salix/45', NULL, NULL),
(834, 'Discuss Patient Journaling With Your Patients', 'discuss-patient-journaling-with-your-patients', 'https://www.liverhealthnow.com/health-tools-detail/discuss-patient-journaling-with-your-patients/salix/22', NULL, NULL),
(835, 'Monitoring and Documenting Overt Hepatic Encephalopathy Episodes', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'https://www.liverhealthnow.com/health-tools-detail/monitoring-and-documenting-overt-hepatic-encephalopathy-episodes/salix/42', NULL, NULL),
(836, 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'counseling-your-patients-with-hepatic-encephalopathy-about-driving', 'https://liverhealthnow.com/health-tools-detail/counseling-your-patients-with-hepatic-encephalopathy-about-driving/salix/19/?image=721', 'https://liverhealthnow.com/storage/userstemplogos/20201008172855.jpeg', NULL),
(837, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=722', 'https://liverhealthnow.com/storage/userstemplogos/20201009163808.jpeg', NULL),
(838, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://www.liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/susan/41', NULL, NULL),
(839, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://www.liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/salix/41/?image=695', 'https://www.liverhealthnow.com/storage', NULL),
(840, 'Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy/kishore/28/?image=725', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(841, 'Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy/kishore/28/?image=727', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(842, 'Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy', 'lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/lifestyle-self-management-for-patients-with-overt-hepatic-encephalopathy/pharmerica/28/?image=728', 'https://liverhealthnow.com/storage/userstemplogos/20201014132629.png', NULL),
(843, 'Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy/bausch-health/26/?image=729', 'https://liverhealthnow.com/storage/userstemplogos/20201014143724.jpeg', NULL),
(844, 'Setting Up Medical Alerts on Digital Devices', 'setting-up-medical-alerts-on-digital-devices', 'https://liverhealthnow.com/health-tools-detail/setting-up-medical-alerts-on-digital-devices/bausch-health/35/?image=731', 'https://liverhealthnow.com/storage/userstemplogos/20201014171753.jpeg', NULL),
(845, 'Prevent Another Attack From Overt Hepatic Encephalopathy', 'prevent-another-attack-from-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/prevent-another-attack-from-overt-hepatic-encephalopathy/bausch-health/43/?image=732', 'https://liverhealthnow.com/storage/userstemplogos/20201014172133.jpeg', NULL),
(846, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/aventria/21/?image=158', 'https://liverhealthnow.com/storage', NULL),
(847, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/piyush/20/?image=734', 'https://liverhealthnow.com/storage/users/QXZlbnRyaWFMb2dvX3dpZGVfM18wOV8xNyBjb3B5LnBuZw==20201015193106.png', NULL),
(848, 'Goal Setting When You Have Chronic Liver Disease', 'goal-setting-when-you-have-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/goal-setting-when-you-have-chronic-liver-disease/piyush/23/?image=735', 'https://liverhealthnow.com/storage/users/QXZlbnRyaWFMb2dvX3dpZGVfM18wOV8xNyBjb3B5LnBuZw==20201015193106.png', NULL),
(849, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/salix/20/?image=736', 'https://liverhealthnow.com/storage/userstemplogos/20201019155937.jpeg', NULL),
(850, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://www.liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/jefferson-health/41', NULL, NULL),
(851, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/kathleen/41/?image=738', 'https://liverhealthnow.com/storage/users/U2NyZWVuIFNob3QgMjAyMC0wNC0yOSBhdCAxMi4yMi4wMiBQTS5qcGc=20200605134001.jpeg', NULL),
(852, 'What Is Hepatic Encephalopathy?', 'what-is-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/what-is-hepatic-encephalopathy/dignity-health-medical-group-liver-disease-transplanation/40', NULL, NULL),
(853, 'The Importance of Patient Journaling', 'the-importance-of-patient-journaling', 'https://www.liverhealthnow.com/health-tools-detail/the-importance-of-patient-journaling/dignity-health-medical-group-liver-disease-transplanation/38', NULL, NULL),
(854, 'Symptoms, Complications, and Management of Cirrhosis', 'symptoms-complications-and-management-of-cirrhosis', 'https://liverhealthnow.com/health-tools-detail/symptoms-complications-and-management-of-cirrhosis/salix/37/?image=741', 'https://liverhealthnow.com/storage/userstemplogos/20201028144252.jpeg', NULL),
(855, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/heather/20/?image=742', 'https://liverhealthnow.com/storage/users/983.jpg', NULL),
(856, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/bausch-health/25', NULL, NULL),
(857, 'I Have HE', 'i-have-he', 'https://liverhealthnow.com/health-tools-detail/i-have-he/scott/25/?image=745', 'https://liverhealthnow.com/storage/users/default.png', NULL),
(858, 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease', 'https://liverhealthnow.com/health-tools-detail/diagnosing-hepatic-encephalopathy-in-patients-with-liver-disease/heather/20/?image=746', 'https://liverhealthnow.com/storage/users/983.jpg', NULL),
(859, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/dignity-health-medical-group-liver-disease-transplanation/30', NULL, NULL),
(860, 'Assess HE With the Stroop Test', 'assess-he-with-the-stroop-test', 'https://liverhealthnow.com/health-tools-detail/assess-he-with-the-stroop-test/scripps-green-hospital/21', NULL, NULL),
(861, 'Cirrhosis and Its Complications', 'cirrhosis-and-its-complications', 'https://liverhealthnow.com/health-tools-detail/cirrhosis-and-its-complications/scripps-green-hospital/41', NULL, NULL),
(862, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/scripps-green-hospital/30', NULL, NULL),
(863, 'Monitoring and Documenting Overt Hepatic Encephalopathy Episodes', 'monitoring-and-documenting-overt-hepatic-encephalopathy-episodes', 'https://liverhealthnow.com/health-tools-detail/monitoring-and-documenting-overt-hepatic-encephalopathy-episodes/scripps-green-hospital/42', NULL, NULL),
(864, 'Stages and Types of Liver Disease', 'stages-and-types-of-liver-disease', 'https://liverhealthnow.com/health-tools-detail/stages-and-types-of-liver-disease/scripps-green-hospital/36', NULL, NULL),
(865, 'Symptoms, Complications, and Management of Cirrhosis', 'symptoms-complications-and-management-of-cirrhosis', 'https://liverhealthnow.com/health-tools-detail/symptoms-complications-and-management-of-cirrhosis/scripps-green-hospital/37', NULL, NULL),
(866, 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/understanding-blood-ammonia-levels-in-patients-with-hepatic-encephalopathy/scripps-green-hospital/45', NULL, NULL),
(867, 'What Is Hepatic Encephalopathy?', 'what-is-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/what-is-hepatic-encephalopathy/scripps-green-hospital/40', NULL, NULL),
(868, 'Nurse Checklist for Patients With Hepatic Encephalopathy Who Are Transitioning to Another Care Setting', 'nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting/salix-pharmaceuticals/44', NULL, NULL),
(869, 'West Haven Criteria', 'west-haven-criteria', 'https://liverhealthnow.com/health-tools-detail/west-haven-criteria/salix-pharmaceuticals/39', NULL, NULL),
(870, 'What Is Hepatic Encephalopathy?', 'what-is-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/what-is-hepatic-encephalopathy/salix-pharmaceuticals/40', NULL, NULL),
(871, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/salix-pharmaceuticals/30', NULL, NULL),
(872, 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'nurse-checklist-for-liver-disease-and-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-liver-disease-and-hepatic-encephalopathy/salix-pharmaceuticals/32', NULL, NULL),
(873, 'Nurse Checklist for Patients With Hepatic Encephalopathy Who Are Transitioning to Another Care Setting', 'nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting', 'https://www.liverhealthnow.com/health-tools-detail/nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting/salix-pharmaceuticals/44', NULL, NULL),
(874, 'Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'https://www.liverhealthnow.com/health-tools-detail/importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy/salix-pharmaceuticals/26', NULL, NULL),
(875, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://www.liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/salix-pharmaceuticals/27', NULL, NULL),
(876, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/bausch-health-care-gi/30', NULL, NULL),
(877, 'Nurse Checklist for Patients With Hepatic Encephalopathy Who Are Transitioning to Another Care Setting', 'nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting', 'https://liverhealthnow.com/health-tools-detail/nurse-checklist-for-patients-with-hepatic-encephalopathy-who-are-transitioning-to-another-care-setting/bausch-health-care-gi/44', NULL, NULL),
(878, 'West Haven Criteria', 'west-haven-criteria', 'https://liverhealthnow.com/health-tools-detail/west-haven-criteria/bausch-health-care-gi/39', NULL, NULL),
(879, 'Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy/salix/26/?image=752', 'https://liverhealthnow.com/storage/userstemplogos/20201106150746.png', NULL),
(880, 'Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy', 'importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy', 'https://liverhealthnow.com/health-tools-detail/importance-of-taking-medication-as-prescribed-for-overt-hepatic-encephalopathy/gastoenterologist-specialist-jefferson-torresdale/26', NULL, NULL),
(881, 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'important-signs-and-symptoms-for-patients-with-chronic-liver-disease', 'https://liverhealthnow.com/health-tools-detail/important-signs-and-symptoms-for-patients-with-chronic-liver-disease/gastoenterologist-specialist-jefferson-torresdale/27', NULL, NULL),
(882, 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers', 'https://liverhealthnow.com/health-tools-detail/medication-information-for-patients-with-hepatic-encephalopathy-and-their-caregivers/salix/30/?image=753', 'https://liverhealthnow.com/storage/userstemplogos/20201108192556.png', NULL),
(883, 'Understanding and Addressing Barriers to Patient Care in Multiple Sclerosis', 'understanding-and-addressing-barriers-to-patient-care-in-multiple-sclerosis', 'http://206.189.131.62/novartis_laravel/public/health-tools-detail/understanding-and-addressing-barriers-to-patient-care-in-multiple-sclerosis/aventia/50', 'http://206.189.131.62/novartis_laravel/public/storage/userstemplogos/20210407093604.jpeg', NULL),
(884, 'A Guide to Understanding Multiple Sclerosis and Common Phrases', 'a-guide-to-understanding-multiple-sclerosis-and-common-phrases', 'http://206.189.131.62/novartis_laravel/public/health-tools-detail/a-guide-to-understanding-multiple-sclerosis-and-common-phrases/aventia/53', 'http://206.189.131.62/novartis_laravel/public/storage/userstemplogos/20210407094650.jpeg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments_healthtools`
--
ALTER TABLE `departments_healthtools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments_users`
--
ALTER TABLE `departments_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ehr_homes`
--
ALTER TABLE `ehr_homes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `evidencebasedworkflowscontents`
--
ALTER TABLE `evidencebasedworkflowscontents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footers`
--
ALTER TABLE `footers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `healthtools`
--
ALTER TABLE `healthtools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `healthtoolstaticcontents`
--
ALTER TABLE `healthtoolstaticcontents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `healthtool_paitienttype`
--
ALTER TABLE `healthtool_paitienttype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `healthtool_targetaudiences`
--
ALTER TABLE `healthtool_targetaudiences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `health_tools_favs`
--
ALTER TABLE `health_tools_favs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homebannercontents`
--
ALTER TABLE `homebannercontents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homesliders`
--
ALTER TABLE `homesliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_page_contents`
--
ALTER TABLE `home_page_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_requests`
--
ALTER TABLE `order_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organizationtypes`
--
ALTER TABLE `organizationtypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `patienttypes`
--
ALTER TABLE `patienttypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient_journeys`
--
ALTER TABLE `patient_journeys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient_prioritey`
--
ALTER TABLE `patient_prioritey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `quality_cares`
--
ALTER TABLE `quality_cares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `salix_managers`
--
ALTER TABLE `salix_managers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_sections`
--
ALTER TABLE `service_sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `shared_priorities`
--
ALTER TABLE `shared_priorities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sources`
--
ALTER TABLE `sources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supports`
--
ALTER TABLE `supports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support_requests`
--
ALTER TABLE `support_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `targetaudiences`
--
ALTER TABLE `targetaudiences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_assessments`
--
ALTER TABLE `user_assessments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_customer_assessments`
--
ALTER TABLE `user_customer_assessments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_health_tools`
--
ALTER TABLE `user_health_tools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=271;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `departments_healthtools`
--
ALTER TABLE `departments_healthtools`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `departments_users`
--
ALTER TABLE `departments_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ehr_homes`
--
ALTER TABLE `ehr_homes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `evidencebasedworkflowscontents`
--
ALTER TABLE `evidencebasedworkflowscontents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `footers`
--
ALTER TABLE `footers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `healthtools`
--
ALTER TABLE `healthtools`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `healthtoolstaticcontents`
--
ALTER TABLE `healthtoolstaticcontents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `healthtool_paitienttype`
--
ALTER TABLE `healthtool_paitienttype`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `healthtool_targetaudiences`
--
ALTER TABLE `healthtool_targetaudiences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `health_tools_favs`
--
ALTER TABLE `health_tools_favs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=808;

--
-- AUTO_INCREMENT for table `homebannercontents`
--
ALTER TABLE `homebannercontents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `homesliders`
--
ALTER TABLE `homesliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `home_page_contents`
--
ALTER TABLE `home_page_contents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `order_requests`
--
ALTER TABLE `order_requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `organizationtypes`
--
ALTER TABLE `organizationtypes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `patienttypes`
--
ALTER TABLE `patienttypes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `patient_journeys`
--
ALTER TABLE `patient_journeys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `patient_prioritey`
--
ALTER TABLE `patient_prioritey`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=232;

--
-- AUTO_INCREMENT for table `quality_cares`
--
ALTER TABLE `quality_cares`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `salix_managers`
--
ALTER TABLE `salix_managers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `service_sections`
--
ALTER TABLE `service_sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `shared_priorities`
--
ALTER TABLE `shared_priorities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sources`
--
ALTER TABLE `sources`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `supports`
--
ALTER TABLE `supports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `support_requests`
--
ALTER TABLE `support_requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `targetaudiences`
--
ALTER TABLE `targetaudiences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=617;

--
-- AUTO_INCREMENT for table `user_assessments`
--
ALTER TABLE `user_assessments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `user_customer_assessments`
--
ALTER TABLE `user_customer_assessments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `user_health_tools`
--
ALTER TABLE `user_health_tools`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=885;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
